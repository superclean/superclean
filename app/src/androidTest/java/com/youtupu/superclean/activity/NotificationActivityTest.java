package com.youtupu.superclean.activity;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;

/**
 * Created by Jiali on 2019/2/18.
 */

@RunWith(AndroidJUnit4.class)
public class NotificationActivityTest {
    private static final String PACKAGE_SUPER_CLEAN = "com.youtupu.superclean";
    private static final String PACKAGE_SETTINGS = "com.android.settings";
    private UiDevice mUiDevice;
    private int mHeight;
    private int mWidth;

    @Before
    public void init() {
        mUiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        mHeight = mUiDevice.getDisplayHeight();
        mWidth = mUiDevice.getDisplayWidth();
    }

    @Test
    public void testNotification() throws Exception {
        mUiDevice.pressHome();
        String launcherPackageName = mUiDevice.getLauncherPackageName();
        assertThat(launcherPackageName, notNullValue());
        mUiDevice.wait(Until.hasObject(By.pkg(launcherPackageName).depth(0)), 3);
        Context context = InstrumentationRegistry.getContext();
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(PACKAGE_SUPER_CLEAN);
        //清除以前的实例
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        mUiDevice.wait(Until.hasObject(By.pkg(PACKAGE_SUPER_CLEAN).depth(0)), 3);

        findAndClickContentView("BusinessSDK");
    }

    public void findAndClickContentView(String content) {
        UiObject contentView = mUiDevice.findObject(new UiSelector().text(content));
        try {
            contentView.click();
        } catch (UiObjectNotFoundException e) {
            mUiDevice.swipe(mWidth / 2, mHeight - 200, mWidth / 2, 0, 50);
            findAndClickContentView(content);
        }
    }
}
