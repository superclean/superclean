package com.youtupu.superclean.trace;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * created by yihao 2019/5/23
 */
public class EventList {
    public HashMap<String, List<UserEvent>> events = new HashMap<>();

    public EventList(){

    }

    public void addEvent(UserEvent event){
        if(event == null){
            return;
        }
        List<UserEvent> eventList = events.get(getKey());
        if(null == eventList){
            eventList = new ArrayList<>();
        }
        eventList.add(event);
        events.put(getKey(), eventList);
    }

    public String getKey(){
        int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        return month + "_" + day;
    }
}
