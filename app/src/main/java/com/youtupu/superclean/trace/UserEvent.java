package com.youtupu.superclean.trace;

import android.os.Bundle;
import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * created by yihao 2019/5/23
 */
public class UserEvent {
    public String eventName;
    public String timeStamp;
    public String extra;

    public UserEvent(String eventName, String extra){
        if(TextUtils.isEmpty(eventName)){
            throw new IllegalArgumentException("eventName is empty");
        }
        this.eventName = eventName;
        this.extra = extra;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date curDate = new Date(System.currentTimeMillis());
        timeStamp = formatter.format(curDate);
    }
}
