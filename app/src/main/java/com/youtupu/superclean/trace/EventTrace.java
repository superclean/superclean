package com.youtupu.superclean.trace;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.util.HashMap;

import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.utils.SystemUtils;
import io.paperdb.Paper;

/**
 * created by yihao 2019/5/23
 */
public class EventTrace {

    private static EventTrace instance;
    private Context context;
    //    private List<UserEvent> events = new ArrayList<>();
    private EventList list;
    private static final String DB_NAME = "instreet.events";

    public static EventTrace getInstance(Context context) {
        if (instance == null && context != null) {
            instance = new EventTrace(context);
        }
        return instance;
    }

    // 记录事件
    public void traceEvent(String eventName, String extra) {
        UserEvent event = new UserEvent(eventName, extra);
        list.addEvent(event);
//        events.put(month + "_" + day, eventList);
        Paper.book(DB_NAME).write("events", list);
    }

    // 将数据和远端同步，只需一张表，以mac地址为Primary Key，以日期（month_day）为字段
    // 每1小时同步一次
    public void syncToRemote() {
        String collectionName = "events";
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference documentReference = db.collection(collectionName).document(SystemUtils.getMacAddress());
        documentReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (null != document && document.exists()) {
                        CommonUtils.debug("DocumentSnapshot data: " + document.getData());
                        uploadEvents(false);
                    } else {
                        CommonUtils.debug("No such document");
                        uploadEvents(true);

                    }
                } else {
                    CommonUtils.debug("get failed with " + task.getException());
                }
            }
        });
    }

    private void uploadEvents(boolean isNew) {
        try {
            String collection = "events";
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference docRefs = db.collection(collection).document(SystemUtils.getMacAddress());
            if (isNew) {
                docRefs.set(list)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void avoid) {
                                CommonUtils.debug("DocumentSnapshot successfully set!");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                CommonUtils.debug("Error adding document" + e.getMessage());
                            }
                        });
            } else {
                HashMap<String, Object> map = new HashMap<>();
                map.put(list.getKey(), list.events.get(list.getKey()));
                docRefs.update(map).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void avoid) {
                        CommonUtils.debug("DocumentSnapshot successfully updated!");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        CommonUtils.debug("Error updating document" + e.getMessage());
                    }
                });
            }

        } catch (Exception exp) {
            exp.printStackTrace();
        }
    }

    private EventTrace(Context context) {
        this.context = context.getApplicationContext();
        list = Paper.book(DB_NAME).read("events");
        if(null == list){
            list = new EventList();
        }
        CommonUtils.debug("read event list " + new Gson().toJson(list));
    }

}
