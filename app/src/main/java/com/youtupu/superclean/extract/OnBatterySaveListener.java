package com.youtupu.superclean.extract;

import android.support.annotation.NonNull;

import java.util.List;

import com.youtupu.superclean.service.BatterySaveService;

/**
 * Created by Jiali on 2019/2/20.
 */

public interface OnBatterySaveListener {
    void killProcess(@NonNull List<String> packageNameList, BatterySaveService.TaskListener listener);
    void appIsShowing(boolean show);

    void finish();
}
