package com.youtupu.superclean.extract;

/**
 * Created by Jiali on 2019/4/28.
 */
public interface OnBackPressedListener {
    void onBackPressed();
}
