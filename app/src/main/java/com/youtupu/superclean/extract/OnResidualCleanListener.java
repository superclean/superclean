package com.youtupu.superclean.extract;

import com.youtupu.superclean.bean.JunkListItem;

import java.util.List;

/**
 * Created by Jiali on 2019/5/21.
 */
public interface OnResidualCleanListener {
    void onResidualScanDone(List<JunkListItem> items);
}
