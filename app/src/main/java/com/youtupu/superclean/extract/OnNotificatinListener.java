package com.youtupu.superclean.extract;

import com.youtupu.superclean.bean.NotificationItem;

import java.util.List;

/**
 * Created by Jiali on 2018/12/28.
 */

public interface OnNotificatinListener {
    List<NotificationItem> getCleanedNotifications();
    void updateCleanedNotifications(List<NotificationItem> list);
    boolean isConnected();
    void toggle();
    void setHome(NotificationHome home);
}
