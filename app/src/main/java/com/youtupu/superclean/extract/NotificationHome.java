package com.youtupu.superclean.extract;

import com.youtupu.superclean.bean.NotificationItem;

import java.util.List;

/**
 * Created by Jiali on 2019/3/20.
 */
public interface NotificationHome {
    void onListenerConnected(List<NotificationItem> list);
    void onListenerDisConnected();
    void onListenerUpdate(List<NotificationItem> list);
}
