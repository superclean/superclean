package com.youtupu.superclean.extract;

import com.youtupu.superclean.bean.JunkListItem;

import java.util.List;

/**
 * Created by Jiali on 2019/1/15.
 */

public interface OnJunkCleanListener {
    void onCacheScanStart();

    void onCacheScanUpdate(long appSize, String filePath);

    void onCacheScanDone(List<JunkListItem> appList);

    void onResidualScanStart();

    void onResidualScanUpdate(long size, String filePath);

    void onResidualScanDone(List<JunkListItem> items);

    void onApkScanStart();

    void onApkScanUpdate(long size, String filePath);

    void onApkScanDone(List<JunkListItem> items);

    void onADScanStart();

    void onADScanUpdate(long size, String filePath);

    void onADScanDone(List<JunkListItem> items);
}
