package com.youtupu.superclean.extract;

import com.youtupu.superclean.bean.BigFileInfo;

import java.util.List;

/**
 * Created by Jiali on 2019/6/4.
 */
public interface OnBigFileListener {
    void onMediaScanStart();

    void onMediaScanUpdate(long size);

    void onMediaScanDone(List<BigFileInfo> bigFileInfos);

    void onAudioScanStart();

    void onAudioScanUpdate(long size);

    void onAudioScanDone(List<BigFileInfo> bigFileInfos);

    void onDocumentStart();

    void onDocumentUpdate(long size);

    void onDocumentDone(List<BigFileInfo> bigFileInfos);

    void onRarelyScanStart();

    void onRarelyScanUpdate(long size);

    void onRarelyScanDone(List<BigFileInfo> bigFileInfos);

}
