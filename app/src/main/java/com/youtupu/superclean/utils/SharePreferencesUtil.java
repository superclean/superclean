package com.youtupu.superclean.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Jiali on 2018/11/26.
 */

public class SharePreferencesUtil {
    private final static String SP_NAME = "Super_Clean_SP";
    private final static String LAST_BOOST_TIME = "last_boost_time";
    private final static String APP_LAST_OPEN_TIME = "app_last_open_time";
    private final static String LAST_BOOST_RESULT = "last_boost_result";
    private final static String COOLER_USED = "cooler_used";
    private final static String POWER_USED = "power_used";
    private final static String NOTIFICATION_USED = "notification_used";
    private final static String LAST_COOL_TIME = "last_cool_time";
    private final static String LAST_CLEAN_ALL_JUNK_TIME = "last_all_junk_time";
    private final static String LAST_POWER_TIME = "last_power_time";
    private final static String LAST_CLEAN_JUNK_TIME = "last_junk_time";
    private final static String LAST_OVERALL_SCAN_TIME = "last_overall_SCAN_TIME";
    private final static String LAST_ABANDON_OPTIMIZE = "last_abandon_optimize";
    private final static String LAST_CHECK_VERSION_TIME = "last_check_version_time";
    private final static String OPENED_NUMBER = "opened_number";
    private final static String ENTER_HOME_TIME = "enter_home_time";
    private final static String LAST_CANCEL_TIME_WHEN_EXIT = "last_cancel_time_when_exit";
    private final static String LAST_JUNK_SIZE = "last_junk_size";
    private final static String LAST_TEMPERATURE = "last_temperature";
    private final static String LAST_CAN_STOP_NUMBER = "last_can_stop_number";
    private static volatile SharePreferencesUtil mInstance;
    private SharedPreferences mSPreferences;
    private SharedPreferences.Editor mEditor;

    private SharePreferencesUtil(Context context) {
        if (context == null) return;
        mSPreferences = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
        mEditor = mSPreferences.edit();
    }

    public static SharePreferencesUtil getInstance(Context context) {
        if (mInstance == null) {
            synchronized (SharePreferencesUtil.class) {
                if (mInstance == null) {
                    mInstance = new SharePreferencesUtil(context);
                }
            }
        }
        return mInstance;
    }

    public void setAppLastOpenTime(long mills) {
        mEditor.putLong(APP_LAST_OPEN_TIME, mills);
        mEditor.commit();
    }

    public long getAppLastOpenTime() {
        return mSPreferences.getLong(APP_LAST_OPEN_TIME, 0);
    }


    public void setLastBoostTime(long mills) {
        mEditor.putLong(LAST_BOOST_TIME, mills);
        mEditor.commit();
    }

    public long getLastBoostTime() {
        return mSPreferences.getLong(LAST_BOOST_TIME, 0);
    }


    public void setLastBoostResult(float percent) {
        mEditor.putFloat(LAST_BOOST_RESULT, percent);
        mEditor.commit();
    }

    public float getLastBoostResult() {
        return mSPreferences.getFloat(LAST_BOOST_RESULT, -1);
    }


    public void setNotificationUsed(boolean used) {
        mEditor.putBoolean(NOTIFICATION_USED, used);
        mEditor.commit();
    }

    public boolean getNotificationUsed() {
        return mSPreferences.getBoolean(NOTIFICATION_USED, false);
    }

    public void setLastCoolTime(long mills) {
        mEditor.putLong(LAST_COOL_TIME, mills);
        mEditor.commit();
    }

    public long getLastCoolTime() {
        return mSPreferences.getLong(LAST_COOL_TIME, 0);
    }

    public void setCoolerUsed(boolean used) {
        mEditor.putBoolean(COOLER_USED, used);
        mEditor.commit();
    }

    public boolean getCoolerUsed() {
        return mSPreferences.getBoolean(COOLER_USED, false);
    }

    public void setPowerUsed(boolean used) {
        mEditor.putBoolean(POWER_USED, used);
        mEditor.commit();
    }

    public boolean getPowerUsed() {
        return mSPreferences.getBoolean(POWER_USED, false);
    }

    public void setLastCleanAllJunkTime(long mills) {
        mEditor.putLong(LAST_CLEAN_ALL_JUNK_TIME, mills);
        mEditor.commit();
    }

    public void setLastCleanJunkTime(long mills) {
        mEditor.putLong(LAST_CLEAN_JUNK_TIME, mills);
        mEditor.commit();
    }

    public long getLastCleanJunkTime() {
        return mSPreferences.getLong(LAST_CLEAN_JUNK_TIME, 0);
    }

    public long getLastCleanAllJunkTime() {
        return mSPreferences.getLong(LAST_CLEAN_ALL_JUNK_TIME, 0);
    }

    public void setLastPowerTime(long mills) {
        mEditor.putLong(LAST_POWER_TIME, mills);
        mEditor.commit();
    }

    public long getLastPowerTime() {
        return mSPreferences.getLong(LAST_POWER_TIME, 0);
    }

    public void setLastOverallScanTime(long mills) {
        mEditor.putLong(LAST_OVERALL_SCAN_TIME, mills);
        mEditor.commit();
    }

    public long getLastOverallScanTime() {
        return mSPreferences.getLong(LAST_OVERALL_SCAN_TIME, 0);
    }

    //记录上次哪个优化中途放弃了，1、垃圾清理 2、降温 3、省电
    public void setLastAbandonOptimize(int index) {
        mEditor.putInt(LAST_ABANDON_OPTIMIZE, index);
        mEditor.commit();
    }

    public int getLastAbandonOptimize() {
        return mSPreferences.getInt(LAST_ABANDON_OPTIMIZE, 0);
    }

    public void setLastCheckVersionTime(long mills) {
        mEditor.putLong(LAST_CHECK_VERSION_TIME, mills);
        mEditor.commit();
    }

    public long getLastCheckVersionTime() {
        return mSPreferences.getLong(LAST_CHECK_VERSION_TIME, 0);
    }

    public void setOpenedNumber(int number) {
        mEditor.putInt(OPENED_NUMBER, number);
        mEditor.commit();
    }

    public int getOpenedNumber() {
        return mSPreferences.getInt(OPENED_NUMBER, 0);
    }

    public void setEnterHomeTime(long mills) {
        mEditor.putLong(ENTER_HOME_TIME, mills);
        mEditor.commit();
    }

    public long getEnterHomeTime() {
        return mSPreferences.getLong(ENTER_HOME_TIME, 0);
    }

    public void setLastCancelTimeWhenExit(long mills) {
        mEditor.putLong(LAST_CANCEL_TIME_WHEN_EXIT, mills);
        mEditor.commit();
    }

    public long getLastCancelTimeWhenExit() {
        return mSPreferences.getLong(LAST_CANCEL_TIME_WHEN_EXIT, 0);
    }

    public void setLastJunkSize(long size) {
        mEditor.putLong(LAST_JUNK_SIZE, size);
        mEditor.commit();
    }

    public long getLastJunkSize() {
        return mSPreferences.getLong(LAST_JUNK_SIZE, 0);
    }

    public void setLastTemperature(int temperature) {
        mEditor.putInt(LAST_TEMPERATURE, temperature);
        mEditor.commit();
    }

    public int getLastTemperature() {
        return mSPreferences.getInt(LAST_TEMPERATURE, 0);
    }

    public void setLastCanStopNumber(int number) {
        mEditor.putInt(LAST_CAN_STOP_NUMBER, number);
        mEditor.commit();
    }

    public int getLastCanStopNumber() {
        return mSPreferences.getInt(LAST_CAN_STOP_NUMBER, 0);
    }

}
