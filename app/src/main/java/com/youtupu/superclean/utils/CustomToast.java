package com.youtupu.superclean.utils;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/3/21.
 */
public class CustomToast {

    private final ObjectAnimator mAnimator;
    private final Toast mToast;

    public CustomToast(@NonNull Context context, String title) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.custom_toast_layout, null);
        TextView tvTitle = view.findViewById(R.id.tv_toast_title);
        final ImageView imgBack = view.findViewById(R.id.img_back);
        ImageView imgFinger = view.findViewById(R.id.img_finger);
        tvTitle.setText(title);
        int distance = SystemUtils.dp2px(context, 20);
        mAnimator = ObjectAnimator.ofFloat(imgFinger, "translationX", 0f, distance);
        mAnimator.setDuration(1500);
        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                imgBack.setImageResource(R.mipmap.switch_anim_back_off);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                imgBack.setImageResource(R.mipmap.switch_anim_back_on);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mToast = new Toast(context);
        mToast.setDuration(Toast.LENGTH_LONG);
        mToast.setView(view);
        mToast.setMargin(0, 0);
        mToast.setGravity(Gravity.BOTTOM, 0, 0);
    }

    public void show() {
        if (mToast != null) {
            mToast.show();
            mAnimator.start();
        }
    }

    public void dismiss() {
        if (mToast != null) {
            mToast.cancel();
        }
    }
}
