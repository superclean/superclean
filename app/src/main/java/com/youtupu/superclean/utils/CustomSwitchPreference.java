package com.youtupu.superclean.utils;


import android.content.Context;
import android.support.v7.preference.PreferenceViewHolder;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import com.youtupu.superclean.R;

/**
 * created by yihao 2019/3/25
 * 该类的目的是使用自己定义的Switch图标
 */
public class CustomSwitchPreference extends SwitchPreferenceCompat {
    public CustomSwitchPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public CustomSwitchPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomSwitchPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSwitchPreference(Context context) {
        super(context);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);

        SwitchCompat switchCompat = holder.itemView.findViewById(R.id.switchWidget);
        switchCompat.setThumbDrawable(null);
        switchCompat.setTrackDrawable(null);
        if (switchCompat.isChecked()) {
            switchCompat.setBackground(getContext().getResources().getDrawable(R.mipmap.checkbox_open));
        } else {
            switchCompat.setBackground(getContext().getResources().getDrawable(R.mipmap.checkbox_close));
        }
    }

}
