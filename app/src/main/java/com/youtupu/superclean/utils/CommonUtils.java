package com.youtupu.superclean.utils;

import android.graphics.Point;
import android.text.TextUtils;
import android.util.Log;

import com.youtupu.superclean.BuildConfig;

import java.util.Random;

/**
 * Created by Jiali on 2018/11/30.
 */

public class CommonUtils {

    //输入圆心坐标，返回该圆上任意一点坐标
    public static Point getBigCircleRandomPoint(int centerX, int centerY, int num) {
        if (centerX <= 0) return new Point(0, 0);
        int radius;
        //x、y取较短的一边作为半径
        if (centerX < centerY) {
            radius = centerX - 10;
        } else {
            radius = centerY - 10;
        }
        Random random = new Random();
        //0->0~44, 1->45~89, ..., 7->315~359
        int randomAngle = random.nextInt(45) + num * 45;
        int randomX = (int) (radius * Math.cos(randomAngle * Math.PI / 180));
        int randomY = (int) (radius * Math.sin(randomAngle * Math.PI / 180));
        Point point = new Point(randomX, randomY);
        return point;
    }

    //删除数组的某一位
    public static int[] delete(int index, int array[]) {
        //数组的删除其实就是覆盖前一位
        int[] arrNew = new int[array.length - 1];
        for (int i = index; i < array.length - 1; i++) {
            array[i] = array[i + 1];
        }
        System.arraycopy(array, 0, arrNew, 0, arrNew.length);
        return arrNew;
    }

    //打印数组
    public static String printArray(int[] array) {
        int size = array.length;
        if (size == 0) return "";
        StringBuilder builder = new StringBuilder();
        builder.append("{");
        for (int i = 0; i < size; i++) {
            builder.append(array[i] + ", ");
        }
        builder.append("}");
        return builder.toString();
    }

    public static void debug(String content){
        if(BuildConfig.DEBUG &&!TextUtils.isEmpty(content)){
            Log.d("SuperClean", content);
        }
    }
}
