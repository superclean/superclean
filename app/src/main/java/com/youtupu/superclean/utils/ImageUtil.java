package com.youtupu.superclean.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by Jiali on 2019/6/11.
 */
public class ImageUtil {

    /**
     * 根据目标View的尺寸压缩图片返回bitmap
     *
     * @param fileName 图标文件的路径
     * @param width    目标view的宽
     * @param height   目标view的高
     * @return 压缩后的bitmap
     */
    public static Bitmap decodeBitmapFromResource(String fileName, int width, int height) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, options);
        //获取采样率
        options.inSampleSize = calculateInSampleSize(options, width, height);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(fileName, options);
    }

    /**
     * 获取采样率
     *
     * @param options   factory的参数
     * @param reqWidth  目标view的宽
     * @param reqHeight 目标view的高
     * @return 采样率
     */
    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int originalWidth = options.outWidth;
        int originalHeight = options.outHeight;
        int inSampleSize = 1;
        if (originalHeight > reqHeight || originalWidth > reqHeight) {
            int halfHeight = originalHeight / 2;
            int halfWidth = originalWidth / 2;
            //压缩后的尺寸与所需的尺寸进行比较
            while ((halfWidth / inSampleSize) >= reqHeight && (halfHeight / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static String getThumbPath(String filePath, Context context) {
        if (TextUtils.isEmpty(filePath) || context == null) return null;
        // MediaStore.Video.Thumbnails.DATA:视频缩略图的文件路径
        String[] thumbColumns = {MediaStore.Video.Thumbnails.DATA, MediaStore.Video.Thumbnails.VIDEO_ID};
        // 视频其他信息的查询条件
        String[] mediaColumns = {MediaStore.Video.Media._ID, MediaStore.Video.Media.DATA};
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver != null) {
            //先查该文件对应的视频信息
            Cursor cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, mediaColumns,
                    MediaStore.Video.Media.DATA + "=?", new String[]{filePath}, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    //获取该视频的id
                    int id = cursor.getInt(cursor.getColumnIndex(MediaStore.Video.Media._ID));
                    Cursor thumbCursor = contentResolver.query(
                            MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI,
                            thumbColumns, MediaStore.Video.Thumbnails.VIDEO_ID
                                    + "=?", new String[]{id + ""}, null);
                    if (thumbCursor != null) {
                        if (thumbCursor.moveToFirst()) {
                            return thumbCursor.getString(thumbCursor
                                    .getColumnIndex(MediaStore.Video.Thumbnails.DATA));
                        }
                        thumbCursor.close();
                    }
                }
                cursor.close();
            }
        }
        return null;
    }
}
