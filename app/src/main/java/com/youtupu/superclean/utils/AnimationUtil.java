package com.youtupu.superclean.utils;

import android.animation.ObjectAnimator;
import android.view.View;

/**
 * created by yihao 2019/4/8
 */
public class AnimationUtil {
    public static void scale(View view, long duration, float... scaleValues){
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(view, "scaleX", scaleValues);
        anim1.setDuration(duration);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(view, "scaleY", scaleValues);
        anim2.setDuration(duration);
        anim1.start();
        anim2.start();
    }
}
