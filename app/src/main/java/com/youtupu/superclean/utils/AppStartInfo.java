package com.youtupu.superclean.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * created by yihao 2019/5/22
 */
public class AppStartInfo {
    public int count;
    public String date;
    public String time;

    public AppStartInfo(int count) {
        this.count = count;
        date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        time = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
    }

    @Override
    public int hashCode() {
        return (date + " : " + count).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof AppStartInfo) {
            AppStartInfo info = (AppStartInfo) obj;
            if (info.time == null) {
                return false;
            }
            if (info.date == null) {
                return false;
            }
            if (info.count == count && info.date.equals(date) &&
                    info.time.equals(time)) {
                return true;
            }
        }
        return false;
    }
}
