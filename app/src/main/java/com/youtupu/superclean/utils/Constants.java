package com.youtupu.superclean.utils;

/**
 * created by yihao 2019/6/4
 */
public class Constants {
    public static final String ADMOB_APP_ID = "ca-app-pub-5976743014794238~1269440440";
    public static final String TEST_ADMOB_APP_ID = "ca-app-pub-3940256099942544~3347511713";

    public static final String TEST_NATIVE_AD = "ca-app-pub-3940256099942544/2247696110";
    public static final String BOOST_NATIVE_AD = "ca-app-pub-3684565239645442/8967170908";
    public static final String JUNK_NATIVE_AD = "ca-app-pub-3684565239645442/8788165364";
    public static final String POWER_NATIVE_AD = "ca-app-pub-3684565239645442/7810179515";
    public static final String COOLER_NATIVE_AD = "ca-app-pub-3684565239645442/3396237880";
    public static final String NOTIFICATION_NATIVE_AD = "ca-app-pub-3684565239645442/6162002028";
    public static final String BIG_FILE_NATIVE_AD = "ca-app-pub-3684565239645442/5639257841";

    public static final String TEST_INTERSTITIAL_AD = "ca-app-pub-3940256099942544/1033173712";
    public static final String BOOST_INTERSTITIAL_AD = "ca-app-pub-5976743014794238/5977883783";
    public static final String JUNK_INTERSTITIAL_AD = "ca-app-pub-5976743014794238/2495726354";
    public static final String POWER_INTERSTITIAL_AD = "ca-app-pub-5976743014794238/2146449981";
    public static final String COOLER_INTERSTITIAL_AD = "ca-app-pub-5976743014794238/8677991328";
    public static final String NOTIFICATION_INTERSTITIAL_AD = "ca-app-pub-5976743014794238/2112582979";

    public static final String TEST_BANNER_AD = "ca-app-pub-3940256099942544/6300978111";
    public static final String BOOST_BANNER_AD = "ca-app-pub-5976743014794238/9669716788";
    public static final String JUNK_BANNER_AD = "ca-app-pub-5976743014794238/6468838378";
    public static final String POWER_BANNER_AD = "ca-app-pub-5976743014794238/1300272175";
    public static final String COOLER_BANNER_AD = "ca-app-pub-5976743014794238/1620013983";
    public static final String NOTIFICATION_BANNER_AD = "ca-app-pub-5976743014794238/5047945493";
    public static final String NOTIFICATION_MGR_BANNER_AD = "ca-app-pub-5976743014794238/4856373801";
    public static final String HOME_BANNER_AD = "ca-app-pub-5976743014794238/2230210467";
    public static final String BIG_FILE_BANNER_AD = "ca-app-pub-5976743014794238/3651103343";

    public static final int ABANDON_NONE = 0;
    public static final int ABANDON_JUNK = 1;
    public static final int ABANDON_COOLER = 2;
    public static final int ABANDON_POWER = 3;
}
