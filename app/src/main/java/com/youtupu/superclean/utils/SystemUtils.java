package com.youtupu.superclean.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import com.youtupu.superclean.service.BatterySaveService;


/**
 * Created by Jiali on 2018/12/4.
 */

public class SystemUtils {
    private static final String TAG = SystemUtils.class.getSimpleName();
    private static final String MEM_INFO_PATH = "/proc/meminfo";  //手机所有内存信息存储在此
    private static final String MEMTOTAL = "MemTotal";
    private static final String MEMFREE = "MemFree";
    private static final String POWER_PROFILE_CLASS = "com.android.internal.os.PowerProfile";
    private static final String[] NOTIFICATION_WHITE_LIST_INIT = {
            "com.tencent.mm",
            "com.tencent.mobileqq",
            "com.facebook.katana",
            "com.google.android.youtube"
    };
    private static final String NOTIFICATION_WHITE_LIST = "notification_white_list.txt";
    private static final String POWER_IGNORE_LIST = "power_ignore_list.txt";
    private static final String[] POWER_IGNORE_LIST_INIT = {
            "com.tencent.mm",
            "com.tencent.mobileqq",
            "com.eg.android.AlipayGphone"
    };

    public static int getScreenWidth(Context context) {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (manager != null) {
            DisplayMetrics metrics = new DisplayMetrics();
            manager.getDefaultDisplay().getMetrics(metrics);
            return metrics.widthPixels;
        }
        return 0;
    }

    public static int getScreenHight(Context context) {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (manager != null) {
            DisplayMetrics metrics = new DisplayMetrics();
            manager.getDefaultDisplay().getMetrics(metrics);
            return metrics.heightPixels;
        }
        return 0;
    }

    public static int getStatusBarHeight(Context context) {
        int statusBarHeight = -1;
        //获取status_bar_height资源的ID
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            //根据资源ID获取响应的尺寸值
            statusBarHeight = context.getResources().getDimensionPixelSize(resourceId);
        }
        return statusBarHeight;
    }

    //获取全部内存空间大小
    public static float getTatalMemory() {
        return getMemInfo(MEMTOTAL);
    }

    //获取已用内存空间大小
    public static float getUsedMemoty() {
        return getTatalMemory() - getMemInfo(MEMFREE);
    }

    //得到所有可用存储空间大小（内部+外部）
    private static long getTotalStorage() {
        return getExternalStorageTotalSize() + getInternalStorageTotalSize();
    }

    public static long getUsedStorage() {
        return getTotalStorage() - getExternalStorageAvailableSize() - getInternalStorageAvailableSize();
    }

    //根据/proc/meminfo里的键值对，得到对应内存信息，返回值单位MB
    private static float getMemInfo(String type) {
        try {
            FileReader reader = new FileReader(MEM_INFO_PATH);
            BufferedReader buffer = new BufferedReader(reader, 4 * 1024);
            String str;
            while ((str = buffer.readLine()) != null) {
                if (str.contains(type)) {
                    break;
                }
            }
            buffer.close();
            /* \\s表示   空格,回车,换行等空白符,
            +号表示一个或多个的意思     */
            if (null != str) {
                String[] array = str.split("\\s+");
                if (array.length >= 2) {
                    // 获得系统总内存，单位是KB，除以1000转换为MB
                    return Integer.valueOf(array[1]) / 1000f;
                }
            }

        } catch (FileNotFoundException e) {
            Log.e(TAG, "file: " + MEM_INFO_PATH + " not found.");
        } catch (IOException e) {
            Log.e(TAG, "buffer read IO exception.");
        }
        return 0;
    }

    //判断sd卡是否可用
    public static boolean isExteralStorageAvailable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    //获取内部存储空间总大小,返回值单位MB
    private static long getInternalStorageTotalSize() {
        File file = Environment.getDataDirectory();
        StatFs statFs = new StatFs(file.getPath());
        return statFs.getBlockSizeLong() * statFs.getBlockCountLong();
    }

    //获取内部可用存储空间大小，返回值单位MB
    private static long getInternalStorageAvailableSize() {
        File file = Environment.getDataDirectory();
        StatFs statFs = new StatFs(file.getPath());
        return statFs.getAvailableBlocksLong() * statFs.getBlockSizeLong();
    }

    //获取外部存储空间总大小，返回值为单位为MB
    public static long getExternalStorageTotalSize() {
        File file = Environment.getExternalStorageDirectory();
        StatFs statFs = new StatFs(file.getPath());
        return statFs.getBlockSizeLong() * statFs.getBlockCountLong();
    }

    //获取外部可用存储空间大小，返回值单位为MB
    public static long getExternalStorageAvailableSize() {
        File file = Environment.getExternalStorageDirectory();
        StatFs statFs = new StatFs(file.getPath());
        return statFs.getAvailableBlocksLong() * statFs.getBlockSizeLong();
    }

    //获取data存储空间大小，返回值单位为MB
    public static long getDataStorageSize() {
        File file = Environment.getDataDirectory();
        StatFs statFs = new StatFs(file.getPath());
        return statFs.getAvailableBlocksLong() * statFs.getBlockSizeLong();
    }

    public static double getBatteryCapacity(Context context) {
        Object mPowerProfile;
        double capacity = 0;
        try {
            mPowerProfile = Class.forName(POWER_PROFILE_CLASS).getConstructor(Context.class).newInstance(context);
            capacity = (double) Class.forName(POWER_PROFILE_CLASS).getMethod("getBatteryCapacity").invoke(mPowerProfile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return capacity;
    }

    public static float getEstimateBatteryDuration(Context context) {
        int screenWidth = getScreenWidth(context);
        Random random = new Random();
        if (screenWidth <= 240) {
            return 20 + random.nextFloat();
        } else if (screenWidth <= 320) {
            return 18 + random.nextFloat();
        } else if (screenWidth <= 480) {
            return 16 + random.nextFloat();
        } else if (screenWidth <= 720) {
            return 14 + random.nextFloat();
        } else if (screenWidth <= 1080) {
            return 12 + random.nextFloat();
        } else {
            return 10 + random.nextFloat();
        }
    }

    public static int dp2px(Context context, int dp) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (dp * density + 0.5f);
    }

    //获取文件夹的大小
    private static long getFolderSize(File directory) {
        if (directory == null || !directory.isDirectory()) return 0;
        long size = 0;
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    size += getFolderSize(file);
                } else {
                    size += file.length();
                }
            }
        }
        return size;
    }

    public static long getFolderSizeByLinux(File directory) {
        long size;
        try {
            String[] args = {"du", "-sk", directory.getPath(), directory.getPath()};
            String result = SystemUtils.execLinux(args);
            //正则表达式：多个空格
            String[] strings = result.split("\\s+");
            String size1 = strings[0];
            size = Integer.parseInt(size1) * 1000;
        } catch (Exception e) {
            e.printStackTrace();
            size = SystemUtils.getFolderSize(directory);
        }
        return size;
    }

    private static String execLinux(String[] args) {
        String result = "";
        ProcessBuilder processBuilder = new ProcessBuilder(args);
        Process process = null;
        InputStream errIs = null;
        InputStream inIs = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int read;
            process = processBuilder.start();
            inIs = process.getInputStream();
            while ((read = inIs.read()) != -1) {
                baos.write(read);
            }
            baos.write('\n');
            errIs = process.getErrorStream();
            while ((read = errIs.read()) != -1) {
                baos.write(read);
            }
            byte[] data = baos.toByteArray();
            result = new String(data);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (errIs != null) {
                    errIs.close();
                }
                if (inIs != null) {
                    inIs.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (process != null) {
                process.destroy();
            }
        }
        return result;
    }

    public static HashSet<String> readNotificationWhiteListFile(@NonNull Context context) {
        HashSet<String> hashSet = null;
        String whiteList = null;
        try {
            File whiteListFile = new File(context.getFilesDir(), NOTIFICATION_WHITE_LIST);
            if (whiteListFile.exists()) {
                FileInputStream inputStream = context.openFileInput(NOTIFICATION_WHITE_LIST);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                int len;
                byte[] buffer = new byte[1024];
                while ((len = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, len);
                }
                whiteList = outputStream.toString();
                outputStream.close();
                inputStream.close();
            } else {
                //白名单文件不存在时，我们自己创建
                boolean createFileResult = whiteListFile.createNewFile();
                if (createFileResult) {
                    FileOutputStream outputStream = context.openFileOutput(NOTIFICATION_WHITE_LIST, Context.MODE_PRIVATE);
                    JSONArray array = new JSONArray();
                    for (String packageName : NOTIFICATION_WHITE_LIST_INIT) {
                        array.put(packageName);
                    }
                    PackageManager packageManager = context.getPackageManager();
                    if (packageManager != null) {
                        List<ApplicationInfo> applicationInfos = packageManager.getInstalledApplications(0);
                        if (applicationInfos != null) {
                            for (ApplicationInfo info : applicationInfos) {
                                //记录下系统应用的包名
                                if ((info.flags & ApplicationInfo.FLAG_SYSTEM) == ApplicationInfo.FLAG_SYSTEM) {
                                    array.put(info.packageName);
                                }
                            }
                        }
                    }
                    outputStream.write(array.toString().getBytes());
                    outputStream.close();
                    whiteList = array.toString();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (whiteList != null) {
            hashSet = new Gson().fromJson(whiteList, new TypeToken<HashSet<String>>() {
            }.getType());
        }
        return hashSet;
    }

    public static void writeWhiteListFile(@NonNull Context context, @NonNull String whiteList) {
        try {
            if (!whiteList.isEmpty()) {
                FileOutputStream outputStream = context.openFileOutput(NOTIFICATION_WHITE_LIST, Context.MODE_PRIVATE);
                outputStream.write(whiteList.getBytes(), 0, whiteList.getBytes().length);
                outputStream.flush();
                outputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getFileSizeArray(long size) {
        List<String> array;
        if (size < 0) {
            array = new ArrayList<>();
            array.add("");
            array.add("B");
            return array;
        }
        int unit = 1000;
        float result = size;
        String suffix = "B";
        long mult = 1;
        if (result > 900) {
            suffix = "KB";
            mult = unit;
            result = result / unit;
        }
        if (result > 900) {
            suffix = "MB";
            mult *= unit;
            result = result / unit;
        }
        if (result > 900) {
            suffix = "GB";
            mult *= unit;
            result = result / unit;
        }
        if (result > 900) {
            suffix = "TB";
            mult *= unit;
            result = result / unit;
        }
        if (result > 900) {
            suffix = "PB";
            mult *= unit;
            result = result / unit;
        }
        final String roundFormat;
        if (mult == 1 || result >= 100) {
            roundFormat = "%.0f";
        } else if (result < 1) {
            roundFormat = "%.2f";
        } else if (result < 10) {
            // 1 <= result < 10
            roundFormat = "%.1f";
        } else {
            // 10 <= result < 100
            roundFormat = "%.0f";
        }
        String roundedString = String.format(roundFormat, result);
        array = new ArrayList<>();
        array.add(roundedString);
        array.add(suffix);
        return array;
    }

    public static String getFileSize(long size) {
        if (size < 0) return "0B";
        int unit = 1000;
        float result = size;
        String suffix = "B";
        long mult = 1;
        if (result > 900) {
            suffix = "KB";
            mult = unit;
            result = result / unit;
        }
        if (result > 900) {
            suffix = "MB";
            mult *= unit;
            result = result / unit;
        }
        if (result > 900) {
            suffix = "GB";
            mult *= unit;
            result = result / unit;
        }
        if (result > 900) {
            suffix = "TB";
            mult *= unit;
            result = result / unit;
        }
        if (result > 900) {
            suffix = "PB";
            mult *= unit;
            result = result / unit;
        }
        final String roundFormat;
        if (mult == 1 || result >= 100) {
            roundFormat = "%.0f";
        } else if (result < 1) {
            roundFormat = "%.2f";
        } else if (result < 10) {
            // 1 <= result < 10
            roundFormat = "%.1f";
        } else {
            // 10 <= result < 100
            roundFormat = "%.0f";
        }
        String roundedString = String.format(roundFormat, result);
        return roundedString + " " + suffix;
    }


    public static long getFileSize(File file) {
        if (null == file || !file.exists()) {
            return 0;
        }
        if (file.isDirectory()) {
            return SystemUtils.getFolderSizeByLinux(file);
        } else {
            return file.length();
        }
    }

    public static boolean isAccessibilitySettingsOn(@NonNull Activity activity) {
        int accessibilityEnable = 0;
        String serviceName = activity.getPackageName() + "/" + BatterySaveService.class.getName();
        try {
            accessibilityEnable = Settings.Secure.getInt(activity.getContentResolver(), Settings.Secure.ACCESSIBILITY_ENABLED, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean result = false;
        if (accessibilityEnable == 1) {
            TextUtils.SimpleStringSplitter mStringSplitter = new TextUtils.SimpleStringSplitter(':');
            String settingValue = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringSplitter.setString(settingValue);
                while (mStringSplitter.hasNext()) {
                    String accessibilityService = mStringSplitter.next();
                    if (accessibilityService.equalsIgnoreCase(serviceName)) {
                        result = true;
                        break;
                    }
                }
            }
        }
        return result;
    }

    public static List<ApplicationInfo> getRunningTasks(Context context) {
        List<ApplicationInfo> result = new ArrayList<>();
        if (null == context) {
            return result;
        }
        PackageManager packageManager = context.getPackageManager();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (null == packageManager || null == activityManager) {
            return result;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            List<PackageInfo> packageInfos = packageManager.getInstalledPackages(PackageManager.MATCH_UNINSTALLED_PACKAGES);
            if (null == packageInfos) {
                return result;
            }
            for (PackageInfo packageInfo : packageInfos) {
                try {
                    ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageInfo.packageName, 0);
                    if ((applicationInfo.flags & (ApplicationInfo.FLAG_STOPPED | ApplicationInfo.FLAG_SYSTEM)) > 0 ||
                            applicationInfo.packageName.equalsIgnoreCase(context.getPackageName())) {
                        continue;
                    }
                    result.add(applicationInfo);
                } catch (PackageManager.NameNotFoundException exp) {
                    exp.printStackTrace();
                }
            }
        } else {
            List<ActivityManager.RunningServiceInfo> runningServices = activityManager.getRunningServices(Integer.MAX_VALUE);
            if (null == runningServices) {
                return result;
            }
            for (ActivityManager.RunningServiceInfo serviceInfo : runningServices) {
                String packageName;
                if ((packageName = serviceInfo.service.getPackageName()).equals(context.getPackageName())) {
                    continue;
                }
                try {
                    ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, 0);
                    if ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == ApplicationInfo.FLAG_SYSTEM) {
                        continue;
                    }
                    result.add(applicationInfo);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }
        return result;
    }

    public static int getBatteryTemperature(Context context) {
        if (context == null) return 0;
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, filter);
        if (batteryStatus == null) {
            return 0;
        } else {
            int temperature = batteryStatus.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0);
            return temperature / 10;
        }
    }

    public static @NonNull String readStringFromAsset(Context context, String fileName){
        if(context == null || TextUtils.isEmpty(fileName)){
            return "";
        }
        try {
            InputStream inputStream = context.getAssets().open(fileName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null){
                sb.append(line).append("\n");
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static HashSet<String> readPowerIgnoreListFile(@NonNull Context context) {
        HashSet<String> hashSet = null;
        String whiteList = null;
        try {
            File whiteListFile = new File(context.getFilesDir(), POWER_IGNORE_LIST);
            if (whiteListFile.exists()) {
                FileInputStream inputStream = context.openFileInput(POWER_IGNORE_LIST);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                int len;
                byte[] buffer = new byte[1024];
                while ((len = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, len);
                }
                whiteList = outputStream.toString();
                outputStream.close();
                inputStream.close();
            } else {
                //白名单文件不存在时，我们自己创建
                boolean createFileResult = whiteListFile.createNewFile();
                if (createFileResult) {
                    FileOutputStream outputStream = context.openFileOutput(POWER_IGNORE_LIST, Context.MODE_PRIVATE);
                    JSONArray array = new JSONArray();
                    for (String packageName : POWER_IGNORE_LIST_INIT) {
                        array.put(packageName);
                    }
                    outputStream.write(array.toString().getBytes());
                    outputStream.close();
                    whiteList = array.toString();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (whiteList != null) {
            hashSet = new Gson().fromJson(whiteList, new TypeToken<HashSet<String>>() {
            }.getType());
        }
        return hashSet;
    }

    public static void writePowerIgnoreListFile(@NonNull Context context, @NonNull String whiteList) {
        try {
            if (!whiteList.isEmpty()) {
                FileOutputStream outputStream = context.openFileOutput(POWER_IGNORE_LIST, Context.MODE_PRIVATE);
                outputStream.write(whiteList.getBytes(), 0, whiteList.getBytes().length);
                outputStream.flush();
                outputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "unknown";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:", b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "unknown";
    }

    /**
     * 获取手机外部总空间大小
     *
     * @return 总大小，字节为单位
     */
    public static long getTotalExternalSpaceSize() {
        if (isSDCardEnable()) {
            //获取SDCard根目录
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSizeLong();
            long totalBlocks = stat.getBlockCountLong();
            return totalBlocks * blockSize;
        } else {
            return -1;
        }
    }

    /**
     * 获取SD卡剩余空间
     *
     * @return SD卡剩余空间
     */
    public static long getFreeSpaceSize() {
        if (!isSDCardEnable()) return -1;
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        long blockSize, availableBlocks;
        availableBlocks = stat.getAvailableBlocksLong();
        blockSize = stat.getBlockSizeLong();
        return availableBlocks * blockSize;
    }

    public static boolean isSDCardEnable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    public static String getVersionName(Context context) {
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        String versionName = "";
        try {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }
}
