package com.youtupu.superclean.utils;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by Jiali on 2018/12/3.
 */

public class StatusBarUtils {
    /*
    设置状态栏全透明，只有5.0以上支持修改状态栏颜色
     */
    public static void setStatusBarFullTransparent(Activity activity) {
        if (activity == null) return;
        //SDK >= 5.0
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = activity.getWindow();
            //两个flag必须一起用，让应用的主体内容占用状态栏的空间
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            //一些机型状态栏本身是有背景色的,比如华为（半透明）
            //清除掉原有的半透明背景色（部分机型有）
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //设置为我们自己绘制背景
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //背景设为全透明
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /*
    设置状态栏半透明，部分机型有用，比如华为
     */
    public static void setStatusBarHalfTransparent(Activity activity) {
        if (activity == null) return;
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = activity.getWindow();
            //两个flag必须一起用，让应用的主体内容占用状态栏的空间
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            //设置为半透明
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    public static void setStatusBarColor(Activity activity, int color) {
        if (activity == null) return;
        //SDK >= 5.0
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = activity.getWindow();
            //两个flag必须一起用，让应用的主体内容占用状态栏的空间
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            //一些机型状态栏本身是有背景色的,比如华为（半透明）
            //清除掉原有的半透明背景色（部分机型有）
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //设置为我们自己绘制背景
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    /*
    隐藏状态栏
     */
    public static void setStatusBarInVisible(Activity activity) {
        if (activity == null) return;
        if (Build.VERSION.SDK_INT >= 21) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.INVISIBLE);
        }
    }

    /*
    是否需要内容紧贴着statusbar
    应该在对应的xml布局文件中，设置根布局fitsSystemWindows=true
     */
    public static void setFitSystemWindow(boolean b, Activity activity) {
        View cotentViewGroup = ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);
        cotentViewGroup.setFitsSystemWindows(b);
    }
}
