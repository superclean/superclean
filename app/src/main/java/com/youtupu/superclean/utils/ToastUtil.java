package com.youtupu.superclean.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Jiali on 2018/11/27.
 */

public class ToastUtil {
    private static Toast mToast;

    public static void showShortToast(Context context, String toShow) {
        if (context == null) return;
        if (mToast == null) {
            mToast = Toast.makeText(context, toShow, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(toShow);
        }
        mToast.show();
    }

    public static void showLongToast(Context context, String toShow) {
        if (context == null) return;
        if (mToast == null) {
            mToast = Toast.makeText(context, toShow, Toast.LENGTH_LONG);
        } else {
            mToast.setText(toShow);
        }
        mToast.show();
    }
}
