package com.youtupu.superclean.utils;

/**
 * Created by Jiali on 2019/6/13.
 */
public class OpenFileUtil {
    private static final String[][] typeArray;

    static {
        typeArray = new String[66][];
        typeArray[0] = new String[]{"3gp", "video/3gpp"};
        typeArray[1] = new String[]{"apk", "application/vnd.android.package-archive"};
        typeArray[2] = new String[]{"asf", "video/x-ms-asf"};
        typeArray[3] = new String[]{"avi", "video/x-msvideo"};
        typeArray[4] = new String[]{"bin", "application/octet-stream"};
        typeArray[5] = new String[]{"bmp", "image/bmp"};
        typeArray[6] = new String[]{"c", "text/plain"};
        typeArray[7] = new String[]{"class", "application/octet-stream"};
        typeArray[8] = new String[]{"conf", "text/plain"};
        typeArray[9] = new String[]{"cpp", "text/plain"};
        typeArray[10] = new String[]{"doc", "application/msword"};
        typeArray[11] = new String[]{"docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
        typeArray[12] = new String[]{"xls", "application/vnd.ms-excel"};
        typeArray[13] = new String[]{"xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"};
        typeArray[14] = new String[]{"exe", "application/octet-stream"};
        typeArray[15] = new String[]{"gif", "image/gif"};
        typeArray[16] = new String[]{"gtar", "application/x-gtar"};
        typeArray[17] = new String[]{"gz", "application/x-gzip"};
        typeArray[18] = new String[]{"h", "text/plain"};
        typeArray[19] = new String[]{"htm", "text/html"};
        typeArray[20] = new String[]{"html", "text/html"};
        typeArray[21] = new String[]{"jar", "application/java-archive"};
        typeArray[22] = new String[]{"java", "text/plain"};
        typeArray[23] = new String[]{"jpeg", "image/jpeg"};
        typeArray[24] = new String[]{"jpg", "image/jpeg"};
        typeArray[25] = new String[]{"js", "application/x-javascript"};
        typeArray[26] = new String[]{"log", "text/plain"};
        typeArray[27] = new String[]{"m3u", "audio/x-mpegurl"};
        typeArray[28] = new String[]{"m4a", "audio/mp4a-latm"};
        typeArray[29] = new String[]{"m4b", "audio/mp4a-latm"};
        typeArray[30] = new String[]{"m4p", "audio/mp4a-latm"};
        typeArray[31] = new String[]{"m4u", "video/vnd.mpegurl"};
        typeArray[32] = new String[]{"m4v", "video/x-m4v"};
        typeArray[33] = new String[]{"mov", "video/quicktime"};
        typeArray[34] = new String[]{"mp2", "audio/x-mpeg"};
        typeArray[35] = new String[]{"mp3", "audio/x-mpeg"};
        typeArray[36] = new String[]{"mp4", "video/mp4"};
        typeArray[37] = new String[]{"mpc", "application/vnd.mpohun.certificate"};
        typeArray[38] = new String[]{"mpe", "video/mpeg"};
        typeArray[39] = new String[]{"mpeg", "video/mpeg"};
        typeArray[40] = new String[]{"mpg", "video/mpeg"};
        typeArray[41] = new String[]{"mpg4", "video/mp4"};
        typeArray[42] = new String[]{"mpga", "audio/mpeg"};
        typeArray[43] = new String[]{"msg", "application/vnd.ms-outlook"};
        typeArray[44] = new String[]{"ogg", "audio/ogg"};
        typeArray[45] = new String[]{"pdf", "application/pdf"};
        typeArray[46] = new String[]{"png", "image/png"};
        typeArray[47] = new String[]{"pps", "application/vnd.ms-powerpoint"};
        typeArray[48] = new String[]{"ppt", "application/vnd.ms-powerpoint"};
        typeArray[49] = new String[]{"pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"};
        typeArray[50] = new String[]{"prop", "text/plain"};
        typeArray[51] = new String[]{"rc", "text/plain"};
        typeArray[52] = new String[]{"rmvb", "audio/x-pn-realaudio"};
        typeArray[53] = new String[]{"rtf", "application/rtf"};
        typeArray[54] = new String[]{"sh", "text/plain"};
        typeArray[55] = new String[]{"tar", "application/x-tar"};
        typeArray[56] = new String[]{"tgz", "application/x-compressed"};
        typeArray[57] = new String[]{"txt", "text/plain"};
        typeArray[58] = new String[]{"wav", "audio/x-wav"};
        typeArray[59] = new String[]{"wma", "audio/x-ms-wma"};
        typeArray[60] = new String[]{"wmv", "audio/x-ms-wmv"};
        typeArray[61] = new String[]{"wps", "application/vnd.ms-works"};
        typeArray[62] = new String[]{"xml", "text/plain"};
        typeArray[63] = new String[]{"z", "application/x-compress"};
        typeArray[64] = new String[]{"zip", "application/x-zip-compressed"};
        typeArray[65] = new String[]{"", "*/*"};
    }

    public static String getType(String suffix) {
        if (suffix == null) return "*/*";
        for (int i = 0; i < typeArray.length; i++) {
            if (suffix.equals(typeArray[i][0])) {
                return typeArray[i][1];
            }
        }
        return "*/*";
    }
}
