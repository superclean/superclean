package com.youtupu.superclean.utils;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.youtupu.superclean.trace.EventTrace;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
/**
 * created by yihao 2019/3/20
 */
public class AnalyticsUtil {

    private static String today;
    private static int startAppTimes = 0;
    // 记录今天的应用开启情况
    private static HashSet<AppStartInfo> appStarts = new HashSet<>();

    public static void logEvent(Context context, String eventName, Bundle bundle) {
        if (TextUtils.isEmpty(eventName) || context == null) {
            return;
        }
        if (bundle == null) {
            bundle = new Bundle();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date curDate = new Date(System.currentTimeMillis());
        bundle.putString("event_time", formatter.format(curDate));
        FirebaseAnalytics.getInstance(context).logEvent(eventName, bundle);

        HashMap<String, String> map = new HashMap<>();
        for (String key : bundle.keySet()) {
            map.put(key, bundle.getString(key));
        }
        EventTrace.getInstance(context).traceEvent(eventName, new Gson().toJson(map));
    }

    public static void traceSpaceInfo() {
        Map<String, Object> space = new HashMap<>();
        NumberFormat format = NumberFormat.getInstance();
        format.setMaximumFractionDigits(2);
        String value = format.format(SystemUtils.getTotalExternalSpaceSize() / 1073741824.0);
        space.put("space_total", value);
        value = format.format(SystemUtils.getFreeSpaceSize() / 1073741824.0);
        space.put("space_free", value);
        FirebaseFirestore.getInstance().collection("storage").document(SystemUtils.getMacAddress()).set(space);
    }

    public static void traceEnterApp(Context context) {
        today = context.getSharedPreferences("trace", Context.MODE_PRIVATE).getString("today", "");
        startAppTimes = context.getSharedPreferences("trace", Context.MODE_PRIVATE).getInt("startAppTimes", 1);
        if (TextUtils.isEmpty(today)) {
            today = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            context.getSharedPreferences("trace", Context.MODE_PRIVATE).edit().putString("today", today).apply();
            startAppTimes = 1;
        } else {
            String now = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            if (!now.equals(today)) {
                today = now;
                context.getSharedPreferences("trace", Context.MODE_PRIVATE).edit().putString("today", today).apply();
                startAppTimes = 1;
            } else {
                startAppTimes++;
            }
        }
        context.getSharedPreferences("trace", Context.MODE_PRIVATE).edit().putInt("startAppTimes", startAppTimes).apply();

        syncLocal(context);

        syncRemote();
    }

    private static void syncLocal(Context context) {
        int dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        String json = context.getSharedPreferences("trace", Context.MODE_PRIVATE).getString("day_" + dayOfMonth + "_use", "");
        CommonUtils.debug("read local app start infos " + json);
        if (!TextUtils.isEmpty(json)) {
            HashSet<AppStartInfo> infos = new Gson().fromJson(json, new TypeToken<HashSet<AppStartInfo>>() {
            }.getType());
            if (null != infos) {
                appStarts.addAll(infos);
            }
        }
        appStarts.add(new AppStartInfo(startAppTimes));
        json = new Gson().toJson(appStarts);
        CommonUtils.debug("write local app start infos " + json);
        context.getSharedPreferences("trace", Context.MODE_PRIVATE).edit().putString("day_" + dayOfMonth + "_use", json).apply();

    }

    public static void syncRemote() {
        String collectionName = "app_enter_" + (Calendar.getInstance().get(Calendar.MONTH) + 1);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference documentReference = db.collection(collectionName).document(SystemUtils.getMacAddress());
        documentReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (null != document && document.exists()) {
                        CommonUtils.debug("DocumentSnapshot data: " + document.getData());
                        uploadAppStartInfos(false);
                    } else {
                        CommonUtils.debug("No such document");
                        uploadAppStartInfos(true);

                    }
                } else {
                    CommonUtils.debug("get failed with " + task.getException());
                }
            }
        });
    }

    public static void uploadAppStartInfos(boolean isNew) {
        try {
            int dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            String collectionName = "app_enter_" + (Calendar.getInstance().get(Calendar.MONTH) + 1);
            HashMap<String, Object> map = new HashMap<>();
            map.put("day_" + dayOfMonth, startAppTimes);
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference docRefs = db.collection(collectionName).document(SystemUtils.getMacAddress());
            if (isNew) {
                docRefs.set(map)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void avoid) {
                                CommonUtils.debug("DocumentSnapshot successfully written!");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                CommonUtils.debug("Error adding document" + e.getMessage());
                            }
                        });
            } else {
                docRefs.update(map).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void avoid) {
                        CommonUtils.debug("DocumentSnapshot successfully updated!");
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                CommonUtils.debug("Error updating document" + e.getMessage());
                            }
                        });
            }

        } catch (Exception exp) {
            exp.printStackTrace();
        }
    }
}
