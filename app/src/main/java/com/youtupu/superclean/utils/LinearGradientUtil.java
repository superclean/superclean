package com.youtupu.superclean.utils;

import android.graphics.Color;

/**
 * Created by Jiali on 2018/11/30.
 * 渐变色取值器
 */

public class LinearGradientUtil {
    private int mStartColor;
    private int mEndColor;

    public LinearGradientUtil(int startColor, int endColor) {
        mStartColor = startColor;
        mEndColor = endColor;
    }

    public void setStartColor(int startColor) {
        mStartColor = startColor;
    }

    public void setEndColor(int endColor) {
        mEndColor = endColor;
    }

    public int getColor(float ratio) {
        int redStart = Color.red(mStartColor);
        int blueStart = Color.blue(mStartColor);
        int greenStart = Color.green(mStartColor);
        int redEnd = Color.red(mEndColor);
        int blueEnd = Color.blue(mEndColor);
        int greenEnd = Color.green(mEndColor);

        int red = (int) (redStart + (redEnd - redStart) * ratio);
        int blue = (int) (blueStart + (blueEnd - blueStart) * ratio);
        int green = (int) (greenStart + (greenEnd - greenStart) * ratio);
        return Color.argb(255, red, blue, green);
    }


}
