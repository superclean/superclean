package com.youtupu.superclean.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.internal.Preconditions;
import com.youtupu.superclean.BuildConfig;
import com.youtupu.superclean.base.BaseActivity;
import com.youtupu.superclean.service.BatterySaveService;
import com.youtupu.superclean.service.MonitorService;
import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.utils.Constants;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.AdDrawerView;
import com.youtupu.superclean.view.AdOuterItemView;
import com.youtupu.superclean.view.CustomTitleBar;
import com.youtupu.superclean.view.DoubleColorsTextView;

import java.lang.ref.WeakReference;

import cn.instreet.business.advertise.BannerAdvertiseManager;
import cn.instreet.business.advertise.NativeAdvertiseManager;
import com.youtupu.superclean.R;

public class OptimizeActivity extends BaseActivity {
    public static final String ACTION_BAR_TEXT = "action_bar_text";
    public static final String OPTIMIZE_LEFT_TEXT = "optimize_left_text";
    public static final String OPTIMIZE_RIGHT_TEXT = "optimize_right_text";
    public static final String OPTIMIZE_LEFT_TEXT_COLOR = "left_color";
    public static final String OPTIMIZE_RIGHT_TEXT_COLOR = "right_color";
    public static final String OPTIMIZE_LEFT_TEXT_SIZE = "left_size";
    public static final String OPTIMIZE_RIGHT_TEXT_SIZE = "right_size";
    public static final String OPTIMIZE_SHOW_AIMATION = "show_animation";
    public static final String OPTIMIZE_TYPE = "optimize_type";
    public static final String OPTIMIZE_TITLE = "optimize_title";
    public static final String NEED_RECORD = "need_record";
    public static final int FROM_BOOST = 1;
    public static final int FROM_JUNK = 2;
    public static final int FROM_COOLER = 3;
    public static final int FROM_POWER = 4;
    public static final int FROM_NOTIFICATION = 5;
    private static final int SHOW_MARK_ANIMATION = 0;
    private static final int SHOW_AD_VIEW = 1;
    private static final int INIT_AD_VIEW = 2;
    private static final int TEXT_MOVE_DURATION = 500;

    private DoubleColorsTextView mDoubleColorsTextView;
    private boolean mShowMarkAnim = true;
    private ImageView mImgMark;
    private AnimatedVectorDrawable mBlueMarkDrawable;
    boolean mIsFirstCreate = true;
    private OptimizeHandler mHandler;
    private LinearLayout mLlCenterContent;
    private int mImgMarkHeight;
    private LinearLayout mLlContent;
    private int mTextHeight;
    private AdDrawerView mAdDrawerView;
    private boolean[] mUsedArray;
    private int mLlCenterContentHeight;
    private int mMainContentHeight;
    private AnimatorSet mAnimatorSet;
    private TextView mTvOptimizedTitle;
    private LinearLayout mLlMain;
    private int mAdMoveDistance;
    private int fromWhere;
    private CustomTitleBar mCustomTitleBar;
    private View mBannerView;

    public static class IntentBuilder {
        private Intent intent;
        public IntentBuilder(){
            intent = new Intent();
            intent.putExtra(OPTIMIZE_LEFT_TEXT, "");
            intent.putExtra(OPTIMIZE_RIGHT_TEXT, "");
        }

        public IntentBuilder setClass(Context context){
            Preconditions.checkNotNull(context);
            intent.setClass(context, OptimizeActivity.class);
            return this;
        }

        public IntentBuilder setActionBar(@NonNull String text){
            Preconditions.checkNotNull(text);
            intent.putExtra(ACTION_BAR_TEXT, text);
            return this;
        }

        public IntentBuilder setLeftText(@NonNull String text, int size, int color){
            Preconditions.checkNotNull(text);
            Preconditions.checkArgument(size >= 0);
            intent.putExtra(OPTIMIZE_LEFT_TEXT, text);
            intent.putExtra(OPTIMIZE_LEFT_TEXT_SIZE, size);
            intent.putExtra(OPTIMIZE_LEFT_TEXT_COLOR, color);
            return this;
        }

        public IntentBuilder setRightText(@NonNull String text, int size, int color){
            Preconditions.checkNotNull(text);
            Preconditions.checkArgument(size >= 0);
            intent.putExtra(OPTIMIZE_RIGHT_TEXT, text);
            intent.putExtra(OPTIMIZE_RIGHT_TEXT_SIZE, size);
            intent.putExtra(OPTIMIZE_RIGHT_TEXT_COLOR, color);
            return this;
        }

        public IntentBuilder setAnimationShow(boolean isShow){
            intent.putExtra(OPTIMIZE_SHOW_AIMATION, isShow);
            return this;
        }

        public IntentBuilder setFrom(int where){
            intent.putExtra(OPTIMIZE_TYPE, where);
            return this;
        }

        public IntentBuilder setTitle(String title){
            Preconditions.checkNotNull(title);
            intent.putExtra(OPTIMIZE_TITLE, title);
            return this;
        }

        public IntentBuilder setNeedRecord(boolean isNeeded){
            intent.putExtra(NEED_RECORD, isNeeded);
            return this;
        }

        public Intent build(){
            return intent;
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_optimize;
    }

    @Override
    protected void initView() {
        mIsFirstCreate = true;
        mCustomTitleBar = findViewById(R.id.custom_title_bar);
        mImgMark = findViewById(R.id.img_blue_mark);
        mDoubleColorsTextView = findViewById(R.id.double_colors_textview);
        mLlCenterContent = findViewById(R.id.ll_optimize_center_content);
        mLlContent = findViewById(R.id.ll_optimize_content);
        mTvOptimizedTitle = findViewById(R.id.tv_optimized_title);
        mLlMain = findViewById(R.id.ll_main);
        mLlCenterContent.setTranslationY(SystemUtils.dp2px(this, 160));
    }

    @Override
    protected void initData() {
        mHandler = new OptimizeHandler(this);
        Intent intent = getIntent();
        if (intent != null) {
            fromWhere = intent.getIntExtra(OPTIMIZE_TYPE, 0);
            //是否需要记录此次优化行为
            boolean needRecord = intent.getBooleanExtra(NEED_RECORD, true);
            mUsedArray = getUsedArray(fromWhere, needRecord);
            String optimizeTitle = intent.getStringExtra(OPTIMIZE_TITLE);
            String actionBarText = intent.getStringExtra(ACTION_BAR_TEXT);
            String optimizeLeftText = intent.getStringExtra(OPTIMIZE_LEFT_TEXT);
            String optimizeRightText = intent.getStringExtra(OPTIMIZE_RIGHT_TEXT);
            int leftColor = intent.getIntExtra(OPTIMIZE_LEFT_TEXT_COLOR, 0);
            int rightColor = intent.getIntExtra(OPTIMIZE_RIGHT_TEXT_COLOR, 0);
            float leftSize = intent.getFloatExtra(OPTIMIZE_LEFT_TEXT_SIZE, 0);
            float rightSize = intent.getFloatExtra(OPTIMIZE_RIGHT_TEXT_SIZE, 0);
            mShowMarkAnim = intent.getBooleanExtra(OPTIMIZE_SHOW_AIMATION, true);
            if (optimizeTitle != null) {
                mTvOptimizedTitle.setText(optimizeTitle);
            }
            if (actionBarText != null) {
                mCustomTitleBar.setTitle(actionBarText);
            }
            if (optimizeLeftText != null && optimizeRightText != null) {
                mDoubleColorsTextView.setText(optimizeLeftText, optimizeRightText);
            }
            if (leftColor != 0 && rightColor != 0) {
                mDoubleColorsTextView.setTextColor(leftColor, rightColor);
            }
            if (leftSize != 0 && rightSize != 0) {
                mDoubleColorsTextView.setTextSize(leftSize, rightSize);
            }
            mDoubleColorsTextView.invalidate();
        }

        mBannerView = BannerAdvertiseManager.getInstance().showLargeBanner(getBannerAdId());
    }

    private boolean[] getUsedArray(int fromWhere, boolean needRecord) {
        SharePreferencesUtil preferencesUtil = SharePreferencesUtil.getInstance(this);
        long currentTime = System.currentTimeMillis();
        Intent monitorIntent = new Intent(MonitorService.ACTION_AFTER_OPTIMIZED);
        int adandonIndex = preferencesUtil.getLastAbandonOptimize();
        switch (fromWhere) {
            case FROM_BOOST:
                if (needRecord) {
                    monitorIntent.putExtra("boost_done", true);
                    preferencesUtil.setLastBoostTime(currentTime);
                }
                break;
            case FROM_JUNK:
                if (needRecord) {
                    monitorIntent.putExtra("clean_done", true);
                    preferencesUtil.setLastCleanJunkTime(currentTime);
                }
                if (adandonIndex == 1) preferencesUtil.setLastAbandonOptimize(0);
                break;
            case FROM_COOLER:
                if (needRecord) {
                    monitorIntent.putExtra("cooler_done", true);
                    preferencesUtil.setLastCoolTime(currentTime);
                }
                if (!preferencesUtil.getCoolerUsed()) {
                    preferencesUtil.setCoolerUsed(true);
                }
                if (adandonIndex == 2) preferencesUtil.setLastAbandonOptimize(0);
                break;
            case FROM_POWER:
                if (needRecord) {
                    monitorIntent.putExtra("power_save_done", true);
                    preferencesUtil.setLastPowerTime(currentTime);
                }
                if (!preferencesUtil.getPowerUsed()) {
                    preferencesUtil.setPowerUsed(true);
                }
                if (adandonIndex == 3) preferencesUtil.setLastAbandonOptimize(0);
                break;
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(monitorIntent);
        boolean[] usedArray = new boolean[5];
        usedArray[0] = preferencesUtil.getLastBoostTime() > 0;
        usedArray[1] = preferencesUtil.getLastCleanJunkTime() > 0;
        usedArray[2] = preferencesUtil.getCoolerUsed();
        usedArray[3] = preferencesUtil.getPowerUsed();
        usedArray[4] = preferencesUtil.getNotificationUsed();
        return usedArray;
    }

    @Override
    protected void initListener() {
        mCustomTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                Intent intent = new Intent(OptimizeActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
        getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                //广告view还没有加入进来时，计算尺寸
                if (mAdDrawerView == null) {
                    //全部界面的高度（即全屏去除actionbar）
                    mMainContentHeight = mLlMain.getHeight() - mCustomTitleBar.getHeight();
                    //对勾的高度
                    mImgMarkHeight = mImgMark.getHeight();
                    //对勾+两行字的高度
                    mLlCenterContentHeight = mLlCenterContent.getHeight();
                    //两行字的高度
                    mTextHeight = mLlCenterContentHeight - mImgMarkHeight;

                }
            }
        });
    }

    public void showMarkAnimation() {
        mImgMark.setImageDrawable(mBlueMarkDrawable);
        mBlueMarkDrawable.start();
    }

    private void createAdView() {
        mAdDrawerView = new AdDrawerView(this, mUsedArray, getBannerAdId());
    }

    private void initShowAd() {
        LinearLayout.LayoutParams adViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int minHeight = mMainContentHeight - mTextHeight;
        //AdView至少要铺满剩下的界面
        mAdDrawerView.setMinimumHeight(minHeight);
        //由于父控件是LinearLayout，广告view的top是mLlCenterContent
        mAdMoveDistance = mMainContentHeight - mTextHeight;
        mAdDrawerView.setTranslationY(mAdMoveDistance);
        mLlContent.addView(mAdDrawerView, adViewParams);
    }

    private void initMoveAnimation() {
        int textMoveDistance = mImgMarkHeight - mLlCenterContent.getTop();
        ObjectAnimator textMoveAnim = ObjectAnimator.ofFloat(mLlCenterContent, "translationY", textMoveDistance, 0);
        textMoveAnim.setDuration(TEXT_MOVE_DURATION);
        ObjectAnimator adMoveAnim = ObjectAnimator.ofFloat(mAdDrawerView, "translationY", mAdMoveDistance, 0);
        adMoveAnim.setDuration(TEXT_MOVE_DURATION);
        mAnimatorSet = new AnimatorSet();
        mAnimatorSet.playTogether(textMoveAnim, adMoveAnim);
        mAnimatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                if (fromWhere == FROM_BOOST) {
//                    showNativeAd();
//
//                } else {
                    showBannerAd();
//                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void showBannerAd() {
        if(mAdDrawerView != null){
            AdOuterItemView outerItemView = new AdOuterItemView(OptimizeActivity.this);
            outerItemView.addView(mBannerView);
            outerItemView.setGravity(Gravity.CENTER_HORIZONTAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.topMargin = SystemUtils.dp2px(OptimizeActivity.this, 10);
            mAdDrawerView.addView(outerItemView, 0, params);
        }
    }

    private void showNativeAd() {
        String adId = getNativeAdId();
        if (mAdDrawerView != null) {
            if (NativeAdvertiseManager.getInstance().isNativeAdsCached(adId)) {
                AdOuterItemView outerItemView = new AdOuterItemView(OptimizeActivity.this);
                NativeAdvertiseManager.getInstance().showNativeAds(adId, outerItemView);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.topMargin = SystemUtils.dp2px(OptimizeActivity.this, 10);
                mAdDrawerView.addView(outerItemView, 0, params);
            }
        }
    }

    private void startMoveAnimation() {
        mAnimatorSet.start();
    }

    private static class OptimizeHandler extends Handler {
        private WeakReference<OptimizeActivity> mWeakReference;
        private OptimizeActivity mActivity;


        OptimizeHandler(OptimizeActivity activity) {
            mWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            mActivity = mWeakReference.get();
            if (mActivity == null) {
                return;
            }
            switch (msg.what) {
                case SHOW_MARK_ANIMATION:
                    mActivity.showMarkAnimation();
                    mActivity.createAdView();
                    mActivity.initShowAd();
                    mActivity.initMoveAnimation();
                    sendEmptyMessageDelayed(SHOW_AD_VIEW, 1000);
                    break;
                case INIT_AD_VIEW:
                    mActivity.createAdView();
                    mActivity.initShowAd();
                    mActivity.initMoveAnimation();
                    sendEmptyMessageDelayed(SHOW_AD_VIEW, 500);
                    break;
                case SHOW_AD_VIEW:
                    mActivity.startMoveAnimation();
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeMessages(SHOW_MARK_ANIMATION);
        mHandler.removeMessages(INIT_AD_VIEW);
        mHandler.removeMessages(SHOW_AD_VIEW);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mIsFirstCreate) {
            mIsFirstCreate = false;
            if (!mShowMarkAnim) {
                mImgMark.setImageDrawable(getDrawable(R.drawable.vector_check_blue_mark));
                mHandler.sendEmptyMessageDelayed(INIT_AD_VIEW, 200);
            } else {
                mBlueMarkDrawable = (AnimatedVectorDrawable) getDrawable(R.drawable.anim_vector_blue_mark);
                mHandler.sendEmptyMessageDelayed(SHOW_MARK_ANIMATION, fromWhere == FROM_POWER ? 700 : 200);
            }
        }
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                BatterySaveService.sendHideCoverBroadcast(OptimizeActivity.this);
            }
        }, 500);


    }

    //        1. 加速结果页：ca-app-pub-4414232724432396/1785364667
//        2. 清理结果页：ca-app-pub-4414232724432396/6301912913
//        3. 省电结果页：ca-app-pub-4414232724432396/6439969122
//        4. 降温结果页：ca-app-pub-4414232724432396/5945145108
//        5. 通知栏结果页：ca-app-pub-4414232724432396/1622756713
    private String getNativeAdId() {
        if (BuildConfig.DEBUG) {
            return Constants.TEST_NATIVE_AD;
        } else {
            switch (fromWhere) {
                case FROM_BOOST:
                    return Constants.BOOST_NATIVE_AD;
                case FROM_JUNK:
                    return Constants.JUNK_NATIVE_AD;
                case FROM_POWER:
                    return Constants.POWER_NATIVE_AD;
                case FROM_COOLER:
                    return Constants.COOLER_NATIVE_AD;
                case FROM_NOTIFICATION:
                    return Constants.NOTIFICATION_NATIVE_AD;
                default:
                    return "";
            }
        }
    }

    private String getInterstitialAdId() {
        if (BuildConfig.DEBUG) {
            return Constants.TEST_INTERSTITIAL_AD;
        } else {
            switch (fromWhere) {
                case FROM_BOOST:
                    return Constants.BOOST_INTERSTITIAL_AD;
                case FROM_JUNK:
                    return Constants.JUNK_INTERSTITIAL_AD;
                case FROM_POWER:
                    return Constants.POWER_INTERSTITIAL_AD;
                case FROM_COOLER:
                    return Constants.COOLER_INTERSTITIAL_AD;
                case FROM_NOTIFICATION:
                    return Constants.NOTIFICATION_INTERSTITIAL_AD;
                default:
                    return "";
            }
        }
    }

    private String getBannerAdId() {
        if (BuildConfig.DEBUG) {
            return Constants.TEST_BANNER_AD;
        } else {
            switch (fromWhere) {
                case FROM_BOOST:
                    return Constants.BOOST_BANNER_AD;
                case FROM_JUNK:
                    return Constants.JUNK_BANNER_AD;
                case FROM_POWER:
                    return Constants.POWER_BANNER_AD;
                case FROM_COOLER:
                    return Constants.COOLER_BANNER_AD;
                case FROM_NOTIFICATION:
                    return Constants.NOTIFICATION_BANNER_AD;
                default:
                    return "";
            }
        }
    }
}
