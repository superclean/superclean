package com.youtupu.superclean.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.instreet.business.utils.CustomTimer;
import com.youtupu.superclean.R;
import com.youtupu.superclean.service.BatterySaveService;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.view.GearCenterView;

/**
 * created by yihao 2019/4/12
 * 在android 8以上的机型中，当用户接到电话时，为避免悬浮窗遮住系统弹出的电话提醒
 * 对话框，使用一个假的Activity来代替悬浮窗，以便用户能够正常接听电话。该Activity被拉起时，
 * 会执行一个假的省电任务。当用户接完电话后再次进行该Activity时，会让BatterySaveService
 * 继续执行真实的省电任务，并结束自己。
 * <p>
 * 还有个使用场景：如果辅助服务权限已经授予，但服务没有注册到PowerDetailFragment，则也需要
 * 一个FakePowerSaveActivity来执行假的任务。
 */
public class FakePowerSaveActivity extends Activity {
    private GearCenterView mGearCenterView;
    private TextView mTvCenter;
    private RelativeLayout mRlContent;
    private AnimatorSet mAnimatorSet;
    private int currentProgress = 0;
    private int totalTask = 0;
    private CustomTimer timer;
    private boolean isPaused = false;
    private boolean mShowTips = true;
    private int mFromWhere = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null != getIntent()) {
            currentProgress = getIntent().getIntExtra("currentProgress", 0);
            totalTask = getIntent().getIntExtra("totalTask", 0);
            mShowTips = getIntent().getBooleanExtra("showTips", true);
            mFromWhere = getIntent().getIntExtra("fromWhere", 0);
            if (mFromWhere > 0) {
                if (mFromWhere == 1) {
                    //从充电弹框来的
                    AnalyticsUtil.logEvent(this, "fake_from_charge", null);
                } else if (mFromWhere == 11) {
                    //从首页退出时的弹框来的
                    AnalyticsUtil.logEvent(this, "fake_from_exit", null);
                }
            }
        }

        if (null != savedInstanceState) {
            currentProgress = savedInstanceState.getInt("currentProgress", 0);
            totalTask = savedInstanceState.getInt("totalTask", 0);
            mShowTips = savedInstanceState.getBoolean("showTips", true);
            mFromWhere = savedInstanceState.getInt("fromWhere", 0);
        }

        View cover = View.inflate(this, R.layout.kill_process_view, null);
        setContentView(cover);
        mGearCenterView = cover.findViewById(R.id.gear_center_view);
        mTvCenter = cover.findViewById(R.id.tv_center);
        mRlContent = cover.findViewById(R.id.rl_content);
        TextView tvTips = cover.findViewById(R.id.tv_tips);
        if (mShowTips) {
            tvTips.setVisibility(View.VISIBLE);
        }
        ObjectAnimator gearRunAnimator = ObjectAnimator.ofFloat(mGearCenterView, GearCenterView.ANGLE, 0, -360);
        gearRunAnimator.setDuration(500);
        gearRunAnimator.setInterpolator(new LinearInterpolator());
        gearRunAnimator.setRepeatCount(5);
        ObjectAnimator dismissAnim = ObjectAnimator.ofFloat(mRlContent, "alpha", 1f, 0f);
        dismissAnim.setDuration(3000);
        ObjectAnimator gradualAnim = ObjectAnimator.ofFloat(mGearCenterView, GearCenterView.PERCENT, 0.5f, 0f);
        gradualAnim.setDuration(3000);
        mAnimatorSet = new AnimatorSet();
        if (mShowTips) {
            ObjectAnimator textDismissAnim = ObjectAnimator.ofFloat(tvTips, "alpha", 1f, 0f);
            textDismissAnim.setDuration(3000);
            mAnimatorSet.playTogether(gearRunAnimator, dismissAnim, gradualAnim, textDismissAnim);
        } else {
            mAnimatorSet.playTogether(gearRunAnimator, dismissAnim, gradualAnim);
        }
        mAnimatorSet.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                goOptimize();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mGearCenterView.setPercent(0.5f);
        mRlContent.setAlpha(1.0f);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("currentProgress", currentProgress);
        outState.putInt("totalTask", totalTask);
        outState.putBoolean("showTips", mShowTips);
        outState.putInt("fromWhere", mFromWhere);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
        mAnimatorSet.pause();
        timer.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimatorSet.isPaused()) {
            mAnimatorSet.resume();
        }
        timer.resume();
        // 如果从pause状态切换而来，则关闭自己，并继续由BatterySaveService执行任务
        if (isPaused && currentProgress < totalTask && BatterySaveService.isStarted) {
            BatterySaveService.sendContinueTaskBroadcast(this, currentProgress);
            mTvCenter.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 1000);
        }
        isPaused = false;

    }

    private void updateProgress() {
        mTvCenter.setText(String.format(getString(R.string.tv_kill_process), currentProgress, totalTask));
    }

    private void goOptimize() {
//        Intent intent = new Intent(this, OptimizeActivity.class);
//        intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_POWER);
//        intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, this.getString(R.string.battery_title));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, this.getString(R.string.apps_dormancy));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, String.valueOf(totalTask));
        Intent intent = new OptimizeActivity.IntentBuilder().setClass(this)
                .setFrom(OptimizeActivity.FROM_POWER)
                .setActionBar(this.getString(R.string.battery_title))
                .setLeftText(String.valueOf(totalTask),0,0)
                .setRightText(this.getString(R.string.apps_dormancy),0,0).build();
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        CommonUtils.debug("hide cover");
        BatterySaveService.sendHideCoverBroadcast(this);
        updateProgress();
        doFakeTask();
    }

    // 执行一个假的省电任务
    private void doFakeTask() {
        timer = new CustomTimer(0, 500, new Runnable() {
            @Override
            public void run() {
                updateProgress();
                if (currentProgress == totalTask) {
                    mAnimatorSet.start();
                    timer.stop();
                    return;
                }
                currentProgress++;

            }
        });
        timer.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //如果是外界把我拉起的，则我中途退出前把首页拉起来
        if (mFromWhere > 0 && mFromWhere < 10) {
            startActivity(new Intent(this, HomeActivity.class));
        }
    }


}
