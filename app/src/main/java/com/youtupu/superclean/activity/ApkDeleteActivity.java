package com.youtupu.superclean.activity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youtupu.superclean.bean.ApkInfo;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.CustomTitleBar;
import com.youtupu.superclean.view.TitleTotalView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.youtupu.superclean.R;

public class ApkDeleteActivity extends AppCompatActivity {
    private static final int TEXT_ANIMATION = 0;
    private static final int DISSMISS_ANIMATION = 1;
    private static final int TO_OPTIMIZE_ACTIVITY = 2;
    private ArrayList<ApkInfo> mApkList;
    private DeleteHandler mHandler;
    private Thread mDeleteThread;
    private boolean mIsFirstCreate = false;
    private List<ObjectAnimator> mAnimatorList;
    private ObjectAnimator mTextAnimator;
    private long mSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String appName = null;
        mApkList = null;
        if (intent != null) {
            appName = intent.getStringExtra("app_name");
            mApkList = intent.getParcelableArrayListExtra("apk_list");
        }
        if (TextUtils.isEmpty(appName) || mApkList == null || mApkList.size() == 0) {
            finish();
            return;
        }
        setContentView(R.layout.activity_apk_delete);
        mIsFirstCreate = true;
        TitleTotalView titleTotalView = findViewById(R.id.junk_total_select);
        LinearLayout llContainer = findViewById(R.id.ll_junk_clean_container);
        mSize = 0;
        for (ApkInfo info : mApkList) {
            mSize += info.getFileSize();
        }
        titleTotalView.setStartSize((int) (mSize / 1000));
        titleTotalView.setFileSize((int) (mSize / 1000));
        int itemHight = SystemUtils.dp2px(this, 60);
        int screenWidth = SystemUtils.getScreenWidth(this);
        int marginTop = SystemUtils.dp2px(this, 10);
        CustomTitleBar customTitleBar = findViewById(R.id.custom_title_bar);
        customTitleBar.measure(0, 0);
        int containerHeight = SystemUtils.getScreenHight(this) - customTitleBar.getMeasuredHeight() - SystemUtils.dp2px(this, 80);
        int maxNumber = containerHeight / (itemHight + marginTop) + 1;
        int needNumber = mApkList.size() > maxNumber ? maxNumber : mApkList.size();
        mAnimatorList = new ArrayList<>();
        for (int i = 0; i < needNumber; i++) {
            ApkInfo info = mApkList.get(i);
            View itemView = View.inflate(this, R.layout.junk_clean_item_layout, null);
            ImageView imgIcon = itemView.findViewById(R.id.img_junk_clean_item_icon);
            imgIcon.setImageResource(R.mipmap.apk_file);
            TextView tvItem = itemView.findViewById(R.id.tv_junk_clean_item);
            tvItem.setText(info.getFilePath());
            TextView tvSize = itemView.findViewById(R.id.tv_junk_item_size);
            tvSize.setText(SystemUtils.getFileSize(info.getFileSize()));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(SystemUtils.dp2px(this, 10), SystemUtils.dp2px(this, 5), SystemUtils.dp2px(this, 10), 0);
            llContainer.addView(itemView, params);
            ObjectAnimator animator = ObjectAnimator.ofFloat(itemView, "translationX", 0f, -screenWidth);
            animator.setDuration(200);
            mAnimatorList.add(animator);
        }
        mHandler = new DeleteHandler(this);
        mDeleteThread = new Thread() {
            @Override
            public void run() {
                for (ApkInfo info : mApkList) {
                    File file = new File(info.getFilePath());
                    if (file.exists()) {
                        file.delete();
                    }
                }
            }
        };
        mDeleteThread.start();
        mTextAnimator = ObjectAnimator.ofInt(titleTotalView, TitleTotalView.FILE_SIZE, titleTotalView.getFileSize(), 0);
        mTextAnimator.setDuration(300);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mIsFirstCreate) {
            mIsFirstCreate = false;
            mHandler.sendEmptyMessageDelayed(TEXT_ANIMATION, 1000);
        }
    }

    private static class DeleteHandler extends Handler {
        private WeakReference<ApkDeleteActivity> mReference;
        private int mIndex = 0;

        DeleteHandler(ApkDeleteActivity activity) {
            mReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            ApkDeleteActivity activity = mReference.get();
            if (activity == null) return;
            switch (msg.what) {
                case TEXT_ANIMATION:
                    if (activity.mDeleteThread.isAlive()) {
                        activity.mDeleteThread.interrupt();
                    }
                    activity.mTextAnimator.start();
                    mIndex = 0;
                    sendEmptyMessageDelayed(DISSMISS_ANIMATION, 300);
                    break;
                case DISSMISS_ANIMATION:
                    if (mIndex < activity.mAnimatorList.size()) {
                        activity.mAnimatorList.get(mIndex).start();
                        mIndex++;
                        sendEmptyMessageDelayed(DISSMISS_ANIMATION, 50);
                    } else {
                        sendEmptyMessageDelayed(TO_OPTIMIZE_ACTIVITY, 150);
                    }
                    break;
                case TO_OPTIMIZE_ACTIVITY:
//                    Intent intent = new Intent(activity, OptimizeActivity.class);
//                    intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, activity.getString(R.string.junk_clean));
//                    intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, SystemUtils.getFileSize(activity.mSize));
//                    intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, activity.getString(R.string.junk_optimized));
//                    intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_JUNK);
//                    intent.putExtra(OptimizeActivity.NEED_RECORD, false);
                    Intent intent = new OptimizeActivity.IntentBuilder().setClass(activity).
                            setActionBar(activity.getString(R.string.junk_clean)).
                            setLeftText(SystemUtils.getFileSize(activity.mSize), 0, 0)
                            .setRightText(activity.getString(R.string.junk_optimized), 0, 0)
                            .setFrom(OptimizeActivity.FROM_JUNK).setNeedRecord(false).build();
                    activity.startActivity(intent);
                    activity.finish();
                    break;
            }
        }
    }

}
