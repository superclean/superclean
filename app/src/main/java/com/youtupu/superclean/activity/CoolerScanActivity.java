package com.youtupu.superclean.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import java.util.Random;

import com.youtupu.superclean.R;
import com.youtupu.superclean.base.BaseActivity;
import com.youtupu.superclean.dialog.DoubleChoicesDialog;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.CustomTitleBar;
import com.youtupu.superclean.view.EllipsisView;
import com.youtupu.superclean.view.ScanningView;

public class CoolerScanActivity extends BaseActivity {
    private static final int SCAN_ONE_CIRCLE_DURATION = 800;
    private static final int DONE_ANIM_DURATION = 600;
    private ScanningView mScanningView;
    private AnimatorSet mScanAnimatorSet;
    private ImageView mImgCenter;
    private EllipsisView mEllipsisView;
    private int mCpuTemperature = 0;
    private boolean mIsFirstCreate = false;
    private DoubleChoicesDialog mExitDialog;
    private int mReduceTemp = 0;
    private CustomTitleBar mCustomTitleBar;
    private boolean mFromOutSide = false;
    private int mFromWhere;

    @Override
    protected boolean beforeSetContentView() {
        long lastCoolTime = SharePreferencesUtil.getInstance(this).getLastCoolTime();
        if (lastCoolTime > 0) {
            long currentTime = System.currentTimeMillis();
            long duration = currentTime - lastCoolTime;
            //1分钟内冷却过，无需再cool
            if (duration < 1000 * 60) {
                waitForCoolDown();
            } else if (duration < 1000 * 60 * 5) {
                noNeedToCoolDown();
            }
        }
        return true;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_cooler;
    }

    @Override
    protected void initView() {
        mCustomTitleBar = findViewById(R.id.custom_title_bar);
        mScanningView = findViewById(R.id.scanning_view);
        mImgCenter = findViewById(R.id.img_scan_center);
        mEllipsisView = findViewById(R.id.ellipsis_view);
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            mFromWhere = intent.getIntExtra("fromWhere", 0);
            //是否从外界而来
            if (mFromWhere > 0) {
                mFromOutSide = true;
                //从控制台而来
                if (mFromWhere == 1) {
                    AnalyticsUtil.logEvent(this, "console_cooler", null);
                }
            }
        }
        mIsFirstCreate = true;
        mCpuTemperature = SystemUtils.getBatteryTemperature(this);
        mScanAnimatorSet = new AnimatorSet();
        if (mCpuTemperature > 30) {
            generateReduceTemp();
            ObjectAnimator gradualAnimator = ObjectAnimator.ofFloat(mScanningView, ScanningView.PERCENT, 0f, 1.0f);
            gradualAnimator.setDuration(SCAN_ONE_CIRCLE_DURATION * 2);
            ObjectAnimator angleAnimator = ObjectAnimator.ofFloat(mScanningView, ScanningView.SWEEP_ANGLE, 0f, 480f);
            angleAnimator.setDuration(SCAN_ONE_CIRCLE_DURATION);
            angleAnimator.setRepeatCount(1);
            ObjectAnimator doneAnimator = ObjectAnimator.ofFloat(mScanningView, ScanningView.DONE_ANGLE, 0f, 360f);
            doneAnimator.setDuration(DONE_ANIM_DURATION);
            mScanAnimatorSet.play(angleAnimator).with(gradualAnimator).before(doneAnimator);
        } else {
            ObjectAnimator angleAnimator = ObjectAnimator.ofFloat(mScanningView, ScanningView.SWEEP_ANGLE, 0f, 480f);
            angleAnimator.setDuration(SCAN_ONE_CIRCLE_DURATION);
            angleAnimator.setRepeatCount(1);
            ObjectAnimator doneAnimator = ObjectAnimator.ofFloat(mScanningView, ScanningView.DONE_ANGLE, 0f, 360f);
            doneAnimator.setDuration(DONE_ANIM_DURATION);
            mScanAnimatorSet.play(angleAnimator).before(doneAnimator);
        }
    }

    @Override
    protected void initListener() {
        mCustomTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                onBackPressed();
            }
        });
        mScanAnimatorSet.addListener(new Animator.AnimatorListener() {
            private boolean isCanceled = false;

            @Override
            public void onAnimationStart(Animator animation) {
                isCanceled = false;
                mEllipsisView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isCanceled) {
                    if (mCpuTemperature > 30) {
                        mImgCenter.setVisibility(View.INVISIBLE);
                        mScanningView.setVisibility(View.INVISIBLE);
                        Intent intent = new Intent(CoolerScanActivity.this, CoolerDownActivity.class);
                        intent.putExtra("CpuTemp", mCpuTemperature);
                        intent.putExtra("reducedTemp", mReduceTemp);
                        intent.putExtra("fromWhere", mFromWhere);
                        startActivity(intent);
                        finish();
                    } else {
                        //温度小于30，不用降温，直接跳转优化界面
                        noNeedToCoolDown();
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                isCanceled = true;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
    }


    private void noNeedToCoolDown() {
//        Intent intent = new Intent(CoolerScanActivity.this, OptimizeActivity.class);
//        intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_COOLER);
//        intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, getString(R.string.cpu_title));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_TITLE, getString(R.string.normal));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, getString(R.string.no_need_to_cool));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, "");
//        intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT_COLOR, getResources().getColor(R.color.colorMainBlue));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT_COLOR, getResources().getColor(R.color.colorMainBlue));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_SHOW_AIMATION, false);
//        intent.putExtra(OptimizeActivity.NEED_RECORD, false);
        Intent intent = new OptimizeActivity.IntentBuilder().setClass(CoolerScanActivity.this)
                .setFrom(OptimizeActivity.FROM_COOLER).setActionBar(getString(R.string.cpu_title)).setTitle(getString(R.string.normal))
                .setRightText(getString(R.string.no_need_to_cool), 0, getResources().getColor(R.color.colorMainBlue))
                .setAnimationShow(false).setNeedRecord(false).build();
        startActivity(intent);
        finish();
    }

    private void waitForCoolDown() {
//        Intent intent = new Intent(CoolerScanActivity.this, OptimizeActivity.class);
//        intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_COOLER);
//        intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, getString(R.string.cpu_title));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_TITLE, getString(R.string.optimized));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, getString(R.string.wait_to_cool));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, "");
//        intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT_COLOR, getResources().getColor(R.color.colorMainBlue));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT_COLOR, getResources().getColor(R.color.colorMainBlue));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_SHOW_AIMATION, false);

        Intent intent = new OptimizeActivity.IntentBuilder()
                .setClass(CoolerScanActivity.this)
                .setFrom(OptimizeActivity.FROM_COOLER)
                .setActionBar(getString(R.string.cpu_title))
                .setTitle(getString(R.string.optimized))
                .setRightText(getString(R.string.wait_to_cool), 0, getResources().getColor(R.color.colorMainBlue))
                .setAnimationShow(false)
                .build();
        startActivity(intent);
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mIsFirstCreate) {
            mIsFirstCreate = false;
            mCustomTitleBar.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startAnimation();
                    mEllipsisView.startEllipsisAnimation();
                }
            }, 400);
        }
        if (mScanAnimatorSet.isPaused()) {
            mScanAnimatorSet.resume();
        }
    }

    @Override
    protected void onStop() {
        if (mScanAnimatorSet.isRunning()) {
            mScanAnimatorSet.pause();
        }
        super.onStop();
    }

    private void startAnimation() {
        mScanAnimatorSet.start();
    }

    private void generateReduceTemp() {
        int overTemp = mCpuTemperature - 30;
        Random random = new Random();
        if (overTemp > 20) {
            mReduceTemp = 10 + random.nextInt(overTemp - 20);
        } else if (overTemp > 10) {
            mReduceTemp = 5 + random.nextInt(overTemp - 10);
        } else {
            mReduceTemp = 2 + random.nextInt(2);
        }
    }


    @Override
    public void onBackPressed() {
        if (mScanAnimatorSet.isRunning()) {
            mScanAnimatorSet.pause();
            showDialog();
        } else {
            //若中途退出时，温度高，则认为是abandon
            if (mCpuTemperature > 30) {
                SharePreferencesUtil.getInstance(this).setLastAbandonOptimize(2);
                SharePreferencesUtil.getInstance(this).setLastTemperature(mCpuTemperature);
            }
            //如果是从外界打开我这个activity，那我中断退出时，把首页拉起
            if (mFromOutSide) {
                startActivity(new Intent(this, HomeActivity.class));
            }
            super.onBackPressed();
        }
    }

    private void showDialog() {
        if (mExitDialog == null) {
            mExitDialog = new DoubleChoicesDialog(this, getString(R.string.interrupted_when_cooling),
                    getString(R.string.abandon), getString(R.string.go_on), R.mipmap.snow,new DoubleChoicesDialog.OnButtonClickListener() {
                @Override
                public void leftButtonClick(DoubleChoicesDialog dlg) {
                    //若中途退出时，温度高，则认为是abandon
                    if (mCpuTemperature > 30) {
                        SharePreferencesUtil.getInstance(CoolerScanActivity.this).setLastAbandonOptimize(2);
                        SharePreferencesUtil.getInstance(CoolerScanActivity.this).setLastTemperature(mCpuTemperature);
                    }
                    //如果是从外界打开我这个activity，那我中断退出时，把首页拉起
                    if (mFromOutSide) {
                        startActivity(new Intent(CoolerScanActivity.this, HomeActivity.class));
                    }
                    finish();
                }

                @Override
                public void rightButtonClick(DoubleChoicesDialog dlg) {
                    dlg.dismiss();
                    if (mScanAnimatorSet.isPaused()) {
                        mScanAnimatorSet.resume();
                    }
                }
            });
        }
        mExitDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mShowActivityView) {
            if (mScanAnimatorSet.isRunning() || mScanAnimatorSet.isPaused()) {
                mScanAnimatorSet.cancel();
            }
        }
    }
}
