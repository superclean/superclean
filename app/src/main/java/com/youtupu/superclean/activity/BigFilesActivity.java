package com.youtupu.superclean.activity;

import android.animation.ObjectAnimator;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;
import java.util.List;

import cn.instreet.business.advertise.NativeAdvertiseManager;
import com.youtupu.superclean.BuildConfig;
import com.youtupu.superclean.R;
import com.youtupu.superclean.app.SuperCleanApplication;
import com.youtupu.superclean.bean.BigFileInfo;
import com.youtupu.superclean.extract.OnBigFileListener;
import com.youtupu.superclean.service.BigFileService;
import com.youtupu.superclean.utils.Constants;
import com.youtupu.superclean.utils.ImageUtil;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.CustomTitleBar;
import com.youtupu.superclean.view.FileSizeText;
import com.youtupu.superclean.view.InterceptLinearLayout;
import com.youtupu.superclean.view.ProportionalBar;
import com.youtupu.superclean.view.WaveBar;
import qiu.niorgai.StatusBarCompat;

public class BigFilesActivity extends AppCompatActivity implements OnBigFileListener {
    private static final int FILE_SIZE_INCREASE_ANIMATION = 0;
    private static final int GOT_SCAN_RESULY = 1;
    private FileSizeText mFtvBig;
    private BigFileService mService;
    private FileSizeText mTvMedia;
    private GridView mGvMedia;
    private ProgressBar mPbMedia;
    private FileSizeText mTvAudio;
    private GridView mGvAudio;
    private ProgressBar mPbAudio;
    private FileSizeText mTvDocument;
    private GridView mGvDocument;
    private ProgressBar mPbDocument;
    private FileSizeText mTvRarely;
    private LinearLayout mLlRarelyContent;
    private ProgressBar mPbRarely;
    private BigFileConnetion mConnetion;
    private long mMediaSize;
    private long mAudioSize;
    private long mDocumentSize;
    private long mRarelySize;
    private long mTotalSize;
    private BigFileHandler mHandler;
    private ObjectAnimator mBigSizeTextAnimator;
    private boolean mGotMediaResult = false;
    private boolean mGotAudioResult = false;
    private boolean mGotDocumentResult = false;
    private boolean mGotRarelyResult = false;
    private boolean mGotTotalResult = false;
    private long mTotalStorage;
    private FileSizeText mFtvOthers;
    private FileSizeText mFtvFree;
    private ProportionalBar mProportionalBar;
    private WaveBar mWaveBar;
    private TextView mTvRarelyNum;
    private LinearLayout mLlEmptyMedia;
    private LinearLayout mLlEmptyAudio;
    private LinearLayout mLlEmptyDocument;
    private LinearLayout mLlEmptyrarely;
    private InterceptLinearLayout mLlMedia;
    private InterceptLinearLayout mLlAudio;
    private InterceptLinearLayout mLlDocument;
    private InterceptLinearLayout mLlRarely;
    private LinearLayout mLlResultMedia;
    private LinearLayout mLlResultAudio;
    private LinearLayout mLlResultDocument;
    private LinearLayout mLlResultRarely;
    private MediaAdapter mMediaAdapter;
    private AudioAdapter mAudioAdapter;
    private DocumentAdapter mDocumentAdapter;
    private List<BigFileInfo> mRarelyList;
    private RelativeLayout mAdContainer;
    private boolean mIsFirstCreate = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTotalStorage = SystemUtils.getExternalStorageTotalSize();
        if (mTotalStorage == 0) {
            finish();
            return;
        }
        mIsFirstCreate = true;
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.clean_button_click));
        setContentView(R.layout.activity_big_files);
        CustomTitleBar titleBar = findViewById(R.id.custom_title_bar);
        TextView tvTotal1 = findViewById(R.id.tv_total1);
        TextView tvTotal2 = findViewById(R.id.tv_total2);
        mFtvBig = findViewById(R.id.ftv_big);
        mFtvOthers = findViewById(R.id.ftv_others);
        mFtvFree = findViewById(R.id.ftv_free);
        mTvMedia = findViewById(R.id.tv_video_size);
        mGvMedia = findViewById(R.id.gv_video);
        mPbMedia = findViewById(R.id.pb_video);
        mTvAudio = findViewById(R.id.tv_audio_size);
        mGvAudio = findViewById(R.id.gv_audio);
        mPbAudio = findViewById(R.id.pb_audio);
        mTvDocument = findViewById(R.id.tv_document_size);
        mGvDocument = findViewById(R.id.gv_document);
        mPbDocument = findViewById(R.id.pb_document);
        mTvRarely = findViewById(R.id.tv_rarely_size);
        mLlRarelyContent = findViewById(R.id.ll_rarely_content);
        mPbRarely = findViewById(R.id.pb_rarely);
        mProportionalBar = findViewById(R.id.proportion_bar);
        mWaveBar = findViewById(R.id.wavebar);
        mTvRarelyNum = findViewById(R.id.tv_rarely_num);
        mLlEmptyMedia = findViewById(R.id.ll_empty_media);
        mLlEmptyAudio = findViewById(R.id.ll_empty_audio);
        mLlEmptyDocument = findViewById(R.id.ll_empty_document);
        mLlEmptyrarely = findViewById(R.id.ll_empty_rarely);
        mLlMedia = findViewById(R.id.ll_media);
        mLlAudio = findViewById(R.id.ll_audio);
        mLlDocument = findViewById(R.id.ll_document);
        mLlRarely = findViewById(R.id.ll_rarely);
        mLlResultMedia = findViewById(R.id.ll_result_media);
        mLlResultAudio = findViewById(R.id.ll_result_audio);
        mLlResultDocument = findViewById(R.id.ll_result_document);
        mLlResultRarely = findViewById(R.id.ll_result_rarely);
        mAdContainer = findViewById(R.id.ad_container);

        titleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                finish();
            }
        });
        StringBuilder builder = new StringBuilder();
        builder.append("/");
        builder.append(SystemUtils.getFileSize(mTotalStorage));
        tvTotal1.setText(builder);
        tvTotal2.setText(builder);
        mHandler = new BigFileHandler(this);
        mConnetion = new BigFileConnetion();
        bindService(new Intent(this, BigFileService.class), mConnetion, BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mIsFirstCreate) {
            mIsFirstCreate = false;
            mAdContainer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //加载原生广告
                    if (BuildConfig.DEBUG) {
                        NativeAdvertiseManager.getInstance().showNativeAds(
                                Constants.TEST_NATIVE_AD, mAdContainer,
                                NativeAdvertiseManager.STYLE_MEDIUM, getResources().getColor(R.color.clean_button));
                    } else {
                        NativeAdvertiseManager.getInstance().showNativeAds(
                                Constants.BIG_FILE_NATIVE_AD, mAdContainer,
                                NativeAdvertiseManager.STYLE_MEDIUM, getResources().getColor(R.color.clean_button));
                    }
                }
            }, 500);
        }
    }

    @Override
    public void onMediaScanStart() {
        mGotMediaResult = false;
        mMediaSize = 0;
        mLlEmptyMedia.setVisibility(View.INVISIBLE);
        mGvMedia.setVisibility(View.INVISIBLE);
        mPbMedia.setVisibility(View.VISIBLE);
        mLlResultMedia.setVisibility(View.VISIBLE);
    }

    @Override
    public void onMediaScanUpdate(long size) {
        mMediaSize = size;
        updateSizeText();
        mTvMedia.setFileSize(size);
    }

    @Override
    public void onMediaScanDone(final List<BigFileInfo> bigFileInfos) {
        mPbMedia.setVisibility(View.INVISIBLE);
        if (bigFileInfos != null) {
            if (bigFileInfos.isEmpty()) {
                mLlEmptyMedia.setVisibility(View.VISIBLE);
                mLlResultMedia.setVisibility(View.INVISIBLE);
            } else {
                mMediaAdapter = new MediaAdapter(this, bigFileInfos);
                mGvMedia.setAdapter(mMediaAdapter);
                mGvMedia.setVisibility(View.VISIBLE);
                View.OnClickListener listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SuperCleanApplication application = (SuperCleanApplication) getApplication();
                        application.setBigFileInfoList(bigFileInfos);
                        Intent intent = new Intent(BigFilesActivity.this, BigFileDetailActivity.class);
                        intent.putExtra("file_style", BigFileDetailActivity.STYLE_MEDIA);
                        startActivityForResult(intent, 1);
                    }
                };
                mLlMedia.setOnClickListener(listener);
            }
        } else {
            mLlEmptyMedia.setVisibility(View.VISIBLE);
            mLlResultMedia.setVisibility(View.INVISIBLE);
        }
        mGotMediaResult = true;
        updateScanResult();
    }

    @Override
    public void onAudioScanStart() {
        mGotAudioResult = false;
        mAudioSize = 0;
        mLlEmptyAudio.setVisibility(View.INVISIBLE);
        mGvAudio.setVisibility(View.INVISIBLE);
        mPbAudio.setVisibility(View.VISIBLE);
        mLlResultAudio.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAudioScanUpdate(long size) {
        mAudioSize = size;
        updateSizeText();
        mTvAudio.setFileSize(size);
    }

    @Override
    public void onAudioScanDone(final List<BigFileInfo> bigFileInfos) {
        mPbAudio.setVisibility(View.INVISIBLE);
        if (bigFileInfos != null) {
            if (bigFileInfos.isEmpty()) {
                mLlEmptyAudio.setVisibility(View.VISIBLE);
                mLlResultAudio.setVisibility(View.INVISIBLE);
            } else {
                mAudioAdapter = new AudioAdapter(this, bigFileInfos);
                mGvAudio.setAdapter(mAudioAdapter);
                mGvAudio.setVisibility(View.VISIBLE);
                mLlAudio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SuperCleanApplication application = (SuperCleanApplication) getApplication();
                        application.setBigFileInfoList(bigFileInfos);
                        Intent intent = new Intent(BigFilesActivity.this, BigFileDetailActivity.class);
                        intent.putExtra("file_style", BigFileDetailActivity.STYLE_AUDIO);
                        startActivityForResult(intent, 2);
                    }
                });
            }
        } else {
            mLlEmptyAudio.setVisibility(View.VISIBLE);
            mLlResultAudio.setVisibility(View.INVISIBLE);
        }
        mGotAudioResult = true;
        updateScanResult();
    }

    @Override
    public void onDocumentStart() {
        mGotDocumentResult = false;
        mDocumentSize = 0;
        mGvDocument.setVisibility(View.INVISIBLE);
        mPbDocument.setVisibility(View.VISIBLE);
        mLlResultDocument.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDocumentUpdate(long size) {
        mDocumentSize = size;
        updateSizeText();
        mTvDocument.setFileSize(size);
    }

    @Override
    public void onDocumentDone(final List<BigFileInfo> bigFileInfos) {
        mPbDocument.setVisibility(View.INVISIBLE);
        if (bigFileInfos != null) {
            if (bigFileInfos.isEmpty()) {
                mLlEmptyDocument.setVisibility(View.VISIBLE);
                mLlResultDocument.setVisibility(View.INVISIBLE);
            } else {
                mDocumentAdapter = new DocumentAdapter(this, bigFileInfos);
                mGvDocument.setAdapter(mDocumentAdapter);
                mGvDocument.setVisibility(View.VISIBLE);
                mLlDocument.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SuperCleanApplication application = (SuperCleanApplication) getApplication();
                        application.setBigFileInfoList(bigFileInfos);
                        Intent intent = new Intent(BigFilesActivity.this, BigFileDetailActivity.class);
                        intent.putExtra("file_style", BigFileDetailActivity.STYLE_DOCUMENT);
                        startActivityForResult(intent, 3);
                    }
                });
            }
        } else {
            mLlEmptyDocument.setVisibility(View.VISIBLE);
            mLlResultDocument.setVisibility(View.INVISIBLE);
        }
        mGotDocumentResult = true;
        updateScanResult();
    }

    @Override
    public void onRarelyScanStart() {
        mGotRarelyResult = false;
        mRarelySize = 0;
        mLlRarelyContent.setVisibility(View.INVISIBLE);
        mPbRarely.setVisibility(View.VISIBLE);
        mLlResultRarely.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRarelyScanUpdate(long size) {
        mRarelySize = size;
        updateSizeText();
        mTvRarely.setFileSize(size);
    }

    @Override
    public void onRarelyScanDone(final List<BigFileInfo> bigFileInfos) {
        mRarelyList = bigFileInfos;
        mPbRarely.setVisibility(View.INVISIBLE);
        if (bigFileInfos != null) {
            if (bigFileInfos.isEmpty()) {
                mLlEmptyrarely.setVisibility(View.VISIBLE);
                mLlResultRarely.setVisibility(View.INVISIBLE);
            } else {
                mLlRarelyContent.setVisibility(View.VISIBLE);
                mTvRarelyNum.setText(String.valueOf(bigFileInfos.size()));
                mLlRarely.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SuperCleanApplication application = (SuperCleanApplication) getApplication();
                        application.setBigFileInfoList(bigFileInfos);
                        Intent intent = new Intent(BigFilesActivity.this, BigFileDetailActivity.class);
                        intent.putExtra("file_style", BigFileDetailActivity.STYLE_RARELY);
                        startActivityForResult(intent, 4);
                    }
                });
            }
        } else {
            mLlEmptyrarely.setVisibility(View.VISIBLE);
            mLlResultRarely.setVisibility(View.INVISIBLE);
        }
        mGotRarelyResult = true;
        updateScanResult();
    }

    private void updateSizeText() {
        mTotalSize = mMediaSize + mAudioSize + mDocumentSize + mRarelySize;
    }

    private void updateScanResult() {
        if (mGotMediaResult && mGotAudioResult && mGotDocumentResult && mGotRarelyResult) {
            mGotTotalResult = true;
        }
    }

    private class BigFileConnetion implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BigFileService.BigFileBinder bigFileBinder = (BigFileService.BigFileBinder) service;
            if (bigFileBinder != null) {
                mService = bigFileBinder.getService();
                if (mService != null) {
                    mService.setOnBigFileListener(BigFilesActivity.this);
                    mGotTotalResult = false;
                    mWaveBar.setVisibility(View.VISIBLE);
                    mService.startScan();
                    mHandler.sendEmptyMessageDelayed(FILE_SIZE_INCREASE_ANIMATION, 1000);
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mConnetion != null) {
            if (mService != null) {
                mService.stopScan();
            }
            unbindService(mConnetion);
        }
    }

    private class MediaAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private List<BigFileInfo> mFileInfoList;
        private Context mContext;

        MediaAdapter(@NonNull Context context, @NonNull List<BigFileInfo> bigFileInfos) {
            mContext = context;
            mInflater = LayoutInflater.from(context);
            mFileInfoList = bigFileInfos;
        }

        @Override
        public int getCount() {
            if (mFileInfoList.size() > 4) {
                return 4;
            }
            return mFileInfoList.size();
        }

        @Override
        public Object getItem(int position) {
            return mFileInfoList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final MediaViewHolder viewHolder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.media_file_item, parent, false);
                viewHolder = new MediaViewHolder();
                viewHolder.imgBack = convertView.findViewById(R.id.img_back);
                viewHolder.tvName = convertView.findViewById(R.id.tv_name);
                viewHolder.imgMore = convertView.findViewById(R.id.img_more);
                viewHolder.tvMore = convertView.findViewById(R.id.tv_more);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (MediaViewHolder) convertView.getTag();
            }
            BigFileInfo info = mFileInfoList.get(position);
            if (position == 3 && mFileInfoList.size() > 4) {
                viewHolder.imgMore.setVisibility(View.VISIBLE);
                viewHolder.tvMore.setVisibility(View.VISIBLE);
                viewHolder.imgMore.setImageResource(R.mipmap.media_more);
                String more = "+" + (mFileInfoList.size() - 3);
                viewHolder.tvMore.setText(more);
            } else {
                viewHolder.imgMore.setVisibility(View.INVISIBLE);
                viewHolder.tvMore.setVisibility(View.INVISIBLE);
                if (info.getStyle() == BigFileInfo.STYLE_IMAGE) {
                    Bitmap decodeBitmap = ImageUtil.decodeBitmapFromResource(info.getPath(),
                            SystemUtils.dp2px(mContext, 70),
                            SystemUtils.dp2px(mContext, 70));
                    viewHolder.imgBack.setImageBitmap(decodeBitmap);
                    viewHolder.tvName.setText(info.getName());
                } else {
                    new ThumbTask((MediaViewHolder) convertView.getTag(), mContext).execute(info);
                }
            }
            return convertView;
        }

        private long getTotalSize() {
            if (mFileInfoList == null) return 0;
            long totalSize = 0;
            for (BigFileInfo info : mFileInfoList) {
                totalSize += info.getSize();
            }
            return totalSize;
        }
    }

    private class MediaViewHolder {
        ImageView imgBack;
        TextView tvName;
        ImageView imgMore;
        TextView tvMore;
    }

    private static class MediaImgInfo {
        String mFileName;
        String mImgPath;

        MediaImgInfo(String fileName, String imgPath) {
            mFileName = fileName;
            mImgPath = imgPath;
        }

        private String getFileName() {
            return mFileName;
        }

        private String getImgPath() {
            return mImgPath;
        }
    }

    private static class ThumbTask extends AsyncTask<BigFileInfo, MediaImgInfo, Void> {
        private WeakReference<MediaViewHolder> mHolderReference;
        private WeakReference<Context> mContextReference;

        ThumbTask(MediaViewHolder viewHolder, Context context) {
            mHolderReference = new WeakReference<>(viewHolder);
            mContextReference = new WeakReference<>(context);
        }

        @Override
        protected Void doInBackground(BigFileInfo... bigFileInfos) {
            Context context = mContextReference.get();
            if (context != null) {
                for (BigFileInfo info : bigFileInfos) {
                    String thumbPath = ImageUtil.getThumbPath(info.getPath(), context);
                    MediaImgInfo imgInfo = new MediaImgInfo(info.getName(), thumbPath);
                    publishProgress(imgInfo);
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(MediaImgInfo... values) {
            MediaViewHolder viewHolder = mHolderReference.get();
            if (viewHolder != null) {
                MediaImgInfo imgInfo = values[0];
                String imgPath = imgInfo.getImgPath();
                if (imgPath != null) {
                    FileInputStream fis;
                    try {
                        fis = new FileInputStream(imgPath);
                        Bitmap bitmap = BitmapFactory.decodeStream(fis);
                        viewHolder.imgBack.setImageBitmap(bitmap);
                    } catch (FileNotFoundException e) {
                        viewHolder.imgBack.setImageResource(R.mipmap.media_back);
                    }
                } else {
                    viewHolder.imgBack.setImageResource(R.mipmap.media_back);
                }
                viewHolder.tvName.setText(imgInfo.getFileName());
            }
        }
    }

    private class AudioAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private List<BigFileInfo> mFileInfoList;

        AudioAdapter(@NonNull Context context, @NonNull List<BigFileInfo> bigFileInfos) {
            mInflater = LayoutInflater.from(context);
            mFileInfoList = bigFileInfos;
        }

        @Override
        public int getCount() {
            if (mFileInfoList.size() > 4) {
                return 4;
            }
            return mFileInfoList.size();
        }

        @Override
        public Object getItem(int position) {
            return mFileInfoList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.big_file_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.imgBack = convertView.findViewById(R.id.img_back);
                viewHolder.tvType = convertView.findViewById(R.id.tv_type);
                viewHolder.tvName = convertView.findViewById(R.id.tv_name);
                viewHolder.imgMore = convertView.findViewById(R.id.img_more);
                viewHolder.tvMore = convertView.findViewById(R.id.tv_more);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            int size = mFileInfoList.size();
            if (position == 3 && size > 4) {
                viewHolder.imgMore.setVisibility(View.VISIBLE);
                viewHolder.tvMore.setVisibility(View.VISIBLE);
                viewHolder.imgMore.setImageResource(R.mipmap.audio_more);
                viewHolder.tvName.setText(getString(R.string.more));
                StringBuilder builder = new StringBuilder("+");
                builder.append(String.valueOf(size - 3));
                viewHolder.tvMore.setText(builder);
            } else {
                viewHolder.imgMore.setVisibility(View.INVISIBLE);
                viewHolder.tvMore.setVisibility(View.INVISIBLE);
                viewHolder.imgBack.setImageResource(R.mipmap.audio_back);
                BigFileInfo info = mFileInfoList.get(position);
                viewHolder.tvType.setText(info.getSuffix());
                viewHolder.tvName.setText(info.getName());
            }
            return convertView;
        }

        private long getTotalSize() {
            if (mFileInfoList == null) return 0;
            long totalSize = 0;
            for (BigFileInfo info : mFileInfoList) {
                totalSize += info.getSize();
            }
            return totalSize;
        }
    }

    private class DocumentAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private List<BigFileInfo> mFileInfoList;

        DocumentAdapter(@NonNull Context context, @NonNull List<BigFileInfo> bigFileInfos) {
            mInflater = LayoutInflater.from(context);
            mFileInfoList = bigFileInfos;
        }

        @Override
        public int getCount() {
            if (mFileInfoList.size() > 4) {
                return 4;
            }
            return mFileInfoList.size();
        }

        @Override
        public Object getItem(int position) {
            return mFileInfoList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.big_file_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.imgBack = convertView.findViewById(R.id.img_back);
                viewHolder.tvType = convertView.findViewById(R.id.tv_type);
                viewHolder.tvName = convertView.findViewById(R.id.tv_name);
                viewHolder.imgMore = convertView.findViewById(R.id.img_more);
                viewHolder.tvMore = convertView.findViewById(R.id.tv_more);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            int size = mFileInfoList.size();
            if (position == 3 && size > 4) {
                viewHolder.imgMore.setVisibility(View.VISIBLE);
                viewHolder.tvMore.setVisibility(View.VISIBLE);
                viewHolder.imgMore.setImageResource(R.mipmap.document_more);
                viewHolder.tvName.setText(getString(R.string.more));
                StringBuilder builder = new StringBuilder("+");
                builder.append(String.valueOf(size - 3));
                viewHolder.tvMore.setText(builder);
            } else {
                viewHolder.imgMore.setVisibility(View.INVISIBLE);
                viewHolder.tvMore.setVisibility(View.INVISIBLE);
                viewHolder.imgBack.setImageResource(R.mipmap.document_back);
                BigFileInfo info = mFileInfoList.get(position);
                viewHolder.tvType.setText(info.getSuffix());
                viewHolder.tvName.setText(info.getName());
            }
            return convertView;
        }

        private long getTotalSize() {
            if (mFileInfoList == null) return 0;
            long totalSize = 0;
            for (BigFileInfo info : mFileInfoList) {
                totalSize += info.getSize();
            }
            return totalSize;
        }
    }

    private class ViewHolder {
        ImageView imgBack;
        TextView tvType;
        TextView tvName;
        ImageView imgMore;
        TextView tvMore;
    }

    private static class BigFileHandler extends Handler {
        private WeakReference<BigFilesActivity> mReference;

        BigFileHandler(BigFilesActivity activity) {
            mReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            BigFilesActivity activity = mReference.get();
            if (activity == null) return;
            switch (msg.what) {
                case FILE_SIZE_INCREASE_ANIMATION:
                    int currentSize = (int) (activity.mFtvBig.getFileSize() / 1000);
                    int targetSize = (int) (activity.mTotalSize / 1000);
                    if (activity.mBigSizeTextAnimator != null) {
                        activity.mBigSizeTextAnimator.cancel();
                    }
                    activity.mBigSizeTextAnimator = ObjectAnimator.ofInt(activity.mFtvBig, FileSizeText.FILE_SIZE, currentSize, targetSize);
                    activity.mBigSizeTextAnimator.setDuration(1000);
                    activity.mBigSizeTextAnimator.start();
                    if (!activity.mGotTotalResult) {
                        sendEmptyMessageDelayed(FILE_SIZE_INCREASE_ANIMATION, 1000);
                    } else {
                        sendEmptyMessage(GOT_SCAN_RESULY);
                    }
                    break;
                case GOT_SCAN_RESULY:
                    long bigSize = activity.mTotalSize;
                    long totalSize = activity.mTotalStorage;
                    long freeSize = SystemUtils.getExternalStorageAvailableSize();
                    long otherSize = totalSize - bigSize - freeSize;
                    float bigPercent = bigSize * 1.0f / totalSize;
                    float otherPercent = otherSize * 1.0f / totalSize;
                    ObjectAnimator bigBarAnim = ObjectAnimator.ofFloat(activity.mProportionalBar, ProportionalBar.BIG_PERCENT, 0, bigPercent);
                    bigBarAnim.setDuration(1000);
                    ObjectAnimator otherBarAnim = ObjectAnimator.ofFloat(activity.mProportionalBar, ProportionalBar.OTHER_PERCENT, 0, otherPercent);
                    otherBarAnim.setDuration(1000);
                    ObjectAnimator otherAnim = ObjectAnimator.ofInt(activity.mFtvOthers, FileSizeText.FILE_SIZE, 0, (int) (otherSize / 1000));
                    otherAnim.setDuration(1000);
                    ObjectAnimator freeAnim = ObjectAnimator.ofInt(activity.mFtvFree, FileSizeText.FILE_SIZE, 0, (int) (freeSize / 1000));
                    freeAnim.setDuration(1000);
                    activity.mWaveBar.setVisibility(View.INVISIBLE);
                    bigBarAnim.start();
                    otherBarAnim.start();
                    otherAnim.start();
                    freeAnim.start();
                    break;
            }
        }
    }

    private void updateBigPercent() {
        if (mMediaAdapter != null && mAudioAdapter != null && mDocumentAdapter != null && mRarelyList != null) {
            long rarelySize = 0;
            for (BigFileInfo info : mRarelyList) {
                rarelySize += info.getSize();
            }
            long totalSize = mMediaAdapter.getTotalSize() + mAudioAdapter.getTotalSize() + mDocumentAdapter.getTotalSize() + rarelySize;
            float bigPercent = totalSize * 1.0f / mTotalStorage;
            mProportionalBar.setBigPercent(bigPercent);
            if (mBigSizeTextAnimator != null && mBigSizeTextAnimator.isRunning()) {
                mBigSizeTextAnimator.cancel();
            }
            int currentSize = (int) (mFtvBig.getFileSize() / 1000);
            int targetSize = (int) (totalSize / 1000);
            ObjectAnimator animator = ObjectAnimator.ofInt(mFtvBig, FileSizeText.FILE_SIZE, currentSize, targetSize);
            animator.setDuration(1000);
            animator.start();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case 1:
                if (mMediaAdapter != null) {
                    mMediaAdapter.notifyDataSetChanged();
                    if (mMediaAdapter.getCount() == 0) {
                        mLlEmptyMedia.setVisibility(View.VISIBLE);
                        mLlResultMedia.setVisibility(View.INVISIBLE);
                        mGvMedia.setVisibility(View.INVISIBLE);
                        mLlMedia.setClickable(false);
                    } else {
                        int currentSize = (int) (mTvMedia.getFileSize() / 1000);
                        int targetSize = (int) (mMediaAdapter.getTotalSize() / 1000);
                        ObjectAnimator animator = ObjectAnimator.ofInt(mTvMedia, FileSizeText.FILE_SIZE, currentSize, targetSize);
                        animator.setDuration(1000);
                        animator.start();
                    }
                }
                updateBigPercent();
                break;
            case 2:
                if (mAudioAdapter != null) {
                    mAudioAdapter.notifyDataSetChanged();
                    if (mAudioAdapter.getCount() == 0) {
                        mLlEmptyAudio.setVisibility(View.VISIBLE);
                        mLlResultAudio.setVisibility(View.INVISIBLE);
                        mGvAudio.setVisibility(View.INVISIBLE);
                        mLlAudio.setClickable(false);
                    } else {
                        int currentSize = (int) (mTvAudio.getFileSize() / 1000);
                        int targetSize = (int) (mAudioAdapter.getTotalSize() / 1000);
                        ObjectAnimator animator = ObjectAnimator.ofInt(mTvAudio, FileSizeText.FILE_SIZE, currentSize, targetSize);
                        animator.setDuration(1000);
                        animator.start();
                    }
                }
                updateBigPercent();
                break;
            case 3:
                if (mDocumentAdapter != null) {
                    mDocumentAdapter.notifyDataSetChanged();
                    if (mDocumentAdapter.getCount() == 0) {
                        mLlEmptyDocument.setVisibility(View.VISIBLE);
                        mLlResultDocument.setVisibility(View.INVISIBLE);
                        mGvDocument.setVisibility(View.INVISIBLE);
                        mLlDocument.setClickable(false);
                    } else {
                        int currentSize = (int) (mTvDocument.getFileSize() / 1000);
                        int targetSize = (int) (mDocumentAdapter.getTotalSize() / 1000);
                        ObjectAnimator animator = ObjectAnimator.ofInt(mTvDocument, FileSizeText.FILE_SIZE, currentSize, targetSize);
                        animator.setDuration(1000);
                        animator.start();
                    }
                }
                updateBigPercent();
                break;
            case 4:
                if (mRarelyList != null) {
                    if (mRarelyList.size() == 0) {
                        mLlEmptyrarely.setVisibility(View.VISIBLE);
                        mLlResultRarely.setVisibility(View.INVISIBLE);
                        mLlRarelyContent.setVisibility(View.INVISIBLE);
                        mLlDocument.setClickable(false);
                    } else {
                        mTvRarelyNum.setText(String.valueOf(mRarelyList.size()));
                        int currentSize = (int) (mTvRarely.getFileSize() / 1000);
                        long totalSize = 0;
                        for (BigFileInfo info : mRarelyList) {
                            totalSize += info.getSize();
                        }
                        int targetSize = (int) (totalSize / 1000);
                        ObjectAnimator animator = ObjectAnimator.ofInt(mTvRarely, FileSizeText.FILE_SIZE, currentSize, targetSize);
                        animator.setDuration(1000);
                        animator.start();
                    }
                }
                updateBigPercent();
                break;
        }
    }
}
