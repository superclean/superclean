package com.youtupu.superclean.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.youtupu.superclean.R;
import com.youtupu.superclean.fragment.SettingFragment;
import com.youtupu.superclean.view.CustomTitleBar;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        CustomTitleBar customTitleBar = findViewById(R.id.custom_title_bar);
        customTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                finish();
            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new SettingFragment()).commit();
    }

}
