package com.youtupu.superclean.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.youtupu.superclean.app.SuperCleanApplication;
import com.youtupu.superclean.bean.ApkInfo;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.StatusBarUtils;
import com.youtupu.superclean.utils.SystemUtils;

import java.util.ArrayList;

import com.youtupu.superclean.R;

public class CleanDialogActivity extends AppCompatActivity {

    private String mAppName;
    private String mPackageName;
    private ArrayList<ApkInfo> mApkList;
    private int mCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clean_dialog);
        final Intent intent = getIntent();
        mAppName = intent.getStringExtra("app_name");
        mPackageName = intent.getStringExtra("package_name");
        mCategory = intent.getIntExtra("category", 0);
        if (TextUtils.isEmpty(mAppName) || mCategory == 0) {
            finish();
            return;
        }
        StatusBarUtils.setStatusBarFullTransparent(this);
        TextView tvTitle = findViewById(R.id.dialog_title);
        if (mCategory == 1) {
            //应用安装
            mApkList = intent.getParcelableArrayListExtra("apk_list");
            if (mApkList != null) {
                long size = 0;
                for (ApkInfo info : mApkList) {
                    size += info.getFileSize();
                }
                String title = getString(R.string.install_title);
                tvTitle.setText(Html.fromHtml(String.format(title, mAppName, SystemUtils.getFileSize(size))));
            } else {
                finish();
                return;
            }
        } else if (mCategory == 2) {
            //保证在对话框acitivy显示时，再有应用卸载，不会再弹出该activity
            SuperCleanApplication application = (SuperCleanApplication) getApplication();
            application.setJunkCleaning(true);
            //应用卸载
            tvTitle.setText(Html.fromHtml(String.format(getString(R.string.uninstall_title), mAppName)));
        }
        TextView tvLeft = findViewById(R.id.dialog_left_choice);
        tvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCategory == 1) {
                    AnalyticsUtil.logEvent(CleanDialogActivity.this, "install_clean_cancel", null);
                }
                if (mCategory == 2) {
                    AnalyticsUtil.logEvent(CleanDialogActivity.this, "uninstall_clean_cancel", null);
                    SuperCleanApplication application = (SuperCleanApplication) getApplication();
                    application.setJunkCleaning(false);
                }
                finish();
            }
        });
        TextView tvRight = findViewById(R.id.dialog_right_choice);
        tvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCategory == 1 && mApkList != null && mApkList.size() > 0) {
                    AnalyticsUtil.logEvent(CleanDialogActivity.this, "install_clean_enter", null);
                    Intent apkIntent = new Intent(CleanDialogActivity.this, ApkDeleteActivity.class);
                    apkIntent.putExtra("app_name", mAppName);
                    apkIntent.putExtra("package_name", mPackageName);
                    apkIntent.putParcelableArrayListExtra("apk_list", mApkList);
                    startActivity(apkIntent);
                } else if (mCategory == 2) {
                    AnalyticsUtil.logEvent(CleanDialogActivity.this, "uninstall_clean_enter", null);
                    Intent residualIntent = new Intent(CleanDialogActivity.this, JunkDetailActivity.class);
                    residualIntent.putExtra("fromOutSide", true);
                    startActivity(residualIntent);
                }
                finish();
            }
        });
    }
}
