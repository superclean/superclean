package com.youtupu.superclean.activity;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;

import com.youtupu.superclean.R;
import com.youtupu.superclean.dialog.PowerPermissionDialog;
import com.youtupu.superclean.utils.CustomToast;
import com.youtupu.superclean.utils.StatusBarUtils;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * 一个透明的Activity，主要用于适配8.0手机申请悬浮窗权限
 * created by yihao 2019/4/24
 */
public class PermissionDialogActivity extends AppCompatActivity {

    private PowerPermissionDialog dialog;
    private Handler handler;
    private int taskSize = 0;
    private boolean isStop;
    private boolean isToFloat = false;
    private boolean isToAccess = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //状态栏透明
        StatusBarUtils.setStatusBarFullTransparent(this);
        handler = new Handler();
        taskSize = getIntent().getIntExtra("taskSize", 0);

        dialog = new PowerPermissionDialog(this);
        dialog.setOwnerActivity(this);
        dialog.setTaskSize(taskSize);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
        // 直接跳转至悬浮窗设置页面
        Intent intent = getIntent();
        String action = intent.getStringExtra("action");
        if ("grant_float_permission".equals(action)) {
            grantFloatPermission();
        } else if ("grant_access_permission".equals(action)) {
            grantAccessPermission();
        }
    }

    private void grantFloatPermission() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            //防止ActivityNotFoundException
            try {
                Intent newIntent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                newIntent.setData(Uri.parse("package:" + getPackageName()));
                isToFloat = true;
                isToAccess = false;
                startActivity(newIntent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        isStop = false;
        // 如果是从设置返回，则采用不断刷新的方式判断权限是否授予
        if (isToFloat) {
            refreshFloatPermission();
        }
        // 如果是从设置返回，但没有授权辅助服务，则显示对话框
        else if (isToAccess && !SystemUtils.isAccessibilitySettingsOn(PermissionDialogActivity.this)) {
            dialog.show();
            dialog.setPermissionGranted(SystemUtils.isAccessibilitySettingsOn(PermissionDialogActivity.this), true);
            dialog.updateStatus();
        }
    }

    private void refreshFloatPermission() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.show();
                    }
                }, 500);

                while (!isStop) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(getApplication())) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (!SystemUtils.isAccessibilitySettingsOn(PermissionDialogActivity.this)) {
                                    grantAccessPermission();
                                }
                                dialog.setPermissionGranted(SystemUtils.isAccessibilitySettingsOn(PermissionDialogActivity.this), true);
                                dialog.updateStatus();
                            }
                        });
                        break;
                    }
                    SystemClock.sleep(100);
                }
            }
        });
        thread.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        isStop = true;
    }

    public void grantAccessPermission() {
        String title = String.format(getString(R.string.please_authorize), getString(R.string.app_name));
        final CustomToast toast = new CustomToast(this, title);
        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        startActivity(intent);
        toast.show();
        isToAccess = true;
        isToFloat = false;
    }
}
