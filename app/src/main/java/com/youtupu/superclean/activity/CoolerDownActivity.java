package com.youtupu.superclean.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

import com.airbnb.lottie.LottieAnimationView;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.view.NumberTextView;
import qiu.niorgai.StatusBarCompat;

public class CoolerDownActivity extends AppCompatActivity {

    private int mReducedTemperature;
    private LottieAnimationView mLottieAnimationView;
    private AnimatorSet mAnimatorSet;
    private boolean mFirstCreate = true;
    private boolean mFromOutSide = false;
    private int mCpuTemperature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirstCreate = true;
        setContentView(R.layout.activity_cooler_down);
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.colorMainDeepBlue));
        NumberTextView numberTextView = findViewById(R.id.tv_number);
        RelativeLayout contentView = findViewById(R.id.rl_content);
        mLottieAnimationView = findViewById(R.id.lottie_view);
        Intent intent = getIntent();
        mCpuTemperature = 35;
        mReducedTemperature = 5;
        if (intent != null) {
            mCpuTemperature = intent.getIntExtra("CpuTemp", 35);
            mReducedTemperature = intent.getIntExtra("reducedTemp", 5);
            int fromWhere = intent.getIntExtra("fromWhere", 0);
            if (fromWhere > 0) {
                mFromOutSide = fromWhere < 10;
                if (fromWhere == 11) {
                    //从首页的退出弹框而来
                    AnalyticsUtil.logEvent(this, "exit_dialog_cooler", null);
                }
            }
        }
        numberTextView.setNumber(mCpuTemperature);
        ObjectAnimator reduceAnimator = ObjectAnimator.ofInt(numberTextView, NumberTextView.NUMBER, mCpuTemperature, mCpuTemperature - mReducedTemperature);
        reduceAnimator.setDuration(3000);
        ObjectAnimator dismissAnimator = ObjectAnimator.ofFloat(contentView, "alpha", 1f, 0f);
        dismissAnimator.setDuration(1000);
        mAnimatorSet = new AnimatorSet();
        mAnimatorSet.play(reduceAnimator).before(dismissAnimator);
        mAnimatorSet.addListener(new Animator.AnimatorListener() {
            boolean isCanceled = false;

            @Override
            public void onAnimationStart(Animator animation) {
                isCanceled = false;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isCanceled) {
                    toOptimizeActivity();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                isCanceled = true;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mFirstCreate) {
            mFirstCreate = false;
            mLottieAnimationView.playAnimation();
            mAnimatorSet.start();

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAnimatorSet.isPaused()) {
            mAnimatorSet.resume();
            mLottieAnimationView.playAnimation();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAnimatorSet.isRunning()) {
            mAnimatorSet.pause();
            mLottieAnimationView.cancelAnimation();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mAnimatorSet.isRunning() || mAnimatorSet.isPaused()) mAnimatorSet.cancel();
    }

    private void toOptimizeActivity() {
//        Intent intent = new Intent(this, OptimizeActivity.class);
//        intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_COOLER);
//        intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, getString(R.string.cpu_title));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, getString(R.string.will_cool_down));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT_COLOR, getResources().getColor(R.color.colorMainBlue));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, String.format(getString(R.string.temp_num_symbol), mReducedTemperature));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT_COLOR, getResources().getColor(R.color.colorDeepPink));
        Intent intent = new OptimizeActivity.IntentBuilder()
                .setClass(this)
                .setFrom(OptimizeActivity.FROM_COOLER)
                .setActionBar(getString(R.string.cpu_title))
                .setLeftText(getString(R.string.will_cool_down), 0, getResources().getColor(R.color.colorMainBlue))
                .setRightText(String.format(getString(R.string.temp_num_symbol), mReducedTemperature), 0, getResources().getColor(R.color.colorDeepPink))
                .build();
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        //在降温动画界面退出，肯定是Abandon
        SharePreferencesUtil.getInstance(this).setLastAbandonOptimize(2);
        SharePreferencesUtil.getInstance(this).setLastTemperature(mCpuTemperature);
        //如果是从外界打开我这个activity，那我中断退出时，把首页拉起
        if (mFromOutSide) {
            startActivity(new Intent(this, HomeActivity.class));
        }
        super.onBackPressed();
    }
}
