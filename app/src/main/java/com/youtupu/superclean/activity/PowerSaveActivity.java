package com.youtupu.superclean.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.youtupu.superclean.BuildConfig;
import com.youtupu.superclean.app.SuperCleanApplication;
import com.youtupu.superclean.base.BaseActivity;
import com.youtupu.superclean.base.BaseFragment;
import com.youtupu.superclean.bean.AppItem;
import com.youtupu.superclean.fragment.PowerDetailFragment;
import com.youtupu.superclean.fragment.PowerScanFragment;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.utils.SharePreferencesUtil;

import java.util.ArrayList;
import java.util.List;

import com.youtupu.superclean.R;

public class PowerSaveActivity extends BaseActivity {
    public static final int SCAN_FRAGMENT_INDEX = 0;
    public static final int DETAIL_FRAGMENT_INDEX = 1;
    private List<AppItem> mCanStopAppList = new ArrayList<>();
    private List<AppItem> mNoStopAppList = new ArrayList<>();
    // 杀进程过程中是否点返回退出了
    private boolean mInterrupt = false;
    private boolean mFromOutSide = false;
    private SuperCleanApplication mApplication;

    @Override
    protected void onResume() {
        super.onResume();
        CommonUtils.debug("PowerSaveActivity onResume");
    }

    @Override
    protected boolean beforeSetContentView() {
        //正式版的5分钟内不再省电
        if (!BuildConfig.DEBUG) {
            long lastTime = SharePreferencesUtil.getInstance(this).getLastPowerTime();
            long nowTime = System.currentTimeMillis();
            if (nowTime - lastTime < 1000 * 60 * 5) {
//                Intent intent = new Intent(this, OptimizeActivity.class);
//                intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_POWER);
//                intent.putExtra(OptimizeActivity.NEED_RECORD, false);
//                intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, getString(R.string.battery_title));
//                intent.putExtra(OptimizeActivity.OPTIMIZE_SHOW_AIMATION, false);
//                intent.putExtra(OptimizeActivity.OPTIMIZE_TITLE, getString(R.string.normal));
//                intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, "");
//                intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, getString(R.string.already_optimized));
                Intent intent = new OptimizeActivity.IntentBuilder()
                        .setClass(this)
                        .setFrom(OptimizeActivity.FROM_POWER)
                        .setNeedRecord(false)
                        .setActionBar(getString(R.string.battery_title))
                        .setAnimationShow(false)
                        .setTitle(getString(R.string.normal))
                        .setRightText(getString(R.string.already_optimized),0,0)
                        .build();
                startActivity(intent);
                finish();
                return false;
            }
        }
        return true;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_power_save;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (BaseFragment fragment : mFragmentsList) {
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            int fromWhere = intent.getIntExtra("fromWhere", 0);
            //是否从外界而来
            if (fromWhere > 0) {
                mFromOutSide = true;
                //从控制台而来
                if (fromWhere == 1) {
                    AnalyticsUtil.logEvent(this, "console_power", null);
                }
            }
        }
        mApplication = (SuperCleanApplication) getApplication();
        mApplication.setPowerSaving(true);
        mFragmentsList = new ArrayList<>();
        PowerScanFragment powerScanFragment = new PowerScanFragment();
        PowerDetailFragment powerDetailFragment = new PowerDetailFragment();
        mFragmentsList.add(powerScanFragment);
        mFragmentsList.add(powerDetailFragment);
        switchFragment(SCAN_FRAGMENT_INDEX, R.id.power_frame_layout);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("start_power_activity"));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mInterrupt) {
            mInterrupt = false;
            switchFragment(DETAIL_FRAGMENT_INDEX, R.id.power_frame_layout);
        }
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CommonUtils.debug("start PowerSaveActivity");
            mInterrupt = true;
            Intent newIntent = new Intent(PowerSaveActivity.this, PowerSaveActivity.class);
            startActivity(newIntent);
        }

    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mApplication != null) {
            mApplication.setPowerSaving(false);
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    public void setCanStopApps(List<AppItem> canStopAppList) {
        if (canStopAppList == null) {
            return;
        }
        mCanStopAppList.clear();
        mCanStopAppList.addAll(canStopAppList);
    }

    public void setNoStopApps(List<AppItem> noStopAppList) {
        if (noStopAppList == null) {
            return;
        }
        mNoStopAppList.clear();
        mNoStopAppList.addAll(noStopAppList);
    }

    public List<AppItem> getCanStopAppList() {
        return mCanStopAppList;
    }

    public List<AppItem> getNoStopAppList() {
        return mNoStopAppList;
    }

    @Override
    public void onBackPressed() {
        if (mCanStopAppList != null && mCanStopAppList.size() > 0) {
            SharePreferencesUtil.getInstance(this).setLastAbandonOptimize(3);
            SharePreferencesUtil.getInstance(this).setLastCanStopNumber(mCanStopAppList.size());
        }
        //如果是从外界打开我这个activity，那我中断退出时，把首页拉起
        if (mFromOutSide) {
            startActivity(new Intent(this, HomeActivity.class));
        }
        super.onBackPressed();
    }

}
