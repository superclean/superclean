package com.youtupu.superclean.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import cn.instreet.business.advertise.InterstitialAdvertiseManager;
import com.youtupu.superclean.R;
import com.youtupu.superclean.base.BaseActivity;
import com.youtupu.superclean.dialog.DoubleChoicesDialog;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.Constants;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.AlignCenterTextView;
import com.youtupu.superclean.view.CustomTitleBar;
import com.youtupu.superclean.view.GearCenterView;

public class BoostActivity extends BaseActivity {
    private static final int START_ANIMATION = 0;
    private static final int TO_OPTIMIZE_ACTIVITY = 1;
    private static final int ICON_FLY_DURATION = 400;
    private float mBoostMemory;
    private DoubleChoicesDialog mExitDialog;
    private GearCenterView mGearCenterView;
    private AlignCenterTextView mCenterTextView;
    private boolean mIsFirstCreate = false;
    private AnimatorSet mAnimatorSet;
    private List<Drawable> mIconsList;
    private ImageView mImgIcon;
    private ObjectAnimator mIconFlyAmimator;
    private TextView mTvProcess;
    private BoostHandler mHandler;
    private CustomTitleBar mCustomTitleBar;
    private boolean mFromOutSide = false;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_boost;
    }

    @Override
    protected void initView() {
        mIsFirstCreate = true;
        mCustomTitleBar = findViewById(R.id.custom_title_bar);
        mGearCenterView = findViewById(R.id.gear_center_view);
        mCenterTextView = findViewById(R.id.tv_center);
        mImgIcon = findViewById(R.id.img_app_icon);
        mTvProcess = findViewById(R.id.tv_boost_process);
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        float boostPercent;
        if (intent != null) {
            boostPercent = intent.getFloatExtra("percent", 0.8f);
            int fromWhere = intent.getIntExtra("fromWhere", 0);
            if (fromWhere > 0) {
                mFromOutSide = fromWhere < 10;
                if (fromWhere == 1) {
                    AnalyticsUtil.logEvent(this, "console_boost", null);
                } else if (fromWhere == 11) {
                    //从首页的退出弹框而来
                    AnalyticsUtil.logEvent(this, "exit_dialog_boost", null);
                }
            }
        } else {
            boostPercent = 0.8f;
        }
        float totalMemory = SystemUtils.getTatalMemory();
        mIconsList = new ArrayList<>();
        getRandomApps();
        if (mIconsList.size() > 4) {
            initIconsFlyAnim();
        }
        mBoostMemory = totalMemory * 0.1f * boostPercent;
        mCenterTextView.setInitSize((int) mBoostMemory);
        mCenterTextView.setPercent(boostPercent);
        mGearCenterView.setPercent(boostPercent);
        mHandler = new BoostHandler(this);
        //旋转动画
        ObjectAnimator gearRunAnimator;
        //渐变动画
        ObjectAnimator gearGradulAnimator = ObjectAnimator.ofFloat(mGearCenterView, GearCenterView.PERCENT, boostPercent, 0f);
        //size减小、颜色渐变动画，提前一点结束
        ObjectAnimator sizeDownAnimator = ObjectAnimator.ofInt(mCenterTextView, AlignCenterTextView.SIZE, (int) mBoostMemory, 0);
        ObjectAnimator sizeGradulAnimator = ObjectAnimator.ofFloat(mCenterTextView, AlignCenterTextView.PERCENT, boostPercent, 0f);
        if (mIconsList.size() > 4) {
            gearRunAnimator = ObjectAnimator.ofFloat(mGearCenterView, GearCenterView.ANGLE, 0, -90 * mIconsList.size());
            gearRunAnimator.setDuration(ICON_FLY_DURATION * mIconsList.size());
            gearGradulAnimator.setDuration(mIconsList.size() * ICON_FLY_DURATION);
            sizeDownAnimator.setDuration((mIconsList.size() - 1) * ICON_FLY_DURATION);
            sizeGradulAnimator.setDuration((mIconsList.size() - 1) * ICON_FLY_DURATION);
        } else {
            gearRunAnimator = ObjectAnimator.ofFloat(mGearCenterView, GearCenterView.ANGLE, 0, -360);
            gearRunAnimator.setDuration(2000);
            gearGradulAnimator.setDuration(2000);
            sizeDownAnimator.setDuration(1600);
            sizeGradulAnimator.setDuration(1600);
        }
        //旋转动画加速再减速
        gearRunAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        //数字下降动画加速
        sizeDownAnimator.setInterpolator(new AccelerateInterpolator(3));
        mAnimatorSet = new AnimatorSet();
        mAnimatorSet.playTogether(gearGradulAnimator, gearRunAnimator, sizeGradulAnimator, sizeDownAnimator);
    }

    @Override
    protected void initListener() {
        mAnimatorSet.addListener(new Animator.AnimatorListener() {
            boolean isCanceled = false;

            @Override
            public void onAnimationStart(Animator animation) {
                isCanceled = false;
                if (mIconFlyAmimator != null) {
                    mIconFlyAmimator.start();
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isCanceled) {
                    mHandler.sendEmptyMessageDelayed(TO_OPTIMIZE_ACTIVITY, 500);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                isCanceled = true;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mCustomTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mIsFirstCreate) {
            mIsFirstCreate = false;
            mHandler.sendEmptyMessageDelayed(START_ANIMATION, 500);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (mAnimatorSet.isPaused()) {
            mAnimatorSet.resume();
        }
        if (mIconFlyAmimator != null && mIconFlyAmimator.isPaused()) {
            mIconFlyAmimator.resume();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAnimatorSet.isRunning()) {
            mAnimatorSet.pause();
        }
        if (mIconFlyAmimator != null && mIconFlyAmimator.isRunning()) {
            mIconFlyAmimator.pause();
        }
    }


    private void onBoostDone() {
        InterstitialAdvertiseManager.getInstance().showAd(Constants.BOOST_INTERSTITIAL_AD, new InterstitialAdvertiseManager.OnAdClosed() {
            @Override
            public void execute() {
//                Intent intent = new Intent(BoostActivity.this, OptimizeActivity.class);
//                intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_BOOST);
//                intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, getString(R.string.boost_title));
//                intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, String.format(Locale.getDefault(), "%.0f MB", mBoostMemory));
//                intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, getString(R.string.memory_free));
                Intent intent = new OptimizeActivity.IntentBuilder()
                        .setClass(BoostActivity.this)
                        .setFrom(OptimizeActivity.FROM_BOOST)
                        .setActionBar(getString(R.string.boost_title))
                        .setLeftText(String.format(Locale.getDefault(), "%.0f MB", mBoostMemory),0,0)
                        .setRightText(getString(R.string.memory_free),0,0).build();
                startActivity(intent);
                Intent resultIntent = new Intent(BoostActivity.this, HomeActivity.class);
                resultIntent.putExtra("boosted", true);
                setResult(0, resultIntent);
                finish();
            }
        });

    }

    private void initIconsFlyAnim() {
        int distance = SystemUtils.dp2px(this, 200);
        mIconFlyAmimator = ObjectAnimator.ofFloat(mImgIcon, "translationY", 0f, -distance);
        mIconFlyAmimator.setDuration(ICON_FLY_DURATION);
        mIconFlyAmimator.setRepeatCount(mIconsList.size() - 1);
        mIconFlyAmimator.addListener(new Animator.AnimatorListener() {
            int index = 0;
            boolean isCanceled = false;

            @Override
            public void onAnimationStart(Animator animation) {
                index = 0;
                isCanceled = false;
                mImgIcon.setImageDrawable(mIconsList.get(0));
                mTvProcess.setText(String.format(getString(R.string.release_app_memory), 1, mIconsList.size()));
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isCanceled) {
                    mTvProcess.setText(String.format(getString(R.string.release_app_memory), mIconsList.size(), mIconsList.size()));
                    if (mAnimatorSet.isRunning()) {
                        mAnimatorSet.end();
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                isCanceled = true;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                index++;
                if (index < mIconsList.size()) {
                    mImgIcon.setImageDrawable(mIconsList.get(index));
                    mTvProcess.setText(String.format(getString(R.string.release_app_memory), index + 1, mIconsList.size()));
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mAnimatorSet.isRunning()) {
            mAnimatorSet.pause();
            if (mIconFlyAmimator != null && mIconFlyAmimator.isRunning()) {
                mIconFlyAmimator.pause();
            }
            showDialog();
        } else {
            mHandler.removeMessages(START_ANIMATION);
            mHandler.removeMessages(TO_OPTIMIZE_ACTIVITY);
            //如果是从外界打开我这个activity，那我中断退出时，把首页拉起
            if (mFromOutSide) {
                startActivity(new Intent(BoostActivity.this, HomeActivity.class));
            }
            super.onBackPressed();
        }
    }


    private void showDialog() {
        if (mExitDialog == null) {
            mExitDialog = new DoubleChoicesDialog(this, getString(R.string.interrupted_when_boosting),
                    getString(R.string.abandon), getString(R.string.go_on), R.mipmap.rocket,new DoubleChoicesDialog.OnButtonClickListener() {
                @Override
                public void leftButtonClick(DoubleChoicesDialog dlg) {
                    //如果是从外界打开我这个activity，那我中断退出时，把首页拉起
                    if (mFromOutSide) {
                        startActivity(new Intent(BoostActivity.this, HomeActivity.class));
                    }
                    finish();
                }

                @Override
                public void rightButtonClick(DoubleChoicesDialog dlg) {
                    mExitDialog.dismiss();
                    if (mAnimatorSet.isPaused()) {
                        mAnimatorSet.resume();
                    }
                    if (mIconFlyAmimator != null && mIconFlyAmimator.isPaused()) {
                        mIconFlyAmimator.resume();
                    }
                }
            });
        }
        mExitDialog.show();
    }

    @Override
    protected void onDestroy() {
        if (mIconFlyAmimator != null && (mIconFlyAmimator.isRunning() || mIconFlyAmimator.isPaused())) {
            mIconFlyAmimator.cancel();
        }
        if (mAnimatorSet.isRunning() || mAnimatorSet.isPaused()) {
            mAnimatorSet.cancel();
        }
        super.onDestroy();
    }

    private void getRandomApps() {
        PackageManager packageManager = getPackageManager();
        if (packageManager != null) {
            List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);
            if (installedApplications != null && installedApplications.size() > 10) {
                Random random = new Random();
                //随机取5-8个
                int size = random.nextInt(4) + 5;
                int installSize = installedApplications.size();
                int index = random.nextInt(installSize - size - 1);
                for (int i = 0; i < size; i++) {
                    ApplicationInfo info = installedApplications.get(index + i);
                    if (info != null) {
                        mIconsList.add(info.loadIcon(packageManager));
                    }
                }
            }
        }
    }

    private static class BoostHandler extends Handler {
        private WeakReference<BoostActivity> mReference;

        BoostHandler(BoostActivity activity) {
            mReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            BoostActivity activity = mReference.get();
            if (activity != null) {
                switch (msg.what) {
                    case START_ANIMATION:
                        activity.mAnimatorSet.start();
                        break;
                    case TO_OPTIMIZE_ACTIVITY:
                        activity.onBoostDone();
                        break;
                }
            }
        }
    }
}
