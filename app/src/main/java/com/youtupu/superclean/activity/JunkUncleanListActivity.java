package com.youtupu.superclean.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.youtupu.superclean.bean.JunkListItem;
import com.youtupu.superclean.dialog.DetailDialog;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.FoldableListView;

import java.util.ArrayList;
import java.util.List;

import com.youtupu.superclean.R;

/**
 * created by yihao 2019/3/27
 * 无法清除的文件列表
 */
public class JunkUncleanListActivity extends Activity {

    private ArrayList<JunkListItem.JunkListMetadata> listMetadata;
    private List<JunkListItem> listItems;
    private DetailDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_junk_unclean_list);

        if (null != getIntent()) {
            try {
                listMetadata = (ArrayList<JunkListItem.JunkListMetadata>) getIntent().
                        getSerializableExtra("data");
                if (null == listMetadata || listMetadata.isEmpty()) {
                    finish();
                }
            } catch (ClassCastException exp) {
                exp.printStackTrace();
            }
        }

        dialog = new DetailDialog(JunkUncleanListActivity.this);

        ListView listView = findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JunkListItem item = listItems.get(position);
                showFileDetailDialog(item);
            }
        });

        // 转换文件列表数据
        PackageManager pm = getPackageManager();
        listItems = JunkListItem.JunkListMetadata.toJunkListItem(listMetadata, pm);
        FoldableListView.JunkListViewAdapter adapter = new FoldableListView.JunkListViewAdapter(listItems,
                this, null);
        listView.setAdapter(adapter);

        Button button = findViewById(R.id.btn_OK);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JunkUncleanListActivity.this, OptimizeActivity.class);
                startActivity(intent);
            }
        });
    }

    private void showFileDetailDialog(JunkListItem item){
        if(null == item){
            return;
        }
        dialog.setPath(item.getFile().getPath());
        dialog.setSize(SystemUtils.getFileSize(item.getSize()));
        String applicationName = item.getApplicationName();
        if (applicationName != null) {
            dialog.setTitle(applicationName);
        } else {
            dialog.setTitle(getString(R.string.residual_file));
        }
        dialog.show();
    }
}
