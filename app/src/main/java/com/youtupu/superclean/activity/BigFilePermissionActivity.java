package com.youtupu.superclean.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.youtupu.superclean.R;
import com.youtupu.superclean.dialog.SingleChoiceDialog;
import pub.devrel.easypermissions.EasyPermissions;
import qiu.niorgai.StatusBarCompat;

public class BigFilePermissionActivity extends AppCompatActivity {
    private boolean mToSettings = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_big_file_permission);
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.clean_button_click));
        Button button = findViewById(R.id.btn_access);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(BigFilePermissionActivity.this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 1);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(BigFilePermissionActivity.this, BigFilesActivity.class);
                startActivity(intent);
                finish();
            } else {
                showPermissionRequestDialog();
            }
        }
    }

    private void showPermissionRequestDialog() {
        SingleChoiceDialog dialog = new SingleChoiceDialog(this);
        dialog.setTitle(getString(R.string.need_storage_permission));
        dialog.setButtonText(getString(R.string.to_setting_authorization));
        dialog.setImgResource(R.mipmap.big_file_deny);
        dialog.setOnButtonClickListener(new SingleChoiceDialog.OnButtonClickListener() {
            @Override
            public void onClick() {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.setData(Uri.fromParts("package", getPackageName(), null));
                mToSettings = true;
                startActivity(intent);
            }
        });
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mToSettings) {
            //从设置界面回来
            mToSettings = false;
            if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Intent intent = new Intent(BigFilePermissionActivity.this, BigFilesActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }
}
