package com.youtupu.superclean.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.youtupu.superclean.R;
import com.youtupu.superclean.view.CustomTitleBar;
import com.youtupu.superclean.view.SlowlyProgressBar;
import qiu.niorgai.StatusBarCompat;

/**
 * created by yihao 2019/3/26
 * 隐私服务和服务条款页面
 */

public class PrivacyActivity extends AppCompatActivity {

    public static void startPrivacy(Activity activity, boolean isFromSplash) {
        if (null == activity) {
            return;
        }
        Intent intent = new Intent(activity, PrivacyActivity.class);
        intent.putExtra("url", "https://instreet-privacy.weebly.com/privacy.html");
        intent.putExtra("isFromSplash", isFromSplash);
        intent.putExtra("type", "privacy");
        activity.startActivity(intent);
    }

    public static void startTermsOfService(Activity activity, boolean isFromSplash) {
        if (null == activity) {
            return;
        }
        Intent intent = new Intent(activity, PrivacyActivity.class);
        intent.putExtra("url", "https://instreet-privacy.weebly.com/termsofuse.html");
        intent.putExtra("isFromSplash", isFromSplash);
        intent.putExtra("type", "termsOfUse");
        activity.startActivity(intent);
    }

    private boolean isFromSplash = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);
        StatusBarCompat.setStatusBarColor(this, 0xFF0EABFE);
        if (null == getIntent()) {
            return;
        }
        if (TextUtils.isEmpty(getIntent().getStringExtra("url"))) {
            return;
        }

        WebView webView = findViewById(R.id.webView);
        final SlowlyProgressBar slowlyProgressBar =
                new SlowlyProgressBar((ProgressBar) findViewById(R.id.ProgressBar));
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                slowlyProgressBar.onProgressStart();
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                slowlyProgressBar.onProgressChange(newProgress);
            }
        });
        webView.loadUrl(getIntent().getStringExtra("url"));

        isFromSplash = getIntent().getBooleanExtra("isFromSplash", false);

        CustomTitleBar customTitleBar = findViewById(R.id.custom_title_bar);
        customTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                goBack();
            }
        });
        String type = getIntent().getStringExtra("type");
        if ("privacy".equals(type)) {
            customTitleBar.setTitle(getString(R.string.privacy_policy_name));
        } else if ("termsOfUse".equals(type)) {
            customTitleBar.setTitle(getString(R.string.terms_of_use));
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void goBack() {
        if (isFromSplash) {
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
        }
        finish();
    }
}
