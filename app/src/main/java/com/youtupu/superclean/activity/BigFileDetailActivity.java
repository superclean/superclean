package com.youtupu.superclean.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cn.instreet.business.advertise.BannerAdvertiseManager;
import com.youtupu.superclean.BuildConfig;
import com.youtupu.superclean.R;
import com.youtupu.superclean.app.SuperCleanApplication;
import com.youtupu.superclean.bean.BigFileInfo;
import com.youtupu.superclean.dialog.BigFileDetailDialog;
import com.youtupu.superclean.dialog.DoubleChoicesDialog;
import com.youtupu.superclean.utils.Constants;
import com.youtupu.superclean.utils.ImageUtil;
import com.youtupu.superclean.utils.OpenFileUtil;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.ThreeStatusCheckBox;
import qiu.niorgai.StatusBarCompat;

public class BigFileDetailActivity extends AppCompatActivity {
    public static final int STYLE_MEDIA = 1;
    public static final int STYLE_AUDIO = 2;
    public static final int STYLE_DOCUMENT = 3;
    public static final int STYLE_RARELY = 4;
    private HashMap<String, Bitmap> mImageMap = new HashMap<>();
    private boolean mLvStop = true;
    private BigFilesAdpater mAdpater;
    private List<BigFileInfo> mBigFileInfoList;
    private DoubleChoicesDialog mDialog;
    private boolean mDeleted = false;
    private int mFileStyle;
    private Spinner mSpinner;
    private ListView mLvBigFiles;
    private Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.clean_button_click));
        setContentView(R.layout.activity_big_file_detail);
        ImageView imgBack = findViewById(R.id.img_left);
        TextView tvTitle = findViewById(R.id.tv_title);
        mLvBigFiles = findViewById(R.id.lv_big_files);
        mButton = findViewById(R.id.button_delete);
        mSpinner = findViewById(R.id.spinner);
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        mFileStyle = intent.getIntExtra("file_style", 0);
        switch (mFileStyle) {
            case STYLE_MEDIA:
                tvTitle.setText(getString(R.string.video_image));
                break;
            case STYLE_AUDIO:
                tvTitle.setText(getString(R.string.audio));
                break;
            case STYLE_DOCUMENT:
                tvTitle.setText(getString(R.string.document));
                break;
            case STYLE_RARELY:
                tvTitle.setText(getString(R.string.rarely_used_files));
                break;
        }
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        initListView();
        initSpinner();
    }


    private void initListView() {
        SuperCleanApplication application = (SuperCleanApplication) getApplication();
        mBigFileInfoList = application.getBigFileInfoList();
        if (mBigFileInfoList != null) {
            mAdpater = new BigFilesAdpater(this, mBigFileInfoList);
            mLvBigFiles.setAdapter(mAdpater);
        }
        mLvBigFiles.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        Log.e("jiali", "BigFileDetailActivity -> SCROLL_STATE_IDLE: ");
                        mLvStop = true;
                        if (mAdpater != null) {
                            mAdpater.notifyDataSetChanged();
                        }
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        Log.e("jiali", "BigFileDetailActivity -> SCROLL_STATE_TOUCH_SCROLL: ");
                        mLvStop = false;
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        Log.e("jiali", "BigFileDetailActivity -> SCROLL_STATE_FLING: ");
                        mLvStop = false;
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        mLvBigFiles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) return;
                final BigFileInfo info = (BigFileInfo) mAdpater.getItem(position);
                BigFileDetailDialog detailDialog = new BigFileDetailDialog(BigFileDetailActivity.this);
                detailDialog.setName(info.getName());
                detailDialog.setSize(SystemUtils.getFileSize(info.getSize()));
                detailDialog.setPath(info.getPath());
                long time = info.getTime();
                String sTime = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date(time));
                detailDialog.setDate(sTime);
                switch (info.getStyle()) {
                    case BigFileInfo.STYLE_VIDEO:
                        detailDialog.setConfirmText(getString(R.string.play));
                        if (mImageMap.containsKey(info.getPath())) {
                            Bitmap bitmap = mImageMap.get(info.getPath());
                            detailDialog.setBitmap(bitmap);
                        } else {
                            detailDialog.setImgResoureId(R.mipmap.media_detail);
                        }
                        break;
                    case BigFileInfo.STYLE_IMAGE:
                        detailDialog.setConfirmText(getString(R.string.view));
                        if (mImageMap.containsKey(info.getPath())) {
                            Bitmap bitmap = mImageMap.get(info.getPath());
                            detailDialog.setBitmap(bitmap);
                        } else {
                            detailDialog.setImgResoureId(R.mipmap.image_detail);
                        }
                        break;
                    case BigFileInfo.STYLE_AUDIO:
                        detailDialog.setConfirmText(getString(R.string.play));
                        detailDialog.setImgResoureId(R.mipmap.audio_detail);
                        break;
                    case BigFileInfo.STYLE_DOCUMENT:
                        detailDialog.setConfirmText(getString(R.string.view));
                        detailDialog.setImgResoureId(R.mipmap.document_detail);
                        break;
                    case BigFileInfo.STYLE_RARELY:
                        detailDialog.setConfirmText(getString(R.string.view));
                        detailDialog.setImgResoureId(R.mipmap.rarely_detail);
                        break;
                }
                detailDialog.setOnConfirmButtonClickListener(new BigFileDetailDialog.OnConfirmButtonClickListener() {
                    @Override
                    public void onConfirmButtonClick() {
                        try {
                            File file = info.getFile();
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            Uri uri;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                uri = FileProvider.getUriForFile(BigFileDetailActivity.this,
                                        BuildConfig.APPLICATION_ID + ".fileProvider", file);
                            } else {
                                uri = Uri.fromFile(file);
                            }
                            String type = OpenFileUtil.getType(info.getSuffix());
                            intent.setDataAndType(uri, type);
                            startActivity(Intent.createChooser(intent, "share"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                detailDialog.show();
            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBigFileInfoList != null) {
                    int deleteNum = 0;
                    for (BigFileInfo info : mBigFileInfoList) {
                        if (info.isChecked()) {
                            deleteNum++;
                        }
                    }
                    if (deleteNum > 0) {
                        mDialog = new DoubleChoicesDialog(
                                BigFileDetailActivity.this, "", getString(R.string.cancel),
                                getString(R.string.delete), R.mipmap.has_junk,
                                DoubleChoicesDialog.STYLE_GREEN, new DoubleChoicesDialog.OnButtonClickListener() {
                            @Override
                            public void leftButtonClick(DoubleChoicesDialog dlg) {
                                mDialog.dismiss();
                            }

                            @Override
                            public void rightButtonClick(DoubleChoicesDialog dlg) {
                                mDialog.dismiss();
                                List<BigFileInfo> deleteList = new ArrayList<>();
                                for (BigFileInfo info : mBigFileInfoList) {
                                    if (info.isChecked()) {
                                        File file = info.getFile();
                                        if (file != null && file.exists()) {
                                            file.delete();
                                            deleteList.add(info);
                                        }
                                    }
                                }
                                mDeleted = true;
                                for (BigFileInfo deleteInfo : deleteList) {
                                    mBigFileInfoList.remove(deleteInfo);
                                }
                                if (mAdpater != null) {
                                    mAdpater.notifyDataSetChanged();
                                }
                            }
                        });
                        Spanned spanned = Html.fromHtml(String.format(getString(R.string.delete_hint), deleteNum));
                        mDialog.setTitleSpanned(spanned);
                        mDialog.show();
                    }
                }
            }
        });
    }

    private void initSpinner() {
        updateSpinnerBack();
        final ArrayList<SortItem> list = new ArrayList<>();
        SortItem item0 = new SortItem(R.mipmap.sort_size, getString(R.string.sort_size));
        SortItem item1 = new SortItem(R.mipmap.sort_date, getString(R.string.sort_date));
        SortItem item2 = new SortItem(0, getString(R.string.sort_name));
        list.add(item0);
        list.add(item1);
        list.add(item2);
        ArrayAdapter spinnerAdapter = new ArrayAdapter<SortItem>(this, R.layout.spinner_checked_text, list) {
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = View.inflate(getContext(), R.layout.spinner_item, null);
                TextView label = view.findViewById(R.id.tv_name);
                label.setText(list.get(position).text);
                ImageView img = view.findViewById(R.id.img_icon);
                if (list.get(position).imgId != 0) {
                    img.setImageResource(list.get(position).imgId);
                }
                return view;
            }
        };
        spinnerAdapter.setDropDownViewResource(R.layout.spinner_item);
        mSpinner.setAdapter(spinnerAdapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        if (mBigFileInfoList != null && mAdpater != null) {
                            mSpinner.setClickable(false);
                            BigFileInfo.mSortByWhat = BigFileInfo.SORT_BY_SIZE;
                            Collections.sort(mBigFileInfoList);
                            mAdpater.notifyDataSetChanged();
                            updateSpinnerBack();
                            mSpinner.setClickable(true);
                        }
                        break;
                    case 1:
                        if (mBigFileInfoList != null && mAdpater != null) {
                            mSpinner.setClickable(false);
                            BigFileInfo.mSortByWhat = BigFileInfo.SORT_BY_TIME;
                            Collections.sort(mBigFileInfoList);
                            mAdpater.notifyDataSetChanged();
                            updateSpinnerBack();
                            mSpinner.setClickable(true);
                        }
                        break;
                    case 2:
                        if (mBigFileInfoList != null && mAdpater != null) {
                            mSpinner.setClickable(false);
                            BigFileInfo.mSortByWhat = BigFileInfo.SORT_BY_NAME;
                            Collections.sort(mBigFileInfoList);
                            mAdpater.notifyDataSetChanged();
                            updateSpinnerBack();
                            mSpinner.setClickable(true);
                        }
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void updateSpinnerBack() {
        if (BigFileInfo.mSortByWhat == BigFileInfo.SORT_BY_SIZE) {
            mSpinner.setBackgroundResource(R.mipmap.sort_size_back);
        } else if (BigFileInfo.mSortByWhat == BigFileInfo.SORT_BY_TIME) {
            mSpinner.setBackgroundResource(R.mipmap.sort_time_back);
        } else if (BigFileInfo.mSortByWhat == BigFileInfo.SORT_BY_NAME) {
            mSpinner.setBackgroundResource(R.mipmap.sort_name_back);
        }
    }

    private class SortItem {
        int imgId;
        String text;

        SortItem(int imgId, String text) {
            this.imgId = imgId;
            this.text = text;
        }
    }


    private class BigFilesAdpater extends BaseAdapter {
        private LayoutInflater mInflater;
        private List<BigFileInfo> mFileInfoList;
        private Context mContext;

        BigFilesAdpater(@NonNull Context context, @NonNull List<BigFileInfo> list) {
            mInflater = LayoutInflater.from(context);
            mFileInfoList = list;
            mContext = context;
        }

        @Override
        public int getCount() {
//            int size = mFileInfoList.size();
//            int adNumber = size / 20 + 1;
//            return size + adNumber;
            return mFileInfoList.size() + 1;
        }

        @Override
        public Object getItem(int position) {
//            if (position % 20 != 0) {
//                int adIndex = position / 20 + 1;
//                return mFileInfoList.get(position - adIndex);
//            } else {
//                return null;
//            }
            if (position == 0) {
                return null;
            } else {
                return mFileInfoList.get(position - 1);
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.big_file_detail_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.imgIcon = convertView.findViewById(R.id.img_icon);
                viewHolder.tvName = convertView.findViewById(R.id.tv_name);
                viewHolder.tvSize = convertView.findViewById(R.id.tv_size);
                viewHolder.tvTime = convertView.findViewById(R.id.tv_time);
                viewHolder.checkBox = convertView.findViewById(R.id.checkbox);
                viewHolder.content = convertView.findViewById(R.id.ll_content);
                viewHolder.adParent = convertView.findViewById(R.id.rl_ad);
                viewHolder.adView = convertView.findViewById(R.id.ad_view);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            if (position == 0) {
                viewHolder.content.setVisibility(View.INVISIBLE);
                viewHolder.adParent.setVisibility(View.VISIBLE);
                if (mLvStop) {
                    if (BuildConfig.DEBUG) {
                        viewHolder.adView.addView(BannerAdvertiseManager.getInstance().showBanner(Constants.TEST_BANNER_AD));
                    } else {
                        viewHolder.adView.addView(BannerAdvertiseManager.getInstance().showBanner(Constants.BIG_FILE_BANNER_AD));
                    }
                }
            } else {
                viewHolder.content.setVisibility(View.VISIBLE);
                viewHolder.adParent.setVisibility(View.INVISIBLE);
                final BigFileInfo info = mFileInfoList.get(position - 1);
                viewHolder.tvName.setText(info.getName());
                viewHolder.tvSize.setText(SystemUtils.getFileSize(info.getSize()));
                String sTime = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date(info.getTime()));
                viewHolder.tvTime.setText(sTime);
                viewHolder.tag = position - 1;
                viewHolder.checkBox.setChecked(info.isChecked());
                viewHolder.checkBox.setOnCheckBoxSelectedListener(new ThreeStatusCheckBox.OnCheckBoxSelectedListener() {
                    @Override
                    public void onChecked() {
                        int tag = viewHolder.tag;
                        if (tag > -1 && tag < mFileInfoList.size()) {
                            mFileInfoList.get(tag).setChecked(true);
                        }
                    }

                    @Override
                    public void onUnChecked() {
                        int tag = viewHolder.tag;
                        if (tag > -1 && tag < mFileInfoList.size()) {
                            mFileInfoList.get(tag).setChecked(false);
                        }
                    }
                });
                switch (info.getStyle()) {
                    case BigFileInfo.STYLE_VIDEO:
                        //先从内存里找
                        if (mImageMap.containsKey(info.getPath())) {
                            Bitmap bitmap = mImageMap.get(info.getPath());
                            viewHolder.imgIcon.setImageBitmap(bitmap);
                        } else {
                            //没有的话，显示默认图片，在停止滚动时，再加载图片
                            viewHolder.imgIcon.setImageResource(R.mipmap.media_detail);
                            if (mLvStop) {
                                new ThumbTask(viewHolder.imgIcon, mContext, mImageMap).execute(info);
                            }
                        }
                        break;
                    case BigFileInfo.STYLE_IMAGE:
                        if (mImageMap.containsKey(info.getPath())) {
                            Bitmap bitmap = mImageMap.get(info.getPath());
                            viewHolder.imgIcon.setImageBitmap(bitmap);
                        } else {
                            viewHolder.imgIcon.setImageResource(R.mipmap.image_detail);
                            if (mLvStop) {
                                new CompressTask(viewHolder.imgIcon, mContext, mImageMap).execute(info);
                            }
                        }
                        break;
                    case BigFileInfo.STYLE_AUDIO:
                        viewHolder.imgIcon.setImageResource(R.mipmap.audio_detail);
                        break;
                    case BigFileInfo.STYLE_DOCUMENT:
                        viewHolder.imgIcon.setImageResource(R.mipmap.document_detail);
                        break;
                    case BigFileInfo.STYLE_RARELY:
                        viewHolder.imgIcon.setImageResource(R.mipmap.rarely_detail);
                        break;
                }
            }
            return convertView;
        }

    }

    private class ViewHolder {
        ImageView imgIcon;
        TextView tvName;
        TextView tvSize;
        TextView tvTime;
        ThreeStatusCheckBox checkBox;
        LinearLayout content;
        RelativeLayout adParent;
        RelativeLayout adView;
        int tag;
    }

    private static class ThumbTask extends AsyncTask<BigFileInfo, Void, Bitmap> {
        private WeakReference<ImageView> mImageViewWeakReference;
        private WeakReference<Context> mContextReference;
        private WeakReference<HashMap<String, Bitmap>> mHashMapWeakReference;

        ThumbTask(ImageView imageView, Context context, HashMap<String, Bitmap> imageMap) {
            mImageViewWeakReference = new WeakReference<>(imageView);
            mContextReference = new WeakReference<>(context);
            mHashMapWeakReference = new WeakReference<>(imageMap);
        }

        @Override
        protected Bitmap doInBackground(BigFileInfo... bigFileInfos) {
            Context context = mContextReference.get();
            if (context != null) {
                BigFileInfo info = bigFileInfos[0];
                String thumbPath = ImageUtil.getThumbPath(info.getPath(), context);
                if (!TextUtils.isEmpty(thumbPath)) {
                    Bitmap bitmap = null;
                    try {
                        FileInputStream fis = new FileInputStream(thumbPath);
                        bitmap = BitmapFactory.decodeStream(fis);
                        HashMap<String, Bitmap> hashMap = mHashMapWeakReference.get();
                        if (hashMap != null) {
                            hashMap.put(info.getPath(), bitmap);
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    return bitmap;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            ImageView imageView = mImageViewWeakReference.get();
            if (imageView != null && bitmap != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    private static class CompressTask extends AsyncTask<BigFileInfo, Void, Bitmap> {
        private WeakReference<ImageView> mImageViewWeakReference;
        private WeakReference<Context> mContextWeakReference;
        private WeakReference<HashMap<String, Bitmap>> mHashMapWeakReference;

        CompressTask(ImageView imageView, Context context, HashMap<String, Bitmap> imageMap) {
            mImageViewWeakReference = new WeakReference<>(imageView);
            mContextWeakReference = new WeakReference<>(context);
            mHashMapWeakReference = new WeakReference<>(imageMap);
        }

        @Override
        protected Bitmap doInBackground(BigFileInfo... bigFileInfos) {
            Context context = mContextWeakReference.get();
            Bitmap image = null;
            if (context != null) {
                BigFileInfo info = bigFileInfos[0];
                image = ImageUtil.decodeBitmapFromResource(info.getPath(),
                        SystemUtils.dp2px(context, 50),
                        SystemUtils.dp2px(context, 50));
                HashMap<String, Bitmap> hashMap = mHashMapWeakReference.get();
                if (hashMap != null) {
                    hashMap.put(info.getPath(), image);
                }
            }
            return image;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            ImageView imageView = mImageViewWeakReference.get();
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (mDeleted) {
            switch (mFileStyle) {
                case STYLE_MEDIA:
                    setResult(1);
                    break;
                case STYLE_AUDIO:
                    setResult(2);
                    break;
                case STYLE_DOCUMENT:
                    setResult(3);
                    break;
                case STYLE_RARELY:
                    setResult(4);
                    break;
            }
        }
        super.onDestroy();
    }
}
