package com.youtupu.superclean.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2018/11/26.
 */

public class SplashActivity extends FragmentActivity {
    private TextView linkPrivacy;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        View privacy = findViewById(R.id.layout_privacy);


        boolean opened = getSharedPreferences("user_action", MODE_PRIVATE).getBoolean("opened", false);
        privacy.setVisibility(opened ? View.GONE : View.VISIBLE);

        if (opened) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                }
            }, 500);
        }

        final Button btnStart = findViewById(R.id.btn_start);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSharedPreferences("user_action", MODE_PRIVATE).edit().putBoolean("opened", true)
                        .apply();

                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        linkPrivacy = findViewById(R.id.link_privacy);
        String language = getResources().getConfiguration().locale.getLanguage();
        if ("zh".equals(language)) {
            setPrivacyString(getString(R.string.privacy_splash), 2, 6, 7, 11);
        } else if ("en".equals(language)) {
            setPrivacyString(getString(R.string.privacy_splash), 13, 27, 32, 48);
        }
    }

    private void setPrivacyString(String content, int privacyStart, int privacyEnd, int termOfServiceStart, int termOfServiceEnd) {
        SpannableString spannableString = new SpannableString(content);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                PrivacyActivity.startPrivacy(SplashActivity.this, true);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.title_color));//设置颜色
                ds.setUnderlineText(true);//去掉下划线
            }
        }, privacyStart, privacyEnd, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
//        spannableString.setSpan(new UnderlineSpan(), termOfServiceStart, termOfServiceEnd, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                PrivacyActivity.startTermsOfService(SplashActivity.this, true);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.title_color));//设置颜色
                ds.setUnderlineText(true);//去掉下划线
            }
        }, termOfServiceStart, termOfServiceEnd, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        linkPrivacy.setText(spannableString);
        linkPrivacy.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
