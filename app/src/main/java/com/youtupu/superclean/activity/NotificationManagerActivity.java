package com.youtupu.superclean.activity;

import android.content.Intent;
import android.provider.Settings;
import android.support.v4.app.NotificationManagerCompat;

import java.util.ArrayList;

import com.youtupu.superclean.R;
import com.youtupu.superclean.base.BaseActivity;
import com.youtupu.superclean.fragment.NotificationGuideFragment;
import com.youtupu.superclean.fragment.NotificationHomeFragment;
import com.youtupu.superclean.fragment.NotificationSettingFragment;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.SharePreferencesUtil;

public class NotificationManagerActivity extends BaseActivity {
    public static final String FROM_WHERE = "from_where";
    public static final String NOTIFICATION_SERVICE = "notification_service";
    public static final int HOME_FRAGMENT_INDEX = 0;
    public static final int SETTING_FRAGMENT_INDEX = 1;
    public static final int GUIDE_FRAGMENT_INDEX = 2;
    public boolean mFromNotificationClick = false;
    //标记用户是否点击授权对话框里的“取消”按键
    public boolean mIsCanceled = false;
    private NotificationSettingFragment mSettingFragment;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_notification_clean;
    }

    @Override
    protected void initData() {
        mFragmentsList = new ArrayList<>();
        NotificationGuideFragment guideFragment;
        NotificationHomeFragment homeFragment = new NotificationHomeFragment();
        mSettingFragment = new NotificationSettingFragment();
        guideFragment = new NotificationGuideFragment();
        mFragmentsList.add(homeFragment);
        mFragmentsList.add(mSettingFragment);
        mFragmentsList.add(guideFragment);
        //如果从通知点击而来，则直接进入详情界面
        Intent intent = getIntent();
        if (intent != null && intent.getStringExtra(FROM_WHERE) != null && intent.getStringExtra(FROM_WHERE).equalsIgnoreCase(NOTIFICATION_SERVICE)) {
            AnalyticsUtil.logEvent(this, "from_notification_box", null);
            switchFragment(HOME_FRAGMENT_INDEX, R.id.notification_framelayout);
            mFromNotificationClick = true;
        } else {
            if (!notificationListenerEnadble() || !isNotificationEnabled()) {
                switchFragment(GUIDE_FRAGMENT_INDEX, R.id.notification_framelayout);
            } else {
                setFunctionUsed();
                switchFragment(HOME_FRAGMENT_INDEX, R.id.notification_framelayout);
            }
        }
    }

    private void setFunctionUsed() {
        AnalyticsUtil.logEvent(this, "noti_mgr_used", null);
        SharePreferencesUtil util = SharePreferencesUtil.getInstance(this);
        if (!util.getNotificationUsed()) {
            util.setNotificationUsed(true);
        }
    }

    public boolean notificationListenerEnadble() {
        boolean enable = false;
        String packageName = getPackageName();
        String flat = Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners");
        if (flat != null) {
            enable = flat.contains(packageName);
        }
        return enable;
    }


    @Override
    public void onBackPressed() {
        if (mCurrentFragment != null && mCurrentFragment == mSettingFragment) {
            mSettingFragment.onBackPressed();
        } else {
            if (mFromNotificationClick) {
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
            }
            super.onBackPressed();
        }
    }

    public boolean isNotificationEnabled() {
        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        return manager.areNotificationsEnabled();
    }

}
