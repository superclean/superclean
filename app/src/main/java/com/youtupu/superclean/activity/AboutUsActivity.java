package com.youtupu.superclean.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.youtupu.superclean.R;
import com.youtupu.superclean.dialog.ProgressDialog;
import com.youtupu.superclean.dialog.ResultDialog;
import com.youtupu.superclean.dialog.UpdateDialog;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.utils.NetWorkUtils;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.view.CustomTitleBar;
import qiu.niorgai.StatusBarCompat;

public class AboutUsActivity extends AppCompatActivity implements View.OnClickListener {
    private boolean mIsCanceled = false;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        StatusBarCompat.setStatusBarColor(this, 0xFF0EABFE);
        TextView tvPolicy = findViewById(R.id.tv_privacy);
        tvPolicy.setOnClickListener(this);
        TextView tvTerms = findViewById(R.id.tv_termsOfUse);
        tvTerms.setOnClickListener(this);
        CustomTitleBar customTitleBar = findViewById(R.id.custom_title_bar);
        customTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                finish();
            }
        });
        try {
            PackageManager packageManager = getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
            String version = getString(getApplicationInfo().labelRes) + " " + packageInfo.versionName;
            TextView tvVersion = findViewById(R.id.tv_version);
            tvVersion.setText(version);
        } catch (Exception e) {
            e.printStackTrace();
        }
        TextView tvCheckVersion = findViewById(R.id.tv_check_version);
        tvCheckVersion.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_privacy:
                PrivacyActivity.startPrivacy(AboutUsActivity.this, false);
                break;
            case R.id.tv_termsOfUse:
                PrivacyActivity.startTermsOfService(AboutUsActivity.this, false);
                break;
            case R.id.tv_check_version:
                AnalyticsUtil.logEvent(this, "check_version", null);
                if (NetWorkUtils.isNetworkConnected(this)) {
                    mProgressDialog = new ProgressDialog(this);
                    mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            mIsCanceled = true;
                        }
                    });
                    mProgressDialog.show();
                    checkNewVersion();
                } else {
                    showNoNetDialog();
                }
                break;
        }
    }

    private void checkNewVersion() {
        final String oldVersion = getVersionName();
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest("https://play.google.com/store/apps/details?id=com.youtupu.superclean", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Pattern p = Pattern.compile("(Current Version)(<.*?>)*(\\d\\.\\d\\.\\d)");
                    Matcher matcher = p.matcher(response);
                    if (matcher.find()) {
                        String currentVersion = matcher.group(3);
                        if (!currentVersion.equals(oldVersion)) {
                            CommonUtils.debug("detect new version");
                            showUpdateDialog(currentVersion);
                        }

                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showErrorDialog();
                CommonUtils.debug(error.getMessage());
            }
        });
        queue.add(request);
    }

    private String getVersionName() {
        PackageManager packageManager = getPackageManager();
        String versionName = "";
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

    private void showUpdateDialog(final String versionName) {
        if (mIsCanceled) return;
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference documentReference = db.collection("updates").document(versionName);
        documentReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (null != document && document.exists()) {
                        String language = getResources().getConfiguration().locale.getLanguage();
                        String contentName = "content_" + language;
                        String content = "";
                        if (document.contains(contentName)) {
                            content = document.get(contentName).toString().replace("\\n", "\n");
                        } else {
                            //若后台数据库出问题，此处会空指针
                            if (document.get("content") != null) {
                                content = document.get("content").toString().replace("\\n", "\n");
                            }
                        }
                        if (TextUtils.isEmpty(content)) return;
                        final UpdateDialog dialog = new UpdateDialog(AboutUsActivity.this, versionName, content);
                        dialog.setOnButtonClickListener(new UpdateDialog.OnClickListener() {
                            @Override
                            public void onCancel() {
                                dialog.dismiss();
                                SharePreferencesUtil.getInstance(AboutUsActivity.this).setLastCheckVersionTime(System.currentTimeMillis());

                            }

                            @Override
                            public void onOK() {
                                SharePreferencesUtil.getInstance(AboutUsActivity.this).setLastCheckVersionTime(System.currentTimeMillis());
                                dialog.dismiss();
                                String packageName = getPackageName();
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
                                startActivity(intent);
                            }
                        });
                        if (!mIsCanceled) {
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                            }
                            dialog.show();
                        }
                    }
                }
            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showErrorDialog();
                CommonUtils.debug(e.getMessage());
            }
        });
    }

    private void showErrorDialog() {
        if (!mIsCanceled) {
            if(mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            ResultDialog errorDialog = new ResultDialog(this);
            errorDialog.setTitle(getString(R.string.connect_error));
            errorDialog.setImgResoure(R.mipmap.net_error);
            errorDialog.show();
        }
    }

    private void showNoNetDialog() {
        ResultDialog noNetDialog = new ResultDialog(this);
        noNetDialog.setTitle(getString(R.string.no_networks));
        noNetDialog.setImgResoure(R.mipmap.no_net);
        noNetDialog.show();
    }


}
