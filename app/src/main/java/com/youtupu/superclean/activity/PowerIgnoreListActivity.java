package com.youtupu.superclean.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.CustomTitleBar;

public class PowerIgnoreListActivity extends AppCompatActivity {

    private List<AppItem> mAppItemList;
    private HashSet<String> mNoExistAppList;
    private PackageManager mPackageManager;
    private IgnoreListApapter mAdapter;
    private boolean mHasModify = true;
    private ListView mLvIgoreList;
    private TextView mTvCenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_power_ignore);
        mLvIgoreList = findViewById(R.id.lv_list);
        mTvCenter = findViewById(R.id.tv_content);
        Button button = findViewById(R.id.button_add);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveIgnoreList();
                startActivityForResult(new Intent(PowerIgnoreListActivity.this, PowerIgnoreSelectActivity.class), 123);
            }
        });
        CustomTitleBar customTitleBar = findViewById(R.id.custom_title_bar);
        customTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                finish();
            }
        });
        HashSet<String> ignoreList = SystemUtils.readPowerIgnoreListFile(this);
        mAppItemList = new ArrayList<>();
        mNoExistAppList = new HashSet<>();
        mPackageManager = getPackageManager();
        if (mPackageManager != null) {
            for (String packageName : ignoreList) {
                try {
                    ApplicationInfo applicationInfo = mPackageManager.getApplicationInfo(packageName, 0);
                    Drawable icon = mPackageManager.getApplicationIcon(packageName);
                    CharSequence label = mPackageManager.getApplicationLabel(applicationInfo);
                    if (icon != null && label != null) {
                        AppItem item = new AppItem(icon, label.toString(), packageName);
                        mAppItemList.add(item);
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    mNoExistAppList.add(packageName);
                    e.printStackTrace();
                }
            }
        }
        mAdapter = new IgnoreListApapter(mAppItemList, this);
        mLvIgoreList.setAdapter(mAdapter);
        if (mAppItemList.size() > 0) {
            mTvCenter.setVisibility(View.INVISIBLE);
            mLvIgoreList.setVisibility(View.VISIBLE);
        } else {
            mLvIgoreList.setVisibility(View.INVISIBLE);
            mTvCenter.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 456 && data != null) {
            ArrayList<String> addedApps = data.getStringArrayListExtra("added_apps");
            if (mPackageManager != null) {
                List<AppItem> addedList = new ArrayList<>();
                for (String packageName : addedApps) {
                    try {
                        ApplicationInfo applicationInfo = mPackageManager.getApplicationInfo(packageName, 0);
                        Drawable icon = mPackageManager.getApplicationIcon(packageName);
                        CharSequence label = mPackageManager.getApplicationLabel(applicationInfo);
                        if (icon != null && label != null) {
                            AppItem item = new AppItem(icon, label.toString(), packageName);
                            addedList.add(item);
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                if (addedList.size() > 0) {
                    mAppItemList.addAll(addedList);
                    afterDataChanged();
                    saveIgnoreList();
                }
            }
        }
    }

    private class IgnoreListApapter extends BaseAdapter {
        private List<AppItem> mAppItemList;
        private LayoutInflater mInflater;

        IgnoreListApapter(@NonNull List<AppItem> list, @NonNull Context context) {
            mAppItemList = list;
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mAppItemList.size();
        }

        @Override
        public Object getItem(int position) {
            return mAppItemList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.power_ignore_list_item, parent, false);
                holder = new ViewHolder();
                holder.imgIcon = convertView.findViewById(R.id.img_app_icon);
                holder.tvName = convertView.findViewById(R.id.tv_app_name);
                holder.imgRemove = convertView.findViewById(R.id.img_remove);
                holder.imgRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAppItemList.remove(holder.position);
                        afterDataChanged();
                    }
                });
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if (mAppItemList.get(position) != null) {
                AppItem info = mAppItemList.get(position);
                if (info != null) {
                    holder.imgIcon.setImageDrawable(info.getIcon());
                    holder.tvName.setText(info.getAppName());
                    holder.position = position;
                }
            }
            return convertView;
        }
    }

    private void afterDataChanged() {
        mAdapter.notifyDataSetChanged();
        if (mAppItemList.size() > 0) {
            mTvCenter.setVisibility(View.INVISIBLE);
            mLvIgoreList.setVisibility(View.VISIBLE);
        } else {
            mLvIgoreList.setVisibility(View.INVISIBLE);
            mTvCenter.setVisibility(View.VISIBLE);
        }
        mHasModify = true;
    }

    private class AppItem {
        private Drawable icon;
        private String appName;
        private String packageName;

        AppItem(Drawable icon, String appName, String packageName) {
            this.icon = icon;
            this.appName = appName;
            this.packageName = packageName;
        }

        public Drawable getIcon() {
            return icon;
        }

        private String getAppName() {
            return appName;
        }

        public String getPackageName() {
            return packageName;
        }
    }

    private class ViewHolder {
        ImageView imgIcon;
        TextView tvName;
        ImageView imgRemove;
        int position;
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveIgnoreList();
    }

    private void saveIgnoreList() {
        if (mHasModify) {
            JSONArray array = new JSONArray();
            for (AppItem item : mAppItemList) {
                array.put(item.getPackageName());
            }
            for (String packageName : mNoExistAppList) {
                array.put(packageName);
            }
            SystemUtils.writePowerIgnoreListFile(this, array.toString());
            mHasModify = false;
        }
    }
}
