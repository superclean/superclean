package com.youtupu.superclean.activity;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.youtupu.superclean.R;
import com.youtupu.superclean.adapter.NotificationSettingAdapter;
import com.youtupu.superclean.bean.CheckInfoApp;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.CustomTitleBar;

public class PowerIgnoreSelectActivity extends AppCompatActivity {

    private List<CheckInfoApp> mAppList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_power_ignore_select);
        CustomTitleBar customTitleBar = findViewById(R.id.custom_title_bar);
        customTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                finish();
            }
        });
        ListView lvApps = findViewById(R.id.lv_apps);
        Button button = findViewById(R.id.button_confirm);
        HashSet<String> ignoreList = SystemUtils.readPowerIgnoreListFile(this);
        mAppList = new ArrayList<>();
        PackageManager packageManager = getPackageManager();
        if (packageManager != null) {
            List<ApplicationInfo> applicationInfos = packageManager.getInstalledApplications(0);
            if (applicationInfos != null) {
                for (ApplicationInfo info : applicationInfos) {
                    //除去自己
                    if (!info.packageName.equalsIgnoreCase(getPackageName())) {
                        //不在白名单里，且不是系统应用
                        if (!ignoreList.contains(info.packageName) && (info.flags & ApplicationInfo.FLAG_SYSTEM) != ApplicationInfo.FLAG_SYSTEM) {
                            CheckInfoApp app = new CheckInfoApp(info, false);
                            mAppList.add(app);
                        }

                    }
                }
            }
        }
        final NotificationSettingAdapter adapter = new NotificationSettingAdapter(this, mAppList, packageManager);
        adapter.setShowRightText(false);
        lvApps.setAdapter(adapter);
        lvApps.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckInfoApp infoApp = (CheckInfoApp) adapter.getItem(position);
                infoApp.setChecked(!infoApp.isChecked());
                adapter.notifyDataSetChanged();
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAppList.size() > 0) {
                    ArrayList<String> addedList = new ArrayList<>();
                    for (CheckInfoApp app : mAppList) {
                        if (app.isChecked()) {
                            addedList.add(app.getInfo().packageName);
                        }
                    }
                    if (addedList.size() > 0) {
                        Intent intent = new Intent();
                        intent.putStringArrayListExtra("added_apps", addedList);
                        setResult(456, intent);
                    }
                }
                finish();
            }
        });
    }


}
