package com.youtupu.superclean.activity;

import android.animation.ObjectAnimator;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mTvDone;
    private ImageView mImgStamp;
    private InputMethodManager mInputMethodManager;
    private EditText mEditText;
    private ObjectAnimator mShowAnimator;
    private ObjectAnimator mDissmissAnimator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        RelativeLayout rlDialog = findViewById(R.id.rl_dialog);
        TextView tvLeft = findViewById(R.id.dialog_left_choice);
        TextView tvRight = findViewById(R.id.dialog_right_choice);
        mEditText = findViewById(R.id.et_feedback);
        mTvDone = findViewById(R.id.tv_done);
        mImgStamp = findViewById(R.id.img_stamp);
        int screenWidth = SystemUtils.getScreenWidth(this);
        rlDialog.setX(screenWidth);
        mShowAnimator = ObjectAnimator.ofFloat(rlDialog, "translationX", screenWidth, 0);
        mShowAnimator.setDuration(100);
        mDissmissAnimator = ObjectAnimator.ofFloat(rlDialog, "translationX", 0, screenWidth);
        mDissmissAnimator.setDuration(100);
        rlDialog.setOnClickListener(this);
        mEditText.setOnClickListener(this);
        mTvDone.setOnClickListener(this);
        tvLeft.setOnClickListener(this);
        tvRight.setOnClickListener(this);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    mTvDone.setVisibility(View.VISIBLE);
                    mImgStamp.setVisibility(View.INVISIBLE);
                } else {
                    mTvDone.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_feedback:
                mEditText.setCursorVisible(true);
                break;
            case R.id.tv_done:
                feedback();
                if (mInputMethodManager != null) {
                    mInputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
                }
                mTvDone.setVisibility(View.INVISIBLE);
                mImgStamp.setVisibility(View.VISIBLE);
                mEditText.setCursorVisible(false);
                mShowAnimator.start();
                break;
            case R.id.dialog_left_choice:
                mDissmissAnimator.start();
                break;
            case R.id.dialog_right_choice:
                finish();
                break;
        }
    }

    public void feedback() {
        try {
            String collection = "feedback";
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference docRefs = db.collection(collection).document(SystemUtils.getMacAddress() + ":" + System.currentTimeMillis());
            String content = mEditText.getText().toString();
            String model = Build.MODEL;
            int version = Build.VERSION.SDK_INT;
            String device = Build.DEVICE;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date curDate = new Date(System.currentTimeMillis());
            String time = formatter.format(curDate);
            HashMap<String, String> dataMap = new HashMap<>();
            dataMap.put("time", time);
            dataMap.put("model", model);
            dataMap.put("device", device);
            dataMap.put("version", String.valueOf(version));
            dataMap.put("content", content);
            dataMap.put("mac_address", SystemUtils.getMacAddress());
            dataMap.put("app_version", getVersionName());
            docRefs.set(dataMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getVersionName() {
        PackageManager packageManager = getPackageManager();
        String versionName = "";
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }
}
