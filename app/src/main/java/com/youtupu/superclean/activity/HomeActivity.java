package com.youtupu.superclean.activity;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.instreet.business.advertise.BannerAdvertiseManager;
import cn.instreet.business.advertise.InterstitialAdvertiseManager;
import cn.instreet.business.advertise.RatingDialog;
import com.youtupu.superclean.BuildConfig;
import com.youtupu.superclean.R;
import com.youtupu.superclean.base.BaseActivity;
import com.youtupu.superclean.dialog.DoubleChoicesDialog;
import com.youtupu.superclean.dialog.ExitDialog;
import com.youtupu.superclean.dialog.UpdateDialog;
import com.youtupu.superclean.service.BatterySaveService;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.utils.Constants;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.BoostButton;
import com.youtupu.superclean.view.CustomTitleBar;
import com.youtupu.superclean.view.IndicatorView;
import pub.devrel.easypermissions.EasyPermissions;

import static com.youtupu.superclean.utils.Constants.ABANDON_COOLER;
import static com.youtupu.superclean.utils.Constants.ABANDON_JUNK;
import static com.youtupu.superclean.utils.Constants.ABANDON_NONE;
import static com.youtupu.superclean.utils.Constants.ABANDON_POWER;


public class HomeActivity extends BaseActivity implements View.OnClickListener {
    private static final String PATH_ON_GOOGLE_PLAY = "https://play.google.com/store/apps/details?id=com.youtupu.superclean";
    private RelativeLayout mRlClean;
    private RelativeLayout mRlCooler;
    private RelativeLayout mRlBattery;
    private RelativeLayout mRlNotification;
    private IndicatorView mIndicatorView;
    private float mBoostPercent;
    private boolean drawerClosed = true;
    private DrawerLayout drawerLayout;
    private LinearLayout mLlSetting;
    private LinearLayout mLlScore;
    private LinearLayout mLlAbout;
    private List<Runnable> mDrawerTasks = new ArrayList<>();
    private Handler handler = new Handler();
    private View mJunkLine;
    private View mCoolerLine;
    private View mPowerLine;
    private View mNotificationLine;
    private boolean isCreated;
    private CustomTitleBar mCustomTitleBar;
    private BoostButton mBoostButton;
    private LinearLayout mLlFeedback;
    private LinearLayout mLlAdView;
    private LinearLayout mLlShare;
    private LinearLayout mLlBigFile;

    @Override
    protected void onStart() {
        super.onStart();
        CommonUtils.debug("HomeActivity onStart");
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_home;
    }

    @Override
    protected void initView() {
        isCreated = false;
        mIndicatorView = findViewById(R.id.indicator_view);
        mBoostButton = findViewById(R.id.boost_button);
        mRlClean = findViewById(R.id.rl_clean);
        mRlCooler = findViewById(R.id.rl_cooler);
        mRlBattery = findViewById(R.id.rl_battery);
        mRlNotification = findViewById(R.id.rl_notification);
        mCustomTitleBar = findViewById(R.id.custom_title_bar);
        drawerLayout = findViewById(R.id.drawer_layout);
        mJunkLine = findViewById(R.id.junk_line);
        mCoolerLine = findViewById(R.id.cooler_line);
        mPowerLine = findViewById(R.id.power_line);
        mNotificationLine = findViewById(R.id.notification_line);
        mLlSetting = findViewById(R.id.ll_item_setting);
        mLlScore = findViewById(R.id.ll_item_score);
        mLlAbout = findViewById(R.id.ll_item_about);
        mLlFeedback = findViewById(R.id.ll_feed_back);
        mLlAdView = findViewById(R.id.ll_ad);
        mLlShare = findViewById(R.id.ll_share);
        mIndicatorView.setPercent(0.05f);
        mLlBigFile = findViewById(R.id.ll_item_bigfile);
    }

    @Override
    protected void initData() {
        SharePreferencesUtil preferencesUtil = SharePreferencesUtil.getInstance(this);
        int openedNumber = preferencesUtil.getOpenedNumber();
        preferencesUtil.setOpenedNumber(openedNumber + 1);
        preferencesUtil.setEnterHomeTime(System.currentTimeMillis());

        Intent intent = getIntent();
        if (intent != null) {
            int fromWhere = intent.getIntExtra("fromWhere", 0);
            //记录是否从控制台而来
            if (fromWhere == 1) {
                AnalyticsUtil.logEvent(this, "console_home", null);
            }
        }

        setBoostPercent();
        checkUpdates();
        setAdView();
    }

    private void setAdView() {
        int bottomHeight = computeBottomHeight();
        float density = getResources().getDisplayMetrics().density;
        if (density > 0) {
            float bottomDp = bottomHeight / density;
            //剩下的部分不到50dp，则不显示
            if (bottomDp > 50 && bottomDp < 100) {
                //显示50dp高度的广告
                if (BuildConfig.DEBUG) {
                    mLlAdView.addView(BannerAdvertiseManager.getInstance().showBanner(Constants.TEST_BANNER_AD));
                } else {
                    mLlAdView.addView(BannerAdvertiseManager.getInstance().showBanner(Constants.HOME_BANNER_AD));
                }
            } else if (bottomDp > 100) {
                //显示100dp高度的广告
                if (BuildConfig.DEBUG) {
                    mLlAdView.addView(BannerAdvertiseManager.getInstance().showMediumBanner(Constants.TEST_BANNER_AD));
                } else {
                    mLlAdView.addView(BannerAdvertiseManager.getInstance().showMediumBanner(Constants.HOME_BANNER_AD));
                }
            }
        }
    }

    private int computeBottomHeight() {
        int screenHeight = SystemUtils.getScreenHight(this);
        int statusBarHeight = SystemUtils.getStatusBarHeight(this);
        //actionBar + IndicatorView + 两排按钮
        int viewsHeight = 55 + 300 + 70 + 70;
        int margins = (int) (3 * getResources().getDimension(R.dimen.guide_margin) + getResources().getDimension(R.dimen.home_below_margin) + getResources().getDimension(R.dimen.button_margin));
        return screenHeight - SystemUtils.dp2px(this, viewsHeight) - margins - statusBarHeight;
    }

    private boolean isFunctionsUsed() {
        SharePreferencesUtil preferencesUtil = SharePreferencesUtil.getInstance(this);
        boolean[] usedArray = new boolean[5];
        usedArray[0] = preferencesUtil.getLastBoostTime() > 0;
        usedArray[1] = preferencesUtil.getLastCleanJunkTime() > 0;
        usedArray[2] = preferencesUtil.getCoolerUsed();
        usedArray[3] = preferencesUtil.getPowerUsed();
        usedArray[4] = preferencesUtil.getNotificationUsed();
        int usedNumber = 0;
        for (boolean b : usedArray) {
            if (b) {
                usedNumber++;
            }
        }
        return usedNumber > 2;
    }

    @Override
    protected void initListener() {
        mBoostButton.setOnClickListener(this);
        mRlClean.setOnClickListener(this);
        mRlCooler.setOnClickListener(this);
        mRlBattery.setOnClickListener(this);
        mRlNotification.setOnClickListener(this);
        mLlSetting.setOnClickListener(this);
        mLlScore.setOnClickListener(this);
        mLlAbout.setOnClickListener(this);
        mLlFeedback.setOnClickListener(this);
        mLlShare.setOnClickListener(this);
        mLlBigFile.setOnClickListener(this);
        mCustomTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                if (drawerClosed) {
                    drawerLayout.openDrawer(Gravity.START);
                } else {
                    drawerLayout.closeDrawer(Gravity.START);
                }
            }
        });
        mCustomTitleBar.setOnRightIconClickListener(new CustomTitleBar.RightIconClickListener() {
            @Override
            public void onRightClick() {
                //去广告
            }
        });
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View view, float v) {
            }

            @Override
            public void onDrawerOpened(@NonNull View view) {
                drawerClosed = false;
                AnalyticsUtil.logEvent(HomeActivity.this, "menu_view_open", null);
            }

            @Override
            public void onDrawerClosed(@NonNull View view) {
                drawerClosed = true;
                if (!mDrawerTasks.isEmpty()) {
                    for (Runnable task : mDrawerTasks) {
                        handler.postDelayed(task, 0);
                    }
                }
                mDrawerTasks.clear();
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.boost_button:
                AnalyticsUtil.logEvent(this, "click_indicator_view", null);

                if (mBoostPercent > 0) {
                    Intent boostIntent = new Intent(this, BoostActivity.class);
                    boostIntent.putExtra("percent", mBoostPercent);
                    startActivityForResult(boostIntent, 0);
                } else {
//                    InterstitialAdvertiseManager.getInstance().showAd(Constants.TEST_INTERSTITIAL_AD);
                    InterstitialAdvertiseManager.getInstance().showAd(Constants.BOOST_INTERSTITIAL_AD, new InterstitialAdvertiseManager.OnAdClosed() {
                        @Override
                        public void execute() {
//                            Intent optimizeIntent = new Intent(HomeActivity.this, OptimizeActivity.class);
//                            optimizeIntent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_BOOST);
//                            optimizeIntent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, "");
//                            optimizeIntent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, getString(R.string.already_accelerated));
//                            optimizeIntent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, getString(R.string.boost_title));
//                            optimizeIntent.putExtra(OptimizeActivity.OPTIMIZE_SHOW_AIMATION, false);
                            Intent intent = new OptimizeActivity.IntentBuilder().setClass(HomeActivity.this)
                                    .setFrom(OptimizeActivity.FROM_BOOST).setRightText(getString(R.string.already_accelerated), 0, 0)
                                    .setActionBar(getString(R.string.boost_title)).setAnimationShow(false).build();
                            startActivity(intent);
                        }
                    });

                }
                break;
            case R.id.rl_clean:
                AnalyticsUtil.logEvent(this, "click_layout_clean", null);
                mRlClean.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (EasyPermissions.hasPermissions(HomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            Intent cleanIntent = new Intent(HomeActivity.this, JunkScanActivity.class);
                            startActivity(cleanIntent);
                        } else {
                            Intent intent = new Intent(HomeActivity.this, JunkPermissionInstructionActivity.class);
                            startActivity(intent);
                        }
                        if (mJunkLine.getVisibility() == View.VISIBLE) {
                            mJunkLine.setVisibility(View.INVISIBLE);
                        }
                    }
                }, 300);
                break;
            case R.id.rl_cooler:
                AnalyticsUtil.logEvent(this, "click_layout_cooler", null);
                mRlCooler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(HomeActivity.this, CoolerScanActivity.class));
                        if (mCoolerLine.getVisibility() == View.VISIBLE) {
                            mCoolerLine.setVisibility(View.INVISIBLE);
                        }
                    }
                }, 300);
                break;
            case R.id.rl_battery:
                AnalyticsUtil.logEvent(this, "click_layout_battery", null);
                mRlBattery.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // 使用省电时关闭授权，以便重置状态
                        BatterySaveService.sendShutDownBroadcast(HomeActivity.this);
                        Intent batteryIntent = new Intent(HomeActivity.this, PowerSaveActivity.class);
                        startActivity(batteryIntent);
                        if (mPowerLine.getVisibility() == View.VISIBLE) {
                            mPowerLine.setVisibility(View.INVISIBLE);
                        }
                    }
                }, 300);
                break;
            case R.id.rl_notification:
                AnalyticsUtil.logEvent(this, "click_layout_notification", null);
                mRlNotification.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent notificationIntent = new Intent(HomeActivity.this, NotificationManagerActivity.class);
                        startActivity(notificationIntent);
                        if (mNotificationLine.getVisibility() == View.VISIBLE) {
                            mNotificationLine.setVisibility(View.INVISIBLE);
                        }
                    }
                }, 300);
                break;
            case R.id.ll_item_score:
                mDrawerTasks.add(new Runnable() {
                    @Override
                    public void run() {
                        RatingDialog dialog = new RatingDialog(HomeActivity.this, new RatingDialog.OnRatingListener() {
                            @Override
                            public void onDontAskAgain() {
                            }

                            @Override
                            public void onNotNow() {
                            }

                            @Override
                            public void onFeedback() {
                            }

                            @Override
                            public void onRate(float rateValue) {
                                Bundle bundle = new Bundle();
                                bundle.putFloat("rate_value", rateValue);
                                AnalyticsUtil.logEvent(HomeActivity.this, "rating_feed_back", bundle);
                                if (rateValue >= 4) {
                                    showRankDialog();

                                } else {
                                    showFeedbackDialog();
                                }
                            }

                            @Override
                            public void onCancel() {

                            }
                        });
                        dialog.show();
                    }
                });
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.ll_item_setting:
                mDrawerTasks.add(new Runnable() {
                    @Override
                    public void run() {
                        Intent settingIntent = new Intent(HomeActivity.this, SettingActivity.class);
                        startActivity(settingIntent);
                    }
                });
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.ll_item_about:
                mDrawerTasks.add(new Runnable() {
                    @Override
                    public void run() {
                        Intent aboutIntent = new Intent(HomeActivity.this, AboutUsActivity.class);
                        startActivity(aboutIntent);
                    }
                });
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.ll_feed_back:
                AnalyticsUtil.logEvent(this, "click_feedback", null);
                mDrawerTasks.add(new Runnable() {
                    @Override
                    public void run() {
                        Intent settingIntent = new Intent(HomeActivity.this, FeedbackActivity.class);
                        startActivity(settingIntent);
                    }
                });
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.ll_share:
                AnalyticsUtil.logEvent(this, "click_share", null);
                sendShareContent();
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.ll_item_bigfile:
                if (EasyPermissions.hasPermissions(HomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Intent cleanIntent = new Intent(HomeActivity.this, BigFilesActivity.class);
                    startActivity(cleanIntent);
                } else {
                    Intent intent = new Intent(HomeActivity.this, BigFilePermissionActivity.class);
                    startActivity(intent);
                }
                drawerLayout.closeDrawer(Gravity.START);
                break;
        }
    }

    private void showFeedbackDialog() {
        final DoubleChoicesDialog feedbackDialog = new DoubleChoicesDialog(HomeActivity.this
                , getString(R.string.feedback_dialog_title), getString(R.string.cancel), getString(R.string.feed_back),
                R.mipmap.note_book, new DoubleChoicesDialog.OnButtonClickListener() {
            @Override
            public void leftButtonClick(DoubleChoicesDialog dlg) {
                dlg.dismiss();
            }

            @Override
            public void rightButtonClick(DoubleChoicesDialog dlg) {
                AnalyticsUtil.logEvent(HomeActivity.this, "lowRate_feedback", null);
                dlg.dismiss();
                startActivity(new Intent(HomeActivity.this, FeedbackActivity.class));
            }
        });
        feedbackDialog.show();
    }

    private void showRankDialog() {
        final DoubleChoicesDialog dialog = new DoubleChoicesDialog(HomeActivity.this
                , getString(R.string.recommend_rating), getString(R.string.no_thanks), getString(R.string.OK), R.mipmap.play_store,
                new DoubleChoicesDialog.OnButtonClickListener() {
                    @Override
                    public void leftButtonClick(DoubleChoicesDialog dlg) {
                        dlg.dismiss();
                    }

                    @Override
                    public void rightButtonClick(DoubleChoicesDialog dlg) {
                        dlg.dismiss();
                        Intent intent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(PATH_ON_GOOGLE_PLAY));
                        startActivity(intent);
                    }
                });
        dialog.show();
    }

    private void sendShareContent() {
        String shareText = getString(R.string.share_text) + "\n" + PATH_ON_GOOGLE_PLAY;
        String share = getString(R.string.share);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, share);
        intent.putExtra(Intent.EXTRA_TEXT, shareText);
        Intent chooseIntent = Intent.createChooser(intent, share);
        startActivity(chooseIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case 0:
                if (data != null) {
                    boolean boosted = data.getBooleanExtra("boosted", false);
                    if (boosted) {
                        mBoostPercent = 0;
                        changePercent(mBoostPercent);
                    }
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharePreferencesUtil.getInstance(this).setLastBoostResult(mBoostPercent);
        CommonUtils.debug("onDestroy");
        BatterySaveService.sendShutDownBroadcast(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        CommonUtils.debug("onStop");
    }

    private void setBoostPercent() {
        SharePreferencesUtil preferencesUtil = SharePreferencesUtil.getInstance(this);
        long lastBoostTime = preferencesUtil.getLastBoostTime();
        if (lastBoostTime > 0) {
            long time = System.currentTimeMillis() - lastBoostTime;
            //若距离上次加速不到5分钟，则不变
            if (time < 300000) {
                mBoostPercent = 0;
                return;
            } else if (time < 600000) {
                //若大于5小于10分钟，则渐变
                mBoostPercent = ((time - 300000) * 1.0f / 300000) * 0.8f;
                return;
            }
        }

        long appLastOpenTime = SharePreferencesUtil.getInstance(this).getAppLastOpenTime();
        //距离上次打开app不到一分钟，则percent不变
        if (appLastOpenTime > 0 && System.currentTimeMillis() - appLastOpenTime < 60000) {
            mBoostPercent = SharePreferencesUtil.getInstance(this).getLastBoostResult();
            if (mBoostPercent < 0) {
                Random random = new Random();
                mBoostPercent = 0.8f + 0.15f * random.nextFloat();
            }
        } else {
            Random random = new Random();
            mBoostPercent = 0.8f + 0.15f * random.nextFloat();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isCreated) {
            isCreated = true;
            changePercent(mBoostPercent);
            //第一次进来时，消除之前的abandon记录
            SharePreferencesUtil.getInstance(this).setLastAbandonOptimize(0);
        }
        //此处逻辑为，防止从控制台进入加速后，再回首页，按钮仍红
        long currentTimeMillis = System.currentTimeMillis();
        long lastBoostTime = SharePreferencesUtil.getInstance(this).getLastBoostTime();
        //距离上次加速5分钟内，指针指示最小
        if (lastBoostTime > 0 && currentTimeMillis - lastBoostTime < 5 * 60 * 1000) {
            mBoostPercent = 0;
            changePercent(0);
        }
        //哪个功能走到一半就退出了（没有到OptimizeActivity）界面，那么回到首页时，该功能下画红线（红线同时只显示一条）
        switch (SharePreferencesUtil.getInstance(this).getLastAbandonOptimize()) {
            case ABANDON_NONE:
                mJunkLine.setVisibility(View.INVISIBLE);
                mCoolerLine.setVisibility(View.INVISIBLE);
                mPowerLine.setVisibility(View.INVISIBLE);
                break;
            case ABANDON_JUNK:
                mJunkLine.setVisibility(View.VISIBLE);
                mCoolerLine.setVisibility(View.INVISIBLE);
                mPowerLine.setVisibility(View.INVISIBLE);
                break;
            case ABANDON_COOLER:
                mJunkLine.setVisibility(View.INVISIBLE);
                mCoolerLine.setVisibility(View.VISIBLE);
                mPowerLine.setVisibility(View.INVISIBLE);
                break;
            case ABANDON_POWER:
                mJunkLine.setVisibility(View.INVISIBLE);
                mCoolerLine.setVisibility(View.INVISIBLE);
                mPowerLine.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void changePercent(float percent) {
        if (percent < 0.05f) {
            percent = 0.05f;
        } else if (percent > 0.95f) {
            percent = 0.95f;
        }
        final float finalPercent = percent;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ObjectAnimator animator = ObjectAnimator.ofFloat(mIndicatorView, IndicatorView.PERCENT, finalPercent);
                OvershootInterpolator interpolator2 = new OvershootInterpolator(1f);
                animator.setInterpolator(interpolator2);
                animator.setDuration(1000);
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playSequentially(animator);
                animatorSet.start();
            }
        }, 400);
    }

    private void checkUpdates() {
        SharePreferencesUtil preferencesUtil = SharePreferencesUtil.getInstance(this);
        preferencesUtil.setAppLastOpenTime(System.currentTimeMillis());
        long lastCheckVersionTime = preferencesUtil.getLastCheckVersionTime();
        //距离上次检查更新不足一天，则返回
        if (lastCheckVersionTime > 0 && ((System.currentTimeMillis() - lastCheckVersionTime) < 1000 * 60 * 60 * 24)) {
            return;
        }
        final String oldVersion = SystemUtils.getVersionName(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest(PATH_ON_GOOGLE_PLAY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Pattern p = Pattern.compile("(Current Version)(<.*?>)*(\\d\\.\\d\\.\\d)");
                    Matcher matcher = p.matcher(response);
                    if (matcher.find()) {
                        String currentVersion = matcher.group(3);
                        if (!currentVersion.equals(oldVersion)) {
                            CommonUtils.debug("detect new version");
                            showUpdateDialog(currentVersion);
                        }

                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CommonUtils.debug(error.getMessage());
            }
        });
        queue.add(request);
    }

    private void showUpdateDialog(final String versionName) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference documentReference = db.collection("updates").document(versionName);
        documentReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (!task.isSuccessful()) {
                    return;
                }
                DocumentSnapshot document = task.getResult();
                if (null == document || !document.exists()) {
                    return;
                }
                String content = getUpdateContent(document);
                if (TextUtils.isEmpty(content)) {
                    return;
                }
                final UpdateDialog dialog = new UpdateDialog(HomeActivity.this, versionName, content);
                dialog.setOnButtonClickListener(new UpdateDialog.OnClickListener() {
                    @Override
                    public void onCancel() {
                        dialog.dismiss();
                        SharePreferencesUtil.getInstance(HomeActivity.this).setLastCheckVersionTime(System.currentTimeMillis());
                    }

                    @Override
                    public void onOK() {
                        SharePreferencesUtil.getInstance(HomeActivity.this).setLastCheckVersionTime(System.currentTimeMillis());
                        dialog.dismiss();
                        String packageName = getPackageName();
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
                        startActivity(intent);
                    }
                });
                if (!HomeActivity.this.isFinishing()) {
                    dialog.show();
                }
            }

            private String getUpdateContent(DocumentSnapshot document) {
                String language = getResources().getConfiguration().locale.getLanguage();
                String contentName = "content_" + language;
                if (document.contains(contentName)) {
                    return document.get(contentName).toString().replace("\\n", "\n");
                } else {
                    //若后台数据库出问题，此处会空指针
                    if (document.get("content") != null) {
                        return document.get("content").toString().replace("\\n", "\n");
                    }
                }
                return "";
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                CommonUtils.debug(e.getMessage());
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawer(Gravity.START);
        } else if (!doSomeThingWhenExit()) {
            super.onBackPressed();
        }
    }

    private boolean doSomeThingWhenExit() {
        SharePreferencesUtil preferencesUtil = SharePreferencesUtil.getInstance(this);
        ExitDialog exitDialog;
        //优先处理有红线的功能，因为红线是用户刚用过且中途放弃的功能
        if (mJunkLine.getVisibility() == View.VISIBLE) {
            long lastJunkSize = preferencesUtil.getLastJunkSize();
            if (lastJunkSize > 0) {
                exitDialog = new ExitDialog(this);
                exitDialog.setJunkSize(lastJunkSize);
                exitDialog.setStyle(ExitDialog.STYLE_JUNK);
                exitDialog.show();
                return true;
            }
        } else if (mCoolerLine.getVisibility() == View.VISIBLE) {
            int lastTemperature = preferencesUtil.getLastTemperature();
            if (lastTemperature > 30) {
                exitDialog = new ExitDialog(this);
                exitDialog.setStyle(ExitDialog.STYLE_COOLER);
                exitDialog.setTemperature(lastTemperature);
                exitDialog.show();
                return true;
            }
        } else if (mPowerLine.getVisibility() == View.VISIBLE) {
            int lastCanStopNumber = preferencesUtil.getLastCanStopNumber();
            if (lastCanStopNumber > 0) {
                exitDialog = new ExitDialog(this);
                exitDialog.setStyle(ExitDialog.STYLE_POWER);
                exitDialog.setCanStopNumber(lastCanStopNumber);
                exitDialog.show();
                return true;
            }
        }
        int openedNumber = preferencesUtil.getOpenedNumber();
        //第一次使用时不弹
        if (openedNumber > 1) {
            long lastCancelTimeWhenExit = preferencesUtil.getLastCancelTimeWhenExit();
            long time = BuildConfig.DEBUG ? 1000 * 10 : 1000 * 60 * 5;
            //若距离上次用户点取消、或者点击优化，不超过5分钟，则不弹界面，直接退出
            if (lastCancelTimeWhenExit > 0 && System.currentTimeMillis() - lastCancelTimeWhenExit < time) {
                return false;
            }
            if (mBoostPercent > 0.8f) {
                exitDialog = new ExitDialog(this);
                exitDialog.setBoostPercent(mBoostPercent);
                exitDialog.setStyle(ExitDialog.STYLE_BOOST);
                exitDialog.show();
                return true;
            }
            long lastCoolTime = preferencesUtil.getLastCoolTime();
            //从未用过降温，或距上次降温超过5分钟
            if (lastCoolTime == 0 || System.currentTimeMillis() - lastCoolTime > time) {
                int temperature = SystemUtils.getBatteryTemperature(this);
                if (temperature > 30) {
                    exitDialog = new ExitDialog(this);
                    exitDialog.setStyle(ExitDialog.STYLE_COOLER);
                    exitDialog.setTemperature(temperature);
                    exitDialog.show();
                    return true;
                }
            }
        }
        return false;
    }

}
