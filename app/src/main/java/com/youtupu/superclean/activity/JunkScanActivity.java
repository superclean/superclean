package com.youtupu.superclean.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cn.instreet.business.advertise.InterstitialAdvertiseManager;
import com.youtupu.superclean.R;
import com.youtupu.superclean.app.SuperCleanApplication;
import com.youtupu.superclean.base.BaseActivity;
import com.youtupu.superclean.bean.JunkListItem;
import com.youtupu.superclean.dialog.DoubleChoicesDialog;
import com.youtupu.superclean.dialog.SingleChoiceDialog;
import com.youtupu.superclean.extract.OnJunkCleanListener;
import com.youtupu.superclean.service.JunkCleanService;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.AnimationUtil;
import com.youtupu.superclean.utils.Constants;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.CustomTitleBar;
import com.youtupu.superclean.view.JunkCircle;
import pub.devrel.easypermissions.EasyPermissions;

public class JunkScanActivity extends BaseActivity implements OnJunkCleanListener {
    private static final String[] PERMISSIONS_TO_GRANT = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final int STORAGE_PERMISSION_REQUEST_CODE = 0;
    private static final int SCAN_ONE_CIRCLE_DURATION = 1000;
    private static final int SCAN_DONE_DURATION = 800;
    private static final int UPDATE_SCAN_FILE_PATH = 0;
    private JunkCircle mJunkCircle;
    private JunkCleanService mService;
    private JunkCleanServiceConnection mServiceConnection;
    private List<String> mPermissionList;
    private TextView mTvAppCache;
    private TextView mTvResidual;
    private boolean mCacheScanRunning = false;
    private boolean mResidualScanRunning = false;
    private boolean mAPKScanRunning = false;
    private boolean mADScanRunning = false;
    private long mAppCacheScanSize = 0;
    private long mResidualScanSize = 0;
    private long mAPKScanSize = 0;
    private long mADScanSize = 0;
    private TextView mTvScanedFile;
    private long mTotalJunkSize;
    private boolean mGoToSetting = false;
    private SingleChoiceDialog mPermissionRequestDialog;
    private SuperCleanApplication mApplication;
    private ObjectAnimator mScanAnimator;
    private ObjectAnimator mGradualAnimator;
    private ObjectAnimator mScanDoneAnimator;
    private AnimatorSet mDoneSet;
    private DoubleChoicesDialog mExitDialog;
    private boolean mGotScanResult = false;
    private ObjectAnimator mRingRunAnimator;
    private ImageView mImgApp;
    private ImageView mImgResidual;
    //垃圾size数字线性增长的动画
    ObjectAnimator mIncreaseAnim;
    private CustomTitleBar mCustomTitleBar;
    private boolean mFromOutSide = false;
    private ImageView mImgApk;
    private ImageView mImgAd;
    private TextView mTvAPK;
    private TextView mTvAd;
    private List<String> mCurrentPathList;
    private ScanHandler mHandler;

    @Override
    protected boolean beforeSetContentView() {
        Intent intent1 = getIntent();
        if (intent1 != null) {
            int fromWhere = intent1.getIntExtra("fromWhere", 0);
            //是否从外界而来
            if (fromWhere > 0) {
                mFromOutSide = fromWhere < 10;
                if (fromWhere == 1) {
                    //从控制台而来
                    AnalyticsUtil.logEvent(this, "console_junk", null);
                } else if (fromWhere == 11) {
                    //从首页退出时的对话框而来
                    AnalyticsUtil.logEvent(this, "exit_dialog_junk", null);
                }
            }
        }
        //进来时发现没有权限，则开启权限引导界面，此种情况针对从控制台进入（因为控制台的逻辑无法即时判断是否有权限，该开哪个activity）
        if (!EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent2 = new Intent(this, JunkPermissionInstructionActivity.class);
            intent2.putExtra("fromOutSide", mFromOutSide);
            startActivity(intent2);
            finish();
            return false;
        }
        long lastCleanTime = SharePreferencesUtil.getInstance(this).getLastCleanAllJunkTime();
        //全部清理后的1分钟内，直接跳转优化界面
        if (lastCleanTime > 0 && System.currentTimeMillis() - lastCleanTime < 1000 * 60) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    InterstitialAdvertiseManager.getInstance().showAd(Constants.JUNK_INTERSTITIAL_AD, new InterstitialAdvertiseManager.OnAdClosed() {
                        @Override
                        public void execute() {
//                            Intent intent = new Intent(JunkScanActivity.this, OptimizeActivity.class);
//                            intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_JUNK);
//                            intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, getString(R.string.junk_clean));
//                            intent.putExtra(OptimizeActivity.OPTIMIZE_SHOW_AIMATION, false);
//                            intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, "");
//                            intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, getString(R.string.storage_is_clean));

                            Intent intent = new OptimizeActivity.IntentBuilder().setClass(JunkScanActivity.this)
                                    .setFrom(OptimizeActivity.FROM_JUNK).setActionBar(getString(R.string.junk_clean))
                                    .setRightText(getString(R.string.storage_is_clean),0,0)
                                    .setAnimationShow(false).build();
                            startActivity(intent);
                        }
                    });
                }
            }, 0);
            finish();
            return false;

        }
        return true;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_junk_scan;
    }


    @Override
    protected void initView() {
        mCustomTitleBar = findViewById(R.id.custom_title_bar);
        mJunkCircle = findViewById(R.id.junk_circle);
        mTvAppCache = findViewById(R.id.tv_app_cache);
        mTvResidual = findViewById(R.id.tv_residual);
        mTvAPK = findViewById(R.id.tv_apk);
        mTvAd = findViewById(R.id.tv_ad);
        mTvScanedFile = findViewById(R.id.tv_scan_file);
        mImgApp = findViewById(R.id.img_app_icon);
        mImgResidual = findViewById(R.id.img_residual_icon);
        mImgApk = findViewById(R.id.img_apk);
        mImgAd = findViewById(R.id.img_ad);
    }

    @Override
    protected void initData() {
        mGotScanResult = false;
        mApplication = (SuperCleanApplication) getApplication();
        mServiceConnection = new JunkCleanServiceConnection();
        mPermissionList = new ArrayList<>();
        mScanAnimator = ObjectAnimator.ofFloat(mJunkCircle, JunkCircle.SWEEP_ANGLE, 0f, 480f);
        mScanAnimator.setDuration(SCAN_ONE_CIRCLE_DURATION);
        mScanAnimator.setRepeatCount(ValueAnimator.INFINITE);
        mGradualAnimator = ObjectAnimator.ofFloat(mJunkCircle, JunkCircle.PERCENT, 0f, 1f);
        mGradualAnimator.setDuration(10000);
        mRingRunAnimator = ObjectAnimator.ofFloat(mJunkCircle, JunkCircle.RING_ANGLE, 0f, 300f);
        mRingRunAnimator.setDuration(10000);
        mScanDoneAnimator = ObjectAnimator.ofFloat(mJunkCircle, JunkCircle.DONE_ANGLE, 0, 360f);
        mScanDoneAnimator.setDuration(SCAN_DONE_DURATION);
        mDoneSet = new AnimatorSet();
        mCurrentPathList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            mCurrentPathList.add("");
        }
        mHandler = new ScanHandler(this);
        bindService(new Intent(this, JunkCleanService.class), mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void initListener() {
        mScanAnimator.addListener(new Animator.AnimatorListener() {
            int mIndex = 0;

            @Override
            public void onAnimationStart(Animator animation) {
                mIndex = 0;
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                mIndex++;
                if (mGotScanResult) {
                    mScanAnimator.cancel();
                    mGradualAnimator.cancel();
                    mRingRunAnimator.cancel();
                    ObjectAnimator ringAnimator = ObjectAnimator.ofFloat(mJunkCircle, JunkCircle.RING_ANGLE, mJunkCircle.getRingAngle(), 360f);
                    ringAnimator.setDuration(SCAN_DONE_DURATION * 2);
                    if (mTotalJunkSize > 0) {
                        mJunkCircle.setFileSize(mTotalJunkSize);
                        ObjectAnimator gradualAnimator = ObjectAnimator.ofFloat(mJunkCircle, JunkCircle.PERCENT, mJunkCircle.getPercent(), 1f);
                        gradualAnimator.setDuration(SCAN_DONE_DURATION);
                        mDoneSet.playTogether(mScanDoneAnimator, gradualAnimator, ringAnimator);
                    } else {
                        mDoneSet.playTogether(mScanDoneAnimator, ringAnimator);
                    }
                    mDoneSet.start();
                } else {
                    if (mTotalJunkSize > 0) {
                        if (mIncreaseAnim != null && mIncreaseAnim.isRunning()) {
                            mIncreaseAnim.cancel();
                        }
                        mIncreaseAnim = ObjectAnimator.ofInt(mJunkCircle, JunkCircle.JUNK_SIZE, mJunkCircle.getJunkSize(), (int) (mTotalJunkSize / 1000));
                        mIncreaseAnim.setDuration(SCAN_ONE_CIRCLE_DURATION);
                        mIncreaseAnim.start();
                    }
                }
            }
        });
        mDoneSet.addListener(new Animator.AnimatorListener() {
            private boolean isCanceled = false;

            @Override
            public void onAnimationStart(Animator animation) {
                isCanceled = false;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                // 0 is together，1 is sequence
                playTogether();
                mTvAppCache.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!isCanceled) {
                            if (mTotalJunkSize > 0) {
                                Intent intent = new Intent(JunkScanActivity.this, JunkDetailActivity.class);
                                intent.putExtra("fromOutSide", mFromOutSide);
                                intent.putExtra("junkSize", mTotalJunkSize);
                                startActivity(intent);
                                finish();
                            } else {
                                InterstitialAdvertiseManager.getInstance().showAd(Constants.JUNK_INTERSTITIAL_AD, new InterstitialAdvertiseManager.OnAdClosed() {
                                    @Override
                                    public void execute() {
                                        SharePreferencesUtil.getInstance(JunkScanActivity.this).setLastCleanAllJunkTime(System.currentTimeMillis());
//                                        Intent intent = new Intent(JunkScanActivity.this, OptimizeActivity.class);
//                                        intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_JUNK);
//                                        intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, getString(R.string.junk_clean));
//                                        intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, "");
//                                        intent.putExtra(OptimizeActivity.OPTIMIZE_TITLE, getString(R.string.normal));
//                                        intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, getString(R.string.storage_is_clean));
//                                        if (SharePreferencesUtil.getInstance(JunkScanActivity.this).getLastCleanJunkTime() == 0) {
//                                            intent.putExtra(OptimizeActivity.NEED_RECORD, true);
//                                        } else {
//                                            intent.putExtra(OptimizeActivity.NEED_RECORD, false);
//                                        }

                                        Intent intent = new OptimizeActivity.IntentBuilder()
                                                .setClass(JunkScanActivity.this)
                                                .setFrom(OptimizeActivity.FROM_JUNK)
                                                .setActionBar(getString(R.string.junk_clean))
                                                .setTitle(getString(R.string.normal))
                                                .setRightText(getString(R.string.storage_is_clean), 0,0)
                                                .setNeedRecord(SharePreferencesUtil.getInstance(JunkScanActivity.this).getLastCleanJunkTime() == 0)
                                                .build();
                                        startActivity(intent);
                                        finish();
                                    }
                                });

                            }
                        }
                    }
                }, 400);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                isCanceled = true;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        mCustomTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                onBackPressed();
            }
        });
    }

    private void playTogether() {
        final long duration = 400;
        final float scaleTo = 1.3f;
        AnimationUtil.scale(mImgApp, duration, 1f, scaleTo, 1f);
        AnimationUtil.scale(mTvAppCache, duration, 1f, scaleTo, 1f);
        AnimationUtil.scale(mImgResidual, duration, 1f, scaleTo, 1f);
        AnimationUtil.scale(mTvResidual, duration, 1f, scaleTo, 1f);
        AnimationUtil.scale(mImgApk, duration, 1f, scaleTo, 1f);
        AnimationUtil.scale(mTvAPK, duration, 1f, scaleTo, 1f);
        AnimationUtil.scale(mImgAd, duration, 1f, scaleTo, 1f);
        AnimationUtil.scale(mTvAd, duration, 1f, scaleTo, 1f);
    }

    @Override
    public void onBackPressed() {
        boolean hasRunning = false;
        if (mScanAnimator.isRunning()) {
            mScanAnimator.pause();
            hasRunning = true;
        }
        if (mGradualAnimator.isRunning()) {
            mGradualAnimator.pause();
            hasRunning = true;
        }
        if (mRingRunAnimator.isRunning()) {
            mRingRunAnimator.pause();
            hasRunning = true;
        }
        if (mDoneSet.isRunning()) {
            mDoneSet.pause();
            hasRunning = true;
        }
        if (mIncreaseAnim != null && mIncreaseAnim.isRunning()) {
            mIncreaseAnim.pause();
            hasRunning = true;
        }
        if (hasRunning) {
            showDialog();
        } else {
            //用户中途主动退出时，如有垃圾，则记录为abandon
            if (mTotalJunkSize > 0) {
                SharePreferencesUtil.getInstance(JunkScanActivity.this).setLastAbandonOptimize(1);
                SharePreferencesUtil.getInstance(JunkScanActivity.this).setLastJunkSize(mTotalJunkSize);
            }
            //如果是从外界打开我这个activity，那我中断退出时，把首页拉起
            if (mFromOutSide) {
                startActivity(new Intent(JunkScanActivity.this, HomeActivity.class));
            }
            mApplication.setJunkCleaning(false);
            super.onBackPressed();
        }
    }

    private void showDialog() {
        if (mExitDialog == null) {
            mExitDialog = new DoubleChoicesDialog(this, getString(R.string.interrupted_when_scaning)
            ,getString(R.string.abandon), getString(R.string.go_on), R.mipmap.scaning, new DoubleChoicesDialog.OnButtonClickListener() {
                @Override
                public void leftButtonClick(DoubleChoicesDialog dlg) {
                    //用户中途主动退出时，如有垃圾，则记录为abandon
                    if (mTotalJunkSize > 0) {
                        SharePreferencesUtil.getInstance(JunkScanActivity.this).setLastAbandonOptimize(1);
                        SharePreferencesUtil.getInstance(JunkScanActivity.this).setLastJunkSize(mTotalJunkSize);
                    }
                    //如果是从外界打开我这个activity，那我中断退出时，把首页拉起
                    if (mFromOutSide) {
                        startActivity(new Intent(JunkScanActivity.this, HomeActivity.class));
                    }
                    mApplication.setJunkCleaning(false);
                    finish();
                }

                @Override
                public void rightButtonClick(DoubleChoicesDialog dlg) {
                    mExitDialog.dismiss();
                    if (mScanAnimator.isPaused()) {
                        mScanAnimator.resume();
                    }
                    if (mGradualAnimator.isPaused()) {
                        mGradualAnimator.resume();
                    }
                    if (mDoneSet.isPaused()) {
                        mDoneSet.resume();
                    }
                    if (mRingRunAnimator.isPaused()) {
                        mRingRunAnimator.resume();
                    }
                    if (mIncreaseAnim != null && mIncreaseAnim.isPaused()) {
                        mIncreaseAnim.resume();
                    }
                }
            });
        }
        mExitDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoToSetting) {
            mGoToSetting = false;
            if (isAllPermissionGranted()) {
                if (mPermissionRequestDialog != null) {
                    mPermissionRequestDialog.dismiss();
                }
                startScan();
            }
        }
        if (mScanAnimator.isPaused()) {
            mScanAnimator.resume();
        }
        if (mGradualAnimator.isPaused()) {
            mGradualAnimator.resume();
        }
        if (mDoneSet.isPaused()) {
            mDoneSet.resume();
        }
        if (mRingRunAnimator.isPaused()) {
            mRingRunAnimator.resume();
        }
        if (mIncreaseAnim != null && mIncreaseAnim.isPaused()) {
            mIncreaseAnim.resume();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mScanAnimator.isRunning()) {
            mScanAnimator.pause();
        }
        if (mGradualAnimator.isRunning()) {
            mGradualAnimator.pause();
        }
        if (mDoneSet.isRunning()) {
            mDoneSet.pause();
        }
        if (mRingRunAnimator.isRunning()) {
            mRingRunAnimator.pause();
        }
        if (mIncreaseAnim != null && mIncreaseAnim.isRunning()) {
            mIncreaseAnim.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mShowActivityView) {
            mScanAnimator.cancel();
            mDoneSet.cancel();
            mGradualAnimator.cancel();
            mRingRunAnimator.cancel();
            if (mIncreaseAnim != null) {
                mIncreaseAnim.cancel();
            }
            if (mService != null) {
                mService.setOnJunkCleanListener(null);
            }
            if (mServiceConnection != null) {
                unbindService(mServiceConnection);
            }
            mHandler.removeMessages(UPDATE_SCAN_FILE_PATH);
        }
    }

    private boolean isAllPermissionGranted() {
        mPermissionList.clear();
        for (String permission : PERMISSIONS_TO_GRANT) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permission);
            }
        }
        return mPermissionList.isEmpty();
    }

    private void startScan() {
        mScanAnimator.start();
        mRingRunAnimator.start();
        if (mService != null) {
            mService.setOnJunkCleanListener(JunkScanActivity.this);
            if (!mService.isScanningOrCleaning()) {
                mApplication.setJunkCleaning(true);
                mService.startScan();
                mHandler.removeMessages(UPDATE_SCAN_FILE_PATH);
                mHandler.sendEmptyMessage(UPDATE_SCAN_FILE_PATH);
            }
        }
    }

    private void requestPermission() {
        int size = mPermissionList.size();
        String[] permissions = mPermissionList.toArray(new String[size]);
        ActivityCompat.requestPermissions(this, permissions, STORAGE_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    showPermissionRequestDialog();
                } else {
                    startScan();
                }
                break;
        }
    }

    private void showPermissionRequestDialog() {
        mPermissionRequestDialog = new SingleChoiceDialog(this);
        mPermissionRequestDialog.setTitle(getString(R.string.need_storage_permission));
        mPermissionRequestDialog.setButtonText(getString(R.string.to_setting_authorization));
        mPermissionRequestDialog.setImgResource(R.mipmap.junk_deny);
        mPermissionRequestDialog.setOnButtonClickListener(new SingleChoiceDialog.OnButtonClickListener() {
            @Override
            public void onClick() {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.setData(Uri.fromParts("package", getPackageName(), null));
                startActivity(intent);
                mGoToSetting = true;
            }
        });
        mPermissionRequestDialog.show();
    }

    private void updateScanTotalResult() {
        if (!mCacheScanRunning) {
            mImgApp.setImageResource(R.mipmap.app_cache_small);
        }
        if (!mResidualScanRunning) {
            mImgResidual.setImageResource(R.mipmap.junk_residual_small);
        }
        if (!mAPKScanRunning) {
            mImgApk.setImageResource(R.mipmap.apk_icon_small);
        }
        if (!mADScanRunning) {
            mImgAd.setImageResource(R.mipmap.ad_icon_small);
        }
        mTotalJunkSize = mAppCacheScanSize + mResidualScanSize + mAPKScanSize + mADScanSize;
        if (mCacheScanRunning || mResidualScanRunning || mAPKScanRunning || mADScanRunning) {
            return;
        }
        mGotScanResult = true;
        mHandler.removeMessages(UPDATE_SCAN_FILE_PATH);
        if (mIncreaseAnim != null && mIncreaseAnim.isRunning()) {
            mIncreaseAnim.cancel();
        }
        mIncreaseAnim = ObjectAnimator.ofInt(mJunkCircle, JunkCircle.JUNK_SIZE, mJunkCircle.getJunkSize(), (int) (mTotalJunkSize / 1000));
        mIncreaseAnim.setDuration(SCAN_DONE_DURATION);
        mIncreaseAnim.start();
    }


    @Override
    public void onCacheScanStart() {
        mCacheScanRunning = true;
        mAppCacheScanSize = 0;
        mTvAppCache.setText(SystemUtils.getFileSize(0));
    }

    @Override
    public void onCacheScanUpdate(long appSize, String filePath) {
        mAppCacheScanSize = appSize;
        mTvAppCache.setText(SystemUtils.getFileSize(appSize));
        mCurrentPathList.set(0, filePath);
        updateScanTotalResult();

    }

    @Override
    public void onCacheScanDone(List<JunkListItem> appList) {
        mCacheScanRunning = false;
        mApplication.setAppCacheList(appList);
        mCurrentPathList.set(0, "");
        updateScanTotalResult();
    }


    @Override
    public void onResidualScanStart() {
        mResidualScanRunning = true;
        mResidualScanSize = 0;
        mTvResidual.setText(SystemUtils.getFileSize(0));
    }

    @Override
    public void onResidualScanUpdate(long size, String filePath) {
        if (size > 0) {
            mResidualScanSize = size;
            mTvResidual.setText(SystemUtils.getFileSize(size));
            updateScanTotalResult();
        }
        mCurrentPathList.set(1, filePath);
    }

    @Override
    public void onResidualScanDone(List<JunkListItem> items) {
        mResidualScanRunning = false;
        mApplication.setResidualList(items);
        mCurrentPathList.set(1, "");
        updateScanTotalResult();
    }

    @Override
    public void onApkScanStart() {
        mAPKScanRunning = true;
        mAPKScanSize = 0;
        mTvAPK.setText(SystemUtils.getFileSize(0));
    }

    @Override
    public void onApkScanUpdate(long size, String filePath) {
        if (size > 0) {
            mAPKScanSize = size;
            mTvAPK.setText(SystemUtils.getFileSize(size));
            updateScanTotalResult();
        }
        mCurrentPathList.set(2, filePath);
    }

    @Override
    public void onApkScanDone(List<JunkListItem> items) {
        mAPKScanRunning = false;
        mApplication.setUnusedAPKList(items);
        mCurrentPathList.set(2, "");
        updateScanTotalResult();
    }

    @Override
    public void onADScanStart() {
        mADScanRunning = true;
        mADScanSize = 0;
        mTvAd.setText(SystemUtils.getFileSize(0));
    }

    @Override
    public void onADScanUpdate(long size, String filePath) {
        if (size > 0) {
            mADScanSize = size;
            mTvAd.setText(SystemUtils.getFileSize(size));
            updateScanTotalResult();
        }
        mCurrentPathList.set(3, filePath);
    }

    @Override
    public void onADScanDone(List<JunkListItem> items) {
        mADScanRunning = false;
        mApplication.setADList(items);
        mCurrentPathList.set(3, "");
        updateScanTotalResult();
    }

    private class JunkCleanServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = ((JunkCleanService.JunkCleanBinder) service).getService();
            mGotScanResult = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isAllPermissionGranted()) {
                    startScan();
                } else {
                    requestPermission();
                }
            } else {
                startScan();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    }

    private static class ScanHandler extends Handler {
        private WeakReference<JunkScanActivity> mReference;

        ScanHandler(JunkScanActivity activity) {
            mReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            JunkScanActivity activity = mReference.get();
            if (activity == null) return;
            switch (msg.what) {
                case UPDATE_SCAN_FILE_PATH:
                    String filePath = "";
                    for (String path : activity.mCurrentPathList) {
                        if (!TextUtils.isEmpty(path)) {
                            filePath = path;
                            break;
                        }
                    }
                    if (!TextUtils.isEmpty(filePath)) {
                        activity.mTvScanedFile.setText(filePath);
                    }
                    sendEmptyMessageDelayed(UPDATE_SCAN_FILE_PATH, 50);
                    break;
            }
        }
    }
}
