package com.youtupu.superclean.activity;

import android.content.Intent;

import java.util.ArrayList;

import com.youtupu.superclean.R;
import com.youtupu.superclean.app.SuperCleanApplication;
import com.youtupu.superclean.base.BaseActivity;
import com.youtupu.superclean.fragment.JunkCleanFragment;
import com.youtupu.superclean.fragment.JunkDetailFragment;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.view.CustomTitleBar;

public class JunkDetailActivity extends BaseActivity {
    public static final int DETAIL_FRAGMENT_INDEX = 0;
    public static final int CLEAN_FRAGMENT_INDEX = 1;
    public boolean mIsSelectAll = false;
    private boolean mFromOutSide = false;
    public SuperCleanApplication mApplication;
    private long mJunkSize = 0;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_junk_detail;
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            mFromOutSide = intent.getBooleanExtra("fromOutSide", false);
            mJunkSize = intent.getLongExtra("junkSize", 0);
        }
        mApplication = (SuperCleanApplication) getApplication();
        mFragmentsList = new ArrayList<>();
        JunkDetailFragment junkDetailFragment = new JunkDetailFragment();
        JunkCleanFragment junkCleanFragment = new JunkCleanFragment();
        mFragmentsList.add(junkDetailFragment);
        mFragmentsList.add(junkCleanFragment);
        switchFragment(DETAIL_FRAGMENT_INDEX, R.id.junk_framelayout);
    }

    @Override
    protected void initView() {
        super.initView();
        CustomTitleBar customTitleBar = findViewById(R.id.custom_title_bar);
        customTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //在垃圾详情界面里，点返回，肯定都是Abandon了
        SharePreferencesUtil.getInstance(this).setLastAbandonOptimize(1);
        SharePreferencesUtil.getInstance(this).setLastJunkSize(mJunkSize);
        mApplication.setJunkCleaning(false);
        super.onBackPressed();
        //如果是从外界打开我这个activity，那我中断退出时，把首页拉起
        if (mFromOutSide) {
            startActivity(new Intent(this, HomeActivity.class));
        }
    }

}
