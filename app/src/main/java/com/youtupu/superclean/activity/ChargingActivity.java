package com.youtupu.superclean.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import cn.instreet.business.BusinessSDK;
import com.youtupu.superclean.R;
import com.youtupu.superclean.service.MonitorService;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.StatusBarUtils;

public class ChargingActivity extends AppCompatActivity {


    private ChargeReceiver mReceiver;
    private int mAppNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        mAppNumber = intent.getIntExtra("app_number", 0);
        if (mAppNumber <= 0) {
            finish();
            return;
        }
        setContentView(R.layout.activity_charging);
        StatusBarUtils.setStatusBarFullTransparent(this);
        TextView tvCharge = findViewById(R.id.tv_charge);
        TextView tvOptimize = findViewById(R.id.dialog_right_choice);
        tvCharge.setText(Html.fromHtml(String.format(getString(R.string.charging_start_title), mAppNumber)));
        tvOptimize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnalyticsUtil.logEvent(ChargingActivity.this, "charge_optimize", null);
                Intent intent = new Intent(ChargingActivity.this, FakePowerSaveActivity.class);
                intent.putExtra("totalTask", mAppNumber);
                intent.putExtra("showTips", false);
                intent.putExtra("fromWhere", 1);
                startActivity(intent);
                finish();
            }
        });
        TextView tvLater = findViewById(R.id.dialog_left_choice);
        tvLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnalyticsUtil.logEvent(ChargingActivity.this, "charge_on_later", null);
                Intent newIntent = new Intent();
                newIntent.setAction(MonitorService.ACTION_CHARGE_ON_LATER);
                LocalBroadcastManager.getInstance(ChargingActivity.this).sendBroadcast(newIntent);
                finish();
            }
        });
        mReceiver = new ChargeReceiver();
        IntentFilter filter = new IntentFilter(BusinessSDK.ACTION_CHARGER_PLUG_OFF);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        }
    }

    private class ChargeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BusinessSDK.ACTION_CHARGER_PLUG_OFF.equals(action)) {
                finish();
            }
        }
    }

}
