package com.youtupu.superclean.service;

import android.accessibilityservice.AccessibilityService;
import android.content.Context;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.youtupu.superclean.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 辅助节点跟踪基类
 * created by yihao 2019/3/18
 */
public abstract class AccessibilityNodeTracker {

    static final int RESULT_CONTINUE = 1;
    static final int RESULT_FAIL = 2;
    static final int RESULT_SUCCESS = 3;

    AccessibilityNodeTracker(Context context){
        if(null == context){
            throw new IllegalArgumentException("context is null");
        }
    }

    abstract public int onEvent(AccessibilityService service, AccessibilityEvent event);

    abstract public void begin(TrackTimeoutListener listener, AccessibilityService service);

    List<AccessibilityNodeInfo> findNodesWithText(AccessibilityService service, AccessibilityNodeInfo info, String[] contents) {
        if (info == null) return null;
        int num = info.getChildCount();
        if (num == 0) return null;
        String language = service.getResources().getConfiguration().locale.getLanguage();
        Locale locale = new Locale(language);
        List<AccessibilityNodeInfo> resultList = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            AccessibilityNodeInfo child = info.getChild(i);
            if(null == child){
                continue;
            }
            CharSequence text = child.getText();
            if (text != null) {
                String textString = text.toString().toLowerCase(locale);
                boolean hasNode = false;
                for (String content : contents) {
                    CommonUtils.debug(textString + " " + content.toLowerCase(locale));
                    if (TextUtils.isEmpty(content)) {
                        continue;
                    }
                    if (textString.contentEquals(content.toLowerCase(locale))
                            && child.isClickable() && child.isEnabled()) {
                        resultList.add(child);
                        hasNode = true;
                        break;
                    }
                }
                if (!hasNode) {
                    List<AccessibilityNodeInfo> childList = findNodesWithText(service, child, contents);
                    if (childList != null && childList.size() > 0) {
                        resultList.addAll(childList);
                    }
                }
            } else {
                List<AccessibilityNodeInfo> childList = findNodesWithText(service, child, contents);
                if (childList != null && childList.size() > 0) {
                    resultList.addAll(childList);
                }
            }
        }
        return resultList;
    }

    public interface TrackTimeoutListener{
        void onTimeOut();
    }
}
