package com.youtupu.superclean.service;

import android.accessibilityservice.AccessibilityService;
import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.youtupu.superclean.utils.CommonUtils;

import java.util.List;

import com.youtupu.superclean.R;
import ezy.assist.compat.RomUtil;

import static android.accessibilityservice.AccessibilityService.GLOBAL_ACTION_BACK;

/**
 * created by yihao 2019/3/18
 * 停止应用
 */
public class StopPackage extends AccessibilityNodeTracker {

    private String[] stopWords;
    private String[] confirmWords;
    private boolean isStopClicked = false;
    private boolean isConfirmClicked = false;
    private Handler handler;
    private TrackTimeoutListener timeoutListener;



    StopPackage(Context context) {
        super(context);
        stopWords = context.getResources().getStringArray(R.array.forceStop);
        confirmWords = context.getResources().getStringArray(R.array.confirm);
        handler = new Handler();
    }

    @Override
    public int onEvent(AccessibilityService service, AccessibilityEvent event) {
        String className = String.valueOf(event.getClassName());
        if (TextUtils.isEmpty(className)) {
            return RESULT_CONTINUE;
        }

        CommonUtils.debug("class name " + className);

        if (RomUtil.isMiui()) {
            if (className.equals("com.miui.appmanager.ApplicationsDetailsActivity")
                    || className.equals("com.android.settings.applications.InstalledAppDetailsTop")) {
                isConfirmClicked = false;
                isStopClicked = false;
                isStopClicked = tryClickStop(service);
                CommonUtils.debug("click stop");
                SystemClock.sleep(100);
                if (!isStopClicked) {
                    return goFail(service);
                } else {
                    return goContinue(service);
                }

            } else if (className.endsWith("AlertDialog")) {
                if (!isConfirmClicked) {
                    SystemClock.sleep(100);
                    isConfirmClicked = tryClickConfirm(service);
                    CommonUtils.debug("click confirm");
                    if (!isConfirmClicked) {
                        return goFail(service);
                    }
                }
            } else {
                if (isStopClicked && isConfirmClicked) {
                    SystemClock.sleep(500);
                    CommonUtils.debug("back");
                    if (service.performGlobalAction(GLOBAL_ACTION_BACK)) {
                        return goSuccess(service);
                    } else {
                        return goContinue(service);
                    }
                }
            }
        } else if (RomUtil.isEmui()) {
            if (className.endsWith("InstalledAppDetailsTop")) {
                if (isStopClicked && isConfirmClicked) {
                    return goSuccess(service);
                } else {
                    isStopClicked = tryClickStop(service);
                    if (!isStopClicked) {
                        return goFail(service);
                    } else {
                        return goContinue(service);
                    }
                }

            } else if (className.endsWith("AlertDialog")) {
                if (!isConfirmClicked) {
                    isConfirmClicked = tryClickConfirm(service);
                    if (!isConfirmClicked) {
                        return goFail(service);
                    } else {
                        return goSuccess(service);
                    }
                }
            }
        } else {
            if (className.endsWith("SubSettings") || className.endsWith("InstalledAppDetailsTop")) {
                if (isStopClicked && isConfirmClicked) {
                    return goSuccess(service);
                } else {
                    isStopClicked = tryClickStop(service);
                    if (!isStopClicked) {
                        return goFail(service);
                    } else {
                        return RESULT_CONTINUE;
                    }
                }

            } else if (className.endsWith("AlertDialog")) {
//                SystemClock.sleep(100);
                if (!isConfirmClicked) {
                    isConfirmClicked = tryClickConfirm(service);
                    if (!isConfirmClicked) {
                        return goFail(service);
                    }
                }
            }
        }
        return goContinue(service);

    }

    @Override
    public void begin(TrackTimeoutListener listener,final AccessibilityService service) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                CommonUtils.debug("power save time out");
                if(null != timeoutListener){
                    cancelTask(service);
                    timeoutListener.onTimeOut();
                }
            }
        }, 1500);
        timeoutListener = listener;
    }

    private boolean tryClickConfirm(AccessibilityService service) {
        boolean result = false;
        AccessibilityNodeInfo rootInActiveWindow = service.getRootInActiveWindow();
        List<AccessibilityNodeInfo> stopNode = findNodesWithText(service, rootInActiveWindow, confirmWords);
        if (stopNode == null || stopNode.size() == 0) {
            return false;
        }
        for (AccessibilityNodeInfo info : stopNode) {
            result = info.performAction(AccessibilityNodeInfo.ACTION_CLICK);
        }
        return result;
    }

    private boolean tryClickStop(AccessibilityService service) {
        boolean result = false;
        AccessibilityNodeInfo rootInActiveWindow = service.getRootInActiveWindow();
        List<AccessibilityNodeInfo> stopNode = findNodesWithText(service, rootInActiveWindow, stopWords);
        if (stopNode == null || stopNode.size() <= 0) {
            return false;
        }
        for (AccessibilityNodeInfo info : stopNode) {
            result = info.performAction(AccessibilityNodeInfo.ACTION_CLICK);
        }
        return result;
    }

    private void cancelTask(AccessibilityService service) {
        handler.removeCallbacksAndMessages(null);
        isStopClicked = false;
        isConfirmClicked = false;
    }

    private int goContinue(AccessibilityService service) {
        CommonUtils.debug("continue");
        return RESULT_CONTINUE;
    }

    private int goSuccess(AccessibilityService service) {
        CommonUtils.debug("success");
        cancelTask(service);
        return RESULT_SUCCESS;
    }

    private int goFail(AccessibilityService service) {
        CommonUtils.debug("fail");
        cancelTask(service);
        return RESULT_FAIL;
    }
}
