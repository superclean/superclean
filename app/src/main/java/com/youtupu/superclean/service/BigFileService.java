package com.youtupu.superclean.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.NonNull;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import com.youtupu.superclean.bean.BigFileInfo;
import com.youtupu.superclean.extract.OnBigFileListener;

public class BigFileService extends Service {
    private static final String[] videoArr = new String[]{"mp4", "3gp", "mov", "rm", "rmvb", "wmv", "m4v", "webm", "swf", "bdmv", "mpeg", "mkv", "avi", "3gpp", "f4v", "xvid", "mpeg4"};
    private static final String[] imageArr = new String[]{"jpg", "gif", "bmp", "jpeg", "png", "psd", "svg", "ai", "ps", "tif", "tiff", "raw"};
    private static final String[] audioArr = new String[]{"mp3", "aac", "flac", "ogg", "wma", "wav", "ape", "m4a", "mid", "wave", "caf", "m4r", "m3u", "ac3", "mka"};
    private static final String[] documentArr = new String[]{"doc", "docx", "xlsx", "xls", "ppt", "pptx", "pdf", "csv", "wps", "txt", "epub", "mobi", "rtf", "pages", "numbers", "key"};
    private static final String[] rarelyArr = new String[]{"7z", "gz", "ace", "tar", "lzma", "rar", "zip", "img", "iso", "dmg", "pack", "lzh", "vhd", "apk", "log", "tmp", "temp", "cache"};
    private static final TreeSet<String> mVideoSet = new TreeSet<>(Arrays.asList(videoArr));
    private static final TreeSet<String> mImageSet = new TreeSet<>(Arrays.asList(imageArr));
    private static final TreeSet<String> mAudioSet = new TreeSet<>(Arrays.asList(audioArr));
    private static final TreeSet<String> mDocumentSet = new TreeSet<>(Arrays.asList(documentArr));
    private static final TreeSet<String> mRarelySet = new TreeSet<>(Arrays.asList(rarelyArr));
    private OnBigFileListener mOnBigFileListener;
    private AudioTask mAudioTask;
    private MediaTask mMediaTask;
    private DocumentTask mDocumentTask;
    private RarelyTask mRarelyTask;

    @Override
    public IBinder onBind(Intent intent) {
        return new BigFileBinder();
    }

    public class BigFileBinder extends Binder {
        public BigFileService getService() {
            return BigFileService.this;
        }
    }

    public void setOnBigFileListener(OnBigFileListener listener) {
        mOnBigFileListener = listener;
    }

    public void startScan() {
        Executor executor = Executors.newFixedThreadPool(4);
        mMediaTask = new MediaTask(this);
        mMediaTask.executeOnExecutor(executor);
        mAudioTask = new AudioTask(this);
        mAudioTask.executeOnExecutor(executor);
        mDocumentTask = new DocumentTask(this);
        mDocumentTask.executeOnExecutor(executor);
        mRarelyTask = new RarelyTask(this);
        mRarelyTask.executeOnExecutor(executor);
    }

    public void stopScan() {
        if (mMediaTask != null) mMediaTask.cancel(true);
        if (mAudioTask != null) mAudioTask.cancel(true);
        if (mDocumentTask != null) mDocumentTask.cancel(true);
        if (mRarelyTask != null) mRarelyTask.cancel(true);
    }

    private static abstract class BigFileTask extends AsyncTask<Void, Long, List<BigFileInfo>> {
        private void setProgress(long size) {
            publishProgress(size);
        }
    }

    private static class MediaTask extends BigFileTask {
        private WeakReference<BigFileService> mReference;

        MediaTask(BigFileService service) {
            mReference = new WeakReference<>(service);
        }

        @Override
        protected void onPreExecute() {
            BigFileService service = mReference.get();
            if (service != null && service.mOnBigFileListener != null) {
                service.mOnBigFileListener.onMediaScanStart();
            }
        }

        @Override
        protected List<BigFileInfo> doInBackground(Void... voids) {
            File rootDir = Environment.getExternalStorageDirectory();
            List<BigFileInfo> mediaFileInfos = scanMediaFiles(rootDir, this, new FileSize(0), 6);
            if (mediaFileInfos != null) {
                Collections.sort(mediaFileInfos);
            }
            return mediaFileInfos;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            BigFileService service = mReference.get();
            if (service != null && service.mOnBigFileListener != null) {
                service.mOnBigFileListener.onMediaScanUpdate(values[0]);
            }
        }

        @Override
        protected void onPostExecute(List<BigFileInfo> bigFileInfos) {
            BigFileService service = mReference.get();
            if (service != null && service.mOnBigFileListener != null) {
                service.mOnBigFileListener.onMediaScanDone(bigFileInfos);
            }
        }
    }

    private static class AudioTask extends BigFileTask {
        private WeakReference<BigFileService> mReference;

        AudioTask(BigFileService service) {
            mReference = new WeakReference<>(service);
        }

        @Override
        protected void onPreExecute() {
            BigFileService service = mReference.get();
            if (service != null && service.mOnBigFileListener != null) {
                service.mOnBigFileListener.onAudioScanStart();
            }
        }

        @Override
        protected List<BigFileInfo> doInBackground(Void... voids) {
            File rootDir = Environment.getExternalStorageDirectory();
            List<BigFileInfo> mediaFileInfos = scanBigFiles(rootDir, mAudioSet, BigFileInfo.STYLE_AUDIO, this, new FileSize(0), 6);
            if (mediaFileInfos != null) {
                Collections.sort(mediaFileInfos);
            }
            return mediaFileInfos;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            BigFileService service = mReference.get();
            if (service != null && service.mOnBigFileListener != null) {
                service.mOnBigFileListener.onAudioScanUpdate(values[0]);
            }
        }

        @Override
        protected void onPostExecute(List<BigFileInfo> bigFileInfos) {
            BigFileService service = mReference.get();
            if (service != null && service.mOnBigFileListener != null) {
                service.mOnBigFileListener.onAudioScanDone(bigFileInfos);
            }
        }
    }

    private static class DocumentTask extends BigFileTask {
        private WeakReference<BigFileService> mReference;

        DocumentTask(BigFileService service) {
            mReference = new WeakReference<>(service);
        }

        @Override
        protected void onPreExecute() {
            BigFileService service = mReference.get();
            if (service != null && service.mOnBigFileListener != null) {
                service.mOnBigFileListener.onDocumentStart();
            }
        }

        @Override
        protected List<BigFileInfo> doInBackground(Void... voids) {
            File rootDir = Environment.getExternalStorageDirectory();
            List<BigFileInfo> mediaFileInfos = scanBigFiles(rootDir, mDocumentSet, BigFileInfo.STYLE_DOCUMENT, this, new FileSize(0), 7);
            if (mediaFileInfos != null) {
                Collections.sort(mediaFileInfos);
            }
            return mediaFileInfos;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            BigFileService service = mReference.get();
            if (service != null && service.mOnBigFileListener != null) {
                service.mOnBigFileListener.onDocumentUpdate(values[0]);
            }
        }

        @Override
        protected void onPostExecute(List<BigFileInfo> bigFileInfos) {
            BigFileService service = mReference.get();
            if (service != null && service.mOnBigFileListener != null) {
                service.mOnBigFileListener.onDocumentDone(bigFileInfos);
            }
        }
    }

    private static class RarelyTask extends BigFileTask {
        private WeakReference<BigFileService> mReference;

        RarelyTask(BigFileService service) {
            mReference = new WeakReference<>(service);
        }

        @Override
        protected void onPreExecute() {
            BigFileService service = mReference.get();
            if (service != null && service.mOnBigFileListener != null) {
                service.mOnBigFileListener.onRarelyScanStart();
            }
        }

        @Override
        protected List<BigFileInfo> doInBackground(Void... voids) {
            File rootDir = Environment.getExternalStorageDirectory();
            List<BigFileInfo> mediaFileInfos = scanBigFiles(rootDir, mRarelySet, BigFileInfo.STYLE_RARELY, this, new FileSize(0), 7);
            if (mediaFileInfos != null) {
                Collections.sort(mediaFileInfos);
            }
            return mediaFileInfos;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            BigFileService service = mReference.get();
            if (service != null && service.mOnBigFileListener != null) {
                service.mOnBigFileListener.onRarelyScanUpdate(values[0]);
            }
        }

        @Override
        protected void onPostExecute(List<BigFileInfo> bigFileInfos) {
            BigFileService service = mReference.get();
            if (service != null && service.mOnBigFileListener != null) {
                service.mOnBigFileListener.onRarelyScanDone(bigFileInfos);
            }
        }
    }

    private static class FileSize {
        private long mSize;

        FileSize(long size) {
            mSize = size;
        }

        public long getSize() {
            return mSize;
        }

        public void setSize(long size) {
            mSize = size;
        }
    }

    public static List<BigFileInfo> scanMediaFiles(File directory, @NonNull BigFileTask task, FileSize fileSize, int maxLevel) {
        if (directory == null || !directory.isDirectory()) return null;
        File[] files = directory.listFiles();
        if (files != null) {
            List<BigFileInfo> bigFileInfos = new ArrayList<>();
            for (File file : files) {
                if (file != null) {
                    if (file.isDirectory()) {
                        if (maxLevel - 1 > 0) {
                            List<BigFileInfo> fileInfos = scanMediaFiles(file, task, fileSize, maxLevel - 1);
                            if (fileInfos != null && fileInfos.size() > 0) {
                                bigFileInfos.addAll(fileInfos);
                            }
                        }
                    } else {
                        String fileName = file.getAbsolutePath();
                        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
                        if (mVideoSet.contains(suffix) && file.length() > 0) {
                            BigFileInfo info = new BigFileInfo(file.getPath(), file.length(), BigFileInfo.STYLE_VIDEO, file.getName(), suffix, file, file.lastModified());
                            bigFileInfos.add(info);
                            fileSize.setSize(fileSize.getSize() + file.length());
                            task.setProgress(fileSize.getSize());
                        } else if (mImageSet.contains(suffix) && file.length() > 0) {
                            BigFileInfo info = new BigFileInfo(file.getPath(), file.length(), BigFileInfo.STYLE_IMAGE, file.getName(), suffix, file, file.lastModified());
                            bigFileInfos.add(info);
                            fileSize.setSize(fileSize.getSize() + file.length());
                            task.setProgress(fileSize.getSize());
                        }
                    }
                }
            }
            return bigFileInfos;
        }
        return null;
    }

    public static List<BigFileInfo> scanBigFiles(File directory, @NonNull TreeSet<String> suffixSet, int style, @NonNull BigFileTask task, FileSize fileSize, int maxLevel) {
        if (directory == null || !directory.isDirectory()) return null;
        File[] files = directory.listFiles();
        if (files != null) {
            List<BigFileInfo> bigFileInfos = new ArrayList<>();
            for (File file : files) {
                if (file != null) {
                    if (file.isDirectory()) {
                        if (maxLevel - 1 > 0) {
                            List<BigFileInfo> fileInfos = scanBigFiles(file, suffixSet, style, task, fileSize, maxLevel - 1);
                            if (fileInfos != null && fileInfos.size() > 0) {
                                bigFileInfos.addAll(fileInfos);
                            }
                        }
                    } else {
                        String fileName = file.getAbsolutePath();
                        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
                        if (suffixSet.contains(suffix) && file.length() > 0) {
                            BigFileInfo info = new BigFileInfo(file.getPath(), file.length(), style, file.getName(), suffix, file, file.lastModified());
                            bigFileInfos.add(info);
                            fileSize.setSize(fileSize.getSize() + file.length());
                            task.setProgress(fileSize.getSize());
                        }
                    }
                }
            }
            return bigFileInfos;
        }
        return null;
    }

}
