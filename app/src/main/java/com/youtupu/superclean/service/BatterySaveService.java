package com.youtupu.superclean.service;

import android.accessibilityservice.AccessibilityService;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.youtupu.superclean.activity.FakePowerSaveActivity;
import com.youtupu.superclean.extract.OnBatterySaveListener;
import com.youtupu.superclean.fragment.PowerDetailFragment;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.view.CustomTitleBar;
import com.youtupu.superclean.view.GearCenterView;

import java.util.ArrayList;
import java.util.List;

import com.youtupu.superclean.R;

import com.youtupu.superclean.activity.PowerSaveActivity;

import ezy.assist.compat.SettingsCompat;

/**
 * Created by Jiali on 2019/2/13.
 */

                    public class BatterySaveService extends AccessibilityService implements OnBatterySaveListener {
    private static final String ACTION_BATTERY_SERVICE = "battery_save_service";
    private static final String EXTRA_ACTION_NAME = "extra_action_name";
    private static final String EXTRA_PROGRESS = "extra_progress";
    private static final String CONTINUE_TASK = "continue_task";
    private static final String HIDE_COVER = "hide_cover";
    private static final String SHUT_DOWN = "shut_down";
    public static boolean isStarted = false;

    private AccessibilityNodeTracker currentTracker = null;
    private View cover;
    private WindowManager windowManager;
    private Handler handler;
    private TaskListener listener;
    private int taskTotal = 0;
    private int taskDone = 0;
    private GearCenterView mGearCenterView;
    private TextView mTvCenter;
    private RelativeLayout mRlContent;
    private AnimatorSet mAnimatorSet;

    private int status = -1;
    private final int STATE_START = 0;
    private final int STATE_PAUSE = 1;
    private final int STATE_STOP = 2;
    private final int STATE_CALLING = 3;

    private WindowManager.LayoutParams coverParams;
    private BatteryServiceReceiver serviceReceiver;

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (event.getSource() == null || currentTracker == null) {
            return;
        }

        CommonUtils.debug("Current State : " + status);
        if ((isPauseEvent(event) || isRecentKeyEvent(event)) && status == STATE_START) {
            CommonUtils.debug("Detect Pause Event");
            AnalyticsUtil.logEvent(this, "Pause in Battery Saving", null);
            onTaskPause();
            return;
        } else if (isCallingEvent(event) && status == STATE_START) {
            CommonUtils.debug("Detect CallingEvent");
            AnalyticsUtil.logEvent(this, "Calling in Battery Saving", null);
            showFakeActivity();
            return;
        } else if (isResumeEvent(event) && status == STATE_PAUSE) {
            CommonUtils.debug("Detect Resume Event");
            AnalyticsUtil.logEvent(this, "Resuming in Battery Saving", null);
            resumeTask();
            return;
        }

        if (status != STATE_START) {
            return;
        }

        int result = currentTracker.onEvent(this, event);
        if (result == AccessibilityNodeTracker.RESULT_SUCCESS || result == AccessibilityNodeTracker.RESULT_FAIL) {
//            performGlobalAction(GLOBAL_ACTION_BACK);
            CommonUtils.debug("click back " + taskDone);
            taskDone++;
            updateTaskProgress();
            if (taskDone < taskTotal) {
                runTask(taskDone);
            } else {
                finishTask();
            }
        }
        CommonUtils.debug("task progress " + taskDone);
    }

    private void updateTaskProgress() {
        mTvCenter.setText(String.format(getString(R.string.tv_kill_process), taskDone, taskTotal));
        if (null != listener) {
            listener.onProgress(taskDone);
        }
    }

    private void runTask(final int taskDone) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null == currentTracker) {
                    return;
                }
                CommonUtils.debug("runTask " + taskDone);
                if(taskDone >= packageList.size()){
                    return;
                }
                currentTracker.begin(trackTimeoutListener, BatterySaveService.this);
                String packageName = packageList.get(taskDone);
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.setData(Uri.fromParts("package", packageName, null));
                startActivity(intent);
                CommonUtils.debug("start stop task " + packageName);
            }
        }, 500);

    }

    private void showFakeActivity() {
        status = STATE_CALLING;
        Intent intent = new Intent(this, FakePowerSaveActivity.class);
        intent.putExtra("currentProgress", taskDone);
        intent.putExtra("totalTask", taskTotal);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void resumeTask() {
        status = STATE_START;
        if (taskDone < packageList.size()) {
            showCover();
            runTask(taskDone);
        } else {
            goOptimize();
        }
    }

    private void goOptimize() {
        if (null != listener) {
            listener.onFinish();
        }
    }

    private boolean isResumeEvent(AccessibilityEvent event) {
        if (status != STATE_PAUSE) {
            return false;
        }

        if (TextUtils.isEmpty(event.getClassName())) {
            return false;
        }

        ComponentName componentName = new ComponentName(
                event.getPackageName().toString(),
                event.getClassName().toString()
        );
        try {
            ActivityInfo activityInfo = getPackageManager().getActivityInfo(componentName, 0);
            return event.getPackageName().equals(getPackageName());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void goBack() {
        shutDown();
        Intent intent = new Intent(this, PowerSaveActivity.class);
        // 因为之前拉起过PowerSaveActivity,所以即使
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private boolean isBackKeyEvent() {
        return false;
    }

    private void onTaskPause() {
        status = STATE_PAUSE;
        hideCover();
        if (null != listener) {
            listener.onPause();
        }
    }

    private boolean isCallingEvent(AccessibilityEvent event) {
        if (TextUtils.isEmpty(event.getPackageName())) {
            return false;
        }
        if (event.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED
                && (event.getPackageName().toString().toLowerCase().contains("dialer") ||
                event.getPackageName().toString().toLowerCase().contains("incallui"))) {
            return true;
        } else if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED
                && (event.getClassName().toString().toLowerCase().contains("InCallActivity") ||
                event.getPackageName().toString().toLowerCase().contains("incallui"))) {
            return true;
        }
        return false;
    }

    private boolean isRecentKeyEvent(@NonNull AccessibilityEvent event) {
        if (TextUtils.isEmpty(event.getClassName())) {
            return false;
        }
        return event.getClassName().toString().toLowerCase().contains("recents");
    }

    private boolean isPauseEvent(@NonNull AccessibilityEvent event) {
        if (TextUtils.isEmpty(event.getClassName())) {
            return false;
        }
        return event.getClassName().toString().toLowerCase().contains("launcher");
    }

    @Override
    public void onInterrupt() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        isStarted = true;
        initCover();
        windowManager = ((WindowManager) getSystemService(WINDOW_SERVICE));
        handler = new Handler();
        serviceReceiver = new BatteryServiceReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(serviceReceiver, new IntentFilter(ACTION_BATTERY_SERVICE));
    }

    private void initCover() {
        cover = View.inflate(this, R.layout.kill_process_view, null);
        CustomTitleBar customTitleBar = cover.findViewById(R.id.custom_title_bar);
        customTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                goBack();
            }
        });
        mGearCenterView = cover.findViewById(R.id.gear_center_view);
        mTvCenter = cover.findViewById(R.id.tv_center);
        mRlContent = cover.findViewById(R.id.rl_content);
        cover.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        PowerDetailFragment.setOnBatterySaveListener(this);
        Intent intent = new Intent(PowerDetailFragment.ACTION_ACCESSIBILITY);
        intent.putExtra(PowerDetailFragment.EXTRA_ACCESSIBILITY, true);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(serviceReceiver);
        onTaskPause();
        isStarted = false;
    }

    @Override
    public void killProcess(@NonNull List<String> packageNameList, final TaskListener listener) {
        if (packageNameList.size() == 0) {
            return;
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("start_power_activity"));
        status = STATE_START;
        taskTotal = packageNameList.size();
        taskDone = 0;
        this.listener = listener;
        createCoverView();
        showCover();
        currentTracker = new StopPackage(this);
        this.packageList.clear();
        this.packageList.addAll(packageNameList);
        if (!packageList.isEmpty()) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    runTask(0);
                }
            }, 2000);
        }
    }

    public static void sendContinueTaskBroadcast(Context context, int progress) {
        if (null == context) {
            return;
        }
        if (progress < 0) {
            return;
        }
        Intent intent = new Intent();
        intent.setAction(ACTION_BATTERY_SERVICE);
        intent.putExtra(EXTRA_PROGRESS, progress);
        intent.putExtra(EXTRA_ACTION_NAME, CONTINUE_TASK);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void sendHideCoverBroadcast(Context context) {
        if (null == context) {
            return;
        }
        Intent intent = new Intent();
        intent.setAction(ACTION_BATTERY_SERVICE);
        intent.putExtra(EXTRA_ACTION_NAME, HIDE_COVER);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void sendShutDownBroadcast(Context context) {
        if (null == context) {
            return;
        }
        Intent intent = new Intent();
        intent.setAction(ACTION_BATTERY_SERVICE);
        intent.putExtra(EXTRA_ACTION_NAME, SHUT_DOWN);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private void createCoverView() {
        if (null != windowManager && null != cover && SettingsCompat.canDrawOverlays(this.getApplicationContext())) {
            mTvCenter.setText(String.format(getString(R.string.tv_kill_process), 0, taskTotal));
            DisplayMetrics metrics = new DisplayMetrics();
            windowManager.getDefaultDisplay().getRealMetrics(metrics);
            int width = metrics.widthPixels;
            int height = metrics.heightPixels - getStatusBarHeight();
            int flag = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_FULLSCREEN |
                    WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                coverParams = new WindowManager.LayoutParams(
                        width, height,
                        WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                        flag,
                        PixelFormat.TRANSLUCENT);
            } else {
                coverParams = new WindowManager.LayoutParams(
                        width, height,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        flag,
                        PixelFormat.TRANSLUCENT);
            }
            coverParams.y = getStatusBarHeight();

            ObjectAnimator gearRunAnimator = ObjectAnimator.ofFloat(mGearCenterView, GearCenterView.ANGLE, 0, -360);
            gearRunAnimator.setDuration(500);
            gearRunAnimator.setInterpolator(new LinearInterpolator());
            gearRunAnimator.setRepeatCount(5);
            ObjectAnimator dismissAnim = ObjectAnimator.ofFloat(mRlContent, "alpha", 1f, 0f);
            dismissAnim.setDuration(3000);
            ObjectAnimator gradualAnim = ObjectAnimator.ofFloat(mGearCenterView, GearCenterView.PERCENT, 0.5f, 0f);
            gradualAnim.setDuration(3000);
            mAnimatorSet = new AnimatorSet();
            mAnimatorSet.playTogether(gearRunAnimator, dismissAnim, gradualAnim);
            mGearCenterView.setPercent(0.5f);
            mRlContent.setAlpha(1.0f);
        }
    }

    private void showCover() {
        if (!cover.isAttachedToWindow() && null != coverParams) {
            try {
                windowManager.addView(cover, coverParams);
            }catch (Exception exp){
                exp.printStackTrace();
            }
        }
        if (listener != null) {
            listener.afterAddFloatView();
        }
    }

    private final List<String> packageList = new ArrayList<>();

    @Override
    public void appIsShowing(boolean show) {
        //当自己app显示时，就不再kill，不再接收access事件
    }

    @Override
    public void finish() {
        listener = null;
    }

    private void tryBackToHostActivity(@NonNull AccessibilityEvent event) {
        CommonUtils.debug("tryBackToHostActivity");
        if (TextUtils.isEmpty(event.getPackageName()) || TextUtils.isEmpty(event.getClassName())) {
            return;
        }
        if (event.getClassName().toString().contains("Dialog")) {
            CommonUtils.debug("perform back in Dialog");
            performGlobalAction(GLOBAL_ACTION_BACK);
            SystemClock.sleep(100);
            return;
        }

        finishTask();
    }

    private void finishTask() {
        status = STATE_STOP;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null != mAnimatorSet) {
                    mAnimatorSet.start();
                }
                // 使用Handler是因为onAnimationEnd不可靠
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        goOptimize();
                    }
                }, 3000);
            }
        }, 500);
    }

    private void hideCover() {
        if (cover.isAttachedToWindow()) {
            windowManager.removeView(cover);
        }
    }

    private void shutDown() {
        handler.removeCallbacksAndMessages(null);
        // 解决荣耀10再次省电时，点击省电没有反应的问题。
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            disableSelf();
        }
        status = STATE_STOP;
        currentTracker = null;
        listener = null;
    }

    private void continueTask(int currentProgress) {
        status = STATE_START;
        taskDone = currentProgress;
        mTvCenter.setText(String.format(getString(R.string.tv_kill_process), taskDone, taskTotal));
        showCover();
        runTask(taskDone);
    }

    public interface TaskListener {
        void onProgress(int progress);

        void afterAddFloatView();

        void onFinish();

        void onPause();

        void onResume();

    }

    @Override
    protected boolean onKeyEvent(KeyEvent paramKeyEvent) {
        if (status != STATE_START) {
            return false;
        }
        CommonUtils.debug("onKeyEvent" + paramKeyEvent.getKeyCode());
        switch (paramKeyEvent.getKeyCode()) {
            case KeyEvent.KEYCODE_HOME:
                onTaskPause();
                break;
            case KeyEvent.KEYCODE_MENU:
                onTaskPause();
                break;
            default:
                break;
        }

        return false;
    }

    private int getStatusBarHeight() {
        int height = 0;
        int resourceId = getApplicationContext().getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            height = getApplicationContext().getResources().getDimensionPixelSize(resourceId);
        }
        return height;
    }

    private final AccessibilityNodeTracker.TrackTimeoutListener trackTimeoutListener = new AccessibilityNodeTracker.TrackTimeoutListener() {
        @Override
        public void onTimeOut() {
            if (status == STATE_START) {
                taskDone++;
                updateTaskProgress();
                if (taskDone == taskTotal) {
                    finishTask();
                    return;
                }
                runTask(taskDone);
            }
        }
    };

    private class BatteryServiceReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra(EXTRA_ACTION_NAME);
            switch (action) {
                case CONTINUE_TASK:
                    int progress = intent.getIntExtra(EXTRA_PROGRESS, 0);
                    continueTask(progress);
                    break;
                case SHUT_DOWN:
                    shutDown();
                    break;
                case HIDE_COVER:
                    hideCover();
                    break;
                default:
                    break;
            }
        }
    }
}
