package com.youtupu.superclean.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.RemoteViews;

import com.youtupu.superclean.activity.NotificationManagerActivity;
import com.youtupu.superclean.bean.NotificationItem;
import com.youtupu.superclean.extract.NotificationHome;
import com.youtupu.superclean.extract.OnNotificatinListener;
import com.youtupu.superclean.fragment.NotificationHomeFragment;
import com.youtupu.superclean.utils.SystemUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2018/12/12.
 */


public class NotificationService extends NotificationListenerService implements OnNotificatinListener {
    public static final String NOTIFICATION_WHITE_LIST_CHANGED = "notification_white_list_changed";
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    private HashSet<String> mWhiteList;
    private List<NotificationItem> mNotificationItemList;
    private PackageManager mPackageManager;
    private boolean mIsConneted = false;
    //防止系统回收activity时，我们还持有他的引用，导致内存泄露
    private WeakReference<NotificationHome> mHomeWeakReference;
    private NotificationReceiver mReceiver;
    private Thread mGetCacheThread;
    private int mCacheSize = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        mPackageManager = getPackageManager();
        mNotificationItemList = new ArrayList<>();
        //反序列化的耗时操作放子线程，防止ANR
        mGetCacheThread = new Thread() {
            @Override
            public void run() {
                List<NotificationItem> notificationsCache = getNotificationsCache();
                if (notificationsCache != null && notificationsCache.size() > 0) {
                    mCacheSize = notificationsCache.size();
                    if (mPackageManager != null) {
                        for (NotificationItem item : notificationsCache) {
                            try {
                                String packageName = item.getPackageName();
                                //循环获取icon图标为耗时操作
                                Drawable icon = mPackageManager.getApplicationIcon(packageName);
                                item.setIcon(icon);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    mNotificationItemList.addAll(notificationsCache);
                }
            }
        };
        mGetCacheThread.start();
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(this);
        Intent intent = new Intent(this, NotificationManagerActivity.class);
        intent.putExtra(NotificationManagerActivity.FROM_WHERE, NotificationManagerActivity.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        mBuilder.setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.notification_small_icon)
                .setVibrate(new long[]{0})
                .setSound(null);
        if (Build.VERSION.SDK_INT >= 26) {
            String id = "2";
            String notificationType = getString(R.string.notifications_box);
            NotificationChannel channel = new NotificationChannel(id, notificationType, NotificationManager.IMPORTANCE_LOW);
            channel.enableVibration(false);
            channel.setVibrationPattern(new long[]{0});
            channel.setSound(null, null);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(id);
        }
        mWhiteList = SystemUtils.readNotificationWhiteListFile(this);
        mReceiver = new NotificationReceiver();
        IntentFilter intentFilter = new IntentFilter(NOTIFICATION_WHITE_LIST_CHANGED);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, intentFilter);

    }


    @Override
    public IBinder onBind(Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equalsIgnoreCase(NotificationHomeFragment.ACTION_BIND_NOTIFICATION_SERVICE)) {
            return new NotificationBinder();
        }
        return super.onBind(intent);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        //只有在有发送通知权限的时候，才收纳别人通知，否则用户不知道我们收纳了别人通知
        if (isNotificationEnabled()) {
            String packageName = sbn.getPackageName();
            String key = sbn.getKey();
            if (sbn.isClearable() && packageName != null && mPackageManager != null && !mWhiteList.contains(packageName) && !packageName.equalsIgnoreCase(getPackageName())) {
                try {
                    Notification notification = sbn.getNotification();
                    if (notification != null) {
                        PendingIntent contentIntent = notification.contentIntent;
                        long time = notification.when;
                        Bundle extras = notification.extras;
                        String title = "";
                        String content = "";
                        if (extras != null) {
                            title = extras.getString(Notification.EXTRA_TITLE);
                            content = extras.getString(Notification.EXTRA_TEXT);
                        }
                        Drawable icon = mPackageManager.getApplicationIcon(packageName);
                        String label = "";
                        ApplicationInfo info = mPackageManager.getApplicationInfo(packageName, 0);
                        if (info != null) {
                            label = mPackageManager.getApplicationLabel(info).toString();
                        }
                        NotificationItem item = new NotificationItem(packageName, title, content, contentIntent, true, time, icon, label);
                        mNotificationItemList.add(item);
                        Collections.sort(mNotificationItemList);

                        cancelNotification(key);
                        updateCleanedNotificationCount();
                        //对于由自己toggle后拉起的场景
                        if (mHomeWeakReference != null && mHomeWeakReference.get() != null) {
                            mHomeWeakReference.get().onListenerUpdate(mNotificationItemList);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

//    private Drawable getSmallIcon(Context context, String pkgName, int id) {
//        Drawable drawable = null;
//        try {
//            Context remotePkgContext = context.createPackageContext(pkgName, 0);
//            drawable = remotePkgContext.getResources().getDrawable(id);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return drawable;
//    }

    @Override
    public void onListenerConnected() {
        mIsConneted = true;
        if (isNotificationEnabled()) {
            //解决极个别java.lang.SecurityException问题
            StatusBarNotification[] activeNotifications;
            try {
                activeNotifications = getActiveNotifications();
            } catch (Exception e) {
                return;
            }
            if (activeNotifications != null) {
                List<NotificationItem> acceptList = new ArrayList<>();
                for (StatusBarNotification sbn : activeNotifications) {
                    if (sbn.isClearable()) {
                        String packageName = sbn.getPackageName();
                        if (packageName != null && mPackageManager != null && !mWhiteList.contains(packageName) && !packageName.equalsIgnoreCase(getPackageName())) {
                            try {
                                Notification notification = sbn.getNotification();
                                if (notification != null) {
                                    PendingIntent contentIntent = notification.contentIntent;
                                    long time = notification.when;
                                    Bundle extras = notification.extras;
                                    String title = "";
                                    String content = "";
                                    if (extras != null) {
                                        title = extras.getString(Notification.EXTRA_TITLE);
                                        content = extras.getString(Notification.EXTRA_TEXT);
                                    }
                                    Drawable icon = mPackageManager.getApplicationIcon(packageName);
                                    ApplicationInfo info = mPackageManager.getApplicationInfo(packageName, 0);
                                    String label = "";
                                    if (info != null) {
                                        label = mPackageManager.getApplicationLabel(info).toString();
                                    }
                                    NotificationItem item = new NotificationItem(packageName, title, content, contentIntent, true, time, icon, label);
                                    acceptList.add(item);
                                    cancelNotification(sbn.getKey());
                                }
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                if (acceptList.size() > 0) {
                    //若序列化已完成，则直接添加数据，否则序列化数据不要了,此场景基本不会出现
                    if (mGetCacheThread.isAlive()) {
                        mGetCacheThread.interrupt();
                        if (mCacheSize > 0 && mNotificationItemList.size() != mCacheSize) {
                            mNotificationItemList.clear();
                        }
                    }
                    mNotificationItemList.addAll(acceptList);
                }
            }
            Collections.sort(mNotificationItemList);
            updateCleanedNotificationCount();
            if (mHomeWeakReference != null && mHomeWeakReference.get() != null) {
                mHomeWeakReference.get().onListenerConnected(mNotificationItemList);
            }
        }
    }

    @Override
    public void onListenerDisconnected() {
        mIsConneted = false;
        if (mHomeWeakReference != null && mHomeWeakReference.get() != null) {
            mHomeWeakReference.get().onListenerDisConnected();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    private void saveNotificationsCache() {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        int num = mNotificationItemList.size();
        try {
            File file = new File(getFilesDir(), "notificaions_cache.txt");
            if (num == 0) {
                if (file.exists()) {
                    file.delete();
                }
                return;
            }
            if (!file.exists() && num > 0) {
                file.createNewFile();
            }
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(mNotificationItemList);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private ArrayList<NotificationItem> getNotificationsCache() {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        ArrayList<NotificationItem> cacheList = null;
        try {
            File file = new File(getFilesDir(), "notificaions_cache.txt");
            if (!file.exists()) {
                return null;
            }
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            cacheList = (ArrayList<NotificationItem>) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return cacheList;
    }

    //
    private void toggleNotificationListenerService() {
        ComponentName name = new ComponentName(this, NotificationService.class);
        PackageManager pm = getPackageManager();
        pm.setComponentEnabledSetting(name, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
        pm.setComponentEnabledSetting(name, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
    }

    private void updateCleanedNotificationCount() {
        int num = 0;
        if (mNotificationItemList != null) {
            num = mNotificationItemList.size();
        }
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.notification_remote_view);
        mBuilder.setContent(remoteViews);
        if (num > 0) {
            remoteViews.setImageViewResource(R.id.img_icon, R.mipmap.bell_notification);
            remoteViews.setViewVisibility(R.id.tv_accept_num, View.VISIBLE);
            remoteViews.setTextViewText(R.id.tv_accept_num, num + "");
            remoteViews.setTextViewText(R.id.tv_accept_text, getString(R.string.notification_hidden));
            remoteViews.setViewVisibility(R.id.img_delete, View.VISIBLE);
        } else {
            remoteViews.setImageViewResource(R.id.img_icon, R.mipmap.empty);
            remoteViews.setViewVisibility(R.id.tv_accept_num, View.GONE);
            remoteViews.setTextViewText(R.id.tv_accept_text, getString(R.string.notification_clean));
            remoteViews.setViewVisibility(R.id.img_delete, View.GONE);
        }
        Notification notification = mBuilder.build();
        mNotificationManager.notify(123, notification);
        saveNotificationsCache();
    }

    @Override
    public List<NotificationItem> getCleanedNotifications() {
        return mNotificationItemList;
    }

    @Override
    public void updateCleanedNotifications(List<NotificationItem> list) {
        if (list.size() != mNotificationItemList.size()) {
            mNotificationItemList.clear();
            mNotificationItemList.addAll(list);
            updateCleanedNotificationCount();
        }
    }

    @Override
    public boolean isConnected() {
        return mIsConneted;
    }

    @Override
    public void toggle() {
        toggleNotificationListenerService();
    }

    @Override
    public void setHome(NotificationHome home) {
        mHomeWeakReference = new WeakReference<>(home);
    }

    private boolean isNotificationEnabled() {
        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        return manager.areNotificationsEnabled();
    }

    public class NotificationBinder extends Binder {
        public OnNotificatinListener getService() {
            return NotificationService.this;
        }
    }

    private class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equalsIgnoreCase(NOTIFICATION_WHITE_LIST_CHANGED)) {
                    mWhiteList = SystemUtils.readNotificationWhiteListFile(NotificationService.this);
                }
            }
        }
    }

}
