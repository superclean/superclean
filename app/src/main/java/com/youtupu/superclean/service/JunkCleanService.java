package com.youtupu.superclean.service;

import android.app.Service;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.youtupu.superclean.bean.ADRule;
import com.youtupu.superclean.bean.JunkListItem;
import com.youtupu.superclean.bean.ResidualRule;
import com.youtupu.superclean.extract.OnJunkCleanListener;
import com.youtupu.superclean.extract.OnResidualCleanListener;
import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.utils.SystemUtils;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class JunkCleanService extends Service {

    private PackageManager mPackageManager;
    private JunkCleanBinder mBinder;
    private File mSdDataFile;
    private OnJunkCleanListener mOnJunkCleanListener;
    private boolean mCacheScanning = false;
    private boolean mResidualScanning = false;
    private boolean mAPKScanning = false;
    private boolean mADScanning = false;
    private List<ApplicationInfo> mPackages;
    private HashMap<String, ApplicationInfo> mMapPackages = new HashMap<>();
    private HashMap<String, ResidualRule> mUninstallResidualRules = new HashMap<>();
    private HashMap<String, ADRule> mADRules = new HashMap<>();
    private OnResidualCleanListener mOnResidualCleanListener;

    @Override
    public void onCreate() {
        mPackageManager = getPackageManager();
        if (mPackageManager != null) {
            mPackages = mPackageManager.getInstalledApplications(PackageManager.GET_META_DATA);
            for (ApplicationInfo app : mPackages) {
                mMapPackages.put(app.packageName, app);
            }
        }
        mBinder = new JunkCleanBinder();
        mSdDataFile = new File(Environment.getExternalStorageDirectory().getPath() + "/Android/data");
        if (!mSdDataFile.exists()) {
            File myDataFile = getExternalCacheDir();
            if (myDataFile != null) {
                mSdDataFile = new File(new File(myDataFile.getParent()).getParent());
            }
        }

        String rulesString = SystemUtils.readStringFromAsset(this, "junk.json");
        try {
            List<ResidualRule> ruleList = new Gson().fromJson(rulesString, new TypeToken<List<ResidualRule>>() {
            }.getType());
            for (ResidualRule rule : ruleList) {
                if (rule.getPath() != null) {
                    // 为方便比较，仅存储根目录
                    String rootPath = rule.getPath().split("/")[1];
                    mUninstallResidualRules.put(rootPath, rule);
                }
            }
        } catch (Exception exp) {
            exp.printStackTrace();
            mUninstallResidualRules = new HashMap<>();
        }

        String adString = SystemUtils.readStringFromAsset(this, "ads_rules.json");
        try {
            List<ADRule> ruleList = new Gson().fromJson(adString, new TypeToken<List<ADRule>>() {
            }.getType());
            for (ADRule rule : ruleList) {
                if (rule.getPath() != null) {
                    // 为方便比较，仅存储根目录
                    String rootPath = rule.getPath().split("/")[1];
                    mADRules.put(rootPath, rule);
                }
            }
        } catch (Exception exp) {
            exp.printStackTrace();
            mUninstallResidualRules = new HashMap<>();
        }
    }

    public class JunkCleanBinder extends Binder {
        public JunkCleanService getService() {
            return JunkCleanService.this;
        }
    }

    public void setOnJunkCleanListener(OnJunkCleanListener listener) {
        mOnJunkCleanListener = listener;
    }

    public void setOnResidualListener(OnResidualCleanListener listener) {
        mOnResidualCleanListener = listener;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void startScan() {
        if (mPackages != null) {
            new CacheScanTask(this).execute();
            new ResidualTask(this).execute();
            new UnusedApkTask(this).execute();
            new ADTask(this).execute();
        }
    }

    public void scanResidualJunk() {
        if (mPackages != null) {
            new ResidualTask(this).execute();
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public boolean isScanningOrCleaning() {
        return mCacheScanning || mResidualScanning || mAPKScanning || mADScanning;
    }

    private static class CacheScanTask extends AsyncTask<Void, FileInfo, List<JunkListItem>> {
        private long mAppCacheSize;
        private WeakReference<JunkCleanService> mReference;

        CacheScanTask(JunkCleanService service) {
            mReference = new WeakReference<>(service);
        }

        @Override
        protected void onPreExecute() {
            JunkCleanService service = mReference.get();
            if (service == null) return;
            service.mCacheScanning = true;
            if (service.mOnJunkCleanListener != null) {
                service.mOnJunkCleanListener.onCacheScanStart();
            }
        }

        @Override
        protected List<JunkListItem> doInBackground(Void... voids) {
            JunkCleanService service = mReference.get();
            if (service == null) return null;
            mAppCacheSize = 0;
            final List<JunkListItem> appList = new ArrayList<>();
            for (ApplicationInfo info : service.mPackages) {
                File file = new File(service.mSdDataFile, info.packageName);
                if (file.exists()) {
                    File cacheFile = new File(file, "cache");
                    long size = SystemUtils.getFolderSizeByLinux(cacheFile);
                    //文件大于10KB
                    if (size > 1000 * 10) {
                        JunkListItem item = new JunkListItem(info.packageName,
                                info.loadLabel(service.mPackageManager).toString(), info.loadIcon(service.mPackageManager), size, cacheFile);
                        //只扫描非系统应用
                        if ((info.flags & ApplicationInfo.FLAG_SYSTEM) != ApplicationInfo.FLAG_SYSTEM) {
                            mAppCacheSize += size;
                            appList.add(item);
                        }
                        FileInfo fileInfo = new FileInfo();
                        fileInfo.setFilePath(cacheFile.getPath());
                        fileInfo.setTotalSize(mAppCacheSize);
                        publishProgress(fileInfo);
                    }
                }
            }
            return appList;
        }

        @Override
        protected void onProgressUpdate(FileInfo... values) {
            JunkCleanService service = mReference.get();
            if (service == null) return;
            if (service.mOnJunkCleanListener != null) {
                FileInfo fileInfo = values[0];
                service.mOnJunkCleanListener.onCacheScanUpdate(fileInfo.getTotalSize(), fileInfo.getFilePath());
            }
        }


        @Override
        protected void onPostExecute(List<JunkListItem> listItems) {
            JunkCleanService service = mReference.get();
            if (service == null) return;
            if (service.mOnJunkCleanListener != null) {
                if (listItems != null) {
                    service.mOnJunkCleanListener.onCacheScanDone(listItems);
                } else {
                    service.mOnJunkCleanListener.onCacheScanDone(null);
                }
            }
            service.mCacheScanning = false;
        }
    }

    private static class FileInfo {
        private long totalSize;
        private String filePath;

        private long getTotalSize() {
            return totalSize;
        }

        private String getFilePath() {
            return filePath;
        }

        private void setTotalSize(long totalSize) {
            this.totalSize = totalSize;
        }

        private void setFilePath(String filePath) {
            this.filePath = filePath;
        }
    }

    private static class ResidualTask extends AsyncTask<Void, FileInfo, List<JunkListItem>> {
        private long mResidualSize;
        private long mScanFileNum;
        private HashSet<String> mFirstNameHashSet;
        private WeakReference<JunkCleanService> mReference;

        ResidualTask(JunkCleanService service) {
            mReference = new WeakReference<>(service);
        }

        @Override
        protected void onPreExecute() {
            JunkCleanService service = mReference.get();
            if (service == null) return;
            service.mResidualScanning = true;
            if (service.mOnJunkCleanListener != null) {
                service.mOnJunkCleanListener.onResidualScanStart();
            }
        }

        @Override
        protected List<JunkListItem> doInBackground(Void... voids) {
            JunkCleanService service = mReference.get();
            if (service == null) return null;
            mResidualSize = 0;
            mScanFileNum = 0;
            List<JunkListItem> residualList1 = scanUninstallResidual();
            List<JunkListItem> result = new ArrayList<>(residualList1);
            mFirstNameHashSet = new HashSet<>();
            //记录所有包名的抬头：com、cn、android等
            for (ApplicationInfo info : service.mPackages) {
                String packageName = info.packageName;
                String[] split = packageName.split("\\.");
                if (split.length > 0) {
                    String header = split[0];
                    mFirstNameHashSet.add(header);
                }
            }
            List<JunkListItem> residualList2 = scanAllComAndCnFile(service, Environment.getExternalStorageDirectory(), 4);
            if (residualList2 != null && residualList2.size() > 0) {
                result.addAll(residualList2);
            }
            return result;
        }

        //遍历寻找com、cn开头的目录，只遍历level层目录
        private List<JunkListItem> scanAllComAndCnFile(@NonNull JunkCleanService service, File directory, int level) {
            if (isCancelled()) {
                return null;
            }
            if (directory == null || !directory.isDirectory()) return null;
            //不扫描sd卡/Android/目录
            if (directory.getPath().equals(service.mSdDataFile.getPath())) return null;
            File[] files = directory.listFiles();
            if (files != null) {
                List<JunkListItem> residualList = new ArrayList<>();
                FileInfo fileInfo = new FileInfo();
                for (File file : files) {
                    if (file != null) {
                        mScanFileNum++;
                        //每扫描100个文件，打印下文件path
                        if (mScanFileNum % 100 == 0) {
                            fileInfo.setTotalSize(0);
                            fileInfo.setFilePath(file.getPath());
                            publishProgress(fileInfo);
                        }
                        String fileName = file.getName().trim();
                        //首先将以.开头的文件的第一个点去掉
                        if (fileName.contains(".") && fileName.indexOf(".") == 0) {
                            fileName = fileName.substring(1);
                        }
                        //然后将文件的后缀名去掉
                        if (file.isFile() && fileName.contains(".")) {
                            fileName = fileName.substring(0, fileName.lastIndexOf("."));
                        }
                        String[] array = fileName.split("\\.");
                        //扫描所有以XX.XX命名的文件
                        if (array.length > 1) {
                            String start = array[0].toLowerCase();
                            //不扫以.开头的文件
                            if (mFirstNameHashSet.contains(start)) {
                                boolean isOnUse = false;
                                for (ApplicationInfo info : service.mPackages) {
                                    //判断文件名是否和当前手机里的应用包名相同，有的话说明该文件有用，否则就是卸载残留
                                    String packageName = info.packageName;
                                    String[] split = packageName.split("\\.");
                                    if (split.length > 1 && split[0].equalsIgnoreCase(start) && split[1].equalsIgnoreCase(array[1])) {
                                        isOnUse = true;
                                    }
                                }
                                if (!isOnUse) {
                                    long size;
                                    if (file.isDirectory()) {
                                        size = SystemUtils.getFolderSizeByLinux(file);
                                    } else {
                                        size = file.length();
                                    }
                                    //文件大于10KB
                                    if (size > 1000 * 10) {
                                        JunkListItem item = new JunkListItem(null, null, null, size, file);
                                        residualList.add(item);
                                        mResidualSize += size;
                                        fileInfo.setFilePath(file.getPath());
                                        fileInfo.setTotalSize(mResidualSize);
                                        publishProgress(fileInfo);
                                    }
                                }
                            }
                        }
                        if (file.isDirectory() && (level - 1) > 0) {
                            List<JunkListItem> childResidualList = scanAllComAndCnFile(service, file, level - 1);
                            if (childResidualList != null && childResidualList.size() > 0) {
                                residualList.addAll(childResidualList);
                            }
                        }
                    }
                }
                return residualList;
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(FileInfo... values) {
            JunkCleanService service = mReference.get();
            if (service == null) return;
            if (service.mOnJunkCleanListener != null) {
                FileInfo fileInfo = values[0];
                service.mOnJunkCleanListener.onResidualScanUpdate(fileInfo.getTotalSize(), fileInfo.getFilePath());
            }
        }

        @Override
        protected void onPostExecute(List<JunkListItem> items) {
            JunkCleanService service = mReference.get();
            if (service == null) return;
            if (service.mOnJunkCleanListener != null) {
                service.mOnJunkCleanListener.onResidualScanDone(items);
            }
            if (service.mOnResidualCleanListener != null) {
                service.mOnResidualCleanListener.onResidualScanDone(items);
            }
        }

        private List<JunkListItem> scanUninstallResidual() {
            List<JunkListItem> result = new ArrayList<>();
            JunkCleanService service = mReference.get();
            if (null == service) {
                return result;
            }

            File file = Environment.getExternalStorageDirectory();
            File dirs[] = file.listFiles();
            if (null == dirs) {
                return result;
            }
            for (File dir : dirs) {
                String dirName = dir.getName();
                ResidualRule rule = service.mUninstallResidualRules.get(dirName);
                if (rule == null) {
                    continue;
                }
                // 全路径是否存在
                File fileResidual = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                        + File.separator + rule.getPath());
                if (!fileResidual.exists()) {
                    continue;
                }
                if (!packageExist(service.mUninstallResidualRules.get(dirName))) {
                    CommonUtils.debug("detect uninstall residual " + dirName);
                    long size;
                    if (dir.isDirectory()) {
                        size = SystemUtils.getFolderSizeByLinux(dir);
                    } else {
                        size = dir.length();
                    }
                    JunkListItem item = new JunkListItem(null, null, null, size, dir);
                    result.add(item);
                    mResidualSize += size;
                    FileInfo fileInfo = new FileInfo();
                    fileInfo.setFilePath(file.getPath());
                    fileInfo.setTotalSize(mResidualSize);
                    publishProgress(fileInfo);
                }
            }
            return result;
        }

        private boolean packageExist(ResidualRule scanRule) {
            JunkCleanService service = mReference.get();
            if (null == service) {
                return true;
            }
            HashMap<String, ApplicationInfo> map = service.mMapPackages;
            for (String app : scanRule.getPackages()) {
                if (map.containsKey(app)) {
                    return true;
                }
            }
            return false;
        }
    }

    private static class UnusedApkTask extends AsyncTask<Void, FileInfo, List<JunkListItem>> {
        private final int MAX_DEPTH = 4;
        long mTotalSize = 0;
        private WeakReference<JunkCleanService> serviceWeakReference;

        UnusedApkTask(JunkCleanService service) {
            serviceWeakReference = new WeakReference<>(service);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            JunkCleanService service = serviceWeakReference.get();
            if (service == null) return;
            service.mAPKScanning = true;
            if (service.mOnJunkCleanListener != null) {
                service.mOnJunkCleanListener.onApkScanStart();
            }
        }

        @Override
        protected List<JunkListItem> doInBackground(Void... voids) {
            List<JunkListItem> result = new ArrayList<>();
            File rootDir = Environment.getExternalStorageDirectory();
            mTotalSize = 0;
            parseDirectories(rootDir, 1, result);
            return result;
        }

        @Override
        protected void onProgressUpdate(FileInfo... values) {
            super.onProgressUpdate(values);
            JunkCleanService service = serviceWeakReference.get();
            if (service == null) return;
            if (service.mOnJunkCleanListener != null) {
                FileInfo fileInfo = values[0];
                service.mOnJunkCleanListener.onApkScanUpdate(fileInfo.getTotalSize(), fileInfo.getFilePath());
            }
        }

        @Override
        protected void onPostExecute(List<JunkListItem> list) {
            super.onPostExecute(list);
            JunkCleanService service = serviceWeakReference.get();
            if (service == null) return;
            if (service.mOnJunkCleanListener != null) {
                service.mOnJunkCleanListener.onApkScanDone(list);
            }
        }

        private void parseDirectories(File currentDirectory, int depth, List<JunkListItem> result) {
            JunkCleanService service = serviceWeakReference.get();
            if (null == service) {
                return;
            }
            PackageManager packageManager = service.getPackageManager();
            if (currentDirectory != null && currentDirectory.isDirectory() && depth <= MAX_DEPTH) {
                File[] files = currentDirectory.listFiles();
                if (files != null) {
                    for (File file : files) {
                        if (file != null) {
                            if (!file.isDirectory() && file.getAbsolutePath().endsWith(".apk")) {
                                PackageInfo pinfo = packageManager.getPackageArchiveInfo(file.getAbsolutePath(), 0);
                                if (null == pinfo) {
                                    continue;
                                }
                                CommonUtils.debug("detect unused apk " + file.getName());
                                // 尝试在手机的已安装列表中找到apk的图标信息
                                ApplicationInfo ainfo = service.mMapPackages.get(pinfo.packageName);
                                if (null == ainfo) {
                                    ainfo = pinfo.applicationInfo;
                                }
                                result.add(new JunkListItem(ainfo.packageName, ainfo.loadLabel(packageManager).toString(),
                                        ainfo.loadIcon(packageManager), file.length(), file));
                                mTotalSize += file.length();
                                // 更新扫描进度
                                FileInfo fileInfo = new FileInfo();
                                fileInfo.setFilePath(file.getPath());
                                fileInfo.setTotalSize(mTotalSize);
                                publishProgress(fileInfo);
                            } else if (file.isDirectory()) {
                                parseDirectories(file, depth + 1, result);
                            }
                        }
                    }
                }
            }
        }
    }

    private static class ADTask extends AsyncTask<Void, FileInfo, List<JunkListItem>> {
        private long mResidualSize;
        private long mScanFileNum;
        private WeakReference<JunkCleanService> mReference;

        ADTask(JunkCleanService service) {
            mReference = new WeakReference<>(service);
        }

        @Override
        protected void onPreExecute() {
            JunkCleanService service = mReference.get();
            if (service == null) return;
            service.mADScanning = true;
            if (service.mOnJunkCleanListener != null) {
                service.mOnJunkCleanListener.onADScanStart();
            }
        }

        @Override
        protected List<JunkListItem> doInBackground(Void... voids) {
            JunkCleanService service = mReference.get();
            if (service == null) return null;
            mResidualSize = 0;
            mScanFileNum = 0;
            List<JunkListItem> junkListItems = scanADs();
            return junkListItems;
        }

        @Override
        protected void onProgressUpdate(FileInfo... values) {
            JunkCleanService service = mReference.get();
            if (service == null) return;
            if (service.mOnJunkCleanListener != null) {
                FileInfo fileInfo = values[0];
                service.mOnJunkCleanListener.onADScanUpdate(fileInfo.getTotalSize(), fileInfo.getFilePath());
            }
        }

        @Override
        protected void onPostExecute(List<JunkListItem> items) {
            JunkCleanService service = mReference.get();
            if (service == null) return;
            if (service.mOnJunkCleanListener != null) {
                service.mOnJunkCleanListener.onADScanDone(items);
            }

        }

        private List<JunkListItem> scanADs() {
            List<JunkListItem> result = new ArrayList<>();
            JunkCleanService service = mReference.get();
            if (null == service) {
                return result;
            }

            File file = Environment.getExternalStorageDirectory();
            File dirs[] = file.listFiles();
            if (null == dirs) {
                return result;
            }
            for (File dir : dirs) {
                String dirName = dir.getName();
                ADRule rule = service.mADRules.get(dirName);
                if (rule == null) {
                    continue;
                }
                // 全路径是否存在
                File fileAD = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                        + File.separator + rule.getPath());
                if (!fileAD.exists()) {
                    continue;
                }
                CommonUtils.debug("detect ad " + dirName);
                long size;
                if (dir.isDirectory()) {
                    size = SystemUtils.getFolderSizeByLinux(dir);
                } else {
                    size = dir.length();
                }
                JunkListItem item = new JunkListItem(null, rule.getDesc(), null, size, fileAD);
                result.add(item);
                mResidualSize += size;
                FileInfo fileInfo = new FileInfo();
                fileInfo.setFilePath(file.getPath());
                fileInfo.setTotalSize(mResidualSize);
                publishProgress(fileInfo);
            }
            return result;
        }

    }


}
