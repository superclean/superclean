package com.youtupu.superclean.service;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.preference.PreferenceManager;
import android.text.TextUtils;

import com.youtupu.superclean.BuildConfig;
import com.youtupu.superclean.activity.ChargingActivity;
import com.youtupu.superclean.app.SuperCleanApplication;
import com.youtupu.superclean.bean.ApkInfo;
import com.youtupu.superclean.bean.AppItem;
import com.youtupu.superclean.bean.JunkListItem;
import com.youtupu.superclean.extract.OnResidualCleanListener;
import com.youtupu.superclean.trace.EventTrace;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.ConsoleView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;

import cn.instreet.business.BusinessSDK;

import com.youtupu.superclean.R;

import com.youtupu.superclean.activity.CleanDialogActivity;

import pub.devrel.easypermissions.EasyPermissions;


public class MonitorService extends Service implements OnResidualCleanListener {
    public static final String ACTION_AFTER_OPTIMIZED = "com.youtupu.superclean.after_optimized";
    public static final String ACTION_MONITOR_DESTROY = "com.youtupu.superclean.monitor_destroy";
    public static final String ACTION_SET_NOTIFICATION = "com.youtupu.superclean.set_notification";
    public static final String ACTION_CHARGE_ON_LATER = "com.youtupu.superclean.charge_on_later";
    private static final int AUTO_SCAN = 0;
    private static final int SCAN_INTALLED_APK = 2;
    private static final int TEST = 3;
    private static final int REMIND_CHARGE = 4;
    //开发版10秒扫描一次，正式版半小时扫描一次
    public static final int AUTO_SCAN_INTERVAL = BuildConfig.DEBUG ? 1000 * 10 : 1000 * 60 * 30;
    private static final int NOTICATION_ID = 234;
    private boolean mNeedClean = false;
    private boolean mNeedPowerSave = false;
    private boolean mNeedCool = false;
    private float mBoostPercent;
    private File mSdDataFile;
    private List<ApplicationInfo> mPackages;
    private MonitorHandler mHandler;
    private CountDownLatch mCountDownLatch;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    private MonitorReceiver mMonitorReceiver;
    private PackageManager mPackageManager;
    //记录手机里所有包名对应的应用名
    private HashMap<String, String> mAppNameMap;
    private CopyOnWriteArrayList<AppItem> mCanStopAppList;
    private JunkCleanConnection mJunkCleanConnection;
    private String mAppName;
    private SuperCleanApplication mApplication;
    private SharedPreferences mSharedPreferences;

    private int laterOnTimes = 0;
    private final int MAX_LATER_ON_TIMES = 2;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = (SuperCleanApplication) getApplication();
        mSdDataFile = new File(Environment.getExternalStorageDirectory().getPath() + "/Android/data");
        if (!mSdDataFile.exists()) {
            File myDataFile = getExternalCacheDir();
            if (myDataFile != null) {
                mSdDataFile = new File(new File(myDataFile.getParent()).getParent());
            }
        }
        //第一次创建时的扫描，不需要发广播
        mBoostPercent = getBoostPercent();
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        startOverallScan();
        mHandler = new MonitorHandler(this);
        mHandler.sendEmptyMessageDelayed(AUTO_SCAN, AUTO_SCAN_INTERVAL);
        mHandler.sendEmptyMessageDelayed(TEST, 5000);
        mAppNameMap = new HashMap<>();
        updateAppNames();
        mMonitorReceiver = new MonitorReceiver();
        IntentFilter monitorFilter = new IntentFilter();
        monitorFilter.addAction(ACTION_AFTER_OPTIMIZED);
        monitorFilter.addAction(ACTION_SET_NOTIFICATION);
        monitorFilter.addAction(ACTION_CHARGE_ON_LATER);
        monitorFilter.addAction(BusinessSDK.ACTION_PACKAGE_ADDED);
        monitorFilter.addAction(BusinessSDK.ACTION_PACKAGE_REMOVED);
        monitorFilter.addAction(BusinessSDK.ACTION_CHARGER_PLUG_IN);
        monitorFilter.addAction(BusinessSDK.ACTION_CHARGER_PLUG_OFF);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMonitorReceiver, monitorFilter);
        mCanStopAppList = new CopyOnWriteArrayList<>();
        startTraceThread();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new Binder();
    }

    //全局扫描，扫描后更新控制台
    private void startOverallScan() {
        mCountDownLatch = new CountDownLatch(2);
        scanJunk();
        scanTemperature();
        scanRunningApps();
        new Thread() {
            @Override
            public void run() {
                try {
                    mCountDownLatch.await();
                    if (mSharedPreferences == null || mSharedPreferences.getBoolean("setting_notification", true)) {
                        sendConsoleView();
                    }
                    long currentTime = System.currentTimeMillis();
                    SharePreferencesUtil.getInstance(MonitorService.this).setLastOverallScanTime(currentTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void scanJunk() {
        mNeedClean = false;
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            new Thread() {
                @Override
                public void run() {
                    PackageManager packageManager = getPackageManager();
                    if (packageManager != null) {
                        List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(0);
                        if (installedApplications != null) {
                            long cacheSize = 0;
                            //开发版大于1MB认为需要清理，正式版大于100MB
                            long maxSize = BuildConfig.DEBUG ? 1000 * 1000 : 1000 * 1000 * 100;
                            for (ApplicationInfo info : installedApplications) {
                                File file = new File(mSdDataFile, info.packageName);
                                if (file.exists()) {
                                    File cacheFile = new File(file, "cache");
                                    long size = SystemUtils.getFolderSizeByLinux(cacheFile);
                                    cacheSize += size;
                                }
                                //如果总缓存大小大于10MB，则认为有垃圾
                                if (cacheSize > maxSize) {
                                    mNeedClean = true;
                                    break;
                                }
                            }
                        }
                    }
                    mCountDownLatch.countDown();
                }
            }.start();
        } else {
            mCountDownLatch.countDown();
        }
    }

    private void scanTemperature() {
        int batteryTemperature = SystemUtils.getBatteryTemperature(this);
        mNeedCool = batteryTemperature > 30;
    }

    private void scanRunningApps() {
        mNeedPowerSave = false;
        new Thread() {
            @Override
            public void run() {
                List<ApplicationInfo> runningTasks = SystemUtils.getRunningTasks(MonitorService.this);
                if (runningTasks.size() >= (BuildConfig.DEBUG ? 5 : 10)) {
                    mNeedPowerSave = true;
                }
                mCountDownLatch.countDown();
            }
        }.start();
    }

    @Override
    public void onResidualScanDone(List<JunkListItem> items) {
        if (items != null && items.size() > 0) {
            mApplication.setResidualList(items);
            mApplication.setAppCacheList(new ArrayList<JunkListItem>());
            mApplication.setUnusedAPKList(new ArrayList<JunkListItem>());
            mApplication.setADList(new ArrayList<JunkListItem>());
            Intent newIntent = new Intent(MonitorService.this, CleanDialogActivity.class);
            newIntent.putExtra("app_name", mAppName);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            newIntent.putExtra("category", 2);
            newIntent.putExtra("fromOutSide", true);
            startActivity(newIntent);
        }
        if (mJunkCleanConnection != null) {
            unbindService(mJunkCleanConnection);
        }
    }


    private static class MonitorHandler extends Handler {
        private WeakReference<MonitorService> mReference;

        MonitorHandler(MonitorService service) {
            mReference = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            MonitorService service = mReference.get();
            if (service == null) return;
            switch (msg.what) {
                case AUTO_SCAN:
                    service.startOverallScan();
                    sendEmptyMessageDelayed(AUTO_SCAN, AUTO_SCAN_INTERVAL);
                    break;
                case SCAN_INTALLED_APK:
                    Bundle data = msg.getData();
                    service.scanApkPath(data.getString("app_name"), data.getString("package_name"));
                    break;
                case REMIND_CHARGE:
                    service.notifyCharge();
                    break;

            }
        }
    }

    private void notifyCharge() {
        //如果用户关闭了充电优化的功能，则直接返回
        if (mSharedPreferences != null && !mSharedPreferences.getBoolean("setting_charge", true))
            return;
        if (!BuildConfig.DEBUG) {
            long lastTime = SharePreferencesUtil.getInstance(MonitorService.this).getLastPowerTime();
            long nowTime = System.currentTimeMillis();
            //如果距离上次省电不到5分钟，则不管
            if (nowTime - lastTime < 1000 * 60 * 5) {
                return;
            }
        }
        if (!mApplication.isPowerSaving()) {
            scanPowerConsumers();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeMessages(AUTO_SCAN);
        mHandler.removeMessages(SCAN_INTALLED_APK);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMonitorReceiver);
        //在自己销毁前，发送广播
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ACTION_MONITOR_DESTROY));
    }

    //发送控制台
    private void sendConsoleView() {
        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        if (mNotificationManager == null) return;
        ConsoleView consoleView = new ConsoleView(this);
        mBoostPercent = getBoostPercent();
        consoleView.setBoostPercent(mBoostPercent, this);
        setRedDot(consoleView);
        if (mBuilder == null) {
            mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setSmallIcon(R.mipmap.console_icon)
                    .setAutoCancel(false)
                    .setVibrate(new long[]{0})
                    .setSound(null);
            if (Build.VERSION.SDK_INT >= 26) {
                String id = "1";
                String notificationType = getString(R.string.console_name);
                NotificationChannel channel = new NotificationChannel(id, notificationType, NotificationManager.IMPORTANCE_LOW);
                channel.enableVibration(false);
                channel.setVibrationPattern(new long[]{0});
                channel.setSound(null, null);
                mNotificationManager.createNotificationChannel(channel);
                mBuilder.setChannelId(id);
            }
        }
        mBuilder.setContent(consoleView);
        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_NO_CLEAR;
        mNotificationManager.notify(NOTICATION_ID, notification);
    }

    private void setRedDot(ConsoleView consoleView) {
        //优化有效间隔，开发版10秒，正式版10分钟
        long intervalTime = BuildConfig.DEBUG ? 10 * 1000 : 10 * 60 * 1000;
        long currentTime = System.currentTimeMillis();
        long lastCleanTime = SharePreferencesUtil.getInstance(this).getLastCleanJunkTime();
        long lastCoolerTime = SharePreferencesUtil.getInstance(this).getLastCoolTime();
        long lastPowerTime = SharePreferencesUtil.getInstance(this).getLastPowerTime();
        List<Integer> showList = new ArrayList<>();
        if (lastCleanTime > 0 && currentTime - lastCleanTime > intervalTime && mNeedClean)
            showList.add(1);
        if (lastCoolerTime > 0 && currentTime - lastCoolerTime > intervalTime && mNeedCool)
            showList.add(2);
        if (lastPowerTime > 0 && currentTime - lastPowerTime > intervalTime && mNeedPowerSave)
            showList.add(3);
        int size = showList.size();
        if (size > 0) {
            int index = new Random().nextInt(size);
            if (index < showList.size()) {
                switch (index) {
                    case 1:
                        consoleView.setCleanEnable(true);
                        consoleView.setCoolingEnable(false);
                        consoleView.setBatteryEnable(false);
                        break;
                    case 2:
                        consoleView.setCleanEnable(false);
                        consoleView.setCoolingEnable(true);
                        consoleView.setBatteryEnable(false);
                        break;
                    case 3:
                        consoleView.setCleanEnable(false);
                        consoleView.setCoolingEnable(false);
                        consoleView.setBatteryEnable(true);
                        break;
                }
            }
        }
    }

    private float getBoostPercent() {
        float boostPercent;
        long lastBoostTime = SharePreferencesUtil.getInstance(this).getLastBoostTime();
        if (lastBoostTime > 0) {
            long time = System.currentTimeMillis() - lastBoostTime;
            //若距离上次加速不到5分钟，则不变
            if (time < 300000) {
                boostPercent = 0;
            } else if (time < 600000) {
                //若大于5小于10分钟，则渐变
                boostPercent = ((time - 300000) * 1.0f / 300000) * 0.8f;
            } else {
                //超过10分钟，则在0.8~1.0间随机设置
                boostPercent = getRandomBoostPercent();
            }
        } else {
            boostPercent = getRandomBoostPercent();
        }
        return boostPercent;
    }

    private float getRandomBoostPercent() {
        float boostPercent;
        long appLastOpenTime = SharePreferencesUtil.getInstance(this).getAppLastOpenTime();
        //距离上次打开app不到一分钟，则percent不变
        if (appLastOpenTime > 0 && System.currentTimeMillis() - appLastOpenTime < 60000) {
            boostPercent = SharePreferencesUtil.getInstance(this).getLastBoostResult();
            if (boostPercent < 0) {
                Random random = new Random();
                boostPercent = 0.75f + 0.2f * random.nextFloat();
            }
        } else {
            Random random = new Random();
            boostPercent = 0.75f + 0.2f * random.nextFloat();
        }
        return boostPercent;
    }

    private void updateAppNames() {
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                if (null == (mPackageManager = getPackageManager())) {
                    return;
                }
                if (null == (mPackages = mPackageManager.getInstalledApplications(0))) {
                    return;
                }
                HashMap<String, String> hashMap = new HashMap<>();
                for (ApplicationInfo info : mPackages) {
                    String packageName = info.packageName;
                    CharSequence label = mPackageManager.getApplicationLabel(info);
                    if (!TextUtils.isEmpty(packageName) && label != null && !TextUtils.isEmpty(label.toString())) {
                        hashMap.put(packageName, label.toString());
                    }
                }
                if (!hashMap.isEmpty()) {
                    mAppNameMap.clear();
                    mAppNameMap.putAll(hashMap);
                }
            }
        });
    }

    private class MonitorReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) return;
            if (ACTION_AFTER_OPTIMIZED.equals(action)) {
                //OptimizeActivity发出的广播
                boolean boostDone = intent.getBooleanExtra("boost_done", false);
                boolean cleanDone = intent.getBooleanExtra("clean_done", false);
                boolean coolerDone = intent.getBooleanExtra("cooler_done", false);
                boolean powerSaveDone = intent.getBooleanExtra("power_save_done", false);
                if (boostDone) mBoostPercent = 0;
                if (cleanDone) mNeedClean = false;
                if (coolerDone) mNeedCool = false;
                if (powerSaveDone) mNeedPowerSave = false;
                if (mSharedPreferences == null || mSharedPreferences.getBoolean("setting_notification", true)) {
                    sendConsoleView();
                }
            } else if (ACTION_SET_NOTIFICATION.equals(action)) {
                //设置界面发送的广播
                boolean showNotification = intent.getBooleanExtra("show_notification", true);
                if (showNotification) {
                    sendConsoleView();
                } else {
                    if (mNotificationManager != null) {
                        mNotificationManager.cancel(NOTICATION_ID);
                    }
                }
            } else if (BusinessSDK.ACTION_PACKAGE_ADDED.equals(action)) {
                //如果用户关闭了监听apk安装的功能，则直接返回
                if (mSharedPreferences != null && !mSharedPreferences.getBoolean("setting_apk", false))
                    return;
                //如果有存储权限才扫描
                if (EasyPermissions.hasPermissions(MonitorService.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Message message = new Message();
                    message.what = SCAN_INTALLED_APK;
                    Bundle bundle = new Bundle();
                    bundle.putString("app_name", intent.getStringExtra("app_name"));
                    bundle.putString("package_name", intent.getStringExtra("package_name"));
                    message.setData(bundle);
                    //考虑到有些机型安装后，会自动删除安装包，所以延迟2秒开始扫描
                    mHandler.sendMessageDelayed(message, 2000);
                }
            } else if (BusinessSDK.ACTION_PACKAGE_REMOVED.equals(action)) {
                //如果用户关闭了监听应用卸载的功能，则直接返回
                if (mSharedPreferences != null && !mSharedPreferences.getBoolean("setting_residual", true))
                    return;
                //判断是否有存储权限
                if (EasyPermissions.hasPermissions(MonitorService.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    if (mApplication.isJunkCleaning()) {
                        AnalyticsUtil.logEvent(MonitorService.this, "uninstall_when_cleaning", null);
                    } else {
                        //此时我们的不在清理状态时，才扫描
                        mAppName = intent.getStringExtra("app_name");
                        scanResidualJunk();
                    }

                }
            } else if (BusinessSDK.ACTION_CHARGER_PLUG_IN.equals(action)) {
                notifyCharge();
            } else if (ACTION_CHARGE_ON_LATER.equals(action)) {
                laterOnTimes++;
                if (laterOnTimes < MAX_LATER_ON_TIMES) {
                    Message message = mHandler.obtainMessage();
                    message.what = REMIND_CHARGE;
                    mHandler.sendMessageDelayed(message, BuildConfig.DEBUG ? 15000 : 600000);
                }
            } else if (BusinessSDK.ACTION_CHARGER_PLUG_OFF.equals(action)) {
                mHandler.removeCallbacksAndMessages(null);
            }
        }
    }


    private void scanApkPath(final String appName, final String packageName) {
        new Thread() {
            @Override
            public void run() {
                int versionCode = 0;
                try {
                    PackageInfo packageInfo = mPackageManager.getPackageInfo(packageName, 0);
                    versionCode = packageInfo.versionCode;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                ArrayList<ApkInfo> apkInfoList = getApkPath(packageName, Environment.getExternalStorageDirectory(), 4, versionCode);
                if (apkInfoList != null && apkInfoList.size() > 0) {
                    Intent newIntent = new Intent(MonitorService.this, CleanDialogActivity.class);
                    newIntent.putExtra("app_name", appName);
                    newIntent.putExtra("package_name", packageName);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    newIntent.putExtra("category", 1);
                    newIntent.putParcelableArrayListExtra("apk_list", apkInfoList);
                    startActivity(newIntent);
                }
            }
        }.start();
    }

    private void scanPowerConsumers() {
        new Thread() {
            @Override
            public void run() {
                getCurrentRunningServices();
                if (mCanStopAppList.size() > 0) {
                    Intent intent = new Intent(MonitorService.this, ChargingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.putExtra("app_number", mCanStopAppList.size());
                    startActivity(intent);
                }
            }
        }.start();
    }

    public void getCurrentRunningServices() {
        mCanStopAppList.clear();
        HashSet<String> ignoreList = SystemUtils.readPowerIgnoreListFile(this);
        PackageManager packageManager = getPackageManager();
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (activityManager != null && packageManager != null) {
            if (Build.VERSION.SDK_INT >= 26) {
                List<PackageInfo> packageInfos = packageManager.getInstalledPackages(PackageManager.MATCH_UNINSTALLED_PACKAGES);
                for (PackageInfo info : packageInfos) {
                    try {
                        ApplicationInfo applicationInfo = packageManager.getApplicationInfo(info.packageName, 0);
                        if (!((applicationInfo.flags & (ApplicationInfo.FLAG_STOPPED | ApplicationInfo.FLAG_SYSTEM)) > 0)) {
                            String packageName = applicationInfo.packageName;
                            //除去自己和Google Play服务
                            if (packageName != null && !packageName.equalsIgnoreCase(getPackageName()) && !packageName.equalsIgnoreCase("com.google.android.gms")) {
                                Drawable icon = applicationInfo.loadIcon(packageManager);
                                CharSequence charSequence = applicationInfo.loadLabel(packageManager);
                                String name = charSequence.toString();
                                boolean canStop = true;
                                for (String pack : ignoreList) {
                                    if (packageName.equalsIgnoreCase(pack)) {
                                        canStop = false;
                                        break;
                                    }
                                }
                                // 再和想定制的应用列表做匹配
                                Set<String> wantStopSet = getSharedPreferences("SuperClean",
                                        Context.MODE_PRIVATE).getStringSet("want_stop", new HashSet<String>());
                                if (wantStopSet.contains(packageName.toLowerCase())) {
                                    canStop = true;
                                }
                                AppItem item = new AppItem(packageName, name, icon, canStop);
                                if (canStop) {
                                    mCanStopAppList.add(item);
                                }
                            }
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                //此方法在7.0（含）以前有效
                List<ActivityManager.RunningServiceInfo> runningServices = activityManager.getRunningServices(Integer.MAX_VALUE);
                if (runningServices != null && runningServices.size() > 0) {
                    for (ActivityManager.RunningServiceInfo info : runningServices) {
                        String packageName = info.service.getPackageName();
                        //除去自己和GooglePlay服务
                        if (!packageName.equalsIgnoreCase(getPackageName()) && !packageName.equalsIgnoreCase("com.google.android.gms")) {
                            try {
                                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, 0);
                                //如果不是系统应用
                                if ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != ApplicationInfo.FLAG_SYSTEM) {
                                    Drawable icon = applicationInfo.loadIcon(packageManager);
                                    CharSequence charSequence = applicationInfo.loadLabel(packageManager);
                                    String name = charSequence.toString();
                                    boolean canStop = true;
                                    for (String pack : ignoreList) {
                                        if (packageName.equalsIgnoreCase(pack)) {
                                            canStop = false;
                                            break;
                                        }
                                    }
                                    AppItem item = new AppItem(packageName, name, icon, canStop);
                                    if (canStop) {
                                        mCanStopAppList.add(item);
                                    }
                                }
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

    //根据包名，获取手机里所有对应apk的路径
    private ArrayList<ApkInfo> getApkPath(String packageName, File directory, int level, int versionCode) {
        if (directory == null || !directory.exists() || !directory.isDirectory() || level < 1 || TextUtils.isEmpty(packageName)) {
            return null;
        }
        File[] files = directory.listFiles();
        if (files == null) {
            return null;
        }
        ArrayList<ApkInfo> pathList = new ArrayList<>();
        for (File file : files) {
            if (file == null) {
                continue;
            }
            String apkPath;
            if (file.isDirectory()) {
                ArrayList<ApkInfo> apkPaths = getApkPath(packageName, file, level - 1, versionCode);
                if (apkPaths != null) {
                    pathList.addAll(apkPaths);
                }
            } else if ((apkPath = file.getAbsolutePath().trim()).endsWith(".apk")) {
                try {
                    PackageInfo packageInfo = mPackageManager.getPackageArchiveInfo(file.getAbsolutePath(), 0);
                    if (packageName.equals(packageInfo.packageName) && (versionCode == 0 || packageInfo.versionCode == versionCode)) {
                        ApkInfo info = new ApkInfo(packageInfo.packageName, apkPath, file.length());
                        pathList.add(info);
                    }
                } catch (NullPointerException exp) {

                }
            }
        }
        return pathList;
    }

    private void scanResidualJunk() {
        if (mJunkCleanConnection == null) {
            mJunkCleanConnection = new JunkCleanConnection();
        }
        bindService(new Intent(this, JunkCleanService.class), mJunkCleanConnection, BIND_AUTO_CREATE);
    }

    private class JunkCleanConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            JunkCleanService.JunkCleanBinder binder = (JunkCleanService.JunkCleanBinder) service;
            JunkCleanService junkCleanService = binder.getService();
            junkCleanService.setOnResidualListener(MonitorService.this);
            junkCleanService.scanResidualJunk();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }

    }

    private void startTraceThread() {
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    SystemClock.sleep(BuildConfig.DEBUG ? 30000 : 3600000);
                    CommonUtils.debug("begin trace");
                    AnalyticsUtil.traceSpaceInfo();
                    EventTrace.getInstance(MonitorService.this).syncToRemote();
                }
            }
        });
    }

}





