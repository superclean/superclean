package com.youtupu.superclean.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/1/3.
 */

public class CalibrationView extends View {
    private Paint mPaint;
    private RectF mRectF;
    private int mHighColor;
    private int mLowColor;
    private int mBorder;
    private int mWidth;
    private int mHeight;
    private Paint mTextPaint;
    private String mLeftTitle;
    private String mRightTitle;
    private float mTextBaseline;

    public CalibrationView(Context context) {
        this(context, null, 0, 0);
    }

    public CalibrationView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public CalibrationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public CalibrationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mRectF = new RectF();
        mHighColor = context.getResources().getColor(R.color.colorMainPink);
        mLowColor = context.getResources().getColor(R.color.colorMainBlue);
        int textColor = context.getResources().getColor(R.color.dayModeItemTextDetail);
        mLeftTitle = context.getResources().getString(R.string.slow);
        mRightTitle = context.getResources().getString(R.string.smooth);
        int strokeWidth = SystemUtils.dp2px(context, 10);
        mBorder = strokeWidth / 2 + 1;
        mPaint = new Paint();
        mPaint.setColor(mHighColor);
        mPaint.setStrokeWidth(strokeWidth);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setAntiAlias(true);
        mTextPaint = new Paint();
        mTextPaint.setColor(textColor);
        mTextPaint.setAntiAlias(true);
        mTextPaint.setTextSize(context.getResources().getDimension(R.dimen.home_item_text_size));
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        if (!getResources().getConfiguration().locale.getLanguage().equals("zh")) {
            mTextPaint.setFakeBoldText(true);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        int radius;
        if (mWidth > mHeight) {
            radius = mHeight / 2;
        } else {
            radius = mWidth / 2;
        }
        SweepGradient sweepGradient = new SweepGradient(mWidth / 2f, mHeight / 2f, new int[]{mHighColor, mLowColor, mHighColor}, new float[]{0f, 0.42f, 1f});
        mPaint.setShader(sweepGradient);
        mRectF.set(mWidth / 2f - radius + mBorder, mHeight / 2f - radius + mBorder,
                mWidth / 2f + radius - mBorder, mHeight / 2f + radius - mBorder);
        Paint.FontMetrics numberMetrics = mTextPaint.getFontMetrics();
        mTextBaseline = mHeight / 2f - numberMetrics.bottom;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawText(mLeftTitle, mRectF.left, mTextBaseline, mTextPaint);
        canvas.drawText(mRightTitle, mRectF.right, mTextBaseline, mTextPaint);
        canvas.rotate(195, mWidth / 2f, mHeight / 2f);
        canvas.drawArc(mRectF, 0, 150, false, mPaint);
    }
}
