package com.youtupu.superclean.view;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Property;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/4/11.
 */
public class GradualTextView extends android.support.v7.widget.AppCompatTextView {
    private float mPercent;
    private int mHighColor;
    private int mLowColor;
    private ArgbEvaluator mArgbEvaluator;
    private boolean mIsNumReduce;
    private int mStartNum = 0;
    private int mReduceNum = 0;

    public GradualTextView(Context context) {
        this(context, null, 0);
    }

    public GradualTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GradualTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.GradualTextView);
        mPercent = typedArray.getFloat(R.styleable.GradualTextView_init_percent, 1f);
        mHighColor = typedArray.getColor(R.styleable.GradualTextView_high_text_color, getResources().getColor(R.color.colorMainPink));
        mLowColor = typedArray.getColor(R.styleable.GradualTextView_low_text_color, getResources().getColor(R.color.colorMainBlue));
        if (mPercent > 1 || mPercent < 0) mPercent = 1;
        typedArray.recycle();
        mArgbEvaluator = new ArgbEvaluator();
        int textColor = (int) mArgbEvaluator.evaluate(mPercent, mLowColor, mHighColor);
        setTextColor(textColor);
    }

    public void setNumReduce(boolean numReduce) {
        mIsNumReduce = numReduce;
    }

    public void setNumber(int start, int reduce) {
        mStartNum = start;
        mReduceNum = reduce;
    }

    public void setPercent(float percent) {
        if (percent > 1 || percent < 0) return;
        if (percent != mPercent) {
            mPercent = percent;
            int textColor = (int) mArgbEvaluator.evaluate(mPercent, mLowColor, mHighColor);
            setTextColor(textColor);
            if (mIsNumReduce) {
                int reduce = (int) (mReduceNum * (1 - percent));
                setText(String.valueOf(mStartNum - reduce));
            }
        }
    }

    public float getPercent() {
        return mPercent;
    }

    public static final Property<GradualTextView, Float> PERCENT = new Property<GradualTextView, Float>(Float.class, "percent") {
        @Override
        public Float get(GradualTextView object) {
            return object.getPercent();
        }

        @Override
        public void set(GradualTextView object, Float value) {
            object.setPercent(value);
        }
    };

}
