package com.youtupu.superclean.view;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/5/6.
 */
public class GradualTempView extends LinearLayout {
    private int mHighTemperature;
    private int mLowTemperature;
    private int mHighColor;
    private int mLowColor;
    private float mPercent;
    private ArgbEvaluator mArgbEvaluator;
    private View mBackView;
    private TextView mTvTemp;
    private int mCha;

    public GradualTempView(Context context) {
        this(context, null, 0, 0);
    }

    public GradualTempView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public GradualTempView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public GradualTempView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        View.inflate(context, R.layout.gradual_temp_view, this);
        mBackView = findViewById(R.id.temp_back);
        mTvTemp = findViewById(R.id.tv_temp);
        mHighColor = context.getResources().getColor(R.color.colorMainBlue);
        mLowColor = context.getResources().getColor(R.color.colorMainBlue);
        mArgbEvaluator = new ArgbEvaluator();
        mPercent = 1;
        afterSetPercent();
    }

    public void initTemperature(int highTemperature, int lowTemperature) {
        mHighTemperature = highTemperature;
        mLowTemperature = lowTemperature;
        mCha = highTemperature - lowTemperature;
        setTemperature(highTemperature);
    }

    private void setTemperature(int temperature) {
        mTvTemp.setText(String.valueOf(temperature) + "℃");
    }

    public void setPercent(float percent) {
        if (percent < 0 || percent > 1) return;
        if (percent != mPercent) {
            mPercent = percent;
            afterSetPercent();
        }
    }

    public float getPercent() {
        return mPercent;
    }

    private void afterSetPercent() {
        if (mPercent > 1 || mPercent < 0) return;
        int color = (int) mArgbEvaluator.evaluate(mPercent, mLowColor, mHighColor);
        mBackView.setBackgroundColor(color);
//        mTvTemp.setTextColor(color);
        int temp = (int) (mHighTemperature - mCha * (1 - mPercent));
        setTemperature(temp);
    }

    public final static Property<GradualTempView, Float> PERCENT = new Property<GradualTempView, Float>(Float.class, "percent") {
        @Override
        public Float get(GradualTempView object) {
            return object.getPercent();
        }

        @Override
        public void set(GradualTempView object, Float value) {
            object.setPercent(value);
        }
    };
}
