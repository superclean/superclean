package com.youtupu.superclean.view;

import android.Manifest;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by Jiali on 2019/6/13.
 */
public class NativeAdContainer extends RelativeLayout {
    private static final float sRatio = 0.527f;

    public NativeAdContainer(Context context) {
        super(context);
    }

    public NativeAdContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NativeAdContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public NativeAdContainer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = (int) (width * sRatio);
        height = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, height);
//        requestLayout();
//        for(int i = 0; i < getChildCount(); i++){
//            View subView = getChildAt(i);
//            subView.requestLayout();
//        }
    }
}
