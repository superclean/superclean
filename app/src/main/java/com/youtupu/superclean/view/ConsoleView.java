package com.youtupu.superclean.view;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.RemoteViews;

import com.youtupu.superclean.activity.BoostActivity;
import com.youtupu.superclean.activity.CoolerScanActivity;
import com.youtupu.superclean.activity.HomeActivity;
import com.youtupu.superclean.activity.JunkScanActivity;

import java.util.Locale;

import com.youtupu.superclean.R;

import com.youtupu.superclean.activity.OptimizeActivity;
import com.youtupu.superclean.activity.PowerSaveActivity;

/**
 * Created by Jiali on 2019/4/26.
 */
public class ConsoleView extends RemoteViews {

    public ConsoleView(Context context) {
        super(context.getPackageName(), R.layout.control_console_view);
        Intent intent = new Intent(context, HomeActivity.class);
        intent.putExtra("fromWhere", 1);
        PendingIntent homeIntent = PendingIntent.getActivity(context, 0, intent, 0);
        setOnClickPendingIntent(R.id.ll_home, homeIntent);
        Intent intent1 = new Intent(context, JunkScanActivity.class);
        intent1.putExtra("fromWhere", 1);
        PendingIntent cleanIntent = PendingIntent.getActivity(context, 0, intent1, 0);
        setOnClickPendingIntent(R.id.ll_clean, cleanIntent);
        Intent intent2 = new Intent(context, CoolerScanActivity.class);
        intent2.putExtra("fromWhere", 1);
        PendingIntent coolingIntent = PendingIntent.getActivity(context, 0, intent2, 0);
        setOnClickPendingIntent(R.id.ll_cooling, coolingIntent);
        Intent intent3 = new Intent(context, PowerSaveActivity.class);
        intent3.putExtra("fromWhere", 1);
        PendingIntent batteryIntent = PendingIntent.getActivity(context, 0, intent3, 0);
        setOnClickPendingIntent(R.id.ll_battery, batteryIntent);
    }

    public void setBoostPercent(float percent, Context context) {
        if (percent > 1 || percent < 0) return;
        if (percent <= 0.4f) {
            setImageViewResource(R.id.img_boost, R.mipmap.boost_level1);
            setViewVisibility(R.id.tv_temp, View.INVISIBLE);
//            Intent optimizeIntent = new Intent(context, OptimizeActivity.class);
//            optimizeIntent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_BOOST);
//            optimizeIntent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, "");
//            optimizeIntent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, context.getString(R.string.already_accelerated));
//            optimizeIntent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, context.getString(R.string.boost_title));
//            optimizeIntent.putExtra(OptimizeActivity.OPTIMIZE_SHOW_AIMATION, false);
            Intent optimizeIntent = new OptimizeActivity.IntentBuilder()
                    .setClass(context)
                    .setFrom(OptimizeActivity.FROM_BOOST)
                    .setRightText(context.getString(R.string.already_accelerated),0,0)
                    .setActionBar(context.getString(R.string.boost_title))
                    .setAnimationShow(false)
                    .build();
            PendingIntent boostIntent = PendingIntent.getActivity(context, 0, optimizeIntent, 0);
            setOnClickPendingIntent(R.id.ll_boost, boostIntent);
        } else {
            setViewVisibility(R.id.tv_temp, View.VISIBLE);
            if (percent <= 0.6f) {
                setImageViewResource(R.id.img_boost, R.mipmap.boost_level2);
            } else if (percent <= 0.8f) {
                setImageViewResource(R.id.img_boost, R.mipmap.boost_level3);
            } else {
                setImageViewResource(R.id.img_boost, R.mipmap.boost_level4);
            }
            setTextViewText(R.id.tv_temp, String.format(Locale.getDefault(), "%.0f", percent * 100) + "%");
            Intent intent = new Intent(context, BoostActivity.class);
            intent.putExtra("percent", percent);
            intent.putExtra("fromWhere", 1);
            PendingIntent boostIntent = PendingIntent.getActivity(context, 0, intent, 0);
            setOnClickPendingIntent(R.id.ll_boost, boostIntent);
        }
    }

    public void setCleanEnable(boolean enable) {
        setViewVisibility(R.id.img_dot_clean, enable ? View.VISIBLE : View.INVISIBLE);
    }

    public void setCoolingEnable(boolean enable) {
        setViewVisibility(R.id.img_dot_cooling, enable ? View.VISIBLE : View.INVISIBLE);
    }

    public void setBatteryEnable(boolean enable) {
        setViewVisibility(R.id.img_dot_battery, enable ? View.VISIBLE : View.INVISIBLE);
    }

}
