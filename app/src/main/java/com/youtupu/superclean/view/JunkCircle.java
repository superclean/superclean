package com.youtupu.superclean.view;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;

import java.util.List;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/1/14.
 */

public class JunkCircle extends View {
    private int mWidth;
    private int mLowColor;
    private int mHighColor;
    private float mPercent = 0;
    private ArgbEvaluator mEvaluator;
    private Paint mPercentPaint;
    private Paint mScanPaint;
    private int mPercentRingWidth;
    private Paint mNumberTextPaint;
    private float mNumberBaseline;
    private Paint mUnitTextPaint;
    private String mTextNum = "0";
    private String mTextUnit = "MB";
    private int mScanHighColor;
    private int mScanLowColor;
    private Paint mStatusTextPaint;
    private RectF mCircleRectF;
    private float mSweepAngle = 0;
    private float mDoneAngle = 0;
    private Paint mBackRingPaint;
    private RectF mPercentRingRectF;
    private float mStatusBaseline;
    private String mTextStatus;
    private int mTextDuration;
    private float mNumTextX;
    private float mUnitTextX;
    private float mRingAngle = 0;
    private int mJunkSize = 0;


    public JunkCircle(Context context) {
        this(context, null, 0, 0);
    }

    public JunkCircle(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public JunkCircle(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public JunkCircle(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mHighColor = context.getResources().getColor(R.color.gear_pink1);
        mLowColor = context.getResources().getColor(R.color.colorMainBlue);
        mScanHighColor = context.getResources().getColor(R.color.gear_pink3);
        mScanLowColor = context.getResources().getColor(R.color.gear_blue3);
        mPercentRingWidth = SystemUtils.dp2px(context, 6);
        int backRingWidth = SystemUtils.dp2px(context, 4);
        mTextDuration = SystemUtils.dp2px(context, 10);
        mTextStatus = context.getString(R.string.junk_found);
        int ringColor = context.getResources().getColor(R.color.junk_circle_ring_color);
        mEvaluator = new ArgbEvaluator();
        //画背景圆环
        mBackRingPaint = new Paint();
        mBackRingPaint.setAntiAlias(true);
        mBackRingPaint.setStyle(Paint.Style.STROKE);
        mBackRingPaint.setStrokeWidth(backRingWidth);
        mBackRingPaint.setColor(ringColor);
        //画进度圆环
        mPercentPaint = new Paint();
        mPercentPaint.setAntiAlias(true);
        mPercentPaint.setStyle(Paint.Style.STROKE);
        mPercentPaint.setStrokeCap(Paint.Cap.ROUND);
        mPercentPaint.setStrokeWidth(mPercentRingWidth);
        mPercentPaint.setColor(mLowColor);
        //画扫描扇形
        mScanPaint = new Paint();
        mScanPaint.setAntiAlias(true);
        mScanPaint.setColor(mScanLowColor);
        mStatusTextPaint = new Paint();
        mStatusTextPaint.setAntiAlias(true);
        mStatusTextPaint.setTextSize(SystemUtils.dp2px(context, 16));
        mStatusTextPaint.setTextAlign(Paint.Align.CENTER);
        mStatusTextPaint.setColor(mLowColor);
        //画中间的垃圾数字
        mNumberTextPaint = new Paint();
        mNumberTextPaint.setAntiAlias(true);
        mNumberTextPaint.setTextAlign(Paint.Align.CENTER);
        mNumberTextPaint.setTextSize(SystemUtils.dp2px(context, 50));
        mNumberTextPaint.setFakeBoldText(true);
        mNumberTextPaint.setColor(mLowColor);
        mUnitTextPaint = new Paint();
        mUnitTextPaint.setAntiAlias(true);
        mUnitTextPaint.setTextAlign(Paint.Align.LEFT);
        mUnitTextPaint.setTextSize(SystemUtils.dp2px(context, 18));
        mUnitTextPaint.setColor(mLowColor);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        int padding = SystemUtils.dp2px(getContext(), 1);
        mCircleRectF = new RectF(mPercentRingWidth - padding, mPercentRingWidth - padding, mWidth - mPercentRingWidth + padding, h - mPercentRingWidth + padding);
        mPercentRingRectF = new RectF(mPercentRingWidth / 2f, mPercentRingWidth / 2f, mWidth - mPercentRingWidth / 2f, h - mPercentRingWidth / 2f);
        Paint.FontMetrics numberMetrics = mNumberTextPaint.getFontMetrics();
        Paint.FontMetrics statusMetrics = mStatusTextPaint.getFontMetrics();
        mNumberBaseline = h / 2.0f;
        mStatusBaseline = h / 2.0f - statusMetrics.top + numberMetrics.bottom;
        mNumTextX = mWidth / 2.0f;
        setPercent(0);
        calculateTextXPosition();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mSweepAngle > 0) {
            float startAngle;
            float sweepAngle;
            if (mSweepAngle < 240) {
                startAngle = mSweepAngle / 2 - 90;
                sweepAngle = mSweepAngle / 2;
            } else if (mSweepAngle < 300) {
                startAngle = mSweepAngle - 210;
                sweepAngle = 120;
            } else {
                startAngle = mSweepAngle - 210;
                sweepAngle = (480 - mSweepAngle) * 2.0f / 3;
            }
            canvas.drawArc(mCircleRectF, startAngle, sweepAngle, true, mScanPaint);
        }
        if (mDoneAngle > 0) {
            //扫描结束后的收尾动画
            canvas.drawArc(mCircleRectF, -90, mDoneAngle, true, mScanPaint);
        }
        //画背景圆环
        canvas.drawArc(mPercentRingRectF, 0, 360, false, mBackRingPaint);
        //画进度弧
        canvas.drawArc(mPercentRingRectF, 270f, mRingAngle, false, mPercentPaint);

        //垃圾大小的数字
        canvas.drawText(mTextNum, mNumTextX, mNumberBaseline, mNumberTextPaint);
        //垃圾大小的单位
        canvas.drawText(mTextUnit, mUnitTextX, mNumberBaseline, mUnitTextPaint);
        //写当前的状态 Junk Found
        if (mTextStatus != null) {
            canvas.drawText(mTextStatus, mWidth / 2f, mStatusBaseline, mStatusTextPaint);
        }
    }

    //must after onSizeChanged
    public void setPercent(float percent) {
        if (percent > 1 || percent < 0) return;
        if (mPercent != percent) {
            mPercent = percent;
            int ringColor = (int) mEvaluator.evaluate(percent, mLowColor, mHighColor);
            int sectorColor = (int) mEvaluator.evaluate(percent, mScanLowColor, mScanHighColor);
            mPercentPaint.setColor(ringColor);
            mScanPaint.setColor(sectorColor);
            mNumberTextPaint.setColor(ringColor);
            mUnitTextPaint.setColor(ringColor);
            mStatusTextPaint.setColor(ringColor);
            invalidate();
        }
    }

    public float getPercent() {
        return mPercent;
    }

    public void setSweepAngle(float sweepAngle) {
        if (sweepAngle < 0 || sweepAngle > 480) return;
        if (mSweepAngle != sweepAngle) {
            mSweepAngle = sweepAngle;
            invalidate();
        }
    }

    public float getSweepAngle() {
        return mSweepAngle;
    }

    public void setDoneAngle(float doneAngle) {
        if (doneAngle < 0 || doneAngle > 360) return;
        if (mDoneAngle != doneAngle) {
            mDoneAngle = doneAngle;
            invalidate();
        }
    }

    public void setFileSize(long size) {
        List<String> strings = SystemUtils.getFileSizeArray(size);
        mTextNum = strings.get(0);
        mTextUnit = strings.get(1);
        calculateTextXPosition();
    }

    //计算两个Text的X坐标
    private void calculateTextXPosition() {
        Rect numRect = new Rect();
        mNumberTextPaint.getTextBounds(mTextNum, 0, mTextNum.length(), numRect);
        mUnitTextX = mNumTextX + numRect.width() / 2f + mTextDuration;
    }

    public float getDoneAngle() {
        return mDoneAngle;
    }

    public void setRingAngle(float ringAngle) {
        if (ringAngle > 360 || ringAngle < 0) return;
        if (mRingAngle != ringAngle) {
            mRingAngle = ringAngle;
            invalidate();
        }
    }

    public void setJunkSize(int junkSize) {
        if (junkSize > 0 && junkSize != mJunkSize) {
            mJunkSize = junkSize;
            long size = ((long) junkSize) * 1000;
            List<String> strings = SystemUtils.getFileSizeArray(size);
            mTextNum = strings.get(0);
            mTextUnit = strings.get(1);
            calculateTextXPosition();
            invalidate();
        }
    }

    public int getJunkSize() {
        return mJunkSize;
    }

    public float getRingAngle() {
        return mRingAngle;
    }

    public final static Property<JunkCircle, Float> SWEEP_ANGLE = new Property<JunkCircle, Float>(Float.class, "sweep_angle") {
        @Override
        public Float get(JunkCircle object) {
            return object.getSweepAngle();
        }

        @Override
        public void set(JunkCircle object, Float value) {
            object.setSweepAngle(value);
        }
    };

    public final static Property<JunkCircle, Float> PERCENT = new Property<JunkCircle, Float>(Float.class, "percent") {
        @Override
        public Float get(JunkCircle object) {
            return object.getPercent();
        }

        @Override
        public void set(JunkCircle object, Float value) {
            object.setPercent(value);
        }
    };

    public final static Property<JunkCircle, Float> DONE_ANGLE = new Property<JunkCircle, Float>(Float.class, "done_angle") {
        @Override
        public Float get(JunkCircle object) {
            return object.getDoneAngle();
        }

        @Override
        public void set(JunkCircle object, Float value) {
            object.setDoneAngle(value);
        }
    };

    public final static Property<JunkCircle, Float> RING_ANGLE = new Property<JunkCircle, Float>(Float.class, "ring_angle") {
        @Override
        public Float get(JunkCircle object) {
            return object.getRingAngle();
        }

        @Override
        public void set(JunkCircle object, Float value) {
            object.setRingAngle(value);
        }
    };

    public final static Property<JunkCircle, Integer> JUNK_SIZE = new Property<JunkCircle, Integer>(Integer.class, "junk_size") {
        @Override
        public Integer get(JunkCircle object) {
            return object.getJunkSize();
        }

        @Override
        public void set(JunkCircle object, Integer value) {
            object.setJunkSize(value);
        }
    };

}
