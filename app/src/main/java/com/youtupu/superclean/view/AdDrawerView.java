package com.youtupu.superclean.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * 优化完成界面
 * Created by Jiali on 2019/3/3.
 */

public class AdDrawerView extends LinearLayout {

    private boolean[] mUsedArray;
    private String mAdUnitId;
    private List<AdSelfItemView> mAdSelfItemViewList;

    public AdDrawerView(Context context, boolean[] usedArray, String adUnitId) {
        this(context, null, 0, 0);
        mAdUnitId = adUnitId;
        mUsedArray = usedArray;
        if (mUsedArray.length > 0) {
            mAdSelfItemViewList = new ArrayList<>();
            int length = mUsedArray.length;
            if (length > 0) {
                LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.topMargin = SystemUtils.dp2px(context, 10);
                for (int i = 0; i < length; i++) {
                    if (!usedArray[i]) {
                        AdSelfItemView itemView = new AdSelfItemView(context, i);
                        mAdSelfItemViewList.add(itemView);
                        addView(itemView, params);
                    }
                }
            }
        }
    }

    public AdDrawerView(Context context) {
        this(context, null, 0, 0);
    }

    public AdDrawerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public AdDrawerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public AdDrawerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setOrientation(VERTICAL);
        int padding = SystemUtils.dp2px(context, 20);
        int paddingTop = SystemUtils.dp2px(context, 10);
        int paddingRight = SystemUtils.dp2px(context, 17);
        setPadding(padding, paddingTop, paddingRight, padding);
        setBackgroundColor(getResources().getColor(R.color.home_background));
    }

}
