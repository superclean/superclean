package com.youtupu.superclean.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/5/8.
 */
public class BoostButton extends android.support.v7.widget.AppCompatImageView {
    private boolean mDownClick = false;
    private int mWidth;
    private int mHeight;

    public BoostButton(Context context) {
        super(context);
    }

    public BoostButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BoostButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mDownClick = false;
        setScaleType(ScaleType.CENTER);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setImageResource(R.mipmap.button);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                float x = event.getX();
                float y = event.getY();
                if (x > 0 && x < mWidth && y > 0 && y < mHeight) {
                    mDownClick = true;
                    setImageResource(R.mipmap.button_click);
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mDownClick) {
                    mDownClick = false;
                    float upX = event.getX();
                    float upY = event.getY();
                    if (upX > 0 && upX < mWidth && upY > 0 && upY < mHeight) {
                        performClick();
                    }
                    setImageResource(R.mipmap.button);
                }
                break;
        }
        return true;
    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }


}
