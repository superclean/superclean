package com.youtupu.superclean.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/4/22.
 */
public class CustomTitleBar extends LinearLayout {
    private LeftIconClickListener mLeftListener;
    private RightIconClickListener mRightListener;
    private TextView mTvTitle;

    public CustomTitleBar(Context context) {
        this(context, null, 0, 0);
    }

    public CustomTitleBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public CustomTitleBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public CustomTitleBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setOrientation(HORIZONTAL);
        View.inflate(context, R.layout.custom_title_bar, this);
        ImageView imgLeft = findViewById(R.id.img_left);
        ImageView imgRight = findViewById(R.id.img_right);
        mTvTitle = findViewById(R.id.tv_title);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomTitleBar);
        Drawable leftIcon = typedArray.getDrawable(R.styleable.CustomTitleBar_left_icon);
        Drawable rightIcon = typedArray.getDrawable(R.styleable.CustomTitleBar_right_icon);
        String title = typedArray.getString(R.styleable.CustomTitleBar_title);
        int titleColor = typedArray.getColor(R.styleable.CustomTitleBar_title_color, Color.BLACK);
        typedArray.recycle();
        mTvTitle.setTextColor(titleColor);
        if (leftIcon != null) {
            imgLeft.setImageDrawable(leftIcon);
            imgLeft.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mLeftListener != null) {
                        mLeftListener.onLeftClick();
                    }
                }
            });
        }
        if (rightIcon != null) {
            imgRight.setImageDrawable(rightIcon);
            imgRight.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mRightListener != null) {
                        mRightListener.onRightClick();
                    }
                }
            });
        }
        if (title != null) {
            mTvTitle.setText(title);
        }
    }

    public void setOnLeftIconClickListener(LeftIconClickListener listener) {
        mLeftListener = listener;
    }

    public void setOnRightIconClickListener(RightIconClickListener listener) {
        mRightListener = listener;
    }

    public interface LeftIconClickListener {
        void onLeftClick();
    }

    public interface RightIconClickListener {
        void onRightClick();
    }

    public void setTitle(@NonNull String title){
        mTvTitle.setText(title);
    }
}
