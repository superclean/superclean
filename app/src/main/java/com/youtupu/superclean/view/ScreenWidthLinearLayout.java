package com.youtupu.superclean.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/5/9.
 */
public class ScreenWidthLinearLayout extends LinearLayout {
    public ScreenWidthLinearLayout(Context context) {
        this(context, null, 0, 0);
    }

    public ScreenWidthLinearLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public ScreenWidthLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ScreenWidthLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        int width = SystemUtils.getScreenWidth(getContext());
        setMinimumWidth(width);
    }

}
