package com.youtupu.superclean.view;

import android.content.Context;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.youtupu.superclean.R;

/**
 * created by yihao 2019/4/23
 */
public class FloatPermissionDetectView extends LinearLayout {

    private OnDrawListener onDrawListener;

    public FloatPermissionDetectView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.float_detect, this);
    }

    public void setOnDrawListener(OnDrawListener listener){
        onDrawListener = listener;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        onDrawListener.onDraw();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        onDrawListener.onDraw();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        onDrawListener.onDraw();

    }

    public interface OnDrawListener{
        void onDraw();
    }


}
