package com.youtupu.superclean.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/5/16.
 */
public class InnerBattery extends View {

    private int mWidth;
    private int mHeight;
    private float mPercent = 1;
    private int mLowColor;
    private int mContentColor;
    private int mHighColor;
    private Paint mPaint;

    public InnerBattery(Context context) {
        this(context, null, 0, 0);
    }

    public InnerBattery(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public InnerBattery(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public InnerBattery(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mLowColor = context.getResources().getColor(R.color.colorMainPink);
        mHighColor = context.getResources().getColor(R.color.battery_green);
        mPercent = 1;
        afterSetPercent();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawRect(0, 0, mWidth * mPercent, mHeight, mPaint);
    }

    public void setPercent(float percent) {
        if (percent < 0 || percent > 1) return;
        mPercent = percent;
        afterSetPercent();
        invalidate();
    }

    private void afterSetPercent() {
        if (mPercent < 0.2f) {
            mContentColor = mLowColor;
        } else {
            mContentColor = mHighColor;
        }
        mPaint.setColor(mContentColor);
    }
}
