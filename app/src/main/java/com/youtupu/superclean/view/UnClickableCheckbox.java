package com.youtupu.superclean.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/2/26.
 */

public class UnClickableCheckbox extends RelativeLayout {

    private ImageView mImgCheckBox;
    private boolean mIsChecked = false;

    public UnClickableCheckbox(Context context) {
        this(context, null, 0, 0);
    }

    public UnClickableCheckbox(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public UnClickableCheckbox(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public UnClickableCheckbox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        View.inflate(context, R.layout.three_status_checkbox_layout, this);
        mImgCheckBox = findViewById(R.id.img_three_status_checkbox);
    }


    public boolean getChecked() {
        return mIsChecked;
    }

    public void setChecked(boolean checked) {
        mIsChecked = checked;
        if (mIsChecked) {
            mImgCheckBox.setImageResource(R.mipmap.checkbox_enable);
        } else {
            mImgCheckBox.setImageResource(R.mipmap.checkbox_grey);
        }
    }

}
