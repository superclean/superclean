package com.youtupu.superclean.view;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/1/8.
 */

public class GearView extends View {
    private int mWidth;
    private int mHeight;
    private Paint mStripeHighPaint;
    private Paint mStripeLowPaint;
    private int mStripeLowColor;
    private int mBackCircleLowColor;
    private int mStripeHighColor;
    private int mBackCircleHighColor;
    private int mStripeMiddleColor;
    private int mBackCircleMiddleColor;
    private float mPercent;
    private ArgbEvaluator mEvaluator;
    private Paint mInnerCirclePaint;
    private float mInnerCircleRadius;
    private Path mStripePath;
    private Path mStripeFirstPath;

    public GearView(Context context) {
        this(context, null, 0, 0);
    }

    public GearView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public GearView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public GearView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mStripeLowColor = context.getResources().getColor(R.color.gear_blue2);
        mBackCircleLowColor = context.getResources().getColor(R.color.gear_blue3);
        mStripeHighColor = context.getResources().getColor(R.color.gear_pink2);
        mBackCircleHighColor = context.getResources().getColor(R.color.gear_pink3);
        mStripeMiddleColor = context.getResources().getColor(R.color.gear_purple2);
        mBackCircleMiddleColor = context.getResources().getColor(R.color.gear_purple3);
        mInnerCirclePaint = new Paint();
        mStripeHighPaint = new Paint();
        mStripeLowPaint = new Paint();
        mInnerCirclePaint.setAntiAlias(true);
        mStripeHighPaint.setAntiAlias(true);
        mStripeLowPaint.setAntiAlias(true);
        mInnerCirclePaint.setColor(Color.WHITE);
        mInnerCirclePaint.setStyle(Paint.Style.FILL);
        mStripeHighPaint.setStyle(Paint.Style.FILL);
        mStripePath = new Path();
        mStripeFirstPath = new Path();
        mEvaluator = new ArgbEvaluator();

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        mInnerCircleRadius = mWidth * 5 / 16f;
        mStripePath.moveTo(mWidth / 2f, mHeight / 2f);
        mStripePath.addArc(mWidth / 2f, 0, mWidth * 3 / 2f, mHeight, 180, 60);
        mStripePath.addArc(0, 0, mWidth, mHeight, 300, 30);
        mStripePath.lineTo(mWidth / 2f, mHeight / 2f);
        mStripeFirstPath.moveTo(mWidth / 2f, mHeight / 2f);
        mStripeFirstPath.addArc(mWidth / 2f, 0, mWidth * 3 / 2f, mHeight, 180, 60);
        mStripeFirstPath.lineTo(mWidth / 2f, mHeight / 2f);
        setPercent(1);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();
        for (int i = 0; i < 12; i++) {
            if (i % 2 == 0) {
                canvas.drawPath(mStripePath, mStripeHighPaint);
            } else {
                canvas.drawPath(mStripePath, mStripeLowPaint);
            }
            canvas.rotate(30, mWidth / 2f, mHeight / 2f);
        }
        canvas.restore();
        canvas.drawPath(mStripeFirstPath, mStripeHighPaint);
        canvas.drawCircle(mWidth / 2f, mHeight / 2f, mInnerCircleRadius, mInnerCirclePaint);
    }


    public void setPercent(float percent) {
        if (percent < 0 || percent > 1) return;
        if (mPercent != percent) {
            mPercent = percent;
            afterSetPercent();
            invalidate();
        }
    }

    public float getPercent() {
        return mPercent;
    }

    private void afterSetPercent() {
        int stripeColor;
        int backCircleColor;
        if (mPercent < 0.5f) {
            stripeColor = (int) mEvaluator.evaluate(mPercent * 2, mStripeLowColor, mStripeMiddleColor);
            backCircleColor = (int) mEvaluator.evaluate(mPercent * 2, mBackCircleLowColor, mBackCircleMiddleColor);
        } else {
            stripeColor = (int) mEvaluator.evaluate((mPercent - 0.5f) * 2, mStripeMiddleColor, mStripeHighColor);
            backCircleColor = (int) mEvaluator.evaluate((mPercent - 0.5f) * 2, mBackCircleMiddleColor, mBackCircleHighColor);
        }
        mStripeHighPaint.setColor(stripeColor);
        mStripeLowPaint.setColor(backCircleColor);
    }


    public static final Property<GearView, Float> PERCENT = new Property<GearView, Float>(Float.class, "percent") {
        @Override
        public Float get(GearView object) {
            return object.getPercent();
        }

        @Override
        public void set(GearView object, Float value) {
            object.setPercent(value);
        }
    };

}
