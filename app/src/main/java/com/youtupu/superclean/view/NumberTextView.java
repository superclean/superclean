package com.youtupu.superclean.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Property;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/5/7.
 */
public class NumberTextView extends android.support.v7.widget.AppCompatTextView {
    private String mStartText;
    private String mEndText;
    private int mNumber;

    public NumberTextView(Context context) {
        this(context, null, 0);
    }

    public NumberTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NumberTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.NumberTextView);
        mStartText = typedArray.getString(R.styleable.NumberTextView_start_text);
        mEndText = typedArray.getString(R.styleable.NumberTextView_end_text);
        mNumber = typedArray.getInt(R.styleable.NumberTextView_init_number, -1);
        typedArray.recycle();
        setFinalText();
    }

    private void setFinalText() {
        StringBuilder builder = new StringBuilder();
        if (mStartText != null && !mStartText.isEmpty()) {
            builder.append(mStartText);
        }
        if (mNumber >= 0) {
            builder.append(String.valueOf(mNumber));
        }
        if (mEndText != null && !mEndText.isEmpty()) {
            builder.append(mEndText);
        }
        setText(builder);
    }

    public void setNumber(int number) {
        if (number < 0) return;
        if (number != mNumber) {
            mNumber = number;
            setFinalText();
        }
    }

    public int getNumber() {
        return mNumber;
    }

    public final static Property<NumberTextView, Integer> NUMBER = new Property<NumberTextView, Integer>(Integer.class, "number") {
        @Override
        public Integer get(NumberTextView object) {
            return object.getNumber();
        }

        @Override
        public void set(NumberTextView object, Integer value) {
            object.setNumber(value);
        }
    };

}
