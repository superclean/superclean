package com.youtupu.superclean.view;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/3/11.
 */

public class AlignCenterTextView extends View {
    private float mPercent = 1;
    private int mLowColor;
    private int mHighColor;
    private ArgbEvaluator mEvaluator;
    private int mWidth;
    private String mLeftText;
    private String mRightText;
    private Paint mLeftPaint;
    private Paint mRightPaint;
    private float mLeftBaseline;
    private int mDistance;
    private int mRightX;
    private int mInitSize = 0;
    private int mSize;

    public AlignCenterTextView(Context context) {
        this(context, null, 0, 0);
    }

    public AlignCenterTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public AlignCenterTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public AlignCenterTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mLowColor = context.getResources().getColor(R.color.colorMainBlue);
        mHighColor = context.getResources().getColor(R.color.colorMainPink);
        //左右文字的间隔
        mDistance = SystemUtils.dp2px(context, 6);
        mEvaluator = new ArgbEvaluator();
        mLeftPaint = new Paint();
        mLeftPaint.setAntiAlias(true);
        mLeftPaint.setTextSize(SystemUtils.dp2px(context, 48));
        mLeftPaint.setTextAlign(Paint.Align.CENTER);
        mLeftPaint.setFakeBoldText(true);
        mRightPaint = new Paint();
        mRightPaint.setAntiAlias(true);
        mRightPaint.setTextSize(SystemUtils.dp2px(context, 20));
        mRightPaint.setTextAlign(Paint.Align.LEFT);
        mRightText = "MB";
        mSize = 0;
        setColor(mLowColor);
        afterSetSize();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        Paint.FontMetrics leftMetrics = mLeftPaint.getFontMetrics();
        //左边文字垂直居中
        mLeftBaseline = (leftMetrics.bottom - leftMetrics.top) / 2 - leftMetrics.bottom + h / 2f;
        Rect rect = new Rect();
        mLeftPaint.getTextBounds(mLeftText, 0, mLeftText.length(), rect);
        int leftWidth = rect.width();
        mRightX = mWidth / 2 + leftWidth / 2 + mDistance;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawText(mLeftText, mWidth / 2f, mLeftBaseline, mLeftPaint);
        canvas.drawText(mRightText, mRightX, mLeftBaseline, mRightPaint);
    }

    private void afterSetSize() {
        mLeftText = String.valueOf(mSize);
        Rect rect = new Rect();
        mLeftPaint.getTextBounds(mLeftText, 0, mLeftText.length(), rect);
        int leftWidth = rect.width();
        mRightX = mWidth / 2 + leftWidth / 2 + mDistance;
    }

    private void setColor(int color) {
        mLeftPaint.setColor(color);
        mRightPaint.setColor(color);
    }

    public float getPercent() {
        return mPercent;
    }

    public void setPercent(float percent) {
        if (percent < 0 || percent > 1) return;
        if (mPercent != percent) {
            mPercent = percent;
            int color = (int) mEvaluator.evaluate(percent, mLowColor, mHighColor);
            setColor(color);
            invalidate();
        }
    }

    //单位MB
    public void setInitSize(int size) {
        if (size > 0) {
            mInitSize = size;
            mSize = size;
            afterSetSize();
        }
    }

    public int getSize() {
        return mSize;
    }

    public void setSize(int size) {
        if (size < 0 || size > mInitSize) return;
        if (mSize != size) {
            mSize = size;
            afterSetSize();
            invalidate();
        }
    }

    public static final Property<AlignCenterTextView, Float> PERCENT = new Property<AlignCenterTextView, Float>(Float.class, "percent") {
        @Override
        public Float get(AlignCenterTextView object) {
            return object.getPercent();
        }

        @Override
        public void set(AlignCenterTextView object, Float value) {
            object.setPercent(value);
        }
    };

    public static final Property<AlignCenterTextView, Integer> SIZE = new Property<AlignCenterTextView, Integer>(Integer.class, "size") {
        @Override
        public Integer get(AlignCenterTextView object) {
            return object.getSize();
        }

        @Override
        public void set(AlignCenterTextView object, Integer value) {
            object.setSize(value);
        }
    };

}
