package com.youtupu.superclean.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/2/26.
 */

public class ThreeStatusCheckBox extends RelativeLayout {

    private ImageView mImgCheckBox;
    private boolean mIsChecked = false;
    private OnCheckBoxSelectedListener mOnCheckBoxSelectedListener;
    private int mTag = -1;

    public ThreeStatusCheckBox(Context context) {
        this(context, null, 0, 0);
    }

    public ThreeStatusCheckBox(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public ThreeStatusCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ThreeStatusCheckBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
        initListener();
    }

    private void initView(Context context) {
        View.inflate(context, R.layout.three_status_checkbox_layout, this);
        mImgCheckBox = findViewById(R.id.img_three_status_checkbox);
    }

    private void initListener() {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsChecked = !mIsChecked;
                if (mIsChecked) {
                    mImgCheckBox.setImageResource(R.mipmap.checkbox_enable);
                    if (mOnCheckBoxSelectedListener != null) {
                        mOnCheckBoxSelectedListener.onChecked();
                    }
                } else {
                    mImgCheckBox.setImageResource(R.mipmap.checkbox_grey);
                    if (mOnCheckBoxSelectedListener != null) {
                        mOnCheckBoxSelectedListener.onUnChecked();
                    }
                }
            }
        });
    }


    public void setCheckBoxPartialSelected() {
        mImgCheckBox.setImageResource(R.mipmap.checkbox_green);
        mIsChecked = false;
    }

    public boolean getChecked() {
        return mIsChecked;
    }

    public void setChecked(boolean checked) {
        mIsChecked = checked;
        if (mIsChecked) {
            mImgCheckBox.setImageResource(R.mipmap.checkbox_enable);
        } else {
            mImgCheckBox.setImageResource(R.mipmap.checkbox_grey);
        }
    }

    public void setOnCheckBoxSelectedListener(OnCheckBoxSelectedListener listener) {
        mOnCheckBoxSelectedListener = listener;
    }


    public interface OnCheckBoxSelectedListener {
        void onChecked();

        void onUnChecked();
    }

    public void setmTag(int tag) {
        mTag = tag;
    }

    public int getmTag() {
        return mTag;
    }
}
