package com.youtupu.superclean.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.lang.ref.WeakReference;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/6/6.
 */
public class WaveBar extends View {
    private static final int WAVE_ANIMATION = 0;
    private int mHeight;
    private int mDuration;
    private int mLineNumber;
    private Paint mPaint;
    private int mShiftX = 0;
    private WaveHandler mHandler;
    private int mStartX;
    private int mEndX;
    private int mEndY;
    private int mStartY;

    public WaveBar(Context context) {
        this(context, null, 0, 0);
    }

    public WaveBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public WaveBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public WaveBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mDuration = SystemUtils.dp2px(context, 20);
        mStartX = mDuration / 4 - 2 * mDuration;
        mEndX = 2 * mDuration - mStartX;
        mEndY = -mDuration / 4;
        float lineWidth = (float) (Math.sqrt(2) * mDuration / 2);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(lineWidth);
        mPaint.setColor(context.getResources().getColor(R.color.wave_bar));
        mHandler = new WaveHandler(this);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mHeight = h;
        mLineNumber = w / mDuration / 2 + 2;
        mStartY = mHeight + mDuration / 4;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for (int i = 0; i < mLineNumber; i++) {
            canvas.drawLine(mStartX + 2 * i * mDuration + mShiftX, mStartY, mStartX + 2 * i * mDuration + mDuration + mShiftX, mEndY, mPaint);
        }
    }

    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (mHandler != null) {
            mHandler.removeMessages(WAVE_ANIMATION);
            if (visibility == VISIBLE) {
                mHandler.sendEmptyMessage(WAVE_ANIMATION);
            }
        }
    }

    private static class WaveHandler extends Handler {
        private WeakReference<WaveBar> mReference;

        WaveHandler(WaveBar waveBar) {
            mReference = new WeakReference<>(waveBar);
        }

        @Override
        public void handleMessage(Message msg) {
            WaveBar waveBar = mReference.get();
            if (waveBar == null) return;
            switch (msg.what) {
                case WAVE_ANIMATION:
                    if (waveBar.mShiftX >= 2 * waveBar.mDuration) {
                        waveBar.mShiftX = 0;
                    } else {
                        waveBar.mShiftX += 3;
                    }
                    waveBar.invalidate();
                    sendEmptyMessageDelayed(WAVE_ANIMATION, 10);
                    break;
            }
        }
    }
}
