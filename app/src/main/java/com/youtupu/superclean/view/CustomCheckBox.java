package com.youtupu.superclean.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/3/13.
 */
public class CustomCheckBox extends android.support.v7.widget.AppCompatImageView {
    private boolean mIsEnable = false;
    private OnCustomCheckBoxCheckedListener mCheckedListener;

    public CustomCheckBox(Context context) {
        this(context, null, 0);
    }

    public CustomCheckBox(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setMinimumHeight(SystemUtils.dp2px(context, 30));
        setMinimumWidth(SystemUtils.dp2px(context, 40));
        setScaleType(ScaleType.CENTER);
        setImageResource(R.mipmap.checkbox_close);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsEnable = !mIsEnable;
                if (mIsEnable) {
                    setImageResource(R.mipmap.checkbox_open);
                    if (mCheckedListener != null) {
                        mCheckedListener.onChecked();
                    }
                } else {
                    setImageResource(R.mipmap.checkbox_close);
                    if (mCheckedListener != null) {
                        mCheckedListener.onUnChecked();
                    }
                }
            }
        });
    }

    public interface OnCustomCheckBoxCheckedListener {
        void onChecked();

        void onUnChecked();
    }

    public void setOnCustomCheckBoxChecked(@NonNull OnCustomCheckBoxCheckedListener listener) {
        mCheckedListener = listener;
    }


}
