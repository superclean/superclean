package com.youtupu.superclean.view;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/1/3.
 */

public class IndicatorView extends View {
    private float mPercent = 0;
    private int mHighColor;
    private int mLowColor;
    private int mStripeHighColor;
    private int mStripeLowColor;
    private ArgbEvaluator mEvaluator;
    private int mStripeWidth;
    private int mRotateDegree;
    private int mWidth;
    private int mHeight;
    private Paint mPaint;
    private RectF mInnerRectF;
    private Path mCirclePath;
    private Path mTrianglePath;
    private int mOutRadius;
    private int mInnerRadius;
    private int mBackgroundColor;

    public IndicatorView(Context context) {
        this(context, null, 0, 0);
    }

    public IndicatorView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public IndicatorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public IndicatorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mRotateDegree = 0;
        mStripeWidth = SystemUtils.dp2px(context, 15);
        mHighColor = context.getResources().getColor(R.color.colorMainPink);
        mLowColor = context.getResources().getColor(R.color.colorMainBlue);
        mStripeHighColor = context.getResources().getColor(R.color.colorDeepPink);
        mStripeLowColor = context.getResources().getColor(R.color.colorMainDeepBlue);
        mBackgroundColor = context.getResources().getColor(R.color.home_background);
        mEvaluator = new ArgbEvaluator();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mInnerRectF = new RectF();
        mCirclePath = new Path();
        mTrianglePath = new Path();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        //设置变色圆环、按钮的大小
        if (mWidth > mHeight) {
            mOutRadius = mHeight / 2;
            mInnerRadius = mHeight * 17 / 40 - 1;
        } else {
            mOutRadius = mWidth / 2;
            mInnerRadius = mWidth * 17 / 40 - 1;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int circleColor = (int) mEvaluator.evaluate(mPercent, mLowColor, mHighColor);
        //绘制背景
        mPaint.setColor(circleColor);
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawRect(0, 0, mWidth, mHeight, mPaint);
        int stripeColor = (int) mEvaluator.evaluate(mPercent, mStripeLowColor, mStripeHighColor);
        //绘制条纹
        mPaint.setColor(stripeColor);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mStripeWidth);
        canvas.save();
        canvas.rotate(mRotateDegree, mWidth / 2f, mHeight / 2f);
        mInnerRectF.set(mWidth / 2f, mHeight / 2f - mInnerRadius, mWidth + mWidth / 2f, mHeight / 2f);
        for (int i = 0; i < 18; i++) {
            canvas.rotate(20, mWidth / 2f, mHeight / 2f);
            canvas.drawArc(mInnerRectF, 180, 270, false, mPaint);
        }
        canvas.restore();
        //勾勒圆形path
        double indicatorAngle = (mPercent * 150 + 15) * Math.PI / 180;
        double leftAngle = (mPercent * 150 + 20) * Math.PI / 180;
        double rightAngle = (mPercent * 150 + 10) * Math.PI / 180;
        float startX = (float) (mOutRadius * Math.cos(indicatorAngle) + mWidth / 2);
        float startY = (float) (mHeight / 2 - mOutRadius * Math.sin(indicatorAngle));
        float leftX = (float) (mInnerRadius * Math.cos(leftAngle) + mWidth / 2);
        float leftY = (float) (mHeight / 2 - mInnerRadius * Math.sin(leftAngle));
        float rightX = (float) (mInnerRadius * Math.cos(rightAngle) + mWidth / 2);
        float rightY = (float) (mHeight / 2 - mInnerRadius * Math.sin(rightAngle));
        mCirclePath.reset();
        mCirclePath.moveTo(mWidth / 2f, 0);
        mCirclePath.lineTo(0, 0);
        mCirclePath.lineTo(0, mHeight);
        mCirclePath.lineTo(mWidth, mHeight);
        mCirclePath.lineTo(mWidth, 0);
        mCirclePath.lineTo(mWidth / 2f, 0);
        mCirclePath.lineTo(mWidth / 2f, mHeight / 2f - mInnerRadius);
        mCirclePath.addArc(mWidth / 2f - mInnerRadius, mHeight / 2f - mInnerRadius, mWidth / 2f + mInnerRadius,
                mHeight / 2f + mInnerRadius, 270, 360);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setStrokeWidth(1);
        mPaint.setColor(mBackgroundColor);
        canvas.drawPath(mCirclePath, mPaint);
        //勾勒三角形path
        mTrianglePath.reset();
        mTrianglePath.moveTo(startX, startY);
        mTrianglePath.lineTo(leftX, leftY);
        mTrianglePath.lineTo(rightX, rightY);
        mTrianglePath.lineTo(startX, startY);
        mPaint.setColor(circleColor);
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawPath(mTrianglePath, mPaint);
    }


    public float getPercent() {
        return mPercent;
    }

    public void setPercent(float percent) {
        if (percent < 0 || percent > 1) return;
        if (mPercent != percent) {
            mPercent = percent;
            invalidate();
        }
    }

    public static final Property<IndicatorView, Float> PERCENT = new Property<IndicatorView, Float>(Float.class, "percent") {
        @Override
        public Float get(IndicatorView object) {
            return object.getPercent();
        }

        @Override
        public void set(IndicatorView object, Float value) {
            object.setPercent(value);
        }
    };

}
