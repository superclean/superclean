package com.youtupu.superclean.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youtupu.superclean.activity.BoostActivity;
import com.youtupu.superclean.activity.CoolerScanActivity;
import com.youtupu.superclean.activity.JunkPermissionInstructionActivity;
import com.youtupu.superclean.activity.JunkScanActivity;
import com.youtupu.superclean.activity.NotificationManagerActivity;

import com.youtupu.superclean.R;

import com.youtupu.superclean.activity.PowerSaveActivity;
import pub.devrel.easypermissions.EasyPermissions;


/**
 * Created by Jiali on 2019/3/3.
 */

public class AdSelfItemView extends LinearLayout {
    public static final int AD_BOOST = 0;
    public static final int AD_CLEAN = 1;
    public static final int AD_COOLER = 2;
    public static final int AD_POWER = 3;
    public static final int AD_NOTIFICATION = 4;
    private TextView mTvTitle;
    private TextView mTvContent;
    private ImageView mImgIcon;
    private Activity mActivity;
    private int mType = -1;

    public AdSelfItemView(@NonNull Context context, int type) {
        this(context, null, 0, 0);
        setType(type);
    }

    public AdSelfItemView(@NonNull Context context) {
        this(context, null, 0, 0);
    }

    public AdSelfItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public AdSelfItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public AdSelfItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if (context instanceof Activity) {
            mActivity = (Activity) context;
        }
        View.inflate(context, R.layout.ad_item_view_layout, this);
        mImgIcon = findViewById(R.id.img_self_ad_icon);
        mTvTitle = findViewById(R.id.tv_self_ad_title);
        mTvContent = findViewById(R.id.tv_self_ad_content);
    }


    public void setType(int type) {
        if (type < 0 || type > 4) return;
        mType = type;
        Class intentTo = null;
        switch (type) {
            case AD_BOOST:
                mTvTitle.setText(getResources().getString(R.string.boost_title));
                mImgIcon.setImageResource(R.mipmap.boost_icon_small);
                mTvContent.setText(getResources().getString(R.string.boost_function_description));
                intentTo = BoostActivity.class;
                break;
            case AD_CLEAN:
                mTvTitle.setText(getResources().getString(R.string.junk_clean));
                mImgIcon.setImageResource(R.mipmap.junk_icon_small);
                mTvContent.setText(getResources().getString(R.string.junk_function_description));
                intentTo = JunkScanActivity.class;
                break;
            case AD_COOLER:
                mTvTitle.setText(getResources().getString(R.string.cpu_title));
                mImgIcon.setImageResource(R.mipmap.cooler_icon_small);
                mTvContent.setText(getResources().getString(R.string.cooler_function_description));
                intentTo = CoolerScanActivity.class;
                break;
            case AD_POWER:
                mTvTitle.setText(getResources().getString(R.string.battery_title));
                mImgIcon.setImageResource(R.mipmap.battery_icon_small);
                mTvContent.setText(getResources().getString(R.string.battery_function_description));
                intentTo = PowerSaveActivity.class;
                break;
            case AD_NOTIFICATION:
                mTvTitle.setText(getResources().getString(R.string.notification_title));
                mImgIcon.setImageResource(R.mipmap.notification_icon_small);
                mTvContent.setText(getResources().getString(R.string.notification_function_description));
                intentTo = NotificationManagerActivity.class;
                break;
        }
        if (mActivity != null) {
            final Class finalIntentTo = intentTo;
            setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mType == AD_CLEAN) {
                        if (EasyPermissions.hasPermissions(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            Intent cleanIntent = new Intent(mActivity, JunkScanActivity.class);
                            mActivity.startActivity(cleanIntent);
                            mActivity.finish();
                        } else {
                            Intent intent = new Intent(mActivity, JunkPermissionInstructionActivity.class);
                            mActivity.startActivity(intent);
                            mActivity.finish();
                        }
                    } else {
                        Intent intent = new Intent(mActivity, finalIntentTo);
                        mActivity.startActivity(intent);
                        mActivity.finish();
                    }
                }
            });
        }
    }

}
