package com.youtupu.superclean.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.youtupu.superclean.bean.JunkListItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.youtupu.superclean.R;

import com.youtupu.superclean.dialog.DetailDialog;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/2/25.
 */

public class FoldableListView extends LinearLayout {

    private TextView mTvTitle;
    private ImageView mImgOpen;
    private String mStringTitle;
    private boolean mIsOpenStatus = true;
    private LinearLayout mLlTotal;
    private ListView mListViewDetail;
    public ThreeStatusCheckBox mCheckBox;
    private ImageView mImgTotalIcon;
    private TextView mTvTotalNum;
    private Drawable mTotalIcon;
    private List<JunkListItem> mJunkItemsList;
    private JunkListViewAdapter mListViewAdapter;
    private RelativeLayout mRlTitle;
    private DetailDialog mDetailDialog;
    private Context mContext;
    private TextView mTvSelectedSize;
    private OnSelectedResultChangedListener mOnSelectedResultChangedListener;
    private boolean mFirstCreate = false;

    public FoldableListView(Context context) {
        this(context, null, 0, 0);
    }

    public FoldableListView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public FoldableListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public FoldableListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.FoldableListView);
        mStringTitle = typedArray.getString(R.styleable.FoldableListView_junk_category_title);
        mTotalIcon = typedArray.getDrawable(R.styleable.FoldableListView_junk_tatal_icon);
        typedArray.recycle();
        mContext = context;
        initView(context);
        initData(context);
        initListener();
    }

    private void initView(Context context) {
        View.inflate(context, R.layout.foldable_listview_layout, this);
        mRlTitle = findViewById(R.id.rl_title);
        mTvTitle = findViewById(R.id.fold_listview_title);
        mImgOpen = findViewById(R.id.img_open);
        mLlTotal = findViewById(R.id.ll_junk_total);
        mListViewDetail = findViewById(R.id.listview_junk_detail);
        mCheckBox = findViewById(R.id.three_status_checkbox);
        mImgTotalIcon = findViewById(R.id.img_junk_total_icon);
        mTvTotalNum = findViewById(R.id.tv_junk_total_num);
        mTvSelectedSize = findViewById(R.id.tv_selected_size);
    }

    private void initData(Context context) {
        mFirstCreate = true;
        mTvTitle.setText(mStringTitle);
        mImgTotalIcon.setImageDrawable(mTotalIcon);
        mJunkItemsList = new ArrayList<>();
        mListViewAdapter = new JunkListViewAdapter(mJunkItemsList, context, this);
        mListViewDetail.setAdapter(mListViewAdapter);
    }

    private void initListener() {
        mListViewDetail.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (mFirstCreate) {
                    mFirstCreate = false;
                    updateFolderListViewHeight(true);
                }
            }
        });
        mRlTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsOpenStatus = !mIsOpenStatus;
                if (mIsOpenStatus) {
                    mImgOpen.setImageResource(R.mipmap.arrow_up);
                    mLlTotal.setVisibility(View.GONE);
                    mListViewDetail.setVisibility(View.VISIBLE);
                } else {
                    mImgOpen.setImageResource(R.mipmap.arrow_down);
                    mLlTotal.setVisibility(View.VISIBLE);
                    mListViewDetail.setVisibility(View.GONE);
                }
                updateFolderListViewHeight(mIsOpenStatus);
            }
        });

        mCheckBox.setOnCheckBoxSelectedListener(new ThreeStatusCheckBox.OnCheckBoxSelectedListener() {
            @Override
            public void onChecked() {
                for (JunkListItem item : mJunkItemsList) {
                    item.setChecked(true);
                }
                mListViewAdapter.notifyDataSetChanged();
                updateSelectedResult();
            }

            @Override
            public void onUnChecked() {
                for (JunkListItem item : mJunkItemsList) {
                    item.setChecked(false);
                }
                mListViewAdapter.notifyDataSetChanged();
                updateSelectedResult();
            }
        });

        mListViewDetail.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JunkListItem item = (JunkListItem) mListViewAdapter.getItem(position);
                if (mDetailDialog == null) {
                    mDetailDialog = new DetailDialog(mContext);
                }
                String applicationName = item.getApplicationName();
                if (applicationName != null) {
                    mDetailDialog.setTitle(applicationName);
                } else {
                    mDetailDialog.setTitle(mContext.getString(R.string.residual_file));
                }
                mDetailDialog.setSize(SystemUtils.getFileSize(item.getSize()));
                mDetailDialog.setPath(item.getFile().getPath());
                mDetailDialog.setSuggestion(mContext.getString(R.string.junk_clean_suggestion));
                mDetailDialog.setRlConfirmText(mContext.getString(R.string.go_on));
                mDetailDialog.show();
            }
        });

    }

    public void setSelectAll(boolean selectAll) {
        for (JunkListItem item : mJunkItemsList) {
            item.setChecked(selectAll);
        }
        mListViewAdapter.notifyDataSetChanged();
        updateSelectedResult();
    }

    private void setJunkTotalNum(int num) {
        if (mTvTotalNum != null) {
            mTvTotalNum.setText(String.format(Locale.getDefault(), "%d", num));
        }
    }

    public void setListViewDetail(@NonNull List<JunkListItem> list) {
        setJunkTotalNum(list.size());
        mJunkItemsList.clear();
        mJunkItemsList.addAll(list);
        mListViewAdapter.notifyDataSetChanged();
    }

    private void updateFolderListViewHeight(boolean isOpened) {
        mRlTitle.measure(0, 0);
        int totalHeight = mRlTitle.getMeasuredHeight();
        LinearLayout.LayoutParams totalParams = (LayoutParams) getLayoutParams();
        if (isOpened) {
            int count = mListViewAdapter.getCount();
            if (count > 0) {
                for (int i = 0; i < count; i++) {
                    View item = mListViewAdapter.getView(i, null, mListViewDetail);
                    //item的布局要求是linearLayout，否则measure(0,0)会报错。
                    item.measure(0, 0);
                    //计算出所有item高度的总和
                    totalHeight += item.getMeasuredHeight();
                }
                //
                totalHeight += mListViewDetail.getDividerHeight() * (count - 1);
            }
        } else {
            mLlTotal.measure(0, 0);
            totalHeight += mLlTotal.getMeasuredHeight();
        }
        totalParams.height = totalHeight;
        setLayoutParams(totalParams);
    }

    private void updateSelectedResult() {
        long selectedSize = 0;
        boolean hasUnChecked = false;
        int selectedNum = 0;
        for (JunkListItem item : mJunkItemsList) {
            if (item.getChecked()) {
                selectedSize += item.getSize();
                selectedNum++;
            } else {
                hasUnChecked = true;
            }
        }
        if (selectedSize > 0) {
            mTvSelectedSize.setText(SystemUtils.getFileSize(selectedSize));
            if (hasUnChecked) {
                mCheckBox.setCheckBoxPartialSelected();
            } else {
                mCheckBox.setChecked(true);
            }
            mTvTotalNum.setTextColor(mContext.getResources().getColor(R.color.clean_button));
            mTvTotalNum.setText(String.format(Locale.getDefault(), "%d", selectedNum));
        } else {
            mTvSelectedSize.setText("");
            mCheckBox.setChecked(false);
            mTvTotalNum.setTextColor(mContext.getResources().getColor(R.color.grey_text_color));
            mTvTotalNum.setText(String.format(Locale.getDefault(), "%d", mJunkItemsList.size()));
        }
        if (mOnSelectedResultChangedListener != null) {
            mOnSelectedResultChangedListener.onSelectChanged(this, selectedSize);
        }
    }

    public void setOnSelectedResultChangedListener(OnSelectedResultChangedListener listener) {
        mOnSelectedResultChangedListener = listener;
    }

    public interface OnSelectedResultChangedListener {
        void onSelectChanged(View view, long selectedSize);
    }

    public static class JunkListViewAdapter extends BaseAdapter {
        private List<JunkListItem> mList;
        private Context mContext;
        private LayoutInflater mLayoutInflater;
        private FoldableListView mListView;

        public JunkListViewAdapter(@NonNull List<JunkListItem> listItems, @NonNull Context context, FoldableListView listView) {
            this.mList = listItems;
            mContext = context;
            this.mLayoutInflater = LayoutInflater.from(context);
            mListView = listView;
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public Object getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.junk_detail_listview_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.imgIcon = convertView.findViewById(R.id.img_junk_detail_icon);
                viewHolder.tvName = convertView.findViewById(R.id.tv_junk_file_name);
                viewHolder.tvSize = convertView.findViewById(R.id.tv_junk_file_size);
                viewHolder.checkbox = convertView.findViewById(R.id.junk_item_checkbox);
                viewHolder.checkbox.setVisibility(null != mListView ? VISIBLE : GONE);
                viewHolder.checkbox.setOnCheckBoxSelectedListener(new ThreeStatusCheckBox.OnCheckBoxSelectedListener() {
                    @Override
                    public void onChecked() {
                        viewHolder.tvSize.setTextColor(mContext.getResources().getColor(R.color.clean_button));
                        int tag = viewHolder.checkbox.getmTag();
                        if (tag > -1 && tag < getCount()) {
                            mList.get(tag).setChecked(true);
                            mListView.updateSelectedResult();
                        }
                    }

                    @Override
                    public void onUnChecked() {
                        viewHolder.tvSize.setTextColor(mContext.getResources().getColor(R.color.grey_text_color));
                        int tag = viewHolder.checkbox.getmTag();
                        if (tag > -1 && tag < getCount()) {
                            mList.get(tag).setChecked(false);
                            mListView.updateSelectedResult();
                        }
                    }
                });
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            JunkListItem item = mList.get(position);
            if (item != null) {
                Drawable icon = item.getApplicationIcon();
                if (icon != null) {
                    viewHolder.imgIcon.setImageDrawable(icon);
                } else {
                    viewHolder.imgIcon.setImageResource(R.mipmap.folder_icon);
                }
                String appName = item.getApplicationName();
                if (appName != null) {
                    viewHolder.tvName.setText(appName);
                } else {
                    viewHolder.tvName.setText(item.getFile().getName());
                }
                long size = item.getSize();
                viewHolder.tvSize.setText(SystemUtils.getFileSize(size));
                boolean isChecked = item.getChecked();
                viewHolder.checkbox.setChecked(isChecked);
                if (isChecked) {
                    viewHolder.tvSize.setTextColor(mContext.getResources().getColor(R.color.clean_button));
                } else {
                    viewHolder.tvSize.setTextColor(mContext.getResources().getColor(R.color.grey_text_color));
                }
                //每个checkbox要知道自己是哪个item里的
                viewHolder.checkbox.setmTag(position);
            }
            return convertView;
        }
    }

    private static class ViewHolder {
        ImageView imgIcon;
        TextView tvName;
        TextView tvSize;
        ThreeStatusCheckBox checkbox;
    }


}
