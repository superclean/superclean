package com.youtupu.superclean.view;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/3/4.
 */

public class ScanningView extends View {
    private int mWidth;
    private int mHeight;
    private Paint mRingPaint;
    private int mHalfRingWidth;
    private int mCircleLowColor;
    private int mCircleHighColor;
    private ArgbEvaluator mEvaluator;
    private Paint mCirclePaint;
    private float mRadius;
    private int mScanLowColor;
    private int mScanHighColor;
    private Paint mScanPaint;
    private float mPercent = 0;
    private float mSweepAngle = 0;
    private RectF mCircleRectF;
    private float mCenterAreaWidth;
    private float mCenterAreaHeight;
    private int mCenterAreaLowColor;
    private int mCenterAreaHighColor;
    private RectF mCenterRectF;
    private Paint mCenterAreaPaint;
    private float mDoneAngle = 0;
    private float mRingRadius;
    private int mCircleColor;

    public ScanningView(Context context) {
        this(context, null, 0, 0);
    }

    public ScanningView(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public ScanningView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ScanningView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ScanningView);
        mCenterAreaWidth = typedArray.getDimension(R.styleable.ScanningView_center_area_width, 0);
        mCenterAreaHeight = typedArray.getDimension(R.styleable.ScanningView_center_area_height, 0);
        boolean greenStyle = typedArray.getBoolean(R.styleable.ScanningView_green_style, true);
        typedArray.recycle();
        if (greenStyle) {
            mCircleLowColor = getResources().getColor(R.color.circle_green);
            mScanLowColor = getResources().getColor(R.color.scan_green);
            mCenterAreaLowColor = getResources().getColor(R.color.thermometer_green);
        } else {
            mCircleLowColor = getResources().getColor(R.color.gear_blue3);
            mScanLowColor = getResources().getColor(R.color.gear_blue2);
            mCenterAreaLowColor = getResources().getColor(R.color.colorMainBlue);
        }
        mCircleColor = mCircleLowColor;
        mCircleHighColor = getResources().getColor(R.color.circle_pink);
        mScanHighColor = getResources().getColor(R.color.scan_pink);
        mCenterAreaHighColor = getResources().getColor(R.color.colorMainPink);
        mRingPaint = new Paint();
        mRingPaint.setAntiAlias(true);
        mHalfRingWidth = SystemUtils.dp2px(context, 2);
        mRingPaint.setStrokeWidth(mHalfRingWidth * 2);
        mRingPaint.setStyle(Paint.Style.STROKE);
        mCirclePaint = new Paint();
        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setStyle(Paint.Style.FILL);
        mScanPaint = new Paint();
        mScanPaint.setAntiAlias(true);
        mScanPaint.setStyle(Paint.Style.FILL);
        mEvaluator = new ArgbEvaluator();
        mRingPaint.setColor(mCircleLowColor);
        mCirclePaint.setColor(mCircleLowColor);
        mScanPaint.setColor(mScanLowColor);
        mCenterAreaPaint = new Paint();
        mCenterAreaPaint.setAntiAlias(true);
        mCenterAreaPaint.setColor(mCenterAreaLowColor);
    }

    public void setPercent(float percent) {
        if (percent < 0 || percent > 1) return;
        if (mPercent != percent) {
            mPercent = percent;
            mCircleColor = (int) mEvaluator.evaluate(percent, mCircleLowColor, mCircleHighColor);
            int scanColor = (int) mEvaluator.evaluate(percent, mScanLowColor, mScanHighColor);
            int centerAreaColor = (int) mEvaluator.evaluate(percent, mCenterAreaLowColor, mCenterAreaHighColor);
            mCirclePaint.setColor(mCircleColor);
            mScanPaint.setColor(scanColor);
            mCenterAreaPaint.setColor(centerAreaColor);
            invalidate();
        }
    }

    public float getPercent() {
        return mPercent;
    }

    public void setSweepAngle(float sweepAngle) {
        if (sweepAngle < 0 || sweepAngle > 480) return;
        if (mSweepAngle != sweepAngle) {
            mSweepAngle = sweepAngle;
            float percent = sweepAngle / 480f;
            mRingRadius = (mWidth / 10f) * percent + mRadius;
            mRingPaint.setColor(mCircleColor);
            mRingPaint.setAlpha((int) ((1f - percent) * 255));
            invalidate();
        }
    }

    public float getSweepAngle() {
        return mSweepAngle;
    }

    public void setDoneAngle(float doneAngle) {
        if (doneAngle < 0 || doneAngle > 360) return;
        if (mDoneAngle != doneAngle) {
            mDoneAngle = doneAngle;
            invalidate();
        }
    }

    public float getDoneAngle() {
        return mDoneAngle;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        mRadius = mWidth * 2.0f / 5;
        mRingRadius = mRadius;
        mCircleRectF = new RectF(mWidth / 2f - mRadius + 1, mHeight / 2f - mRadius + 1, mWidth / 2f + mRadius - 1, mHeight / 2f + mRadius - 1);
        mCenterRectF = new RectF(mWidth / 2f - mCenterAreaWidth / 2, mHeight / 2f - mCenterAreaHeight / 2, mWidth / 2f + mCenterAreaWidth / 2, mHeight / 2f + mCenterAreaHeight / 2);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(mWidth / 2f, mHeight / 2f, mRadius, mCirclePaint);
        //画扫描扇形
        if (mSweepAngle > 0) {
            canvas.drawArc(mWidth / 2f - mRingRadius + mHalfRingWidth,
                    mHeight / 2f - mRingRadius + mHalfRingWidth,
                    mWidth / 2f + mRingRadius - mHalfRingWidth,
                    mHeight / 2f + mRingRadius - mHalfRingWidth,
                    0, 360f, false, mRingPaint);
            float startAngle;
            float sweepAngle;
            if (mSweepAngle < 240) {
                startAngle = mSweepAngle / 2 - 90;
                sweepAngle = mSweepAngle / 2;
            } else if (mSweepAngle < 300) {
                startAngle = mSweepAngle - 210;
                sweepAngle = 120;
            } else {
                startAngle = mSweepAngle - 210;
                sweepAngle = (480 - mSweepAngle) * 2.0f / 3;
            }
            canvas.drawArc(mCircleRectF, startAngle, sweepAngle, true, mScanPaint);
        }
        if (mDoneAngle > 0) {
            canvas.drawArc(mCircleRectF, -90, mDoneAngle, true, mScanPaint);
        }
        canvas.drawRect(mCenterRectF, mCenterAreaPaint);
    }

    public final static Property<ScanningView, Float> PERCENT = new Property<ScanningView, Float>(Float.class, "percent") {
        @Override
        public Float get(ScanningView object) {
            return object.getPercent();
        }

        @Override
        public void set(ScanningView object, Float value) {
            object.setPercent(value);
        }
    };

    public final static Property<ScanningView, Float> SWEEP_ANGLE = new Property<ScanningView, Float>(Float.class, "sweep_angle") {
        @Override
        public Float get(ScanningView object) {
            return object.getSweepAngle();
        }

        @Override
        public void set(ScanningView object, Float value) {
            object.setSweepAngle(value);
        }
    };

    public final static Property<ScanningView, Float> DONE_ANGLE = new Property<ScanningView, Float>(Float.class, "done_angle") {
        @Override
        public Float get(ScanningView object) {
            return object.getDoneAngle();
        }

        @Override
        public void set(ScanningView object, Float value) {
            object.setDoneAngle(value);
        }
    };

}
