package com.youtupu.superclean.view;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/1/8.
 */

public class GearCenterView extends View {
    private int mWidth;
    private int mHeight;
    private Paint mStripePaint;
    private Paint mBackCirclePaint;
    private int mStripeLowColor;
    private int mBackCircleLowColor;
    private int mStripeHighColor;
    private int mBackCircleHighColor;
    private int mStripeMiddleColor;
    private int mBackCircleMiddleColor;
    private int mWideLineWidth;
    private float mPercent;
    private ArgbEvaluator mEvaluator;
    private Paint mInnerCirclePaint;
    private Path mPath;
    private RectF mLineRectF;
    private float mInnerCircleRadius;
    private Paint mShadowPaint;
    private int mShadowStartColor;
    private int mShadowEndColor;
    private int mShadowWidth;
    private float mAngle = 0;

    public GearCenterView(Context context) {
        this(context, null, 0, 0);
    }

    public GearCenterView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public GearCenterView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public GearCenterView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.GearCenterView);
        mPercent = typedArray.getFloat(R.styleable.GearCenterView_initial_percent, 1.0f);
        typedArray.recycle();
        mStripeLowColor = context.getResources().getColor(R.color.gear_blue2);
        mBackCircleLowColor = context.getResources().getColor(R.color.gear_blue3);
        mStripeHighColor = context.getResources().getColor(R.color.gear_pink2);
        mBackCircleHighColor = context.getResources().getColor(R.color.gear_pink3);
        mStripeMiddleColor = context.getResources().getColor(R.color.gear_purple2);
        mBackCircleMiddleColor = context.getResources().getColor(R.color.gear_purple3);
        mShadowStartColor = context.getResources().getColor(R.color.shadow_start_color);
        mShadowEndColor = context.getResources().getColor(R.color.shadow_end_color);
        mWideLineWidth = SystemUtils.dp2px(context, 30);
        mShadowWidth = SystemUtils.dp2px(context, 20);
        mInnerCirclePaint = new Paint();
        mStripePaint = new Paint();
        mBackCirclePaint = new Paint();
        mInnerCirclePaint.setAntiAlias(true);
        mStripePaint.setAntiAlias(true);
        mBackCirclePaint.setAntiAlias(true);
        mInnerCirclePaint.setColor(Color.WHITE);
        mInnerCirclePaint.setStyle(Paint.Style.FILL);
        mStripePaint.setStyle(Paint.Style.STROKE);
        mStripePaint.setStrokeWidth(mWideLineWidth);
        mPath = new Path();
        mEvaluator = new ArgbEvaluator();
        mShadowPaint = new Paint();
        mShadowPaint.setAntiAlias(true);
        afterSetPercent();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        mPath.moveTo(mWidth / 2f, 0);
        mPath.lineTo(0, 0);
        mPath.lineTo(0, mHeight);
        mPath.lineTo(mWidth, mHeight);
        mPath.lineTo(mWidth, 0);
        mPath.lineTo(mWidth / 2f, 0);
        mPath.addArc(0, 0, mWidth, mHeight, 270, 360);
        mLineRectF = new RectF(mWidth / 2f - mWideLineWidth / 2f, mWideLineWidth / 2f, mWidth * 3 / 2f - mWideLineWidth / 2f, mHeight - mWideLineWidth / 2f);
        mInnerCircleRadius = mWidth * 3 / 8f - 1;
        float percent1 = mInnerCircleRadius * 1.0f / (mInnerCircleRadius + mShadowWidth);
        float percent2 = (1 - percent1) / 2 + percent1;
        RadialGradient radialGradient = new RadialGradient(mWidth / 2f, mHeight / 2f,
                mInnerCircleRadius + mShadowWidth, new int[]{mShadowStartColor, mShadowStartColor, mShadowEndColor, mShadowEndColor},
                new float[]{0, percent1, percent2, 1.0f}, Shader.TileMode.MIRROR);
        mShadowPaint.setShader(radialGradient);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(mWidth / 2f, mHeight / 2f, mWidth / 2f, mBackCirclePaint);
        canvas.save();
        canvas.rotate(mAngle, mWidth / 2f, mHeight / 2f);
        for (int i = 0; i < 12; i++) {
            canvas.rotate(30, mWidth / 2f, mHeight / 2f);
            canvas.drawArc(mLineRectF, 180, 90, false, mStripePaint);
        }
        canvas.restore();
        canvas.drawCircle(mWidth / 2f, mHeight / 2f, mInnerCircleRadius + mShadowWidth, mShadowPaint);
        canvas.drawCircle(mWidth / 2f, mHeight / 2f, mInnerCircleRadius, mInnerCirclePaint);
        canvas.drawPath(mPath, mInnerCirclePaint);
    }


    public void setPercent(float percent) {
        if (percent < 0 || percent > 1) return;
        if (mPercent != percent) {
            mPercent = percent;
            afterSetPercent();
            invalidate();
        }
    }

    public float getPercent() {
        return mPercent;
    }

    private void afterSetPercent() {
        int stripeColor;
        int backCircleColor;
        if (mPercent < 0.5f) {
            stripeColor = (int) mEvaluator.evaluate(mPercent * 2, mStripeLowColor, mStripeMiddleColor);
            backCircleColor = (int) mEvaluator.evaluate(mPercent * 2, mBackCircleLowColor, mBackCircleMiddleColor);
        } else {
            stripeColor = (int) mEvaluator.evaluate((mPercent - 0.5f) * 2, mStripeMiddleColor, mStripeHighColor);
            backCircleColor = (int) mEvaluator.evaluate((mPercent - 0.5f) * 2, mBackCircleMiddleColor, mBackCircleHighColor);
        }
        mStripePaint.setColor(stripeColor);
        mBackCirclePaint.setColor(backCircleColor);
    }

    public void setAngle(float angle) {
        if (mAngle != angle) {
            mAngle = angle;
            invalidate();
        }
    }

    public float getAngle() {
        return mAngle;
    }

    public static final Property<GearCenterView, Float> PERCENT = new Property<GearCenterView, Float>(Float.class, "percent") {
        @Override
        public Float get(GearCenterView object) {
            return object.getPercent();
        }

        @Override
        public void set(GearCenterView object, Float value) {
            object.setPercent(value);
        }
    };

    public static final Property<GearCenterView, Float> ANGLE = new Property<GearCenterView, Float>(Float.class, "angle") {
        @Override
        public Float get(GearCenterView object) {
            return object.getAngle();
        }

        @Override
        public void set(GearCenterView object, Float value) {
            object.setAngle(value);
        }
    };
}
