package com.youtupu.superclean.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Property;

import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/6/5.
 */
public class FileSizeText extends android.support.v7.widget.AppCompatTextView {
    private long mFileSize;

    public FileSizeText(Context context) {
        super(context);
    }

    public FileSizeText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FileSizeText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setFileSize(long fileSize) {
        if (fileSize < 0) return;
        mFileSize = fileSize;
        setText(SystemUtils.getFileSize(fileSize));
    }

    public long getFileSize() {
        return mFileSize;
    }

    public static final Property<FileSizeText, Integer> FILE_SIZE = new Property<FileSizeText, Integer>(Integer.class, "percent") {

        @Override
        public Integer get(FileSizeText object) {
            return (int) (object.getFileSize() / 1000);
        }

        @Override
        public void set(FileSizeText object, Integer value) {
            long size = value;
            object.setFileSize(size * 1000);
        }
    };

}
