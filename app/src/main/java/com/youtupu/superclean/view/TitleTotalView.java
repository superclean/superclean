package com.youtupu.superclean.view;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;

import java.util.List;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/2/25.
 */

public class TitleTotalView extends View {

    private int mWidth;
    private int mHeight;
    private int mLeftHighColor;
    private String mLeftText;
    private String mRightText;
    private int mLeftTextSize;
    private int mRightTextSize;
    private float mDistance;
    private Paint mLeftTextPaint;
    private Paint mRightTextPaint;
    private float mLeftBaseLine;
    private int mFileSize;
    private int mLeftLowColor;
    private float mPercent;
    private int mLineWidth;
    private Paint mLinePaint;
    private int mRightHighColor;
    private int mRightLowColor;
    private ArgbEvaluator mArgbEvaluator;
    private int mStartSize = 0;

    public TitleTotalView(Context context) {
        this(context, null, 0, 0);
    }

    public TitleTotalView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public TitleTotalView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public TitleTotalView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TitleTotalView);
        mPercent = typedArray.getFloat(R.styleable.TitleTotalView_title_percent, 1);
        mLeftHighColor = typedArray.getColor(R.styleable.TitleTotalView_high_left_color, context.getResources().getColor(R.color.colorMainPink));
        mLeftLowColor = typedArray.getColor(R.styleable.TitleTotalView_low_left_color, context.getResources().getColor(R.color.colorMainBlue));
        mRightHighColor = typedArray.getColor(R.styleable.TitleTotalView_high_right_color, context.getResources().getColor(R.color.colorMainPink));
        mRightLowColor = typedArray.getColor(R.styleable.TitleTotalView_low_right_color, context.getResources().getColor(R.color.colorMainBlue));
        mLeftText = typedArray.getString(R.styleable.TitleTotalView_left_text);
        if (mLeftText == null) {
            mLeftText = "";
        }
        mRightText = typedArray.getString(R.styleable.TitleTotalView_right_text);
        if (mRightText == null) {
            mRightText = "";
        }
        mLeftTextSize = typedArray.getDimensionPixelSize(R.styleable.TitleTotalView_left_text_size, SystemUtils.dp2px(context, 48));
        mRightTextSize = typedArray.getDimensionPixelSize(R.styleable.TitleTotalView_right_text_size, SystemUtils.dp2px(context, 24));
        mDistance = typedArray.getDimension(R.styleable.TitleTotalView_distance_between, SystemUtils.dp2px(context, 6));
        typedArray.recycle();
        mLeftTextPaint = new Paint();
        mLeftTextPaint.setAntiAlias(true);
        mLeftTextPaint.setFakeBoldText(true);
        mLeftTextPaint.setTextAlign(Paint.Align.RIGHT);
        mLeftTextPaint.setTextSize(mLeftTextSize);
        mRightTextPaint = new Paint();
        mRightTextPaint.setAntiAlias(true);
        mRightTextPaint.setFakeBoldText(true);
        mRightTextPaint.setTextAlign(Paint.Align.LEFT);
        mRightTextPaint.setTextSize(mRightTextSize);
        mLineWidth = SystemUtils.dp2px(context, 10);
        mLinePaint = new Paint();
        mLinePaint.setAntiAlias(true);
        mLinePaint.setStyle(Paint.Style.STROKE);
        mLinePaint.setStrokeWidth(mLineWidth);
        mArgbEvaluator = new ArgbEvaluator();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        //大写字母和数字的baseline就是字母和数字的底部
        Paint.FontMetrics leftTextPaintFontMetrics = mLeftTextPaint.getFontMetrics();
        mLeftBaseLine = (leftTextPaintFontMetrics.bottom - leftTextPaintFontMetrics.top) / 2 - leftTextPaintFontMetrics.bottom + mHeight / 2f;
        afterSetPercent();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawText(mLeftText, mWidth / 2f - mDistance, mLeftBaseLine, mLeftTextPaint);
        canvas.drawText(mRightText, mWidth / 2f + mDistance, mLeftBaseLine, mRightTextPaint);
        canvas.drawLine(0, mHeight - mLineWidth / 2f - 1, mWidth, mHeight - mLineWidth / 2f - 1, mLinePaint);
    }

    public void afterSetPercent() {
        int leftColor = (int) mArgbEvaluator.evaluate(mPercent, mLeftLowColor, mLeftHighColor);
        int rightColor = (int) mArgbEvaluator.evaluate(mPercent, mRightLowColor, mRightHighColor);
        mLeftTextPaint.setColor(leftColor);
        mRightTextPaint.setColor(rightColor);
        LinearGradient gradient = new LinearGradient(0, 0, mWidth, mHeight, leftColor, Color.WHITE, Shader.TileMode.MIRROR);
        mLinePaint.setShader(gradient);
    }

    public float getPercent() {
        return mPercent;
    }

    public void setPercent(float percent) {
        if (percent > 1 || percent < 0) return;
        if (mPercent != percent) {
            mPercent = percent;
            afterSetPercent();
            invalidate();
        }
    }

    public int getFileSize() {
        return mFileSize;
    }

    public void setFileSize(int fileSize) {
        if (fileSize < 0 || fileSize == mFileSize) return;
        mFileSize = fileSize;
        long size = ((long) mFileSize) * 1000;
        List<String> strings = SystemUtils.getFileSizeArray(size);
        mLeftText = strings.get(0);
        mRightText = strings.get(1);
        if (mStartSize > 0 && fileSize <= mStartSize) {
            mPercent = fileSize * 1.0f / mStartSize;
            afterSetPercent();
        }
        invalidate();
    }

    public void setStartSize(int startSize) {
        mStartSize = startSize;
    }

    public final static Property<TitleTotalView, Integer> FILE_SIZE = new Property<TitleTotalView, Integer>(Integer.class, "file_size") {
        @Override
        public Integer get(TitleTotalView object) {
            return object.getFileSize();
        }

        @Override
        public void set(TitleTotalView object, Integer value) {
            object.setFileSize(value);
        }
    };

    public void setLeftRightText(String leftText, String rightText) {
        mLeftText = leftText;
        mRightText = rightText;
        invalidate();
    }

}
