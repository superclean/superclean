package com.youtupu.superclean.view;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Locale;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/5/16.
 */
public class ScanBattery extends RelativeLayout {

    private ImageView mImgScan;
    private int mDistance;
    private ObjectAnimator mAnimator;
    private float mPercent;
    private InnerBattery mInnerBattery;
    private TextView mTvPercent;

    public ScanBattery(Context context) {
        this(context, null, 0, 0);
    }

    public ScanBattery(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public ScanBattery(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ScanBattery(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        View.inflate(context, R.layout.scan_battery, this);
        mImgScan = findViewById(R.id.img_scan);
        mDistance = SystemUtils.dp2px(context, 140);
        mInnerBattery = findViewById(R.id.inner_battery);
        mTvPercent = findViewById(R.id.tv_percent);
    }

    public void startScan() {
        if (mAnimator != null && mAnimator.isRunning()) {
            mAnimator.cancel();
        }
        mAnimator = ObjectAnimator.ofFloat(mImgScan, "translationX", mDistance * mPercent);
        mAnimator.setDuration(2000);
        mAnimator.setRepeatCount(ValueAnimator.INFINITE);
        mAnimator.start();
    }

    public void setPercent(float percent) {
        if (percent < 0 || percent > 1) return;
        mPercent = percent;
        mInnerBattery.setPercent(percent);
        String center = String.format(Locale.getDefault(), "%.0f", percent * 100) + "%";
        mTvPercent.setText(center);
    }
}
