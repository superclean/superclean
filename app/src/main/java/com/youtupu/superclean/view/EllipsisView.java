package com.youtupu.superclean.view;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.lang.ref.WeakReference;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/3/5.
 */

public class EllipsisView extends LinearLayout {

    private static final int ELLIPSIS_ANIMATION = 0;
    private ImageView mImgDot1;
    private ImageView mImgDot2;
    private ImageView mImgDot3;
    private MyHandler mHandler;
    private int mIndex = 0;

    public EllipsisView(Context context) {
        this(context, null, 0, 0);
    }

    public EllipsisView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public EllipsisView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public EllipsisView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setOrientation(HORIZONTAL);
        setGravity(Gravity.BOTTOM);
        int padding = SystemUtils.dp2px(context, 10);
        setPadding(padding, padding, padding, padding);
        setMinimumHeight(SystemUtils.dp2px(context, 30));
        setMinimumWidth(SystemUtils.dp2px(context, 50));
        initView(context);
        mHandler = new MyHandler(this);
    }

    private void initView(Context context) {
        View.inflate(context, R.layout.ellipsis_view, this);
        mImgDot1 = findViewById(R.id.img_dot1);
        mImgDot2 = findViewById(R.id.img_dot2);
        mImgDot3 = findViewById(R.id.img_dot3);
    }

    public void startEllipsisAnimation() {
        mIndex = 0;
        mHandler.removeMessages(ELLIPSIS_ANIMATION);
        mHandler.sendEmptyMessage(ELLIPSIS_ANIMATION);
    }

    public void stopEllipsisAnimation() {
        mHandler.removeMessages(ELLIPSIS_ANIMATION);
    }

    private static class MyHandler extends Handler {
        private WeakReference<EllipsisView> mReference;

        MyHandler(EllipsisView view) {
            mReference = new WeakReference<EllipsisView>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            EllipsisView mEllipsisView = mReference.get();
            if (mEllipsisView == null) return;
            switch (msg.what) {
                case ELLIPSIS_ANIMATION:
                    if (mEllipsisView.mIndex % 4 == 0) {
                        mEllipsisView.mImgDot1.setVisibility(INVISIBLE);
                        mEllipsisView.mImgDot2.setVisibility(INVISIBLE);
                        mEllipsisView.mImgDot3.setVisibility(INVISIBLE);
                    } else if (mEllipsisView.mIndex % 4 == 1) {
                        mEllipsisView.mImgDot1.setVisibility(VISIBLE);
                        mEllipsisView.mImgDot2.setVisibility(INVISIBLE);
                        mEllipsisView.mImgDot3.setVisibility(INVISIBLE);
                    } else if (mEllipsisView.mIndex % 4 == 2) {
                        mEllipsisView.mImgDot1.setVisibility(VISIBLE);
                        mEllipsisView.mImgDot2.setVisibility(VISIBLE);
                        mEllipsisView.mImgDot3.setVisibility(INVISIBLE);
                    } else if (mEllipsisView.mIndex % 4 == 3) {
                        mEllipsisView.mImgDot1.setVisibility(VISIBLE);
                        mEllipsisView.mImgDot2.setVisibility(VISIBLE);
                        mEllipsisView.mImgDot3.setVisibility(VISIBLE);
                    }
                    mEllipsisView.mIndex++;
                    sendEmptyMessageDelayed(ELLIPSIS_ANIMATION, 500);
                    break;
            }
        }
    }
}
