package com.youtupu.superclean.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.youtupu.superclean.bean.AppItem;

import java.util.ArrayList;
import java.util.List;

import com.youtupu.superclean.R;

import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/3/12.
 */

public class RunningAppFoldView extends LinearLayout {
    private ImageView mImgOpen;
    private LinearLayout mLlTotal;
    private ListView mLvDetail;
    private boolean mIsOpen = true;
    private int mTitleHeight;
    private List<AppItem> mAppItemList;
    private AppListAdapter mListAdapter;
    private TextView mTvSelectNum;
    private ThreeStatusCheckBox mCheckBox;
    private FoldViewListener mFoldViewListener;

    public RunningAppFoldView(Context context) {
        this(context, null, 0, 0);
    }

    public RunningAppFoldView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public RunningAppFoldView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public RunningAppFoldView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RunningAppFoldView);
        String title = typedArray.getString(R.styleable.RunningAppFoldView_app_total_title);
        Drawable iconDrawable = typedArray.getDrawable(R.styleable.RunningAppFoldView_app_total_icon);
        typedArray.recycle();
        setOrientation(VERTICAL);
        mTitleHeight = SystemUtils.dp2px(context, 40);
        View.inflate(context, R.layout.running_app_fold_view, this);
        LinearLayout llTitle = findViewById(R.id.ll_title);
        TextView tvTitle = findViewById(R.id.tv_title);
        mImgOpen = findViewById(R.id.img_open);
        mLlTotal = findViewById(R.id.ll_total);
        mLvDetail = findViewById(R.id.listview_detail);
        ImageView imgIcon = findViewById(R.id.img_total_icon);
        mTvSelectNum = findViewById(R.id.tv_total_num);
        mCheckBox = findViewById(R.id.checkbox);
        tvTitle.setText(title);
        imgIcon.setImageDrawable(iconDrawable);
        mAppItemList = new ArrayList<>();
        mListAdapter = new AppListAdapter(mAppItemList, context);
        mLvDetail.setAdapter(mListAdapter);
        llTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsOpen = !mIsOpen;
                if (mIsOpen) {
                    mLlTotal.setVisibility(GONE);
                    mLvDetail.setVisibility(VISIBLE);
                    mImgOpen.setImageResource(R.mipmap.arrow_up);
                } else {
                    mLlTotal.setVisibility(VISIBLE);
                    mLvDetail.setVisibility(GONE);
                    mImgOpen.setImageResource(R.mipmap.arrow_down);
                }
                calculateHeight();
            }
        });
        mCheckBox.setOnCheckBoxSelectedListener(new ThreeStatusCheckBox.OnCheckBoxSelectedListener() {
            @Override
            public void onChecked() {
                for (AppItem item : mAppItemList) {
                    item.setChecked(true);

                }
                mListAdapter.notifyDataSetChanged();
                updateSelecteStatus();
            }

            @Override
            public void onUnChecked() {
                for (AppItem item : mAppItemList) {
                    item.setChecked(false);
                }
                mListAdapter.notifyDataSetChanged();
                updateSelecteStatus();
            }
        });
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                calculateHeight();
            }
        });
    }

    public void setAppList(@NonNull List<AppItem> appList) {
        if (mAppItemList != null) {
            mAppItemList.clear();
            mAppItemList.addAll(appList);
            mListAdapter.notifyDataSetChanged();
            updateSelecteStatus();
        }
    }

    public List<AppItem> getAppList() {
        return mAppItemList;
    }

    private void calculateHeight() {
        LayoutParams params = (LayoutParams) getLayoutParams();
        int totalHeight = mTitleHeight;
        if (mIsOpen) {
            int count = mListAdapter.getCount();
            if (count > 0) {
                for (int i = 0; i < count; i++) {
                    View item = mListAdapter.getView(i, null, mLvDetail);
                    //item的布局要求是linearLayout，否则measure(0,0)会报错。
                    item.measure(0, 0);
                    //计算出所有item高度的总和
                    totalHeight += item.getMeasuredHeight();
                }
                //加上所有分割线的高度
                totalHeight += mLvDetail.getDividerHeight() * (count - 1);
            }
        } else {
            mLlTotal.measure(0, 0);
            totalHeight += mLlTotal.getMeasuredHeight();
        }
        params.height = totalHeight;
        setLayoutParams(params);
    }

    private void updateSelecteStatus() {
        int selectedNum = 0;
        for (AppItem item : mAppItemList) {
            if (item.isChecked()) {
                selectedNum++;
            }
        }
        if (selectedNum == 0) {
            mCheckBox.setChecked(false);
            mTvSelectNum.setTextColor(getResources().getColor(R.color.grey_text_color));
            mTvSelectNum.setText(String.valueOf(mAppItemList.size()));
        } else if (selectedNum == mAppItemList.size()) {
            mCheckBox.setChecked(true);
            mTvSelectNum.setTextColor(getResources().getColor(R.color.clean_button));
            mTvSelectNum.setText(String.valueOf(selectedNum));
        } else {
            mCheckBox.setCheckBoxPartialSelected();
            mTvSelectNum.setTextColor(getResources().getColor(R.color.clean_button));
            mTvSelectNum.setText(String.valueOf(selectedNum));
        }
        if (mFoldViewListener != null) {
            mFoldViewListener.onSelectChange(selectedNum);
        }
    }

    private class AppListAdapter extends BaseAdapter {
        private List<AppItem> mAppItemList;
        private LayoutInflater mInflater;

        AppListAdapter(@NonNull List<AppItem> appItemList, Context context) {
            mAppItemList = appItemList;
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mAppItemList.size();
        }

        @Override
        public Object getItem(int position) {
            return mAppItemList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.app_item_layout, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.icon = convertView.findViewById(R.id.img_app_icon);
                viewHolder.name = convertView.findViewById(R.id.tv_app_name);
                viewHolder.checkBox = convertView.findViewById(R.id.app_item_checkbox);
                viewHolder.checkBox.setOnCheckBoxSelectedListener(new ThreeStatusCheckBox.OnCheckBoxSelectedListener() {
                    @Override
                    public void onChecked() {
                        int index = viewHolder.checkBox.getmTag();
                        if (index > -1 && index < mAppItemList.size()) {
                            mAppItemList.get(index).setChecked(true);
                            updateSelecteStatus();
                        }
                    }

                    @Override
                    public void onUnChecked() {
                        int index = viewHolder.checkBox.getmTag();
                        if (index > -1 && index < mAppItemList.size()) {
                            mAppItemList.get(index).setChecked(false);
                            updateSelecteStatus();
                        }
                    }
                });
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            AppItem item = mAppItemList.get(position);
            viewHolder.icon.setImageDrawable(item.getIcon());
            viewHolder.name.setText(item.getName());
            viewHolder.checkBox.setChecked(item.isChecked());
            viewHolder.checkBox.setmTag(position);
            return convertView;
        }
    }

    private class ViewHolder {
        ImageView icon;
        TextView name;
        ThreeStatusCheckBox checkBox;
    }

    public void setFoldViewListener(FoldViewListener listener) {
        mFoldViewListener = listener;
    }

    public interface FoldViewListener {
        void onSelectChange(int selectNum);
    }

}
