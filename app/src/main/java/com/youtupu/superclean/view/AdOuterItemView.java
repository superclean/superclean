package com.youtupu.superclean.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import cn.instreet.business.advertise.AdvertiseManager;

/**
 * Created by Jiali on 2019/3/14.
 */
public class AdOuterItemView extends LinearLayout {
    private boolean mIsLoadSuccess = false;

    public AdOuterItemView(Context context) {
        this(context, null, 0, 0);
    }

    public AdOuterItemView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public AdOuterItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public AdOuterItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
//        if (AdvertiseManager.isInit() && AdvertiseManager.getInstance().isNativeAdsCached()) {
//            if (AdvertiseManager.getInstance().showNativeAds(this, AdvertiseManager.MODE_STYLE_2)) {
//                mIsLoadSuccess = true;
//            }
//        }
    }

    public boolean loadSuccess() {
        return mIsLoadSuccess;
    }

}
