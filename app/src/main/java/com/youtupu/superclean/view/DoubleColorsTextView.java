package com.youtupu.superclean.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.youtupu.superclean.R;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/3/1.
 */

public class DoubleColorsTextView extends View {

    private int mWidth;
    private int mHeight;
    private float mLeftTextSize;
    private float mMRightTextSize;
    private int mLeftTextColor;
    private int mRightTextColor;
    private Paint mLeftPaint;
    private Paint mRightPaint;
    private float mDuration;
    private String mLeftText;
    private String mRightText;
    private float mRightStartX;
    private float mLeftEndX;
    private float mLeftBaseLine;
    private float mRightBaseLine;

    public DoubleColorsTextView(Context context) {
        this(context, null, 0, 0);
    }

    public DoubleColorsTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public DoubleColorsTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public DoubleColorsTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.DoubleColorsTextView);
        mLeftTextSize = typedArray.getDimension(R.styleable.DoubleColorsTextView_double_left_text_size, SystemUtils.dp2px(context, 15));
        mMRightTextSize = typedArray.getDimension(R.styleable.DoubleColorsTextView_double_right_text_size, SystemUtils.dp2px(context, 15));
        mLeftTextColor = typedArray.getColor(R.styleable.DoubleColorsTextView_double_left_text_color, Color.GRAY);
        mRightTextColor = typedArray.getColor(R.styleable.DoubleColorsTextView_double_right_text_color, Color.GRAY);
        mLeftText = typedArray.getString(R.styleable.DoubleColorsTextView_double_left_text);
        if (mLeftText == null) {
            mLeftText = "";
        }
        mRightText = typedArray.getString(R.styleable.DoubleColorsTextView_double_right_text);
        if (mRightText == null) {
            mRightText = "";
        }
        mDuration = typedArray.getDimension(R.styleable.DoubleColorsTextView_double_text_duration, SystemUtils.dp2px(context, 4));
        typedArray.recycle();
        initData();
    }

    private void initData() {
        mLeftPaint = new Paint();
        mLeftPaint.setAntiAlias(true);
        mLeftPaint.setTextAlign(Paint.Align.RIGHT);
        mLeftPaint.setColor(mLeftTextColor);
        mRightPaint = new Paint();
        mRightPaint.setAntiAlias(true);
        mRightPaint.setTextAlign(Paint.Align.LEFT);
        mRightPaint.setColor(mRightTextColor);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        calculateWidth();
        calculateHeight();
    }

    private void calculateWidth() {
        mLeftPaint.setTextSize(mLeftTextSize);
        mRightPaint.setTextSize(mMRightTextSize);
        float leftWidth = mLeftPaint.measureText(mLeftText);
        float rightWidth = mRightPaint.measureText(mRightText);
        float totalWidth = leftWidth + rightWidth + mDuration;
        if (rightWidth > totalWidth / 2) {
            mRightStartX = mWidth / 2f - (rightWidth - totalWidth / 2);
        } else if (leftWidth > totalWidth / 2) {
            mRightStartX = (leftWidth - totalWidth / 2) + mDuration + mWidth / 2f;
        } else {
            mRightStartX = totalWidth / 2 - rightWidth + mWidth / 2f;
        }
        mLeftEndX = mRightStartX - mDuration;
    }

    private void calculateHeight() {
        mLeftPaint.setTextSize(mLeftTextSize);
        mRightPaint.setTextSize(mMRightTextSize);
        Paint.FontMetrics fontMetricsLeft = mLeftPaint.getFontMetrics();
        mLeftBaseLine = mHeight / 2f + ((fontMetricsLeft.bottom - fontMetricsLeft.top) / 2 - fontMetricsLeft.bottom);
        Paint.FontMetrics fontMetricsRight = mRightPaint.getFontMetrics();
        mRightBaseLine = mHeight / 2f + ((fontMetricsRight.bottom - fontMetricsRight.top) / 2 - fontMetricsRight.bottom);
    }

    public void setText(String leftText, String rightText) {
        mLeftText = leftText;
        mRightText = rightText;
        calculateWidth();
    }

    public void setTextColor(int leftTextColor, int rightTextColor) {
        if (leftTextColor != 0 && rightTextColor != 0) {
            mLeftPaint.setColor(leftTextColor);
            mRightPaint.setColor(rightTextColor);
        }
    }

    public void setTextSize(float leftTextSize, float rightTextSize) {
        mLeftTextSize = leftTextSize;
        mMRightTextSize = rightTextSize;
        calculateHeight();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawText(mLeftText, mLeftEndX, mLeftBaseLine, mLeftPaint);
        canvas.drawText(mRightText, mRightStartX, mRightBaseLine, mRightPaint);
    }
}
