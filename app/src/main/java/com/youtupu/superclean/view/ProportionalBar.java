package com.youtupu.superclean.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/6/4.
 */
public class ProportionalBar extends View {
    private float mBigPercent = 0f;
    private float mOtherPercent = 0;
    private int mWidth;
    private int mHeight;
    private Paint mBackPaint;
    private Paint mBigPaint;
    private Paint mOtherPaint;

    public ProportionalBar(Context context) {
        this(context, null, 0, 0);
    }

    public ProportionalBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public ProportionalBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ProportionalBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mBackPaint = new Paint();
        mBackPaint.setAntiAlias(true);
        mBackPaint.setColor(context.getResources().getColor(R.color.usable_space));
        mBigPaint = new Paint();
        mBigPaint.setAntiAlias(true);
        mBigPaint.setColor(context.getResources().getColor(R.color.big_file));
        mOtherPaint = new Paint();
        mOtherPaint.setAntiAlias(true);
        mOtherPaint.setColor(context.getResources().getColor(R.color.other_file));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawRect(0, 0, mWidth, mHeight, mBackPaint);
        float bigWidth = mBigPercent * mWidth;
        float otherWidth = (mOtherPercent + mBigPercent) * mWidth;
        canvas.drawRect(bigWidth, 0, otherWidth, mHeight, mOtherPaint);
        canvas.drawRect(0, 0, bigWidth, mHeight, mBigPaint);
    }

    public float getBigPercent() {
        return mBigPercent;
    }

    public float getOtherPercent() {
        return mOtherPercent;
    }

    public void setBigPercent(float bigPercent) {
        if (bigPercent < 0 || bigPercent > 1) return;
        if (mBigPercent != bigPercent) {
            mBigPercent = bigPercent;
            invalidate();
        }
    }

    public void setOtherPercent(float otherPercent) {
        if (otherPercent < 0 || otherPercent > 1) return;
        if (mOtherPercent != otherPercent) {
            mOtherPercent = otherPercent;
            invalidate();
        }
    }

    public static final Property<ProportionalBar, Float> BIG_PERCENT = new Property<ProportionalBar, Float>(Float.class, "big_percent") {

        @Override
        public Float get(ProportionalBar object) {
            return object.getBigPercent();
        }

        @Override
        public void set(ProportionalBar object, Float value) {
            object.setBigPercent(value);
        }
    };

    public static final Property<ProportionalBar, Float> OTHER_PERCENT = new Property<ProportionalBar, Float>(Float.class, "other_percent") {

        @Override
        public Float get(ProportionalBar object) {
            return object.getOtherPercent();
        }

        @Override
        public void set(ProportionalBar object, Float value) {
            object.setOtherPercent(value);
        }
    };
}
