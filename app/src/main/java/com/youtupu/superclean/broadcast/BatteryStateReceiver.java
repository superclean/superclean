package com.youtupu.superclean.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;

import com.youtupu.superclean.base.BaseActivity;
import com.youtupu.superclean.bean.BatteryInfo;

/**
 * Created by Jiali on 2018/12/10.
 */

public class BatteryStateReceiver extends BroadcastReceiver {
    private BatteryInfo batteryInfo;
    private BaseActivity mActivity;

    public BatteryStateReceiver(BaseActivity activity) {
        batteryInfo = new BatteryInfo();
        mActivity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null) return;
        String action = intent.getAction();
        if (action != null && action.equalsIgnoreCase(Intent.ACTION_BATTERY_CHANGED)) {
            //获取电池电压
            int voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            int icon = intent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL, -1);
            String technology = intent.getStringExtra(BatteryManager.EXTRA_TECHNOLOGY);
            //1:充电器充电 2:USB充电 4:无线充电
            int pluggeed = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            int health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, -1);
            //当前是否有电池
            boolean present = intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT, true);
            int temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
            switch (health) {
                //没电
                case BatteryManager.BATTERY_HEALTH_DEAD:
                    break;
                //过热
                case BatteryManager.BATTERY_HEALTH_OVERHEAT:
                    break;
                //健康
                default:
                    break;
            }
            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            switch (status) {
                //充电
                case BatteryManager.BATTERY_STATUS_CHARGING:
                    break;
                //断开充电
                case BatteryManager.BATTERY_STATUS_DISCHARGING:
                    break;
                //充满
                case BatteryManager.BATTERY_STATUS_FULL:
                    break;
                //过充
                case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                    break;
                //状态未知
                case BatteryManager.BATTERY_STATUS_UNKNOWN:
                    break;
            }
            switch (pluggeed) {
                //电池供电
                case 0:
                    break;
                //充电器供电
                case 1:
                    break;
                //usb供电
                case 2:
                    break;
                //无线供电
                case 4:
                    break;
            }
            batteryInfo.setIcon(icon);
            batteryInfo.setLevel(level);
            batteryInfo.setScale(scale);
            batteryInfo.setPresent(present);
            batteryInfo.setTechnology(technology);
            batteryInfo.setVoltage(voltage);
            batteryInfo.setTemperature(temperature / 10);
        }
    }
}
