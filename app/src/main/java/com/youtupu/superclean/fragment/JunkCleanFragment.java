package com.youtupu.superclean.fragment;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youtupu.superclean.activity.JunkDetailActivity;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.youtupu.superclean.R;

import com.youtupu.superclean.activity.OptimizeActivity;
import com.youtupu.superclean.app.SuperCleanApplication;
import com.youtupu.superclean.base.BaseFragment;
import com.youtupu.superclean.bean.JunkListItem;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.TitleTotalView;

/**
 * Created by Jiali on 2019/2/28.
 */

public class JunkCleanFragment extends BaseFragment {
    private static final int LEAVE_ANIMATION = 0;
    private static final int ANIMATION_DONE = 1;
    private static final int SIZE_NUMBER_REDUCE = 2;
    private static final int LEAVE_ANIMATION_DURATION = 200;
    private static final int LEAVE_ANIMATION_INTERVAL = 50;
    private static final int REDUCE_ANIM_DURATION = 500;
    private TitleTotalView mTitleText;
    private LinearLayout mLlContainer;
    private JunkDetailActivity mActivity;
    private List<View> mItemViewList;
    private int mIndex = 0;
    private CleanHandler mHandler;
    boolean mIsCleaning = false;
    private long mTotalJunkSize;
    private Animator mReduceAnim;
    private long mRemainSize;
    private ArrayList<JunkListItem> mUncleanList = new ArrayList<>();

    @Override
    protected void initActivity(Context context) {
        mActivity = (JunkDetailActivity) context;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.junk_clean_fragment_layout;
    }

    @Override
    protected void initView() {
        mTitleText = mActivity.findViewById(R.id.junk_total_select);
        mLlContainer = mRootView.findViewById(R.id.ll_junk_clean_container);
        FrameLayout frameLayout = mActivity.findViewById(R.id.junk_framelayout);
        mUncleanList.clear();
        mHandler = new CleanHandler(this);
        mTotalJunkSize = mTitleText.getFileSize() * 1000;
        mTitleText.setStartSize(mTitleText.getFileSize());
        SuperCleanApplication application = (SuperCleanApplication) mActivity.getApplication();
        List<JunkListItem> appCacheList = application.getAppCacheList();
        List<JunkListItem> residualList = application.getResidualList();
        List<JunkListItem> apkList = application.getUnusedAPKList();
        List<JunkListItem> adList = application.getADList();
        List<JunkListItem> selectedList = new ArrayList<>();
        for (JunkListItem item : appCacheList) {
            if (item.getChecked()) {
                selectedList.add(item);
            }
        }
        for (JunkListItem item : residualList) {
            if (item.getChecked()) {
                selectedList.add(item);
            }
        }

        for (JunkListItem item : apkList) {
            if (item.getChecked()) {
                selectedList.add(item);
            }
        }

        for (JunkListItem item : adList) {
            if (item.getChecked()) {
                selectedList.add(item);
            }
        }
        int containerHeight = frameLayout.getHeight();
        int itemHeight = SystemUtils.dp2px(mActivity, 65);
        int needItemNum = containerHeight / itemHeight + 1;
        if (selectedList.size() < needItemNum) {
            needItemNum = selectedList.size();
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(SystemUtils.dp2px(mActivity, 10), SystemUtils.dp2px(mActivity, 5), SystemUtils.dp2px(mActivity, 10), 0);
        params.height = SystemUtils.dp2px(mActivity, 60);
        mItemViewList = new ArrayList<>();
        for (int i = 0; i < needItemNum; i++) {
            JunkListItem item = selectedList.get(i);
            View itemView = View.inflate(mActivity, R.layout.junk_clean_item_layout, null);
            ImageView imgIcon = itemView.findViewById(R.id.img_junk_clean_item_icon);
            TextView text = itemView.findViewById(R.id.tv_junk_clean_item);
            TextView tvSize = itemView.findViewById(R.id.tv_junk_item_size);
            Drawable icon = item.getApplicationIcon();
            if (icon != null) {
                imgIcon.setImageDrawable(icon);
                text.setText(item.getApplicationName());
            } else {
                imgIcon.setImageResource(R.mipmap.folder_icon);
                text.setText(item.getFile().getName());
            }
            tvSize.setText(SystemUtils.getFileSize(item.getSize()));
            mItemViewList.add(itemView);
            mLlContainer.addView(itemView, params);
        }

        new JunkCleanTask(this, selectedList).execute();
    }

    private void animationDone() {
        mLlContainer.removeAllViews();
        //如果全选地清理
        if (mActivity.mIsSelectAll) {
            SharePreferencesUtil.getInstance(mActivity).setLastCleanAllJunkTime(System.currentTimeMillis());
        }
        if (!mUncleanList.isEmpty()) {
            AnalyticsUtil.logEvent(mActivity, "has_unclean_junk", null);
        }
//        else {
//            Intent intent = new Intent(mActivity, JunkUncleanListActivity.class);
//            intent.putExtra("data", JunkListItem.JunkListMetadata.fromJunkListItem(mUncleanList));
//            startActivity(intent);
//        }

//        Intent intent = new Intent(mActivity, OptimizeActivity.class);
//        intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, getString(R.string.junk_clean));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, SystemUtils.getFileSize(mTotalJunkSize));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, getString(R.string.junk_optimized));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_JUNK);

        Intent intent = new OptimizeActivity.IntentBuilder()
                .setClass(mActivity)
                .setFrom(OptimizeActivity.FROM_JUNK)
                .setActionBar(getString(R.string.junk_clean))
                .setLeftText(SystemUtils.getFileSize(mTotalJunkSize), 0,0)
                .setRightText(getString(R.string.junk_optimized), 0,0)
                .build();
        mActivity.startActivity(intent);
        mActivity.mApplication.setJunkCleaning(false);
        mActivity.finish();
    }

    private void onDeleteStart() {
        mHandler.sendEmptyMessageDelayed(SIZE_NUMBER_REDUCE, REDUCE_ANIM_DURATION);
    }

    private void onDeleteUpdate(long deletedSize) {
        if (deletedSize <= mTotalJunkSize) {
            mRemainSize = mTotalJunkSize - deletedSize;
        }
    }

    private void onDeleteDown() {
        mRemainSize = 0;

    }

    private static class CleanHandler extends Handler {
        private WeakReference<JunkCleanFragment> mReference;

        CleanHandler(JunkCleanFragment fragment) {
            mReference = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            JunkCleanFragment fragment = mReference.get();
            if (fragment == null) return;
            switch (msg.what) {
                case LEAVE_ANIMATION:
                    fragment.mIsCleaning = true;
                    TranslateAnimation animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0f,
                            Animation.RELATIVE_TO_SELF, -1f, Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, 0f);
                    animation.setDuration(LEAVE_ANIMATION_DURATION);
                    animation.setFillAfter(true);
                    fragment.mItemViewList.get(fragment.mIndex).startAnimation(animation);
                    fragment.mIndex++;
                    if (fragment.mIndex < fragment.mItemViewList.size()) {
                        sendEmptyMessageDelayed(LEAVE_ANIMATION, LEAVE_ANIMATION_INTERVAL);
                    } else {
                        sendEmptyMessageDelayed(ANIMATION_DONE, LEAVE_ANIMATION_DURATION - LEAVE_ANIMATION_INTERVAL);
                        removeMessages(LEAVE_ANIMATION);
                    }
                    break;
                case ANIMATION_DONE:
                    fragment.mIsCleaning = false;
                    fragment.mIndex = 0;
                    fragment.animationDone();
                    break;
                case SIZE_NUMBER_REDUCE:
                    if (fragment.mRemainSize > 0) {
                        fragment.mReduceAnim = ObjectAnimator.ofInt(fragment.mTitleText, TitleTotalView.FILE_SIZE, fragment.mTitleText.getFileSize(), (int) (fragment.mRemainSize / 1000));
                        fragment.mReduceAnim.setDuration(REDUCE_ANIM_DURATION);
                        fragment.mReduceAnim.start();
                        sendEmptyMessageDelayed(SIZE_NUMBER_REDUCE, REDUCE_ANIM_DURATION);
                    } else {
                        if (fragment.mReduceAnim != null) fragment.mReduceAnim.cancel();
                        fragment.mReduceAnim = ObjectAnimator.ofInt(fragment.mTitleText, TitleTotalView.FILE_SIZE, fragment.mTitleText.getFileSize(), 0);
                        fragment.mReduceAnim.setDuration(REDUCE_ANIM_DURATION);
                        fragment.mReduceAnim.start();
                        sendEmptyMessageDelayed(LEAVE_ANIMATION, REDUCE_ANIM_DURATION);
                    }
                    break;
            }
        }
    }

    private static class JunkCleanTask extends AsyncTask<Void, Long, Void> {
        private List<JunkListItem> mJunkListItems;
        private WeakReference<JunkCleanFragment> mReference;
        private long mDeletedSize;

        JunkCleanTask(JunkCleanFragment fragment, @NonNull List<JunkListItem> list) {
            this.mJunkListItems = list;
            mReference = new WeakReference<>(fragment);
        }

        @Override
        protected void onPreExecute() {
            mDeletedSize = 0;
            JunkCleanFragment fragment = mReference.get();
            if (fragment == null) return;
            fragment.onDeleteStart();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (JunkListItem item : mJunkListItems) {
                File file = item.getFile();
                CommonUtils.debug("delete " + file.getAbsolutePath());
                if (file.exists()) {
                    deleteFileRecursively(file, item);
                    mDeletedSize += item.getSize();
                    publishProgress(mDeletedSize);
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            JunkCleanFragment fragment = mReference.get();
            if (fragment == null) return;
            fragment.onDeleteUpdate(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            JunkCleanFragment fragment = mReference.get();
            if (fragment == null) return;
            fragment.onDeleteDown();
        }

        private void deleteFileRecursively(File file, JunkListItem item) {
            if (null == file) {
                return;
            }
            if (file.isDirectory()) {
                File[] subFiles = file.listFiles();
                if (null == subFiles) {
                    return;
                }
                for (File subFile : subFiles) {
                    deleteFileRecursively(subFile, item);
                }
            }

            if (!file.delete()) {
                JunkCleanFragment fragment = mReference.get();
                if (fragment == null) return;
                fragment.mUncleanList.add(new JunkListItem(item.getPackageName(), item.getApplicationName(), item.getApplicationIcon()
                        , SystemUtils.getFileSize(file), file));
            }
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        mHandler.removeMessages(LEAVE_ANIMATION);
        mHandler.removeMessages(ANIMATION_DONE);
        mHandler.removeMessages(SIZE_NUMBER_REDUCE);
    }

}
