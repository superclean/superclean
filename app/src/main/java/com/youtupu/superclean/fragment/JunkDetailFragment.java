package com.youtupu.superclean.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.youtupu.superclean.activity.JunkDetailActivity;

import java.util.List;

import com.youtupu.superclean.R;

import com.youtupu.superclean.app.SuperCleanApplication;
import com.youtupu.superclean.base.BaseFragment;
import com.youtupu.superclean.bean.JunkListItem;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.ToastUtil;
import com.youtupu.superclean.view.TitleTotalView;
import com.youtupu.superclean.view.FoldableListView;

/**
 * Created by Jiali on 2019/2/28.
 */

public class JunkDetailFragment extends BaseFragment implements FoldableListView.OnSelectedResultChangedListener {
    private FoldableListView mAppView;
    private FoldableListView mResidualView;
    private FoldableListView mUnusedAPKView;
    private FoldableListView mADsView;
    private long mAppSelectedSize = 0;
    private long mResidualSelectedSize = 0;
    private long mAPKSelectedSize = 0;
    private long mADsSelectedSize = 0;
    private TitleTotalView mTotalSelectView;
    private long mTotalSize = 0;
    private JunkDetailActivity mActivity;

    @Override
    protected void initActivity(Context context) {
        mActivity = (JunkDetailActivity) context;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.junk_detail_fragment_layout;
    }

    @Override
    protected void initView() {
        mTotalSelectView = mActivity.findViewById(R.id.junk_total_select);
        mAppView = mRootView.findViewById(R.id.fold_listview_app);
        mResidualView = mRootView.findViewById(R.id.fold_listview_residual);
        mUnusedAPKView = mRootView.findViewById(R.id.fold_listview_apk);
        mADsView = mRootView.findViewById(R.id.fold_listview_ads);
        Button cleanButton = mRootView.findViewById(R.id.button_junk_clean);
        View mDivider1 = mRootView.findViewById(R.id.divider1);
        View mDivider2 = mRootView.findViewById(R.id.divider2);
        View mDivider3 = mRootView.findViewById(R.id.divider3);
        SuperCleanApplication application = (SuperCleanApplication) mActivity.getApplication();
        List<JunkListItem> appCacheList = application.getAppCacheList();
        List<JunkListItem> residualList = application.getResidualList();
        List<JunkListItem> apkList = application.getUnusedAPKList();
        List<JunkListItem> adList = application.getADList();
        if (appCacheList == null || apkList == null || residualList == null || adList == null) {
            mActivity.finish();
            return;
        }
        if (appCacheList.size() > 0) {
            mAppView.setListViewDetail(appCacheList);
            mAppView.setOnSelectedResultChangedListener(this);
            mAppView.setSelectAll(true);
            //下方的都没有时，则下方的分割线去掉
            if (residualList.size() == 0 && apkList.size() == 0 && adList.size() == 0) {
                mDivider1.setVisibility(View.GONE);
            }
        } else {
            mAppView.setVisibility(View.GONE);
            mDivider1.setVisibility(View.GONE);
        }
        if (residualList.size() > 0) {
            mResidualView.setListViewDetail(residualList);
            mResidualView.setOnSelectedResultChangedListener(this);
            mResidualView.setSelectAll(true);
            //下方的都没有时，则下方的分割线去掉
            if (apkList.size() == 0 && adList.size() == 0) {
                mDivider2.setVisibility(View.GONE);
            }
        } else {
            mResidualView.setVisibility(View.GONE);
            mDivider2.setVisibility(View.GONE);
        }

        if (apkList.size() > 0) {
            mUnusedAPKView.setListViewDetail(apkList);
            mUnusedAPKView.setOnSelectedResultChangedListener(this);
            mUnusedAPKView.setSelectAll(true);
            //下方的都没有时，则下方的分割线去掉
            if (adList.size() == 0) {
                mDivider3.setVisibility(View.GONE);
            }
        } else {
            mUnusedAPKView.setVisibility(View.GONE);
            mDivider3.setVisibility(View.GONE);
        }

        if (adList.size() > 0) {
            mADsView.setListViewDetail(adList);
            mADsView.setOnSelectedResultChangedListener(this);
            mADsView.setSelectAll(true);
        } else {
            mADsView.setVisibility(View.GONE);
        }
        cleanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTotalSize == 0) {
                    ToastUtil.showShortToast(mActivity, getString(R.string.junk_clean_no_select));
                } else {
                    //判断是否全选了文件
                    mActivity.mIsSelectAll = mAppView.mCheckBox.getChecked() && mResidualView.mCheckBox.getChecked()
                            && mUnusedAPKView.mCheckBox.getChecked() && mADsView.mCheckBox.getChecked();
                    Bundle bundle = new Bundle();
                    bundle.putLong("junk_size", mTotalSize);
                    bundle.putBoolean("select_all", mActivity.mIsSelectAll);
                    AnalyticsUtil.logEvent(mActivity, "junk_result", bundle);
                    mActivity.switchFragment(JunkDetailActivity.CLEAN_FRAGMENT_INDEX, R.id.junk_framelayout);
                }
            }
        });
    }

    private void updateTotalSelectedSize() {
        mTotalSize = mAppSelectedSize + mResidualSelectedSize + mAPKSelectedSize + mADsSelectedSize;
        mTotalSelectView.setFileSize((int) (mTotalSize / 1000));
    }

    @Override
    public void onSelectChanged(View view, long selectedSize) {
        switch (view.getId()) {
            case R.id.fold_listview_app:
                mAppSelectedSize = selectedSize;
                break;
            case R.id.fold_listview_residual:
                mResidualSelectedSize = selectedSize;
                break;
            case R.id.fold_listview_apk:
                mAPKSelectedSize = selectedSize;
                break;
            case R.id.fold_listview_ads:
                mADsSelectedSize = selectedSize;
                break;
        }
        updateTotalSelectedSize();
    }
}
