package com.youtupu.superclean.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.youtupu.superclean.R;
import com.youtupu.superclean.activity.FakePowerSaveActivity;
import com.youtupu.superclean.activity.OptimizeActivity;
import com.youtupu.superclean.activity.PermissionDialogActivity;
import com.youtupu.superclean.activity.PowerSaveActivity;
import com.youtupu.superclean.app.SuperCleanApplication;
import com.youtupu.superclean.base.BaseFragment;
import com.youtupu.superclean.bean.AppItem;
import com.youtupu.superclean.extract.OnBatterySaveListener;
import com.youtupu.superclean.service.BatterySaveService;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.utils.ToastUtil;
import com.youtupu.superclean.view.RunningAppFoldView;
import com.youtupu.superclean.view.TitleTotalView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import ezy.assist.compat.SettingsCompat;

/**
 * Created by Jiali on 2019/3/12.
 */

public class PowerDetailFragment extends BaseFragment {
    //辅助服务启动时发的广播
    public static String ACTION_ACCESSIBILITY = "superclean.action.accessibility";
    public static String EXTRA_ACCESSIBILITY = "superclean.extra.access_on";
    private PowerSaveActivity mActivity;
    private RunningAppFoldView mCanStopFoldView;
    private RunningAppFoldView mNoStopFoldView;
    private TitleTotalView mTitleTotalView;
    private static OnBatterySaveListener mOnBatterySaveListener;
    private boolean mAccessibilityPermission = false;
    private boolean mFloatWindowPermission = false;
    private List<String> mSelectedAppList;
    private int mCanStopSelectNum;
    private int mNoStopSelectNum;
    private AccessibilityBroadcastReceiver mAccessibilityReceiver;
    private Handler mHandler = new Handler();
    private final int REQ_FLOAT_PERMISSION = 1;
    private final int REQ_ACCESSIBILITY_PERMISSION = 2;
    private boolean isToSetting = false;

    private class AccessibilityBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra(EXTRA_ACCESSIBILITY, false)) {
                startKillProcess();
            }
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.power_detail_fragment_layout;
    }

    @Override
    protected void initActivity(Context context) {
        mActivity = (PowerSaveActivity) context;
        mAccessibilityReceiver = new AccessibilityBroadcastReceiver();
        LocalBroadcastManager.getInstance(context).registerReceiver(mAccessibilityReceiver, new IntentFilter(ACTION_ACCESSIBILITY));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mActivity != null) {
            LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mAccessibilityReceiver);
        }
    }

    @Override
    protected void initView() {
        mTitleTotalView = mRootView.findViewById(R.id.app_total);
        mCanStopFoldView = mRootView.findViewById(R.id.fold_view_can_stop);
        mNoStopFoldView = mRootView.findViewById(R.id.fold_view_no_stop);
        Button powerSaveButton = mRootView.findViewById(R.id.button_power_save);
        if (mActivity != null) {
            //从activity那里拿数据
            if (mActivity.getCanStopAppList() != null && (mCanStopSelectNum = mActivity.getCanStopAppList().size()) > 0) {
                mCanStopFoldView.setAppList(mActivity.getCanStopAppList());
                mCanStopFoldView.setFoldViewListener(new RunningAppFoldView.FoldViewListener() {
                    @Override
                    public void onSelectChange(int selectNum) {
                        mCanStopSelectNum = selectNum;
                        updateTotalSelectNum();
                    }
                });
            } else {
                mCanStopFoldView.setVisibility(View.GONE);
            }
            if (mActivity.getNoStopAppList() != null && mActivity.getNoStopAppList().size() > 0) {
                mNoStopFoldView.setAppList(mActivity.getNoStopAppList());
                mNoStopFoldView.setFoldViewListener(new RunningAppFoldView.FoldViewListener() {
                    @Override
                    public void onSelectChange(int selectNum) {
                        mNoStopSelectNum = selectNum;
                        updateTotalSelectNum();
                    }
                });
            } else {
                mNoStopFoldView.setVisibility(View.GONE);
            }

        }
        mNoStopSelectNum = 0;
        mTitleTotalView.setLeftRightText(String.valueOf(mCanStopSelectNum), getString(R.string.app_select));
        mSelectedAppList = new ArrayList<>();
        powerSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<AppItem> appItemList = new ArrayList<>();
                appItemList.addAll(mCanStopFoldView.getAppList());
                appItemList.addAll(mNoStopFoldView.getAppList());
                mSelectedAppList.clear();
                for (AppItem item : appItemList) {
                    if (item.isChecked()) {
                        String packageName = item.getPackageName();
                        mSelectedAppList.add(packageName);
                    }
                }
                if (mSelectedAppList.size() > 0) {
                    toSavePower();
                } else {
                    ToastUtil.showShortToast(mActivity, getString(R.string.power_save_no_select));
                }
            }
        });
        mHandler = new Handler();

        startFloatWindowThread();
    }

    private void startFloatWindowThread() {
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                boolean isGranted = false;
                while (null != mActivity && !mActivity.isFinishing() && !isGranted) {
                    Context context = getContext();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        CommonUtils.debug("can draw overlays " + (isGranted = Settings.canDrawOverlays(context)));
                    }
                    SystemClock.sleep(100);
                }
            }
        });

    }

    private void updateTotalSelectNum() {
        int total = mCanStopSelectNum + mNoStopSelectNum;
        String rightText = getString(R.string.app_select);
        mTitleTotalView.setLeftRightText(String.valueOf(total), rightText);
    }

    private void toSavePower() {
        checkPermission();
        if (mAccessibilityPermission && mFloatWindowPermission) {
            if (null != mOnBatterySaveListener) {
                startKillProcess();
            }
            // 某些情况下，辅助服务开启但没有赋值给mOnBatterySaveListener，拉起一个假的省电Activity
            else {
                if (null != getActivity()) {
                    showFakeSaveActivity();
                }
            }
        } else {
            if (!mFloatWindowPermission) {
                gotoFloatPermissionActivity();
            } else {
                gotoAccessibilityPermissionActivity();
            }
        }
    }

    private void showFakeSaveActivity() {
        Intent intent = new Intent(getActivity(), FakePowerSaveActivity.class);
        intent.putExtra("currentProgress", 0);
        intent.putExtra("totalTask", mSelectedAppList.size());
        startActivity(intent);
    }

    private void checkPermission() {
        //判断是否有Accessibility权限
        mAccessibilityPermission = isAccessibilityGranted();

        //判断是否有悬浮窗权限
        mFloatWindowPermission = isFloatWindowGranted();
    }

    private boolean isFloatWindowGranted() {
        boolean result;
        result = SettingsCompat.canDrawOverlays(SuperCleanApplication.instance);
        CommonUtils.debug("isFloatWindowGranted " + result);
        return result;
    }

    private boolean isAccessibilityGranted() {
        if (mActivity == null) return false;
        String serviceName = mActivity.getPackageName() + "/" + BatterySaveService.class.getName();
        int accessibilityEnable = 0;
        try {
            accessibilityEnable = Settings.Secure.getInt(mActivity.getContentResolver(), Settings.Secure.ACCESSIBILITY_ENABLED, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean result = false;
        if (accessibilityEnable == 1) {
            TextUtils.SimpleStringSplitter mStringSplitter = new TextUtils.SimpleStringSplitter(':');
            String settingValue = Settings.Secure.getString(mActivity.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringSplitter.setString(settingValue);
                while (mStringSplitter.hasNext()) {
                    String accessibilityService = mStringSplitter.next();
                    if (accessibilityService.equalsIgnoreCase(serviceName)) {
                        result = true;
                        break;
                    }
                }
            }
        }
        return result;
    }


    private void startKillProcess() {
        AnalyticsUtil.logEvent(getContext(), "start_kill_process", null);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mOnBatterySaveListener != null) {
                    mOnBatterySaveListener.killProcess(mSelectedAppList, new BatterySaveService.TaskListener() {
                        @Override
                        public void onProgress(int progress) {
                            CommonUtils.debug("battery save progress " + progress);
                        }

                        @Override
                        public void afterAddFloatView() {
                        }

                        @Override
                        public void onFinish() {
                            mOnBatterySaveListener = null;
                            if (mActivity != null) {
//                                Intent intent = new Intent(mActivity, OptimizeActivity.class);
//                                intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_POWER);
//                                intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, mActivity.getString(R.string.battery_title));
//                                intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, String.valueOf(mSelectedAppList.size()));
//                                intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, mActivity.getString(R.string.apps_dormancy));
                                Intent intent = new OptimizeActivity.IntentBuilder()
                                        .setClass(mActivity)
                                        .setFrom(OptimizeActivity.FROM_POWER)
                                        .setActionBar(mActivity.getString(R.string.battery_title))
                                        .setLeftText(String.valueOf(mSelectedAppList.size()),0,0)
                                        .setRightText(mActivity.getString(R.string.apps_dormancy),0,0)
                                        .build();
                                mActivity.startActivity(intent);
                                mActivity.finish();
                            }
                        }

                        @Override
                        public void onPause() {

                        }

                        @Override
                        public void onResume() {

                        }
                    });
                }
            }
        }, 500);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler.removeCallbacksAndMessages(null);
        if(null != mOnBatterySaveListener){
            mOnBatterySaveListener.finish();
            mOnBatterySaveListener = null;
        }
    }

    public static void setOnBatterySaveListener(@NonNull OnBatterySaveListener listener) {
        mOnBatterySaveListener = listener;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_FLOAT_PERMISSION:
                if (mHandler != null) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!isFloatWindowGranted()) {
                                showFloatPermissionDialog();
                            } else {
                                gotoAccessibilityPermissionActivity();
                            }
                        }
                    }, 500);
                }
                break;

            case REQ_ACCESSIBILITY_PERMISSION:
                if (!isAccessibilityGranted()) {
                    showAccessibilityPermissionDialog();
                }
                break;
        }
    }

    private void showFloatPermissionDialog() {
        if (null == mActivity) {
            return;
        }
        Intent newIntent = new Intent(mActivity, PermissionDialogActivity.class);
        newIntent.putExtra("taskSize", mSelectedAppList.size());
        startActivity(newIntent);
    }

    private void showAccessibilityPermissionDialog() {
        if (mActivity != null) {
            Intent newIntent = new Intent(mActivity, PermissionDialogActivity.class);
            newIntent.putExtra("taskSize", mSelectedAppList.size());
            startActivity(newIntent);
        }
    }

    private void gotoFloatPermissionActivity() {
        if (null == mActivity) {
            return;
        }
        Intent intent;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // 使用PermissionDialogActivity来作为跳转到设置页面的中介，以加快权限判断速度
            intent = new Intent(mActivity, PermissionDialogActivity.class);
            intent.putExtra("action", "grant_float_permission");
            intent.putExtra("taskSize", mSelectedAppList.size());
            mActivity.startActivity(intent);

            isToSetting = true;
        }

    }

    private void gotoAccessibilityPermissionActivity() {
        if (mActivity == null) {
            return;
        }
        Intent intent;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // 使用PermissionDialogActivity来作为跳转到设置页面的中介，以加快权限判断速度
            intent = new Intent(mActivity, PermissionDialogActivity.class);
            intent.putExtra("action", "grant_access_permission");
            intent.putExtra("taskSize", mSelectedAppList.size());
            mActivity.startActivity(intent);
            isToSetting = true;
        }
    }



}
