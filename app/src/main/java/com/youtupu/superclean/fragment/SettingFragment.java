package com.youtupu.superclean.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.SwitchPreferenceCompat;

import com.youtupu.superclean.activity.PowerIgnoreListActivity;

import cn.instreet.business.BusinessSDK;
import cn.instreet.business.lockscreen.LockScreenPreferenceActivity;
import com.youtupu.superclean.R;

import com.youtupu.superclean.service.MonitorService;
import com.youtupu.superclean.utils.AnalyticsUtil;

/**
 * Created by Jiali on 2018/12/6.
 */

public class SettingFragment extends PreferenceFragmentCompat {


    private SwitchPreferenceCompat mLockscreenPref = null;

    private SwitchPreferenceCompat mPhoneRemindPref = null;

    private SwitchPreferenceCompat mBatteryRemindPref = null;

    private BusinessSDK mBusinessSDK;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        mBusinessSDK = BusinessSDK.getInstance();
        mLockscreenPref = (SwitchPreferenceCompat) getPreferenceManager().findPreference("lockScreen");
        mLockscreenPref.setChecked(mBusinessSDK.isLockScreenOn());
        mLockscreenPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Activity activity = getActivity();
                if (activity != null) {
                    Intent intent = new Intent(activity, LockScreenPreferenceActivity.class);
                    activity.startActivity(intent);
                }
                return false;
            }
        });
        mBusinessSDK.setNormalLockScreenStyle(-1, "", null, null);

//        mPhoneRemindPref = (SwitchPreferenceCompat) getPreferenceManager().findPreference("phoneRemind");
//        mPhoneRemindPref.setChecked(mBusinessSDK.isPhoneRemindOn());
//        mPhoneRemindPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
//            @Override
//            public boolean onPreferenceChange(Preference preference, Object o) {
//                if (o.equals(Boolean.TRUE)) {
//                    mBusinessSDK.enablePhoneRemind(null);
//                } else {
//                    mBusinessSDK.disablePhoneRemind();
//                }
//                return true;
//            }
//        });

//        mBatteryRemindPref = (SwitchPreferenceCompat) getPreferenceManager().findPreference("batteryRemind");
//        mBatteryRemindPref.setChecked(mBusinessSDK.isBatteryRemindOn());
//        mBatteryRemindPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
//            @Override
//            public boolean onPreferenceChange(Preference preference, Object o) {
//                if (o.equals(Boolean.TRUE)) {
//                    mBusinessSDK.enableBatteryReminder();
//                } else {
//                    mBusinessSDK.disableBatteryReminder();
//                }
//                return true;
//            }
//        });
        final SwitchPreferenceCompat notificationPref = (SwitchPreferenceCompat) getPreferenceManager().findPreference("setting_notification");
        notificationPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Activity activity = getActivity();
                if (activity != null) {
                    Intent intent = new Intent(MonitorService.ACTION_SET_NOTIFICATION);
                    //判断点击时候的状态
                    if (notificationPref.isChecked()) {
                        //要关闭
                        AnalyticsUtil.logEvent(activity, "notification_set_off", null);
                        intent.putExtra("show_notification", false);
                    } else {
                        //要开启
                        AnalyticsUtil.logEvent(activity, "notification_set_on", null);
                        intent.putExtra("show_notification", true);
                    }
                    LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                }
                return true;
            }
        });
        final SwitchPreferenceCompat apkPref = (SwitchPreferenceCompat) getPreferenceManager().findPreference("setting_apk");
        apkPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Activity activity = getActivity();
                if (activity != null) {
                    //判断点击时候的状态
                    if (apkPref.isChecked()) {
                        //要关闭
                        AnalyticsUtil.logEvent(activity, "setting_apk_off", null);
                    } else {
                        //要开启
                        AnalyticsUtil.logEvent(activity, "setting_apk_on", null);
                    }
                }
                return true;
            }
        });
        final SwitchPreferenceCompat uninstallPref = (SwitchPreferenceCompat) getPreferenceManager().findPreference("setting_residual");
        uninstallPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Activity activity = getActivity();
                if (activity != null) {
                    //判断点击时候的状态
                    if (uninstallPref.isChecked()) {
                        //要关闭
                        AnalyticsUtil.logEvent(activity, "setting_residual_off", null);
                    } else {
                        //要开启
                        AnalyticsUtil.logEvent(activity, "setting_residual_on", null);
                    }
                }
                return true;
            }
        });
        final SwitchPreferenceCompat chargePref = (SwitchPreferenceCompat) getPreferenceManager().findPreference("setting_charge");
        chargePref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Activity activity = getActivity();
                if (activity != null) {
                    //判断点击时候的状态
                    if (chargePref.isChecked()) {
                        //要关闭
                        AnalyticsUtil.logEvent(activity, "setting_charge_off", null);
                    } else {
                        //要开启
                        AnalyticsUtil.logEvent(activity, "setting_charge_on", null);
                    }
                }
                return true;
            }
        });
        Preference powerIgnorePref = getPreferenceManager().findPreference("setting_power_ignore");
        powerIgnorePref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Activity activity = getActivity();
                if (activity != null) {
                    Intent intent = new Intent(activity, PowerIgnoreListActivity.class);
                    activity.startActivity(intent);
                }
                return false;
            }
        });
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != mBusinessSDK) {
            mLockscreenPref.setChecked(mBusinessSDK.isLockScreenOn());
        }
    }
}
