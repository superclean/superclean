package com.youtupu.superclean.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.youtupu.superclean.activity.NotificationManagerActivity;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.youtupu.superclean.R;

import com.youtupu.superclean.adapter.NotificationSettingAdapter;
import com.youtupu.superclean.base.BaseFragment;
import com.youtupu.superclean.bean.CheckInfoApp;
import com.youtupu.superclean.extract.OnBackPressedListener;
import com.youtupu.superclean.service.NotificationService;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.CustomTitleBar;
import com.youtupu.superclean.view.ThreeStatusCheckBox;

/**
 * Created by Jiali on 2018/12/12.
 */

public class NotificationSettingFragment extends BaseFragment implements OnBackPressedListener {
    private ListView mNonSysListView;
    private ListView mSysListView;
    private List<CheckInfoApp> mNonSysAppList;
    private List<CheckInfoApp> mSysAppList;
    private NotificationSettingAdapter mNonSysAdapter;
    private NotificationSettingAdapter mSysAdapter;
    private NotificationManagerActivity mActivity;
    private boolean mHasChanged = false;
    private ThreeStatusCheckBox mCheckBoxNonSys;
    private ThreeStatusCheckBox mCheckBoxSys;
    private boolean mIsFirstCreate = false;
    private RelativeLayout mRlNonSysTitle;
    private RelativeLayout mRlSysTitle;
    private boolean mNonSysOpened = true;
    private boolean mSysOpened = true;
    private ImageView mImgNonSys;
    private ImageView mImgSys;
    private LinearLayout mLlNonSys;
    private LinearLayout mLlSys;
    private View mDividerLine1;
    private View mDividerLine2;

    @Override
    protected int getLayoutRes() {
        return R.layout.notification_clean_fragment;
    }

    @Override
    protected void initActivity(Context context) {
        mActivity = (NotificationManagerActivity) context;
    }


    @Override
    protected void initView() {
        CustomTitleBar customTitleBar = mRootView.findViewById(R.id.custom_title_bar);
        mNonSysListView = mRootView.findViewById(R.id.lv_non_sys);
        mSysListView = mRootView.findViewById(R.id.lv_sys);
        mCheckBoxNonSys = mRootView.findViewById(R.id.checkbox_non_sys);
        mCheckBoxSys = mRootView.findViewById(R.id.checkbox_sys);
        mRlNonSysTitle = mRootView.findViewById(R.id.rl_non_sys);
        mRlSysTitle = mRootView.findViewById(R.id.rl_sys);
        mImgNonSys = mRootView.findViewById(R.id.img_non_sys);
        mImgSys = mRootView.findViewById(R.id.img_sys);
        mLlNonSys = mRootView.findViewById(R.id.ll_non_sys);
        mLlSys = mRootView.findViewById(R.id.ll_sys);
        mDividerLine1 = mRootView.findViewById(R.id.divider1);
        mDividerLine2 = mRootView.findViewById(R.id.divider2);
        mIsFirstCreate = true;
        HashSet<String> whiteList;
        whiteList = SystemUtils.readNotificationWhiteListFile(mActivity);
        PackageManager packageManager = mActivity.getPackageManager();
        mNonSysAppList = new ArrayList<>();
        mSysAppList = new ArrayList<>();
        if (packageManager != null && whiteList != null) {
            List<ApplicationInfo> applicationInfos = packageManager.getInstalledApplications(0);
            if (applicationInfos != null) {
                for (ApplicationInfo info : applicationInfos) {
                    //除去自己
                    if (!info.packageName.equalsIgnoreCase(mActivity.getPackageName())) {
                        CheckInfoApp app;
                        //默认都收纳（true），只有在白名单里的app不收纳（false）
                        if (whiteList.contains(info.packageName)) {
                            app = new CheckInfoApp(info, false);
                        } else {
                            app = new CheckInfoApp(info, true);
                        }
                        if ((info.flags & ApplicationInfo.FLAG_SYSTEM) == ApplicationInfo.FLAG_SYSTEM) {
                            mSysAppList.add(app);
                        } else {
                            mNonSysAppList.add(app);
                        }
                    }
                }
            }
        }
        mNonSysAdapter = new NotificationSettingAdapter(mActivity, mNonSysAppList, packageManager);
        mSysAdapter = new NotificationSettingAdapter(mActivity, mSysAppList, packageManager);
        customTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                onBackPressed();
            }
        });
        mRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (mIsFirstCreate) {
                    mIsFirstCreate = false;
                    mRootView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mNonSysListView.setAdapter(mNonSysAdapter);
                            mNonSysListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    CheckInfoApp item = (CheckInfoApp) mNonSysAdapter.getItem(position);
                                    item.setChecked(!item.isChecked());
                                    mNonSysAdapter.notifyDataSetChanged();
                                    updateNonSysSelectState();
                                    mHasChanged = true;
                                }
                            });
                            mSysListView.setAdapter(mSysAdapter);
                            mSysListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    CheckInfoApp item = (CheckInfoApp) mSysAdapter.getItem(position);
                                    item.setChecked(!item.isChecked());
                                    mSysAdapter.notifyDataSetChanged();
                                    updateSysSelectState();
                                    mHasChanged = true;
                                }
                            });
                            updateListViewHeight(mNonSysListView);
                            updateListViewHeight(mSysListView);
                            updateNonSysSelectState();
                            updateSysSelectState();
                            addListener();
                        }
                    }, 400);
                }
            }
        });
    }

    private void addListener() {
        mRlNonSysTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNonSysOpened = !mNonSysOpened;
                if (mNonSysOpened) {
                    mImgNonSys.setImageResource(R.mipmap.arrow_up);
                    mNonSysListView.setVisibility(View.VISIBLE);
                    mDividerLine1.setVisibility(View.VISIBLE);
                } else {
                    if (mLlNonSys.getVisibility() == View.VISIBLE)
                        mLlNonSys.setVisibility(View.GONE);
                    mImgNonSys.setImageResource(R.mipmap.arrow_down);
                    mNonSysListView.setVisibility(View.GONE);
                    mDividerLine1.setVisibility(View.GONE);
                }
            }
        });
        mRlSysTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSysOpened = !mSysOpened;
                if (mSysOpened) {
                    mImgSys.setImageResource(R.mipmap.arrow_up);
                    mSysListView.setVisibility(View.VISIBLE);
                    mDividerLine2.setVisibility(View.VISIBLE);
                } else {
                    if (mLlSys.getVisibility() == View.VISIBLE) mLlSys.setVisibility(View.GONE);
                    mImgSys.setImageResource(R.mipmap.arrow_down);
                    mSysListView.setVisibility(View.GONE);
                    mDividerLine2.setVisibility(View.GONE);
                }
            }
        });
        mCheckBoxNonSys.setOnCheckBoxSelectedListener(new ThreeStatusCheckBox.OnCheckBoxSelectedListener() {
            @Override
            public void onChecked() {
                for (CheckInfoApp app : mNonSysAppList) {
                    app.setChecked(true);
                }
                mNonSysAdapter.notifyDataSetChanged();
            }

            @Override
            public void onUnChecked() {
                for (CheckInfoApp app : mNonSysAppList) {
                    app.setChecked(false);
                }
                mNonSysAdapter.notifyDataSetChanged();
            }
        });
        mCheckBoxSys.setOnCheckBoxSelectedListener(new ThreeStatusCheckBox.OnCheckBoxSelectedListener() {
            @Override
            public void onChecked() {
                for (CheckInfoApp app : mSysAppList) {
                    app.setChecked(true);
                }
                mSysAdapter.notifyDataSetChanged();
            }

            @Override
            public void onUnChecked() {
                for (CheckInfoApp app : mSysAppList) {
                    app.setChecked(false);
                }
                mSysAdapter.notifyDataSetChanged();
            }
        });
    }

    private void updateNonSysSelectState() {
        int selectNum = 0;
        for (CheckInfoApp app : mNonSysAppList) {
            if (app.isChecked()) {
                selectNum++;
            }
        }
        if (selectNum == 0) {
            mCheckBoxNonSys.setChecked(false);
        } else if (selectNum < mNonSysAppList.size()) {
            mCheckBoxNonSys.setCheckBoxPartialSelected();
        } else {
            mCheckBoxNonSys.setChecked(true);
        }
    }

    private void updateSysSelectState() {
        int selectNum = 0;
        for (CheckInfoApp app : mSysAppList) {
            if (app.isChecked()) {
                selectNum++;
            }
        }
        if (selectNum == 0) {
            mCheckBoxSys.setChecked(false);
        } else if (selectNum < mSysAppList.size()) {
            mCheckBoxSys.setCheckBoxPartialSelected();
        } else {
            mCheckBoxSys.setChecked(true);
        }
    }


    private void updateListViewHeight(@NonNull ListView listView) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) listView.getLayoutParams();
        int totalHeight = 0;
        int count = listView.getAdapter().getCount();
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                View item = listView.getAdapter().getView(i, null, listView);
                //item的布局要求是linearLayout，否则measure(0,0)会报错。
                item.measure(0, 0);
                //计算出所有item高度的总和
                totalHeight += item.getMeasuredHeight();
            }
            //加上所有分割线的高度
            totalHeight += listView.getDividerHeight() * (count - 1);
        }
        params.height = totalHeight;
        listView.setLayoutParams(params);
    }

    private void saveWhiteList() {
        JSONArray array = new JSONArray();
        for (CheckInfoApp app : mNonSysAppList) {
            if (!app.isChecked()) {
                array.put(app.getInfo().packageName);
            }
        }
        for (CheckInfoApp app : mSysAppList) {
            if (!app.isChecked()) {
                array.put(app.getInfo().packageName);
            }
        }
        SystemUtils.writeWhiteListFile(mActivity, array.toString());
        LocalBroadcastManager.getInstance(mActivity).sendBroadcast(new Intent(NotificationService.NOTIFICATION_WHITE_LIST_CHANGED));
    }

    @Override
    public void onBackPressed() {
        if (mHasChanged) {
            saveWhiteList();
            mHasChanged = false;
        }
        mActivity.switchFragment(NotificationManagerActivity.HOME_FRAGMENT_INDEX, R.id.notification_framelayout);
    }
}
