package com.youtupu.superclean.fragment;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.youtupu.superclean.R;
import com.youtupu.superclean.activity.OptimizeActivity;
import com.youtupu.superclean.activity.PowerSaveActivity;
import com.youtupu.superclean.base.BaseFragment;
import com.youtupu.superclean.bean.AppItem;
import com.youtupu.superclean.dialog.DoubleChoicesDialog;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.utils.SystemUtils;
import com.youtupu.superclean.view.CustomTitleBar;
import com.youtupu.superclean.view.EllipsisView;
import com.youtupu.superclean.view.ScanningView;

/**
 * Created by Jiali on 2019/3/12.
 */

public class PowerScanFragment extends BaseFragment {
    private static final int SCAN_ONE_CIRCLE_DURATION = 800;
    private static final int SCAN_DONE_DURATION = 600;
    private ScanningView mScanningView;
    private TextView mTvStatus;
    private EllipsisView mEllipsisView;
    private AnimatorSet mAnimatorSet;
    private List<AppItem> mCanStopAppList;
    private List<AppItem> mNoStopAppList;
    private DoubleChoicesDialog mExitDialog;
    private PowerSaveActivity mActivity;
    private CustomTitleBar mCustomTitleBar;

    @Override
    protected void initActivity(Context context) {
        mActivity = (PowerSaveActivity) context;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.power_scan_fragment_layout;
    }

    @Override
    protected void initView() {
        if (mActivity != null) {
            mCustomTitleBar = mActivity.findViewById(R.id.custom_title_bar);
            mCustomTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
                @Override
                public void onLeftClick() {
                    if (mAnimatorSet.isRunning()) {
                        mAnimatorSet.pause();
                        showDialog();
                    } else {
                        if (mCanStopAppList != null && mCanStopAppList.size() > 0) {
                            SharePreferencesUtil.getInstance(mActivity).setLastAbandonOptimize(3);
                            SharePreferencesUtil.getInstance(mActivity).setLastCanStopNumber(mCanStopAppList.size());
                        }
                        mActivity.finish();
                    }
                }
            });
        }
        mScanningView = mRootView.findViewById(R.id.scanning_view);
        mTvStatus = mRootView.findViewById(R.id.tv_cooler_status);
        mEllipsisView = mRootView.findViewById(R.id.ellipsis_view);
        mCanStopAppList = new ArrayList<>();
        mNoStopAppList = new ArrayList<>();
        getCurrentRunningServices();
        mAnimatorSet = new AnimatorSet();
        mAnimatorSet.addListener(new Animator.AnimatorListener() {
            boolean isCanceled = false;

            @Override
            public void onAnimationStart(Animator animation) {
                isCanceled = false;
                mTvStatus.setText(getString(R.string.scanning));
                mEllipsisView.setVisibility(View.VISIBLE);
                mEllipsisView.startEllipsisAnimation();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isCanceled) {
                    mEllipsisView.stopEllipsisAnimation();
                    mEllipsisView.setVisibility(View.INVISIBLE);
                    mTvStatus.setVisibility(View.INVISIBLE);
                    if (mActivity == null) return;
                    if (mCanStopAppList.size() + mNoStopAppList.size() > 0) {
                        mActivity.switchFragment(PowerSaveActivity.DETAIL_FRAGMENT_INDEX, R.id.power_frame_layout);
                    } else {
//                        Intent intent = new Intent(mActivity, OptimizeActivity.class);
//                        intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, getString(R.string.battery_title));
//                        intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_POWER);
//                        intent.putExtra(OptimizeActivity.OPTIMIZE_TITLE, getString(R.string.normal));
//                        intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, "");
//                        intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, getString(R.string.power_consumption));
//                        intent.putExtra(OptimizeActivity.NEED_RECORD, false);

                        Intent intent = new OptimizeActivity.IntentBuilder()
                                .setClass(mActivity)
                                .setActionBar(getString(R.string.battery_title))
                                .setFrom(OptimizeActivity.FROM_POWER)
                                .setTitle(getString(R.string.normal))
                                .setRightText(getString(R.string.power_consumption), 0, 0)
                                .setNeedRecord(false).build();
                        mActivity.startActivity(intent);
                        mActivity.finish();
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                isCanceled = true;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mCustomTitleBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                //防止动画还没开始，fragment就退出了
                if (mScanningView.isAttachedToWindow() && !mAnimatorSet.isRunning()) {
                    startAnimation();
                }
            }
        }, 400);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mAnimatorSet.isPaused()) {
            mAnimatorSet.resume();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAnimatorSet.isRunning()) {
            mAnimatorSet.pause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mAnimatorSet.isRunning() || mAnimatorSet.isPaused()) {
            mAnimatorSet.cancel();
        }
    }

    private void showDialog() {
        if (mExitDialog == null && mActivity != null) {
            mExitDialog = new DoubleChoicesDialog(mActivity, getString(R.string.interrupted_when_scaning)
                    , getString(R.string.abandon), getString(R.string.go_on), R.mipmap.battery_check,
                    DoubleChoicesDialog.STYLE_GREEN, new DoubleChoicesDialog.OnButtonClickListener() {
                @Override
                public void leftButtonClick(DoubleChoicesDialog dlg) {
                    if (null != mActivity) {
                        if (mCanStopAppList != null && mCanStopAppList.size() > 0) {
                            SharePreferencesUtil.getInstance(mActivity).setLastAbandonOptimize(3);
                            SharePreferencesUtil.getInstance(mActivity).setLastCanStopNumber(mCanStopAppList.size());
                        }
                        mActivity.finish();
                    }
                }

                @Override
                public void rightButtonClick(DoubleChoicesDialog dlg) {
                    dlg.dismiss();
                    if (mAnimatorSet.isPaused()) {
                        mAnimatorSet.resume();
                    }
                }
            });
        }
        if (mExitDialog != null) {
            mExitDialog.show();
        }
    }

    private void startAnimation() {
        // 为避免低版本（6.0等）产生Circular dependencies cannot exist in AnimatorSet异常，ObjectAnimator都单独创建、使用
        if (mCanStopAppList.size() + mNoStopAppList.size() > 0) {
            //在获得结果后，就把结果复制给activity
            if (mActivity != null) {
                mActivity.setCanStopApps(mCanStopAppList);
                mActivity.setNoStopApps(mNoStopAppList);
            }
            ObjectAnimator gradualAnimator = ObjectAnimator.ofFloat(mScanningView, ScanningView.PERCENT, 0f, 1.0f);
            gradualAnimator.setDuration(SCAN_ONE_CIRCLE_DURATION * 2);
            ObjectAnimator angleAnimator1 = ObjectAnimator.ofFloat(mScanningView, ScanningView.SWEEP_ANGLE, 0f, 480f);
            angleAnimator1.setDuration(SCAN_ONE_CIRCLE_DURATION);
            angleAnimator1.setRepeatCount(1);
            ObjectAnimator doneAnimator1 = ObjectAnimator.ofFloat(mScanningView, ScanningView.DONE_ANGLE, 0f, 360f);
            doneAnimator1.setDuration(SCAN_DONE_DURATION);
            mAnimatorSet.play(gradualAnimator).with(angleAnimator1).before(doneAnimator1);
        } else {
            ObjectAnimator angleAnimator2 = ObjectAnimator.ofFloat(mScanningView, ScanningView.SWEEP_ANGLE, 0f, 480f);
            angleAnimator2.setDuration(SCAN_ONE_CIRCLE_DURATION);
            angleAnimator2.setRepeatCount(1);
            ObjectAnimator doneAnimator2 = ObjectAnimator.ofFloat(mScanningView, ScanningView.DONE_ANGLE, 0f, 360f);
            doneAnimator2.setDuration(SCAN_DONE_DURATION);
            mAnimatorSet.play(angleAnimator2).before(doneAnimator2);
        }
        mAnimatorSet.start();
    }

    public void getCurrentRunningServices() {
        new Thread() {
            @Override
            public void run() {
                HashSet<String> ignoreList = SystemUtils.readPowerIgnoreListFile(mActivity);
                if (mActivity == null) return;
                PackageManager packageManager = mActivity.getPackageManager();
                ActivityManager activityManager = (ActivityManager) mActivity.getSystemService(Context.ACTIVITY_SERVICE);
                if (activityManager != null && packageManager != null) {
                    if (Build.VERSION.SDK_INT >= 26) {
                        List<PackageInfo> packageInfos = packageManager.getInstalledPackages(PackageManager.MATCH_UNINSTALLED_PACKAGES);
                        for (PackageInfo info : packageInfos) {
                            try {
                                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(info.packageName, 0);
                                if (!((applicationInfo.flags & (ApplicationInfo.FLAG_STOPPED | ApplicationInfo.FLAG_SYSTEM)) > 0)) {
                                    String packageName = applicationInfo.packageName;
                                    //除去自己和Google Play服务
                                    if (packageName != null && !packageName.equalsIgnoreCase(mActivity.getPackageName()) && !packageName.equalsIgnoreCase("com.google.android.gms")) {
                                        Drawable icon = applicationInfo.loadIcon(packageManager);
                                        CharSequence charSequence = applicationInfo.loadLabel(packageManager);
                                        String name = charSequence.toString();
                                        boolean canStop = true;
                                        for (String pack : ignoreList) {
                                            if (packageName.equalsIgnoreCase(pack)) {
                                                canStop = false;
                                                break;
                                            }
                                        }
                                        AppItem item = new AppItem(packageName, name, icon, canStop);
                                        if (canStop) {
                                            mCanStopAppList.add(item);
                                        } else {
                                            mNoStopAppList.add(item);
                                        }
                                    }
                                }
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        //此方法在7.0（含）以前有效
                        List<ActivityManager.RunningServiceInfo> runningServices = activityManager.getRunningServices(Integer.MAX_VALUE);
                        if (runningServices != null && runningServices.size() > 0) {
                            for (ActivityManager.RunningServiceInfo info : runningServices) {
                                String packageName = info.service.getPackageName();
                                //除去自己和GooglePlay服务
                                if (!packageName.equalsIgnoreCase(mActivity.getPackageName()) && !packageName.equalsIgnoreCase("com.google.android.gms")) {
                                    try {
                                        ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, 0);
                                        //如果不是系统应用
                                        if ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != ApplicationInfo.FLAG_SYSTEM) {
                                            Drawable icon = applicationInfo.loadIcon(packageManager);
                                            CharSequence charSequence = applicationInfo.loadLabel(packageManager);
                                            String name = charSequence.toString();
                                            boolean canStop = true;
                                            for (String pack : ignoreList) {
                                                if (packageName.equalsIgnoreCase(pack)) {
                                                    canStop = false;
                                                    break;
                                                }
                                            }
                                            AppItem item = new AppItem(packageName, name, icon, canStop);
                                            if (canStop) {
                                                mCanStopAppList.add(item);
                                            } else {
                                                mNoStopAppList.add(item);
                                            }
                                        }
                                    } catch (PackageManager.NameNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }.start();
    }

}
