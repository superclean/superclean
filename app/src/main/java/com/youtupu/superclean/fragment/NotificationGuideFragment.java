package com.youtupu.superclean.fragment;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.youtupu.superclean.activity.NotificationManagerActivity;

import com.youtupu.superclean.R;
import com.youtupu.superclean.base.BaseFragment;
import com.youtupu.superclean.dialog.NotificationPermissionDialog;
import com.youtupu.superclean.utils.CustomToast;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.utils.SystemUtils;

/**
 * Created by Jiali on 2019/3/18.
 */
public class NotificationGuideFragment extends BaseFragment {
    private Button mButton;
    private NotificationManagerActivity mActivity;
    private LottieAnimationView mLottieAnimationView;
    private NotificationPermissionDialog mDialog;
    private boolean mNotificationMgrUsed;
    private ObjectAnimator mLlAnimator;
    private AnimatorSet mAnimatorSet;
    private int mLottieTransY;
    private int mLlY = 0;
    private boolean mGotoSetting;
    private CustomToast mCustomToast;

    @Override
    protected void initActivity(Context context) {
        mActivity = (NotificationManagerActivity) context;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.notification_guide_layout;
    }

    @Override
    protected void initView() {
        ImageView imgGuide = mRootView.findViewById(R.id.img_default_back);
        mButton = mRootView.findViewById(R.id.button_notification_permisson);
        mLottieAnimationView = mRootView.findViewById(R.id.lottie_view);
        LinearLayout mLlHint = mRootView.findViewById(R.id.ll_hint);
        mNotificationMgrUsed = SharePreferencesUtil.getInstance(mActivity).getNotificationUsed();
        if (mNotificationMgrUsed) {
            mLottieAnimationView.setVisibility(View.INVISIBLE);
            imgGuide.setVisibility(View.VISIBLE);
        } else {
            mLottieAnimationView.setVisibility(View.VISIBLE);
            imgGuide.setVisibility(View.INVISIBLE);
            mLottieAnimationView.playAnimation();
            mLlHint.setTranslationY(SystemUtils.dp2px(mActivity, 160));
            mLlY = SystemUtils.dp2px(mActivity, 160);
            mLlAnimator = ObjectAnimator.ofFloat(mLlHint, "translationY", mLlY, 0f);
            mLlAnimator.setDuration(500);
            mAnimatorSet = new AnimatorSet();
            mLottieAnimationView.addAnimatorListener(new Animator.AnimatorListener() {
                boolean mIsCanceled;

                @Override
                public void onAnimationStart(Animator animation) {
                    mIsCanceled = false;
                    mButton.setClickable(false);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (!mIsCanceled) {
                        mAnimatorSet.start();
                        mButton.setClickable(true);
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    mIsCanceled = true;
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
        mDialog = new NotificationPermissionDialog(mActivity);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean listenerPermission = mActivity.notificationListenerEnadble();
                boolean notificationPermission = mActivity.isNotificationEnabled();
                if (listenerPermission && notificationPermission) {
                    mActivity.switchFragment(NotificationManagerActivity.HOME_FRAGMENT_INDEX, R.id.notification_framelayout);
                } else {
                    //先检查有没有发通知权限
                    if (!notificationPermission) {
                        gotoSendNotificationSetting();
                    } else {
                        //有通知使用权，但没有发送通知权限
                        gotoNotificationAccessSetting();
                        //延迟一点显示toast
                        mButton.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                String title = String.format(mActivity.getString(R.string.please_authorize), mActivity.getString(R.string.app_name));
                                mCustomToast = new CustomToast(mActivity, title);
                                mCustomToast.show();
                            }
                        }, 500);
                    }
                }
            }
        });
        mLottieAnimationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //如果用户没有用过该功能，才显示动画，否则显示静态图片
                if (!mNotificationMgrUsed) {
                    mLottieTransY = mLottieAnimationView.getTop() / 2;
                    if (mLottieTransY > mLlY) mLottieTransY = mLlY;
                    ObjectAnimator lottieAnimator = ObjectAnimator.ofFloat(mLottieAnimationView, "translationY", 0f, -mLottieTransY);
                    lottieAnimator.setDuration(500);
                    mAnimatorSet.playTogether(mLlAnimator, lottieAnimator);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean listenerPermission = mActivity.notificationListenerEnadble();
        boolean notificationPermission = mActivity.isNotificationEnabled();
        if (listenerPermission && notificationPermission) {
            if (mDialog.isShowing()) {
                mDialog.setPermissionGranted(true, true);
                mDialog.dismiss();
            }
            mActivity.switchFragment(NotificationManagerActivity.HOME_FRAGMENT_INDEX, R.id.notification_framelayout);
        } else {
            //对话框只在一种情况下显示，就是用户授予发送通知的权限回来，发现还没有通知使用权
            if (mGotoSetting && notificationPermission) {
                mGotoSetting = false;
                mDialog.setPermissionGranted(true, false);
                mDialog.show();
            }
        }
        //防止回来的过快，toast还在显示
        if (mCustomToast != null) {
            mCustomToast.dismiss();
        }
    }

    //跳往设置的通知使用权界面
    private void gotoNotificationAccessSetting() {
        try {
            Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //由于设置的那个界面activity是single_instance或single_task，会导致onActivityResult立即执行，所以startActivityForResult没有意义
            mActivity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            try {
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ComponentName name = new ComponentName("com.android.settings",
                        "com.android.settings.Settings$NotificationAccessSettingsActivity");
                intent.setComponent(name);
                intent.putExtra(":settings:show_fragment", "NotificationAccessSettings");
                mActivity.startActivity(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    //跳往设置的开启通知的界面
    private void gotoSendNotificationSetting() {
        Intent intent = new Intent();
        String packageName = mActivity.getPackageName();
        if (Build.VERSION.SDK_INT >= 26) {
            // android 8.0引导
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("android.provider.extra.APP_PACKAGE", packageName);
        } else {
            // android 5.0-7.0
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("app_package", packageName);
            intent.putExtra("app_uid", mActivity.getApplicationInfo().uid);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mActivity.startActivity(intent);
        mGotoSetting = true;
    }

}
