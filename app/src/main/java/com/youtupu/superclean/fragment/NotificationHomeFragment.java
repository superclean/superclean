package com.youtupu.superclean.fragment;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.youtupu.superclean.activity.NotificationManagerActivity;
import com.youtupu.superclean.utils.Constants;
import com.youtupu.superclean.view.CustomTitleBar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.instreet.business.advertise.NativeAdvertiseManager;
import com.youtupu.superclean.BuildConfig;
import com.youtupu.superclean.R;

import com.youtupu.superclean.activity.OptimizeActivity;
import com.youtupu.superclean.base.BaseFragment;
import com.youtupu.superclean.bean.NotificationItem;
import com.youtupu.superclean.extract.NotificationHome;
import com.youtupu.superclean.extract.OnNotificatinListener;
import com.youtupu.superclean.service.NotificationService;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.utils.ToastUtil;
import com.youtupu.superclean.view.ThreeStatusCheckBox;

/**
 * Created by Jiali on 2018/12/12.
 */

public class NotificationHomeFragment extends BaseFragment implements NotificationHome {
    public static final String ACTION_BIND_NOTIFICATION_SERVICE = "com.youtupu.superclean.notification";
    private static final int LOADING_VIEW = 0;
    private static final int DETAIL_VIEW = 1;
    private static final int NOTHING_VIEW = 2;
    //    private static final String NOTIFICATION_AD_ID = "ca-app-pub-4414232724432396/4846312709";
    private NotificationManagerActivity mActivity;
    private Button mButton;
    private ListView mLvNotifications;
    private NotificationServiceConnection mServiceConnection;
    private OnNotificatinListener mNotificatinListener;
    private List<NotificationItem> mNotificationItemList;
    private NotificationsAdapter mAdapter;
    private PackageManager mPackageManager;
    private LinearLayout mLlNoNotification;
    private LinearLayout mLlDetail;
    private TextView mTvNum;
    private LinearLayout mRlLoading;
    private ThreeStatusCheckBox mCheckBox;
    private boolean mFirstCreate = false;
    private LinearLayout mLlAdView;

    @Override
    protected void initActivity(Context context) {
        mActivity = (NotificationManagerActivity) context;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.notification_home_fragment;
    }

    @Override
    protected void initView() {
        mFirstCreate = true;
        CustomTitleBar customTitleBar = mRootView.findViewById(R.id.custom_title_bar);
        mButton = mRootView.findViewById(R.id.button_notification_clean);
        mLvNotifications = mRootView.findViewById(R.id.lv_notifications);
        mCheckBox = mRootView.findViewById(R.id.checkbox_total);
        mLlNoNotification = mRootView.findViewById(R.id.ll_no_notification);
        mLlDetail = mRootView.findViewById(R.id.ll_detail);
        mTvNum = mRootView.findViewById(R.id.tv_total_num);
        mRlLoading = mRootView.findViewById(R.id.ll_loading);
        mLlAdView = mRootView.findViewById(R.id.ll_ad_title);
        if (!SharePreferencesUtil.getInstance(mActivity).getNotificationUsed()) {
            SharePreferencesUtil.getInstance(mActivity).setNotificationUsed(true);
        }
        mPackageManager = mActivity.getPackageManager();
        mNotificationItemList = new ArrayList<>();
        mAdapter = new NotificationsAdapter(mActivity, mNotificationItemList, this);
        mLvNotifications.setAdapter(mAdapter);
        Intent serviceIntent = new Intent(mActivity, NotificationService.class);
        serviceIntent.setAction(ACTION_BIND_NOTIFICATION_SERVICE);
        mServiceConnection = new NotificationServiceConnection();
        mActivity.bindService(serviceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
        customTitleBar.setOnLeftIconClickListener(new CustomTitleBar.LeftIconClickListener() {
            @Override
            public void onLeftClick() {
                mActivity.onBackPressed();
            }
        });
        customTitleBar.setOnRightIconClickListener(new CustomTitleBar.RightIconClickListener() {
            @Override
            public void onRightClick() {
                mActivity.switchFragment(NotificationManagerActivity.SETTING_FRAGMENT_INDEX, R.id.notification_framelayout);
            }
        });
        mCheckBox.setOnCheckBoxSelectedListener(new ThreeStatusCheckBox.OnCheckBoxSelectedListener() {
            @Override
            public void onChecked() {
                for (NotificationItem item : mNotificationItemList) {
                    item.setChecked(true);
                }
                mAdapter.notifyDataSetChanged();
                updateStatus();
            }

            @Override
            public void onUnChecked() {
                for (NotificationItem item : mNotificationItemList) {
                    item.setChecked(false);
                }
                mAdapter.notifyDataSetChanged();
                updateStatus();
            }
        });
        mLvNotifications.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NotificationItem item = (NotificationItem) mAdapter.getItem(position);
                PendingIntent pendingIntent = item.getPendingIntent();
                if (pendingIntent != null) {
                    try {
                        pendingIntent.send();
                    } catch (PendingIntent.CanceledException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (mPackageManager != null) {
                        String packageName = item.getPackageName();
                        Intent intent = mPackageManager.getLaunchIntentForPackage(packageName);
                        if (intent != null) {
                            mActivity.startActivity(intent);
                        }
                    }

                }
            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<NotificationItem> deleteList = new ArrayList<>();
                for (NotificationItem item : mNotificationItemList) {
                    if (item.isChecked()) {
                        deleteList.add(item);
                    }
                }
                if (deleteList.size() > 0) {
                    mNotificationItemList.removeAll(deleteList);
                    mAdapter.notifyDataSetChanged();
                    updateStatus();
                    if (mNotificatinListener != null) {
                        mNotificatinListener.updateCleanedNotifications(mNotificationItemList);
                    }
                    toOptimizeActivity(deleteList.size());
                } else {
                    ToastUtil.showShortToast(mActivity, getString(R.string.notification_no_select));
                }

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mFirstCreate) {
            mFirstCreate = false;
//            mLlAdView.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (BuildConfig.DEBUG) {
//                        NativeAdvertiseManager.getInstance().showNativeAds(
//                                Constants.TEST_NATIVE_AD, mLlAdView,
//                                NativeAdvertiseManager.STYLE_MEDIUM, getResources().getColor(R.color.clean_button));
//                    } else {
//                        NativeAdvertiseManager.getInstance().showNativeAds(
//                                Constants.NOTIFICATION_NATIVE_AD, mLlAdView,
//                                NativeAdvertiseManager.STYLE_MEDIUM, getResources().getColor(R.color.clean_button));
//                    }
//                }
//            }, 500);
        }
        updateNotificationsList();
    }

    //必须在mNotificationItemList ！= null的情况下调用
    private void updateStatus() {
        //部分手机（摩托罗拉）在Acitity关闭后，收纳通知时，activity已被回收，fragment没有context了
        if (isAdded()) {
            int num = mNotificationItemList.size();
            if (num > 0) {
                mTvNum.setText(String.valueOf(num));
                switchView(DETAIL_VIEW);
                updateListViewHeight();
            } else {
                switchView(NOTHING_VIEW);
            }
        }
    }

    private void updateSelectedStatus() {
        int selectNum = 0;
        for (NotificationItem item : mNotificationItemList) {
            if (item.isChecked()) {
                selectNum++;
            }
        }
        if (selectNum == 0) {
            mCheckBox.setChecked(false);
        } else if (selectNum == mNotificationItemList.size()) {
            mCheckBox.setChecked(true);
        } else {
            mCheckBox.setCheckBoxPartialSelected();
        }
    }

    private void switchView(int view) {
        switch (view) {
            case LOADING_VIEW:
                mRlLoading.setVisibility(View.VISIBLE);
                mLlDetail.setVisibility(View.INVISIBLE);
                mLlNoNotification.setVisibility(View.INVISIBLE);
                mButton.setVisibility(View.GONE);
                break;
            case DETAIL_VIEW:
                mRlLoading.setVisibility(View.INVISIBLE);
                mLlDetail.setVisibility(View.VISIBLE);
                mLlNoNotification.setVisibility(View.INVISIBLE);
                mButton.setVisibility(View.VISIBLE);
                updateSelectedStatus();
                break;
            case NOTHING_VIEW:
                mRlLoading.setVisibility(View.INVISIBLE);
                mLlDetail.setVisibility(View.INVISIBLE);
                mLlNoNotification.setVisibility(View.VISIBLE);
                mButton.setVisibility(View.GONE);
                break;
        }
    }

    private void updateNotificationsList() {
        if (mNotificatinListener != null && mNotificatinListener.isConnected()) {
            List<NotificationItem> cleanedNotifications = mNotificatinListener.getCleanedNotifications();
            mNotificationItemList.clear();
            mNotificationItemList.addAll(cleanedNotifications);
            mAdapter.notifyDataSetChanged();
            updateStatus();
        } else {
            switchView(LOADING_VIEW);
        }
    }

    private void updateListViewHeight() {
        int count = mAdapter.getCount();
        if (count > 0) {
            int totalHeight = 0;
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mLvNotifications.getLayoutParams();
            for (int i = 0; i < count; i++) {
                View item = mAdapter.getView(i, null, mLvNotifications);
                //item的布局要求是linearLayout，否则measure(0,0)会报错。
                item.measure(0, 0);
                //计算出所有item高度的总和
                totalHeight += item.getMeasuredHeight();
            }
            params.height = totalHeight;
            mLvNotifications.setLayoutParams(params);
        }
    }

    private void toOptimizeActivity(int deleteNum) {
//        Intent intent = new Intent(mActivity, OptimizeActivity.class);
//        intent.putExtra(OptimizeActivity.OPTIMIZE_TYPE, OptimizeActivity.FROM_NOTIFICATION);
//        intent.putExtra(OptimizeActivity.ACTION_BAR_TEXT, getString(R.string.notification_title));
//        intent.putExtra(OptimizeActivity.OPTIMIZE_LEFT_TEXT, String.valueOf(deleteNum));
//        String result = getString(R.string.notification_cleaned);
//        intent.putExtra(OptimizeActivity.OPTIMIZE_RIGHT_TEXT, result);

        Intent intent = new OptimizeActivity.IntentBuilder()
                .setClass(mActivity)
                .setFrom(OptimizeActivity.FROM_NOTIFICATION)
                .setActionBar(getString(R.string.notification_title))
                .setLeftText(String.valueOf(deleteNum), 0, 0)
                .setRightText(getString(R.string.notification_cleaned), 0, 0)
                .build();
        mActivity.startActivity(intent);
    }

    @Override
    public void onListenerConnected(List<NotificationItem> list) {
        mNotificationItemList.clear();
        mNotificationItemList.addAll(list);
        mAdapter.notifyDataSetChanged();
        updateStatus();
    }

    @Override
    public void onListenerDisConnected() {

    }

    @Override
    public void onListenerUpdate(List<NotificationItem> list) {
        mNotificationItemList.clear();
        mNotificationItemList.addAll(list);
        mAdapter.notifyDataSetChanged();
        updateStatus();
    }

    private void onGotService() {
        List<NotificationItem> cleanedNotifications = mNotificatinListener.getCleanedNotifications();
        mNotificationItemList.clear();
        mNotificationItemList.addAll(cleanedNotifications);
        mAdapter.notifyDataSetChanged();
        updateStatus();
    }

    private class NotificationServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            NotificationService.NotificationBinder binder = (NotificationService.NotificationBinder) service;
            if (binder != null) {
                mNotificatinListener = binder.getService();
                if (mNotificatinListener != null) {
                    mNotificatinListener.setHome(NotificationHomeFragment.this);
                    //两种情况：
                    if (!mNotificatinListener.isConnected()) {
                        //1、绑定服务后，该服务还没和系统取得联系，此时让该服务自己重启下，即toggle，等待和系统联系上，在联系上之前，显示加载界面
                        mNotificatinListener.toggle();
                    } else {
                        //2、绑定服务后，该服务已和系统取得联系，此时可以直接用；
                        onGotService();
                    }
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mNotificatinListener = null;
        }
    }

    private class NotificationsAdapter extends BaseAdapter {
        private LayoutInflater mLayoutInflater;
        private List<NotificationItem> mList;
        private long mADayTime;
        private final DateFormat mDateFormat;
        private NotificationHomeFragment mFragment;

        NotificationsAdapter(@NonNull Context context, @NonNull List<NotificationItem> list, NotificationHomeFragment fragment) {
            mList = list;
            mLayoutInflater = LayoutInflater.from(context);
            mADayTime = 1000 * 60 * 60 * 24;
            mDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            mFragment = fragment;
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public Object getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.notification_detail_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.imgIcon = convertView.findViewById(R.id.img_app_icon);
                viewHolder.tvTitle = convertView.findViewById(R.id.tv_notification_title);
                viewHolder.tvContent = convertView.findViewById(R.id.tv_notification_content);
                viewHolder.checkbox = convertView.findViewById(R.id.checkbox_app);
                viewHolder.tvTime = convertView.findViewById(R.id.tv_time);
                viewHolder.checkbox.setOnCheckBoxSelectedListener(new ThreeStatusCheckBox.OnCheckBoxSelectedListener() {
                    @Override
                    public void onChecked() {
                        int index = viewHolder.checkbox.getmTag();
                        if (index < mList.size()) {
                            mList.get(index).setChecked(true);
                            mFragment.updateSelectedStatus();
                        }
                    }

                    @Override
                    public void onUnChecked() {
                        int index = viewHolder.checkbox.getmTag();
                        if (index < mList.size()) {
                            mList.get(index).setChecked(false);
                            mFragment.updateSelectedStatus();
                        }
                    }
                });
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            NotificationItem item = mList.get(position);
            if (item != null) {
                Drawable icon = item.getIcon();
                String label = item.getLabel();
                String title = item.getTitle();
                String content = item.getContent();
                long time = item.getTime();
                if (icon != null) {
                    viewHolder.imgIcon.setImageDrawable(icon);
                    if (title != null && !title.isEmpty()) {
                        viewHolder.tvTitle.setText(title);
                    } else {
                        viewHolder.tvTitle.setText(label);
                    }
                    viewHolder.tvContent.setText(content);
                } else {
                    viewHolder.imgIcon.setImageResource(R.mipmap.folder_icon);
                    viewHolder.tvTitle.setText(label);
                    viewHolder.tvContent.setText(title);
                }
                viewHolder.checkbox.setChecked(item.isChecked());
                //每个checkbox要知道自己是哪个item里的
                viewHolder.checkbox.setmTag(position);
                if (time > 0) {
                    long currentTime = System.currentTimeMillis();
                    long zeroTime = currentTime - currentTime % (1000 * 3600 * 24);
                    if (time > zeroTime) {
                        //今天
                        String text = mDateFormat.format(new Date(time));
                        viewHolder.tvTime.setText(text);
                    } else if (zeroTime - time < mADayTime) {
                        //昨天
                        viewHolder.tvTime.setText(getString(R.string.yesterday));
                    } else if (zeroTime - time < 2 * mADayTime) {
                        viewHolder.tvTime.setText(getString(R.string.before_yesterday));
                        //前天
                    } else {
                        //三天前
                        viewHolder.tvTime.setText(getString(R.string.three_days_ago));
                    }
                }
            }
            return convertView;
        }
    }

    private class ViewHolder {
        ImageView imgIcon;
        TextView tvTitle;
        TextView tvContent;
        ThreeStatusCheckBox checkbox;
        TextView tvTime;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mNotificatinListener != null) {
            mActivity.unbindService(mServiceConnection);
        }
    }


}
