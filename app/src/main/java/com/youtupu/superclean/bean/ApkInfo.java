package com.youtupu.superclean.bean;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by Jiali on 2019/5/20.
 */
public class ApkInfo implements Parcelable {
    private String packageName;
    private String filePath;
    private long fileSize;

    public ApkInfo(@NonNull String packageName, @NonNull String filePath, long fileSize) {
        this.packageName = packageName;
        this.filePath = filePath;
        this.fileSize = fileSize;
    }

    private ApkInfo(Parcel in) {
        packageName = in.readString();
        filePath = in.readString();
        fileSize = in.readLong();
    }

    public static final Creator<ApkInfo> CREATOR = new Creator<ApkInfo>() {
        @Override
        public ApkInfo createFromParcel(Parcel in) {
            return new ApkInfo(in);
        }

        @Override
        public ApkInfo[] newArray(int size) {
            return new ApkInfo[size];
        }
    };

    public String getPackageName() {
        return packageName;
    }

    public String getFilePath() {
        return filePath;
    }

    public long getFileSize() {
        return fileSize;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(packageName);
        dest.writeString(filePath);
        dest.writeLong(fileSize);
    }
}
