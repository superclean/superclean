package com.youtupu.superclean.bean;

/**
 * Created by Jiali on 2019/5/27.
 */
public class ADRule {
    private String path;
    private String desc;

    public ADRule(String path, String desc) {
        this.path = path;
        this.desc = desc;
    }

    public String getPath() {
        return path;
    }

    public String getDesc() {
        return desc;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
