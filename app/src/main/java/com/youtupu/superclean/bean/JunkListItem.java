package com.youtupu.superclean.bean;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jiali on 2018/12/18.
 */

public class JunkListItem {

    private long mSize;
    private String mPackageName;
    private String mApplicationName;
    private Drawable mIcon;
    private boolean mIsChecked;
    private File mFile;

    public JunkListItem(String packageName, String applicationName,
                        Drawable icon, long size, @NonNull File file) {
        mSize = size;
        mPackageName = packageName;
        mApplicationName = applicationName;
        mIcon = icon;
        mIsChecked = false;
        mFile = file;
    }

    public File getFile() {
        return mFile;
    }

    public Drawable getApplicationIcon() {
        return mIcon;
    }

    public String getApplicationName() {
        return mApplicationName;
    }

    public long getSize() {
        return mSize;
    }

    public String getPackageName() {
        return mPackageName;
    }

    public void setChecked(boolean checked) {
        mIsChecked = checked;
    }

    public boolean getChecked() {
        return mIsChecked;
    }

    public static class JunkListMetadata implements Serializable {
        public String packageName;
        public File file;
        public long size;

        public JunkListMetadata(String packageName, long size, File file){
            this.packageName = packageName;
            this.size = size;
            this.file = file;
        }

        public static ArrayList<JunkListMetadata> fromJunkListItem(List<JunkListItem> list){
            if(null == list){
                return new ArrayList<>();
            }
            ArrayList result = new ArrayList(list.size());
            for (JunkListItem item : list) {
                result.add(new JunkListMetadata(item.mPackageName, item.mSize, item.mFile));
            }
            return result;
        }

        public static ArrayList<JunkListItem> toJunkListItem(List<JunkListMetadata> list, PackageManager packageManager){
            if(null == list || null == packageManager){
                return new ArrayList<>();
            }
            ArrayList result = new ArrayList(list.size());
            for (JunkListMetadata metadata : list) {
                try {
                    ApplicationInfo appInfo = packageManager.getApplicationInfo(metadata.packageName, 0);
                    JunkListItem item = new JunkListItem(appInfo.packageName, null,
                            null, metadata.size, metadata.file);
                    result.add(item);
                } catch (PackageManager.NameNotFoundException e) {
                    result.add(new JunkListItem(null, null, null,
                            metadata.size, metadata.file));
                }
            }
            return result;
        }
    }



}
