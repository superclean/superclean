package com.youtupu.superclean.bean;

/**
 * Created by Jiali on 2019/5/27.
 */
public class ResidualRule {
    private String path;
    private String[] packages;
    private String desc;

    public ResidualRule(String path, String[] packages, String desc) {
        this.path = path;
        this.packages = packages;
        this.desc = desc;
    }

    public String getPath() {
        return path;
    }

    public String[] getPackages() {
        return packages;
    }

    public String getDesc() {
        return desc;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setPackages(String[] packages) {
        this.packages = packages;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
