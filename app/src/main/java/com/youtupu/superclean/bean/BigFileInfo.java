package com.youtupu.superclean.bean;

import android.support.annotation.NonNull;

import java.io.File;

/**
 * Created by Jiali on 2019/6/5.
 */
public class BigFileInfo implements Comparable<BigFileInfo> {
    public static final int SORT_BY_SIZE = 0;
    public static final int SORT_BY_TIME = 1;
    public static final int SORT_BY_NAME = 2;
    public static int mSortByWhat = SORT_BY_SIZE;
    public static final int STYLE_VIDEO = 0;
    public static final int STYLE_IMAGE = 1;
    public static final int STYLE_AUDIO = 2;
    public static final int STYLE_DOCUMENT = 3;
    public static final int STYLE_RARELY = 4;
    private String mPath;
    private long mSize;
    private int mStyle;
    private String mName;
    private String mSuffix;
    private boolean mChecked = false;
    private File mFile;
    private long mTime;

    public BigFileInfo(String path, long size, int style, String name, String suffix, File file, long time) {
        mPath = path;
        mSize = size;
        mStyle = style;
        mName = name;
        mSuffix = suffix;
        mFile = file;
        mTime = time;
    }

    public String getPath() {
        return mPath;
    }

    public long getSize() {
        return mSize;
    }

    public int getStyle() {
        return mStyle;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public void setSize(long size) {
        mSize = size;
    }

    public void setStyle(int style) {
        mStyle = style;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSuffix() {
        return mSuffix;
    }

    public void setSuffix(String suffix) {
        mSuffix = suffix;
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void setChecked(boolean checked) {
        mChecked = checked;
    }

    public File getFile() {
        return mFile;
    }

    public void setFile(File file) {
        mFile = file;
    }

    public long getTime() {
        return mTime;
    }

    @Override
    public int compareTo(@NonNull BigFileInfo info) {
        if (mSortByWhat == SORT_BY_SIZE) {
            if (info.getSize() > this.getSize()) {
                return 1;
            } else if (info.getSize() < this.getSize()) {
                return -1;
            } else {
                return info.getPath().compareTo(this.getPath());
            }
        } else if (mSortByWhat == SORT_BY_TIME) {
            if (info.getTime() > this.getTime()) {
                return 1;
            } else if (info.getTime() < this.getTime()) {
                return -1;
            } else {
                return info.getPath().compareTo(this.getPath());
            }
        } else {
            return info.getName().compareTo(this.getName());
        }
    }
}
