package com.youtupu.superclean.bean;

import android.graphics.drawable.Drawable;

import java.io.File;

/**
 * Created by Jiali on 2018/12/20.
 */

public class RubbishListItem {
    private String info;
    private long size;
    private boolean isChecked;
    private File mFile;

    public RubbishListItem(String info, long size, File file) {
        this.info = info;
        this.size = size;
        isChecked = false;
        mFile = file;
    }

    public File getFile() {
        return mFile;
    }

    public String getInfo() {
        return info;
    }

    public long getSize() {
        return size;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean getChecked() {
        return isChecked;
    }

    @Override
    public String toString() {
        return "RubbishListItem{" +
                "info='" + info + '\'' +
                ", size=" + size +
                '}';
    }
}
