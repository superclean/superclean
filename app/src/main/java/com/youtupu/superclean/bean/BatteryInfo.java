package com.youtupu.superclean.bean;

/**
 * Created by Jiali on 2018/12/10.
 */

public class BatteryInfo {
    public String health;
    public int level;
    public String status;
    public int scale;
    public int voltage;
    public int icon;
    public String technology;
    public String plugged;
    public boolean present;
    public int temperature;

    public String getHealth() {
        return health;
    }

    public int getLevel() {
        return level;
    }

    public String getStatus() {
        return status;
    }

    public int getScale() {
        return scale;
    }

    public int getVoltage() {
        return voltage;
    }

    public int getIcon() {
        return icon;
    }

    public String getTechnology() {
        return technology;
    }

    public String getPlugged() {
        return plugged;
    }

    public boolean getPresent() {
        return present;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public void setPlugged(String plugged) {
        this.plugged = plugged;
    }

    public void setPresent(boolean present) {
        this.present = present;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "BatteryInfo{" +
                "health='" + health + '\'' +
                ", level=" + level +
                ", status='" + status + '\'' +
                ", scale=" + scale +
                ", voltage=" + voltage +
                ", icon=" + icon +
                ", technology='" + technology + '\'' +
                ", plugged=" + plugged +
                ", present=" + present +
                ", temperature=" + temperature +
                '}';
    }
}
