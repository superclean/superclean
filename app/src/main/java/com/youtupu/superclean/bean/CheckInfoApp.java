package com.youtupu.superclean.bean;

import android.content.pm.ApplicationInfo;
import android.support.annotation.NonNull;

/**
 * Created by Jiali on 2018/12/25.
 */

public class CheckInfoApp {
    private ApplicationInfo mInfo;
    private boolean mIsChecked;

    public CheckInfoApp(@NonNull ApplicationInfo info, boolean checked) {
        mInfo = info;
        mIsChecked = checked;
    }

    public ApplicationInfo getInfo() {
        return mInfo;
    }

    public boolean isChecked() {
        return mIsChecked;
    }

    public void setChecked(boolean checked) {
        mIsChecked = checked;
    }
}
