package com.youtupu.superclean.bean;

import android.app.PendingIntent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by Jiali on 2019/3/19.
 */
public class NotificationItem implements Serializable, Comparable<NotificationItem>{
    private String mPackageName;
    private String mTitle;
    private String mContent;
    private transient PendingIntent mPendingIntent;
    private boolean mIsChecked;
    private long mTime;
    private transient Drawable mIcon;
    private String mLabel;

    public NotificationItem(String packageName, String title, String content, PendingIntent pendingIntent, boolean isChecked, long time, Drawable icon, String label) {
        mPackageName = packageName;
        mTitle = title;
        mContent = content;
        mPendingIntent = pendingIntent;
        mIsChecked = isChecked;
        mTime = time;
        mIcon = icon;
        mLabel = label;
    }

    public void setLabel(String label) {
        mLabel = label;
    }

    public void setIcon(Drawable icon) {
        mIcon = icon;
    }

    public void setPackageName(String packageName) {
        mPackageName = packageName;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public void setPendingIntent(PendingIntent pendingIntent) {
        mPendingIntent = pendingIntent;
    }

    public void setChecked(boolean checked) {
        mIsChecked = checked;
    }

    public void setTime(long time) {
        mTime = time;
    }

    public String getPackageName() {
        return mPackageName;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getContent() {
        return mContent;
    }

    public PendingIntent getPendingIntent() {
        return mPendingIntent;
    }

    public boolean isChecked() {
        return mIsChecked;
    }

    public long getTime() {
        return mTime;
    }

    public Drawable getIcon() {
        return mIcon;
    }

    public String getLabel() {
        return mLabel;
    }

    @Override
    public int compareTo(@NonNull NotificationItem item) {
        return (int) (item.getTime() - this.getTime());
    }
}
