package com.youtupu.superclean.bean;

import android.graphics.drawable.Drawable;

/**
 * Created by Jiali on 2019/3/12.
 */

public class AppItem {
    private String packageName;
    private String name;
    private Drawable icon;
    private boolean isChecked;

    public AppItem(String packageName, String name, Drawable icon, boolean isChecked) {
        this.packageName = packageName;
        this.name = name;
        this.icon = icon;
        this.isChecked = isChecked;
    }

    public Drawable getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public String getPackageName() {
        return packageName;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
