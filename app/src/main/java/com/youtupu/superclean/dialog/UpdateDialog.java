package com.youtupu.superclean.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/2/21.
 */

public class UpdateDialog extends Dialog {

    private String mVersionName;
    private String mVersionContent;
    private OnClickListener mOnClickListener;

    public UpdateDialog(@NonNull Context context) {
        super(context);
    }

    public UpdateDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected UpdateDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public UpdateDialog(@NonNull Context context, String versionName, String versionContent) {
        super(context);
        setCancelable(false);
        mVersionName = versionName;
        mVersionContent = versionContent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_dialog_layout);
        TextView tvVersionName = findViewById(R.id.tv_version_name);
        TextView tvVersionContent = findViewById(R.id.tv_version_content);
        TextView tvThanks = findViewById(R.id.tv_thanks);
        TextView tvOk = findViewById(R.id.tv_ok);
        tvVersionName.setText(mVersionName);
        tvVersionContent.setText(mVersionContent);
        tvThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onCancel();
                }
            }
        });
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onOK();
                }
            }
        });
    }

    public interface OnClickListener {
        void onCancel();

        void onOK();
    }

    public void setOnButtonClickListener(OnClickListener listener) {
        mOnClickListener = listener;
    }

}
