package com.youtupu.superclean.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/2/27.
 * 清理文件详情对话框
 */

public class DetailDialog extends Dialog {

    private TextView mTvTitle;
    private TextView mTvSize;
    private TextView mTvPath;
    private TextView mTvSuggestion;
    private RelativeLayout mRlConfirm;
    private String mStringTitle;
    private String mStringSize;
    private String mStringPath;
    private String mStringSuggestion;
    private String mStringConfirm;
    private TextView mTvConfirm;

    public DetailDialog(@NonNull Context context) {
        super(context);
    }

    public DetailDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected DetailDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_dialog_layout);
        initView();
        initListener();
    }

    private void initView() {
        mTvTitle = findViewById(R.id.tv_detail_dialog_title);
        mTvSize = findViewById(R.id.tv_detail_dialog_size);
        mTvPath = findViewById(R.id.tv_detail_dialog_path);
        mTvSuggestion = findViewById(R.id.tv_detail_dialog_suggestion);
        mRlConfirm = findViewById(R.id.rl_confirm);
        mTvConfirm = findViewById(R.id.tv_detail_dialog_confirm);
    }

    private void setData() {
        mTvTitle.setText(mStringTitle);
        mTvSize.setText(mStringSize);
        mTvPath.setText(mStringPath);
        mTvSuggestion.setText(mStringSuggestion);
        mTvConfirm.setText(mStringConfirm);
    }

    private void initListener() {
        mRlConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void show() {
        super.show();
        setData();
    }

    public void setTitle(@NonNull String title) {
        mStringTitle = title;
    }

    public void setSize(@NonNull String size) {
        mStringSize = size;
    }

    public void setPath(@NonNull String path) {
        mStringPath = path;
    }

    public void setSuggestion(@NonNull String suggestion) {
        mStringSuggestion = suggestion;
    }

    public void setRlConfirmText(@NonNull String confirmText) {
        mStringConfirm = confirmText;
    }
}
