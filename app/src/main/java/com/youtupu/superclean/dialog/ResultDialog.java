package com.youtupu.superclean.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/2/20.
 */

public class ResultDialog extends Dialog {
    private String mTitle;
    private int mImgResoure = 0;

    public ResultDialog(@NonNull Context context) {
        super(context);
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setImgResoure(int resoureID) {
        mImgResoure = resoureID;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_dialog_layout);
        TextView tvConfirm = findViewById(R.id.dialog_choice);
        TextView tvTitle = findViewById(R.id.dialog_title);
        if (mTitle != null) {
            tvTitle.setText(mTitle);
        }
        ImageView imgResult = findViewById(R.id.img_result);
        if (mImgResoure != 0) {
            imgResult.setImageResource(mImgResoure);
        }
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

}
