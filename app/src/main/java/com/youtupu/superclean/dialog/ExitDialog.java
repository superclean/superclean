package com.youtupu.superclean.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.youtupu.superclean.activity.BoostActivity;
import com.youtupu.superclean.activity.CoolerDownActivity;
import com.youtupu.superclean.activity.FakePowerSaveActivity;
import com.youtupu.superclean.activity.JunkScanActivity;
import com.youtupu.superclean.utils.SharePreferencesUtil;
import com.youtupu.superclean.utils.SystemUtils;

import java.util.Locale;
import java.util.Random;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/2/21.
 */

public class ExitDialog extends Dialog {
    public static final int STYLE_BOOST = 0;
    public static final int STYLE_JUNK = 1;
    public static final int STYLE_COOLER = 2;
    public static final int STYLE_POWER = 3;
    private int mStyle = STYLE_BOOST;
    private float mBoostPercent = 0.8f;
    private Intent mIntent;
    private long mJunkSize = 0;
    private int mTemperature = 0;
    private Activity mActivity;
    private int mCanStopNumber = 0;

    public ExitDialog(@NonNull Context context) {
        super(context);
        setCancelable(false);
        if (context instanceof Activity) {
            mActivity = (Activity) context;
        }
    }

    public void setStyle(int style) {
        if (style < 0 || style > 3) return;
        mStyle = style;
    }

    public void setBoostPercent(float percent) {
        if (percent < 0 || percent > 1) return;
        mBoostPercent = percent;
    }

    public void setJunkSize(long size) {
        if (size > 0) {
            mJunkSize = size;
        }
    }

    public void setTemperature(int temperature) {
        if (temperature > 30) {
            mTemperature = temperature;
        }
    }

    public void setCanStopNumber(int number) {
        if (number > 0) {
            mCanStopNumber = number;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exit_dialog_layout);
        TextView tvTitle = findViewById(R.id.dialog_title);
        TextView tvDescription = findViewById(R.id.dialog_description);
        TextView tvLeftButton = findViewById(R.id.dialog_left_choice);
        TextView tvRightButton = findViewById(R.id.dialog_right_choice);
        ImageView imgShow = findViewById(R.id.img_show);
        switch (mStyle) {
            case STYLE_BOOST:
                if (mBoostPercent > 0.8f) {
                    String percent = String.format(Locale.getDefault(), "%.0f%%", mBoostPercent * 100);
                    tvTitle.setText(Html.fromHtml(String.format(getContext().getString(R.string.exit_boost_title), percent)));
                    tvRightButton.setText(getContext().getString(R.string.boost));
                    tvDescription.setText(getContext().getString(R.string.exit_boost_description));
                    imgShow.setImageResource(R.mipmap.boost_high);
                    mIntent = new Intent(getContext(), BoostActivity.class);
                    mIntent.putExtra("percent", mBoostPercent);
                    mIntent.putExtra("fromWhere", 11);
                } else {
                    dismiss();
                }
                break;
            case STYLE_JUNK:
                if (mJunkSize > 0) {
                    String size = SystemUtils.getFileSize(mJunkSize);
                    String junkTitle = String.format(getContext().getString(R.string.exit_junk_title), size);
                    tvTitle.setText(Html.fromHtml(junkTitle));
                    tvDescription.setText(getContext().getString(R.string.exit_junk_description));
                    tvRightButton.setText(getContext().getString(R.string.clean));
                    imgShow.setImageResource(R.mipmap.has_junk);
                    mIntent = new Intent(getContext(), JunkScanActivity.class);
                    mIntent.putExtra("fromWhere", 11);
                } else {
                    dismiss();
                }
                break;
            case STYLE_COOLER:
                if (mTemperature > 30) {
                    String temp = String.format(getContext().getString(R.string.temp_num_symbol), mTemperature);
                    String coolerTitle = String.format(getContext().getString(R.string.exit_cooler_title), temp);
                    tvTitle.setText(Html.fromHtml(coolerTitle));
                    tvDescription.setText(getContext().getString(R.string.exit_cooler_description));
                    tvRightButton.setText(getContext().getString(R.string.cool));
                    imgShow.setImageResource(R.mipmap.temp_high);
                    mIntent = new Intent(getContext(), CoolerDownActivity.class);
                    mIntent.putExtra("CpuTemp", mTemperature);
                    mIntent.putExtra("reducedTemp", generateReduceTemp(mTemperature));
                    mIntent.putExtra("fromWhere", 11);
                } else {
                    dismiss();
                }
                break;
            case STYLE_POWER:
                if (mCanStopNumber > 0) {
                    String powerTitle = String.format(getContext().getString(R.string.exit_power_title), mCanStopNumber);
                    tvTitle.setText(Html.fromHtml(powerTitle));
                    tvDescription.setText(getContext().getString(R.string.exit_power_description));
                    tvRightButton.setText(getContext().getString(R.string.improve));
                    mIntent = new Intent(getContext(), FakePowerSaveActivity.class);
                    mIntent.putExtra("totalTask", mCanStopNumber);
                    mIntent.putExtra("showTips", false);
                    mIntent.putExtra("fromWhere", 11);
                }
                break;
        }
        tvRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIntent != null) {
                    getContext().startActivity(mIntent);
                }
                SharePreferencesUtil.getInstance(getContext()).setLastCancelTimeWhenExit(System.currentTimeMillis());
                dismiss();
            }
        });
        tvLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharePreferencesUtil.getInstance(getContext()).setLastCancelTimeWhenExit(System.currentTimeMillis());
                dismiss();
                if (mActivity != null) {
                    mActivity.finish();
                }
            }
        });
    }

    private int generateReduceTemp(int temperature) {
        if (temperature <= 30) return 0;
        int reduceTemp;
        int overTemp = temperature - 30;
        Random random = new Random();
        if (overTemp > 20) {
            reduceTemp = 10 + random.nextInt(overTemp - 20);
        } else if (overTemp > 10) {
            reduceTemp = 5 + random.nextInt(overTemp - 10);
        } else {
            reduceTemp = 2 + random.nextInt(2);
        }
        return reduceTemp;
    }

}
