package com.youtupu.superclean.dialog;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.youtupu.superclean.activity.NotificationManagerActivity;
import com.youtupu.superclean.utils.CustomToast;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/3/15.
 */
public class NotificationPermissionDialog extends Dialog {

    private boolean mListenerPermission = false;
    private boolean mNotificationPermission = false;
    private CustomToast mToast;
    private TextView mTvTitle;
    private TextView mTvDescription;

    public NotificationPermissionDialog(@NonNull Context context) {
        super(context);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_permission_dialog_layout);
        mTvTitle = findViewById(R.id.tv_permission);
        mTvDescription = findViewById(R.id.tv_description);
        Button confirmButton = findViewById(R.id.button_grant_permission);
        Button cancelButton = findViewById(R.id.button_cancel);
        String title = String.format(getContext().getString(R.string.please_authorize), getContext().getString(R.string.app_name));
        mToast = new CustomToast(getContext(), title);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mListenerPermission) {
                    gotoNotificationAccessSetting();
                    mToast.show();
                } else if (!mNotificationPermission) {
                    gotoSendNotificationSetting();
                }
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (getContext() instanceof NotificationManagerActivity) {
                    NotificationManagerActivity activity = (NotificationManagerActivity) getContext();
                    activity.switchFragment(NotificationManagerActivity.HOME_FRAGMENT_INDEX, R.id.notification_framelayout);
                }
            }
        });
        updateStatus();
    }

    private void updateStatus() {
        if (mNotificationPermission) {
            if (mListenerPermission) {
                dismiss();
            } else {
                mTvTitle.setText(getContext().getString(R.string.listener_permission));
                mTvDescription.setText(getContext().getString(R.string.listener_permission_detail));
            }
        } else {
            mTvTitle.setText(getContext().getString(R.string.notification_permission));
            mTvDescription.setText(getContext().getString(R.string.notification_permission_detail));
        }
    }

    public void setPermissionGranted(boolean notificationPermission, boolean listenerPermission) {
        mNotificationPermission = notificationPermission;
        mListenerPermission = listenerPermission;
        if (isShowing()) {
            updateStatus();
        }
    }

    private void gotoNotificationAccessSetting() {
        try {

            Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //由于设置的那个界面activity是single_instance或single_task，会导致onActivityResult立即执行，所以startActivityForResult没有意义
            getContext().startActivity(intent);
        } catch (ActivityNotFoundException e) {
            try {
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ComponentName name = new ComponentName("com.android.settings",
                        "com.android.settings.Settings$NotificationAccessSettingsActivity");
                intent.setComponent(name);
                intent.putExtra(":settings:show_fragment", "NotificationAccessSettings");
                getContext().startActivity(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void gotoSendNotificationSetting() {
        Intent intent = new Intent();
        String packageName = getContext().getPackageName();
        if (Build.VERSION.SDK_INT >= 26) {
            // android 8.0引导
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("android.provider.extra.APP_PACKAGE", packageName);
        } else {
            // android 5.0-7.0
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("app_package", packageName);
            intent.putExtra("app_uid", getContext().getApplicationInfo().uid);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
    }

}
