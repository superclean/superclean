package com.youtupu.superclean.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.youtupu.superclean.activity.FakePowerSaveActivity;
import com.youtupu.superclean.activity.PermissionDialogActivity;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.CustomToast;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/3/15.
 */
public class PowerPermissionDialog extends Dialog {

    private boolean mAccessibilityPermission = false;
    private boolean mFloatWindowPermission = false;
    private CustomToast mToast;
    private TextView mTvTitle;
    private TextView mTvDescription;
    private int taskSize;

    public PowerPermissionDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.power_permission_dialog_layout);
        mTvTitle = findViewById(R.id.tv_permission);
        mTvDescription = findViewById(R.id.tv_description);
        Button permissonButton = findViewById(R.id.button_grant_permission);
        Button cancelButton = findViewById(R.id.button_cancel);
        String title = String.format(getContext().getString(R.string.please_authorize), getContext().getString(R.string.app_name));
        mToast = new CustomToast(getContext(), title);
        permissonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mFloatWindowPermission) {
                    AnalyticsUtil.logEvent(getContext(), "grant_floatwindow", null);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        try {
                            Intent intent;
                            intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getContext().getPackageName()));
                            getContext().startActivity(intent);
                        } catch (ActivityNotFoundException exception) {
                            exception.printStackTrace();
                        }
                    }
                } else if (!mAccessibilityPermission) {
                    goAccess();
                }

            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 点击取消按钮
                Bundle bundle = new Bundle();
                bundle.putBoolean("access_permission", mAccessibilityPermission);
                bundle.putBoolean("float_permission", mFloatWindowPermission);
                AnalyticsUtil.logEvent(getContext(), "grant_cancel", bundle);
                showFakeSaveActivity();
            }
        });
        updateStatus();
    }

    public void updateStatus() {
        if(isShowing()){
            if (mFloatWindowPermission) {
                if (mAccessibilityPermission) {
                    dismiss();
                } else {
                    mTvTitle.setText(getContext().getString(R.string.permission_accessibility));
                    mTvDescription.setText(getContext().getString(R.string.accessibility_description));
                }
            } else {
                mTvTitle.setText(getContext().getString(R.string.permission_float_window));
                mTvDescription.setText(getContext().getString(R.string.float_window_description));
            }
        }

    }

    public void setPermissionGranted(boolean accessibilityPermission, boolean floatWindowPermission) {
        mAccessibilityPermission = accessibilityPermission;
        mFloatWindowPermission = floatWindowPermission;
        if (isShowing()) {
            updateStatus();
        }
    }

    public void setTaskSize(int taskSize) {
        this.taskSize = taskSize;
    }

    private void showFakeSaveActivity() {
        Intent intent = new Intent(getContext(), FakePowerSaveActivity.class);
        intent.putExtra("currentProgress", 0);
        intent.putExtra("totalTask", taskSize);
        getContext().startActivity(intent);

        Activity ownerActivity = getOwnerActivity();
        if (null != ownerActivity) {
            ownerActivity.finish();
        }
    }

    private void goAccess() {
        PermissionDialogActivity activity = (PermissionDialogActivity) getOwnerActivity();
        if (activity != null) {
            activity.grantAccessPermission();
        }
    }
}
