package com.youtupu.superclean.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/6/12.
 */
public class BigFileDetailDialog extends Dialog {
    private String mName = "";
    private Bitmap mBitmap;
    private int mImgResoureId = 0;
    private String mSize = "";
    private String mDate = "";
    private String mPath = "";
    private String mConfirmText = "";
    private OnConfirmButtonClickListener mListener;

    public BigFileDetailDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.big_file_detail_dialog);
        TextView tvName = findViewById(R.id.tv_name);
        ImageView imgIcon = findViewById(R.id.img_icon);
        TextView tvSize = findViewById(R.id.tv_size);
        TextView tvDate = findViewById(R.id.tv_date);
        TextView tvPath = findViewById(R.id.tv_path);
        Button leftButton = findViewById(R.id.btn_left);
        Button rightButton = findViewById(R.id.btn_right);
        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onConfirmButtonClick();
                }
                dismiss();
            }
        });
        tvName.setText(mName);
        if (mBitmap != null) {
            imgIcon.setImageBitmap(mBitmap);
        } else if (mImgResoureId != 0) {
            imgIcon.setImageResource(mImgResoureId);
        }
        rightButton.setText(mConfirmText);
        tvSize.setText(mSize);
        tvDate.setText(mDate);
        tvPath.setText(mPath);
    }

    public void setName(String name) {
        mName = name;
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
    }

    public void setImgResoureId(int imgResoureId) {
        mImgResoureId = imgResoureId;
    }

    public void setSize(String size) {
        mSize = size;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public void setConfirmText(String confirmText) {
        mConfirmText = confirmText;
    }

    public void setOnConfirmButtonClickListener(OnConfirmButtonClickListener listener) {
        mListener = listener;
    }

    public interface OnConfirmButtonClickListener {
        void onConfirmButtonClick();
    }
}
