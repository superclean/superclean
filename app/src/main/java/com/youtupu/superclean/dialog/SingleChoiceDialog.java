package com.youtupu.superclean.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.youtupu.superclean.utils.SystemUtils;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/2/20.
 */

public class SingleChoiceDialog extends Dialog {
    private TextView mTvTitle;
    private OnButtonClickListener mOnButtonClickListener;
    private String mStringTitle;
    private String mStringButton;
    private TextView mTvChoice;
    private int mImgResource = 0;
    private ImageView mImgShow;

    public SingleChoiceDialog(@NonNull Context context) {
        super(context);
    }

    public SingleChoiceDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected SingleChoiceDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_choice_dialog_layout);
        //点击空白处不能取消dialog
        setCanceledOnTouchOutside(false);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        mTvTitle = findViewById(R.id.dialog_title);
        mTvChoice = findViewById(R.id.dialog_choice);
        mImgShow = findViewById(R.id.img_show);
    }

    private void initData() {
        if (mStringTitle != null) {
            mTvTitle.setText(mStringTitle);
        }
        if (mStringButton != null) {
            mTvChoice.setText(mStringButton);
        }
        if (mImgResource != 0) {
            mImgShow.setVisibility(View.VISIBLE);
            mImgShow.setImageResource(mImgResource);
        } else {
            mImgShow.setVisibility(View.GONE);
            mTvTitle.setMinHeight(SystemUtils.dp2px(getContext(), 40));
        }
    }

    private void initListener() {
        mTvChoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnButtonClickListener != null) {
                    mOnButtonClickListener.onClick();
                }
            }
        });
    }

    public void setTitle(@NonNull String title) {
        mStringTitle = title;
    }

    public void setButtonText(@NonNull String buttonText) {
        mStringButton = buttonText;
    }

    public interface OnButtonClickListener {
        void onClick();
    }

    public void setOnButtonClickListener(@NonNull OnButtonClickListener listener) {
        mOnButtonClickListener = listener;
    }

    public void setImgResource(int imgResource) {
        mImgResource = imgResource;
    }

}
