package com.youtupu.superclean.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.internal.Preconditions;
import com.youtupu.superclean.utils.SystemUtils;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2019/2/21.
 */

public class DoubleChoicesDialog extends Dialog {
    private static final int STYLE_BLUE = 0;
    public static final int STYLE_GREEN = 1;
    private TextView mTvTitle;
    private TextView mTvLeftButton;
    private TextView mTvRightButton;
    private String mStringTitle;
    private String mStringLeftButton;
    private String mStringRightButton;
    private OnButtonClickListener mOnButtonClickListener;
    private int mStyle = STYLE_BLUE;
    private int mImgResource = 0;
    private ImageView mImgShow;
    private Spanned mTitleSpanned;

    public DoubleChoicesDialog(@NonNull Context context, @NonNull String title, @NonNull String leftButtonText,
                               @NonNull String rightButtonText, int imgResource, @Nullable OnButtonClickListener onButtonClickListener) {
        super(context);
        Preconditions.checkNotNull(title);
        Preconditions.checkNotNull(leftButtonText);
        Preconditions.checkNotNull(rightButtonText);
        mStringTitle = title;
        mImgResource = imgResource;
        mStringLeftButton = leftButtonText;
        mStringRightButton = rightButtonText;
        mOnButtonClickListener = onButtonClickListener;
    }

    public DoubleChoicesDialog(@NonNull Context context, @NonNull String title, @NonNull String leftButtonText,
                               @NonNull String rightButtonText, int imgResource, int style, @Nullable OnButtonClickListener onButtonClickListener) {
        super(context);
        Preconditions.checkNotNull(title);
        Preconditions.checkNotNull(leftButtonText);
        Preconditions.checkNotNull(rightButtonText);
        mStringTitle = title;
        mImgResource = imgResource;
        mStringLeftButton = leftButtonText;
        mStringRightButton = rightButtonText;
        mStyle = style;
        mOnButtonClickListener = onButtonClickListener;
    }

    public DoubleChoicesDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected DoubleChoicesDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.double_choices_dialog_layout);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        mTvTitle = findViewById(R.id.dialog_title);
        mTvLeftButton = findViewById(R.id.dialog_left_choice);
        mTvRightButton = findViewById(R.id.dialog_right_choice);
        mImgShow = findViewById(R.id.img_show);
    }

    private void initData() {
        if (!TextUtils.isEmpty(mStringTitle)) {
            mTvTitle.setText(mStringTitle);
        } else if (mTitleSpanned != null) {
            mTvTitle.setText(mTitleSpanned);
        }
        mTvLeftButton.setText(mStringLeftButton);
        mTvRightButton.setText(mStringRightButton);
        if (mImgResource != 0) {
            mImgShow.setVisibility(View.VISIBLE);
            mImgShow.setImageResource(mImgResource);
        } else {
            mImgShow.setVisibility(View.GONE);
            mTvTitle.setMinHeight(SystemUtils.dp2px(getContext(), 40));
        }
        switch (mStyle) {
            case STYLE_BLUE:
                mTvRightButton.setBackgroundResource(R.drawable.selector_blue_button);
                break;
            case STYLE_GREEN:
                mTvRightButton.setBackgroundResource(R.drawable.selector_green_button);
                break;
        }
    }

    private void initListener() {
        if (mOnButtonClickListener != null) {
            mTvLeftButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnButtonClickListener.leftButtonClick(DoubleChoicesDialog.this);
                }
            });
            mTvRightButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnButtonClickListener.rightButtonClick(DoubleChoicesDialog.this);
                }
            });
        }
    }

    public void setStyle(int style) {
        if (style == STYLE_BLUE || style == STYLE_GREEN) {
            mStyle = style;
        }
    }

    public void setTitle(@NonNull String title) {
        Preconditions.checkNotNull(title);
        mStringTitle = title;
    }

    public void setLeftButtonText(@NonNull String text) {
        Preconditions.checkNotNull(text);
        mStringLeftButton = text;
    }

    public void setRightButtonText(@NonNull String text) {
        Preconditions.checkNotNull(text);
        mStringRightButton = text;
    }

    public interface OnButtonClickListener {
        void leftButtonClick(DoubleChoicesDialog dlg);

        void rightButtonClick(DoubleChoicesDialog dlg);
    }

    public void setOnButtonClickListener(OnButtonClickListener listener) {
        mOnButtonClickListener = listener;
    }

    public void setImgResource(int imgResource) {
        mImgResource = imgResource;
    }

    public void setTitleSpanned(Spanned titleSpanned) {
        mTitleSpanned = titleSpanned;
    }
}
