package com.youtupu.superclean.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Jiali on 2018/12/6.
 */

public abstract class BaseFragment extends Fragment {
    protected View mRootView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        initActivity(context);
    }

    protected void initActivity(Context context) {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(getLayoutRes(), container, false);
        initView();
        return mRootView;
    }

    protected abstract int getLayoutRes();

    protected void initView() {
    }



}
