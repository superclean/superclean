package com.youtupu.superclean.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.youtupu.superclean.utils.AnalyticsUtil;

import java.util.List;

public abstract class BaseActivity extends AppCompatActivity {
    protected List<BaseFragment> mFragmentsList;
    protected BaseFragment mCurrentFragment;
    protected BaseFragment mNextFragment;
    protected int mCurrentFragmentIndex = -1;
    protected boolean mShowActivityView = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mShowActivityView = beforeSetContentView();
        if (mShowActivityView) {
            setContentView(getLayoutRes());
            initView();
            initData();
            initListener();
        }
    }

    protected boolean beforeSetContentView() {
        return true;
    }

    protected abstract @LayoutRes
    int getLayoutRes();

    protected void initView() {
    }

    protected void initData() {

    }

    protected void initListener() {

    }

    public void switchFragment(int index, int frameLayout) {
        mCurrentFragmentIndex = index;
        mNextFragment = mFragmentsList.get(index);
        if (mCurrentFragment != null && mNextFragment == mCurrentFragment) return;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (!mNextFragment.isAdded()) {
            if (mCurrentFragment != null) {
                transaction.hide(mCurrentFragment);
            }
            Bundle bundle = new Bundle();
            bundle.putString("fragment_name", mNextFragment.getClass().getSimpleName());
            AnalyticsUtil.logEvent(getApplicationContext(), "switch_fragment", bundle);
            transaction.add(frameLayout, mNextFragment);
        } else {
            transaction.hide(mCurrentFragment).show(mNextFragment);
        }
        mCurrentFragment = mNextFragment;
        //为了防止java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState错误
        transaction.commitAllowingStateLoss();
    }

}
