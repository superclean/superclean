package com.youtupu.superclean.adapter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.youtupu.superclean.bean.CheckInfoApp;
import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.view.UnClickableCheckbox;

import java.lang.ref.WeakReference;
import java.util.List;

import com.youtupu.superclean.R;

/**
 * Created by Jiali on 2018/12/24.
 */

public class NotificationSettingAdapter extends BaseAdapter {
    private List<CheckInfoApp> mAppList;
    private LayoutInflater mInflater;
    private PackageManager mPackageManager;
    private LruCache<String, Drawable> mIconCache;
    private boolean mShowRightText = true;

    public NotificationSettingAdapter(@NonNull Context context, @NonNull List<CheckInfoApp> list, PackageManager manager) {
        mAppList = list;
        mInflater = LayoutInflater.from(context);
        mPackageManager = manager;
        mIconCache = new LruCache<>(20);
    }

    public void setShowRightText(boolean showRightText) {
        mShowRightText = showRightText;
    }

    @Override
    public int getCount() {
        return mAppList.size();
    }

    @Override
    public Object getItem(int position) {
        return mAppList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.notification_setting_item, parent, false);
            holder = new ViewHolder();
            holder.icon = convertView.findViewById(R.id.img_app_icon);
            holder.info = convertView.findViewById(R.id.tv_app_label);
            holder.hide = convertView.findViewById(R.id.tv_hide);
            holder.checkbox = convertView.findViewById(R.id.checkbox_app);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (mAppList.get(position) != null) {
            CheckInfoApp info = mAppList.get(position);
            if (info != null) {
                long begin = System.currentTimeMillis();
//                holder.icon.setImageDrawable(info.getInfo().loadIcon(mPackageManager));
                loadIcon(holder.icon, info);
                CommonUtils.debug("load icon " + (System.currentTimeMillis() - begin) + " "
                        + info.getInfo().packageName);
                if (mPackageManager != null) {
                    holder.info.setText(info.getInfo().loadLabel(mPackageManager));
                }
                if (mShowRightText) {
                    if (info.isChecked()) {
                        holder.hide.setVisibility(View.VISIBLE);
                    } else {
                        holder.hide.setVisibility(View.INVISIBLE);
                    }
                } else {
                    holder.hide.setVisibility(View.INVISIBLE);
                }
                holder.checkbox.setChecked(info.isChecked());
            }
        }
        return convertView;
    }

    private void loadIcon(final ImageView imageView, final CheckInfoApp info) {
        AsyncTask<String, Integer, Drawable> asyncTask = new CacheIconTask(mPackageManager, mIconCache, info, imageView);
        asyncTask.execute(info.getInfo().packageName);
    }

    private class ViewHolder {
        ImageView icon;
        TextView info;
        TextView hide;
        UnClickableCheckbox checkbox;
    }

    public static class CacheIconTask extends AsyncTask<String, Integer, Drawable> {

        private PackageManager mPackageManager;
        private LruCache<String, Drawable> mIconCache;
        private CheckInfoApp mInfo;
        private WeakReference<ImageView> mImageViewRef;

        CacheIconTask(PackageManager packageManager, LruCache<String, Drawable> lruCache, CheckInfoApp info, ImageView imageView) {
            mPackageManager = packageManager;
            mIconCache = lruCache;
            mInfo = info;
            mImageViewRef = new WeakReference<>(imageView);
        }

        @Override
        protected Drawable doInBackground(String... strings) {
            Drawable drawable = mIconCache.get(mInfo.getInfo().packageName);
            if (null == drawable) {
                drawable = mInfo.getInfo().loadIcon(mPackageManager);
                mIconCache.put(mInfo.getInfo().packageName, drawable);
            }
            return drawable;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            super.onPostExecute(drawable);
            ImageView imageView = mImageViewRef.get();
            if (null != imageView) {
                imageView.setImageDrawable(drawable);
            }
            mPackageManager = null;
            mIconCache = null;
            mInfo = null;
        }
    }
}
