package com.youtupu.superclean.app;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import com.crashlytics.android.Crashlytics;
import com.youtupu.superclean.BuildConfig;
import com.youtupu.superclean.activity.ChargingActivity;
import com.youtupu.superclean.activity.PermissionDialogActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import cn.instreet.business.BusinessSDK;
import cn.instreet.business.advertise.BannerAdvertiseManager;
import cn.instreet.business.advertise.InterstitialAdvertiseManager;
import cn.instreet.business.advertise.NativeAdvertiseManager;
import cn.instreet.business.lockscreen.AmoledLockScreenActivity;
import cn.instreet.business.lockscreen.LockScreenActivity;
import com.youtupu.superclean.activity.CleanDialogActivity;
import com.youtupu.superclean.bean.BigFileInfo;
import com.youtupu.superclean.bean.JunkListItem;
import com.youtupu.superclean.service.MonitorService;
import com.youtupu.superclean.utils.AnalyticsUtil;
import com.youtupu.superclean.utils.CommonUtils;
import com.youtupu.superclean.utils.Constants;

import io.fabric.sdk.android.Fabric;
import io.paperdb.Paper;


/**
 * Created by Jiali on 2018/11/26.
 */

public class SuperCleanApplication extends Application {
    private List<JunkListItem> mAppCacheList = new ArrayList<>();
    private List<JunkListItem> mResidualList = new ArrayList<>();
    private List<JunkListItem> mUnusedAPKList = new ArrayList<>();
    private List<JunkListItem> mADList = new ArrayList<>();
    private List<BigFileInfo> mBigFileInfoList;

    public static SuperCleanApplication instance = null;
    public static IBinder sBinder;
    private boolean mJunkCleaning = false;
    private boolean mPowerSaving = false;

    private HashSet<String> mIgnoredActivity;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
//        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
//            return;
//        }
//        LeakCanary.install(this);

        Fabric.with(this, new Crashlytics());



        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                BusinessSDK.InitSDK(SuperCleanApplication.this, new BusinessSDK.OnSDKInitListener() {
                    @Override
                    public void onInit(BusinessSDK businessSDK) {
                    }
                });
            }
        }, 500);
        MonitorConnection connection = new MonitorConnection();
        bindService(new Intent(this, MonitorService.class), connection, BIND_AUTO_CREATE);

        listenAppFrontBackChange();

        Paper.init(this);

        loadAds();


    }

    private void loadAds() {
//        1. 加速结果页：ca-app-pub-4414232724432396/1785364667
//        2. 清理结果页：ca-app-pub-4414232724432396/6301912913
//        3. 省电结果页：ca-app-pub-4414232724432396/6439969122
//        4. 降温结果页：ca-app-pub-4414232724432396/5945145108
//        5. 通知栏结果页：ca-app-pub-4414232724432396/1622756713
        if (BuildConfig.DEBUG) {
            BannerAdvertiseManager.getInstance().init(this, Constants.TEST_ADMOB_APP_ID);
            NativeAdvertiseManager.getInstance().init(this, Constants.TEST_ADMOB_APP_ID,
                    Constants.TEST_NATIVE_AD);
            InterstitialAdvertiseManager.getInstance().init(this, Constants.TEST_ADMOB_APP_ID);
        } else {
//            NativeAdvertiseManager.getInstance().init(this, Constants.ADMOB_APP_ID,
//                    Constants.BOOST_NATIVE_AD,Constants.COOLER_NATIVE_AD, Constants.JUNK_NATIVE_AD,
//                    Constants.POWER_NATIVE_AD, Constants.BIG_FILE_NATIVE_AD, Constants.NOTIFICATION_NATIVE_AD);
            BannerAdvertiseManager.getInstance().init(this, Constants.ADMOB_APP_ID);
            InterstitialAdvertiseManager.getInstance().init(this, Constants.ADMOB_APP_ID);
        }
    }

    private int activityStartCount = 0;

    private void listenAppFrontBackChange() {

        mIgnoredActivity = new HashSet<>(Arrays.asList(
                AmoledLockScreenActivity.class.getCanonicalName(),
                LockScreenActivity.class.getCanonicalName(),
                ChargingActivity.class.getCanonicalName(),
                CleanDialogActivity.class.getCanonicalName(),
                PermissionDialogActivity.class.getCanonicalName()
        ));
        this.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(Activity activity) {
                if (isIgnoredActivity(activity.getClass().getCanonicalName())) {
                    return;
                }
                activityStartCount++;
                if (activityStartCount == 1) {
                    CommonUtils.debug("app change to front");
                    AnalyticsUtil.traceEnterApp(getApplicationContext());
                    AnalyticsUtil.logEvent(getApplicationContext(), "change_front", null);
                }
            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {
                if (isIgnoredActivity(activity.getClass().getCanonicalName())) {
                    return;
                }
                activityStartCount--;
                if (activityStartCount == 0) {
                    CommonUtils.debug("app change to back");
                    AnalyticsUtil.logEvent(getApplicationContext(), "change_back", null);
                }
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }

    private boolean isIgnoredActivity(String className) {
        return mIgnoredActivity.contains(className);
    }


    public List<JunkListItem> getAppCacheList() {
        return mAppCacheList;
    }

    public List<JunkListItem> getResidualList() {
        return mResidualList;
    }

    public List<JunkListItem> getUnusedAPKList() {
        return mUnusedAPKList;
    }

    public List<JunkListItem> getADList() {
        return mADList;
    }


    public void setAppCacheList(List<JunkListItem> appCacheList) {
        if (null != appCacheList) {
            mAppCacheList = appCacheList;
        }
    }

    public void setResidualList(List<JunkListItem> residualList) {
        if (null != residualList) {
            mResidualList = residualList;
        }
    }

    public void setUnusedAPKList(List<JunkListItem> list) {
        if (null != list) {
            mUnusedAPKList = list;
        }
    }

    public void setADList(List<JunkListItem> list) {
        if (null != mADList) {
            mADList = list;
        }
    }

    private class MonitorConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            sBinder = service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            sBinder = null;
        }
    }


    public boolean isJunkCleaning() {
        return mJunkCleaning;
    }

    public void setJunkCleaning(boolean junkCleaning) {
        mJunkCleaning = junkCleaning;
    }

    public boolean isPowerSaving() {
        return mPowerSaving;
    }

    public void setPowerSaving(boolean powerSaving) {
        mPowerSaving = powerSaving;
    }

    public List<BigFileInfo> getBigFileInfoList() {
        return mBigFileInfoList;
    }

    public void setBigFileInfoList(List<BigFileInfo> bigFileInfoList) {
        mBigFileInfoList = bigFileInfoList;
    }
}
